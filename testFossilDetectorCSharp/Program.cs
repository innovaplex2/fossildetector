﻿using System;
using System.IO;
using System.Runtime.InteropServices;
using System.Collections.Generic;


using OpenCvSharp;
using OpenCvSharp.CPlusPlus;


namespace testFossilDetectorCSharp
{
    class Program
    {
        public enum eFOSSILE_TYPE { FOSSIL_TYPE_BLOB, FOSSIL_TYPE_LINE };
        public enum eFOSSILE_UPDATE_FLAG { FOSSIL_UPDATE_NONE, FOSSIL_UPDATE_UPDATE} ;

        public struct RECT
        {
            public int left;
            public int top;
            public int right;
            public int bottom;
        };

        public struct FOSSILDETECTInfo
        {
            public eFOSSILE_UPDATE_FLAG flag;		// Update flag

            public int fossil_id;
            public int image_index;
            public int class_no;
            public float class_distance;	// distance in class (low score is close)

            public RECT rc;
            public eFOSSILE_TYPE type;

            public float mean;			//normalize by global mean
            public float stddev;

            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 3)]
            public float[] rgb_mean;

            public float width;
            public float height;
            public float angle;
            public float area;

            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 7)]
            public double[] hu_moments;

            public int size_of_descriptor;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 1764)]
            public short[] descriptors;
        }

                
        [DllImport("FossilDetector.dll")]
        public static extern IntPtr SetupFossilDetector([MarshalAs(UnmanagedType.LPStr)]String szInputFile);

        [DllImport("FossilDetector.dll")]
        public static extern bool ProcessFossilDetector(IntPtr handle, int width, int height, int step,
                IntPtr image, ref int pMaxDetectCount);

        [DllImport("FossilDetector.dll")]
        public static extern bool GetFossilObject(IntPtr handle, [Out] FOSSILDETECTInfo[] info, ref int pDetectCount);

        [DllImport("FossilDetector.dll")]
        public static extern bool UpdateFossilObject(IntPtr handle, int nDetectCount, [Out] FOSSILDETECTInfo[] info);

        [DllImport("FossilDetector.dll")]
        public static extern bool ProcessUpdateFossilObject(IntPtr handle, int width, int height, int step,
                IntPtr image, int nDetectCount, [Out] FOSSILDETECTInfo[] info);

        [DllImport("FossilDetector.dll")]
        public static extern void EndupFossilDetector(IntPtr handle);


        [DllImport("FossilClassifier2.dll")]
        public static extern IntPtr CreateClassifier();

        [DllImport("FossilClassifier2.dll")]
        public static extern bool ProcessClassification2(IntPtr handle, int nTotalFossil, [In] FOSSILDETECTInfo[] in_info, [Out] FOSSILDETECTInfo[] out_info);

        [DllImport("FossilClassifier2.dll")]
        public static extern void DestroyClassifier(IntPtr handle);


        static void Main(string[] args)
        {
            Mat img_input;
            IntPtr hDetector = SetupFossilDetector("test.xml");

            int nTotalDETECTInfo = 0;

            int nMaxDetectCnt;
            int nDetectCnt;
            
            //use List to easy insert/remove item
            List<List<FOSSILDETECTInfo>> TotalFossilInfoList = new List<List<FOSSILDETECTInfo>>();

            for(int i = 1 ; i < 10 ; i++){
                string input_file_name = "D:\\work2\\InnovaPlex\\unmarked sample\\multirotation_p0" + i + "z3t1c1.jpg";
                //string input_file_name = "D:\\multirotation_p01z3t1c1.jpg";
                img_input = new Mat(input_file_name, LoadMode.Color);

                nMaxDetectCnt = 0;
                ProcessFossilDetector(hDetector, img_input.Width, img_input.Height, (int)img_input.Step1(), img_input.Data, ref nMaxDetectCnt);

                List<FOSSILDETECTInfo> FossilInfoList = new List<FOSSILDETECTInfo>();
                FOSSILDETECTInfo[] FossilInfos = new FOSSILDETECTInfo[nMaxDetectCnt];

                nDetectCnt = 0;
                GetFossilObject(hDetector, FossilInfos, ref nDetectCnt);

                for (int j = 0; j < nDetectCnt; j++){
			        FossilInfos[j].image_index	= i;
			        FossilInfos[j].class_no		= -1;
			        FossilInfoList.Add(FossilInfos[j]);
		        }

                nTotalDETECTInfo += nDetectCnt;
                TotalFossilInfoList.Add(FossilInfoList);
            }
            EndupFossilDetector(hDetector);


            FOSSILDETECTInfo[] pTotalFossilInfos = new FOSSILDETECTInfo[nTotalDETECTInfo];
            int k = 0;
            foreach (List<FOSSILDETECTInfo> sublist in TotalFossilInfoList)
            {
                foreach (FOSSILDETECTInfo value in sublist)
                {
                    pTotalFossilInfos[k++] = value;
                }
            }
            
            IntPtr hClassifier = CreateClassifier();
            ProcessClassification2(hClassifier, nTotalDETECTInfo, pTotalFossilInfos, pTotalFossilInfos);

            //Update List 
            k = 0;
            for (int i = 0; i < TotalFossilInfoList.Count; i++)
            {
                List<FOSSILDETECTInfo> FossilInfoList = TotalFossilInfoList[i];
                for (int j = 0; j < TotalFossilInfoList[i].Count; j++)
                {
                    FossilInfoList[j] = pTotalFossilInfos[k];
                    k++;
                }
            }

            DestroyClassifier(hClassifier);

        }
    }
}

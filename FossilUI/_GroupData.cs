﻿namespace FossilUI
{
	public class GroupData
	{
		public int idx;
		public int order;
		public float rmin;
		public float amin;
		public string name;
	}
}

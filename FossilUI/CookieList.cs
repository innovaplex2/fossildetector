﻿using System;
using System.IO;
using System.Linq;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Threading;
using System.Windows.Input;
using System.Windows.Shapes;
using System.Windows.Media;
using System.Windows.Media.Effects;
using System.Windows.Media.Animation;
using OpenCvSharp.CPlusPlus;

namespace FossilUI
{
	public partial class MainWindow : System.Windows.Window
	{
		public void AddCookie(FileData file_data, FossilAPI.FOSSILDETECTInfo cookie_data, int fossil_id)
		{
			if (PANE_COOKIE_LIST.Dispatcher.CheckAccess())
			{
				bool cookie_selected = (fossil_id > MainWindow.UNDEFINED_FOSSIL && cookie_data.fossil_id == fossil_id) ? true : false;
				OpenCvSharp.CPlusPlus.Rect cookie_rect = CommonUtil.SubRect(cookie_data.rc, file_data.mat.Width, file_data.mat.Height);
				MemoryStream img_cookie = null;

				try
				{
					img_cookie = file_data.mat.SubMat(cookie_rect).ToMemoryStream();
				}
				catch(Exception ex)
				{
				}

				if (img_cookie == null)
					return;

				int img_left = cookie_rect.X;
				int img_top = cookie_rect.Y;
				int img_width = cookie_rect.Width;
				int img_height = cookie_rect.Height;

				double list_width = PANE_COOKIE_LIST.ActualWidth;
				int add_height = 20;

				StackPanel item_pane = new StackPanel
				{
					Orientation = Orientation.Horizontal,
					Height = img_height + add_height
				};
				StackPanel img_pane = new StackPanel
				{
					Orientation = Orientation.Vertical,
					Background = CommonUtil.GetBrush(cookie_selected ? "#FF04AEDA" : "#FFa9a9a9"),
					Width = list_width * 0.20,
					Height = img_height + add_height,
					HorizontalAlignment = HorizontalAlignment.Left
				};
				Border cookie_border = new Border
				{
					Width = img_width,
					Height = img_height,
					HorizontalAlignment = HorizontalAlignment.Left,
					BorderBrush = Brushes.DimGray,
					BorderThickness = new Thickness(1),
					Margin = new Thickness(10)
				};
				Image cookie_img = new Image
				{
					Width = img_width,
					Height = img_height,
					Source = CommonUtil.MemImg(img_cookie),
					HorizontalAlignment = HorizontalAlignment.Left,
					Opacity = 1
				};
				Border class_border = new Border
				{
					Background = CommonUtil.GetBrush(cookie_selected ? "#FF04AEDA" : "#FFa9a9a9"),
					Width = list_width * 0.20,
					Height = img_height + add_height,
					Padding = new Thickness(5,0,5,0)
				};
				ComboBox class_list = new ComboBox
				{
					Height = 20,
					Tag = cookie_data
				};
				Label left_label = new Label
				{
					Background = CommonUtil.GetBrush(cookie_selected ? "#FF04AEDA" : "#FF969696"),
					Width = list_width * 0.12,
					Height = img_height + add_height,
					HorizontalContentAlignment = HorizontalAlignment.Center,
					VerticalContentAlignment = VerticalAlignment.Center,
					Content = img_left.ToString()
				};
				Label top_label = new Label
				{
					Background = CommonUtil.GetBrush(cookie_selected ? "#FF04AEDA" : "#FFa9a9a9"),
					Width = list_width * 0.12,
					Height = img_height + add_height,
					HorizontalContentAlignment = HorizontalAlignment.Center,
					VerticalContentAlignment = VerticalAlignment.Center,
					Content = img_top.ToString()
				};
				Label width_label = new Label
				{
					Background = CommonUtil.GetBrush(cookie_selected ? "#FF04AEDA" : "#FF969696"),
					Width = list_width * 0.12,
					Height = img_height + add_height,
					HorizontalContentAlignment = HorizontalAlignment.Center,
					VerticalContentAlignment = VerticalAlignment.Center,
					Content = img_width.ToString()
				};
				Label height_label = new Label
				{
					Background = CommonUtil.GetBrush(cookie_selected ? "#FF04AEDA" : "#FFa9a9a9"),
					Width = list_width * 0.12,
					Height = img_height + add_height,
					HorizontalContentAlignment = HorizontalAlignment.Center,
					VerticalContentAlignment = VerticalAlignment.Center,
					Content = img_height.ToString()
				};
				Label area_label = new Label
				{
					Background = CommonUtil.GetBrush(cookie_selected ? "#FF04AEDA" : "#FF969696"),
					Width = list_width * 0.12,
					Height = img_height + add_height,
					HorizontalContentAlignment = HorizontalAlignment.Center,
					VerticalContentAlignment = VerticalAlignment.Center,
					Content = (img_width * img_height).ToString()
				};

				left_label.FontWeight = top_label.FontWeight = width_label.FontWeight = height_label.FontWeight = area_label.FontWeight =
						cookie_data.proc_step >= ProcStep.category ? FontWeights.Bold : FontWeights.Normal;

				left_label.Foreground = top_label.Foreground = width_label.Foreground = height_label.Foreground = area_label.Foreground =
						cookie_data.proc_step >= ProcStep.category ? Brushes.Green : Brushes.Black;

				cookie_border.Child = cookie_img;
				img_pane.Children.Add(cookie_border);

				class_list.Items.Add(
					new ComboBoxItem
					{
						Content = "unidentified group",
						Foreground = Brushes.Red
					}
				);

				class_list.SelectedIndex = 0;

				foreach (GroupData group_data in this.group_list)
				{
					if (group_data.idx <= MainWindow.UNDEFINED_GROUP)
						continue;

					ComboBoxItem list_item = new ComboBoxItem
					{
						Content = (group_data.idx < 0 ? "c-" + Math.Abs(group_data.idx + 1) : Math.Abs(group_data.idx + this.start_order).ToString()) + ". " + group_data.name,
						Foreground = Brushes.Blue
					};

					class_list.Items.Add(list_item);

					if (cookie_data.class_no == group_data.idx)
						class_list.SelectedItem = list_item;
				}

				class_list.PreviewMouseWheel += new MouseWheelEventHandler(this.WheelClass);
				class_list.SelectionChanged += new SelectionChangedEventHandler(this.DarknetClass);
				class_border.Child = class_list;

				item_pane.Children.Add(img_pane);
				item_pane.Children.Add(class_border);
				item_pane.Children.Add(left_label);
				item_pane.Children.Add(top_label);
				item_pane.Children.Add(width_label);
				item_pane.Children.Add(height_label);
				item_pane.Children.Add(area_label);

				img_pane.MouseEnter += (_sender, _e) => this.EnterItem(_sender, _e, img_pane, class_border, left_label, top_label,
						width_label, height_label, area_label);
				class_border.MouseEnter += (_sender, _e) => this.EnterItem(_sender, _e, img_pane, class_border, left_label, top_label,
						width_label, height_label, area_label);
				left_label.MouseEnter += (_sender, _e) => this.EnterItem(_sender, _e, img_pane, class_border, left_label, top_label,
						width_label, height_label, area_label);
				top_label.MouseEnter += (_sender, _e) => this.EnterItem(_sender, _e, img_pane, class_border, left_label, top_label,
						width_label, height_label, area_label);
				width_label.MouseEnter += (_sender, _e) => this.EnterItem(_sender, _e, img_pane, class_border, left_label, top_label,
						width_label, height_label, area_label);
				height_label.MouseEnter += (_sender, _e) => this.EnterItem(_sender, _e, img_pane, class_border, left_label, top_label,
						width_label, height_label, area_label);
				area_label.MouseEnter += (_sender, _e) => this.EnterItem(_sender, _e, img_pane, class_border, left_label, top_label,
						width_label, height_label, area_label);

				img_pane.MouseLeave += (_sender, _e) => this.LeaveItem(_sender, _e, img_pane, class_border, left_label, top_label,
						width_label, height_label, area_label);
				class_border.MouseLeave += (_sender, _e) => this.LeaveItem(_sender, _e, img_pane, class_border, left_label, top_label,
						width_label, height_label, area_label);
				left_label.MouseLeave += (_sender, _e) => this.LeaveItem(_sender, _e, img_pane, class_border, left_label, top_label,
						width_label, height_label, area_label);
				top_label.MouseLeave += (_sender, _e) => this.LeaveItem(_sender, _e, img_pane, class_border, left_label, top_label,
						width_label, height_label, area_label);
				width_label.MouseLeave += (_sender, _e) => this.LeaveItem(_sender, _e, img_pane, class_border, left_label, top_label,
						width_label, height_label, area_label);
				height_label.MouseLeave += (_sender, _e) => this.LeaveItem(_sender, _e, img_pane, class_border, left_label, top_label,
						width_label, height_label, area_label);
				area_label.MouseLeave += (_sender, _e) => this.LeaveItem(_sender, _e, img_pane, class_border, left_label, top_label,
						width_label, height_label, area_label);

				PANE_COOKIE_LIST.Children.Add(item_pane);

				ContextMenu cookie_context = new ContextMenu();
				MenuItem remove_cookie = new MenuItem
				{
					Header = CommonUtil.LangData("main_window", "remove_cookie"),
					Icon = new Image
					{
						Source = (ImageSource)new ImageSourceConverter().ConvertFromString(
								"pack://application:,,,/resources/context.remove.cookie.png")
					}
				};

				remove_cookie.Click += new RoutedEventHandler(this.RemoveCookie);
				cookie_context.Items.Add(remove_cookie);
				item_pane.ContextMenu = cookie_context;

				this.ResizeCookie();
			}
			else
			{
				PANE_COOKIE_LIST.Dispatcher.BeginInvoke(
					new Action(
						() => this.AddCookie(file_data, cookie_data, fossil_id)
					)
				);
			}
		}

		private void WheelClass(object sender, MouseWheelEventArgs e)
		{
			e.Handled = true;
		}

		private void RemoveCookie(object sender, RoutedEventArgs e)
		{
			e.Handled = true;
			MenuItem cur_item = (MenuItem)sender;
			if (cur_item == null) return;
			StackPanel cur_pane = (StackPanel)((ContextMenu)cur_item.Parent).PlacementTarget;
			if (cur_pane == null) return;
			this.RemoveCookie(cur_pane);
		}

		private void RemoveCookie(StackPanel cur_pane)
		{
			ComboBox specify_class = (ComboBox)((Border)cur_pane.Children[1]).Child;
			FossilAPI.FOSSILDETECTInfo cookie_data = (FossilAPI.FOSSILDETECTInfo)specify_class.Tag;
			StackPanel cookie_list = (StackPanel)cur_pane.Parent;

			try
			{
				cookie_list.Children.Remove(cur_pane);
			}
			catch (Exception ex)
			{
			}
			finally
			{
				FileData cur_data = (FileData)LIST_FILE_ITEM.SelectedItem;
				this.disp_img = cur_data.mat.Clone();

				for (int i = 0; i < this.cookie_list.Count; i++)
				{
					for (int j = this.cookie_list[i].fossils.Count - 1; j >= 0; j--)
					{
						if (this.cookie_list[i].fossils[j].image_index != cookie_data.image_index)
							continue;

						if (this.cookie_list[i].fossils[j].fossil_id == cookie_data.fossil_id)
						{
							this.cookie_list[i].fossils.RemoveAt(j);
						}
						else
						{
							OpenCvSharp.CPlusPlus.Rect cookie_rect = CommonUtil.SubRect(this.cookie_list[i].fossils[j].rc, this.disp_img.Width, this.disp_img.Height);

							this.disp_img.Rectangle
							(
								cookie_rect,
								new OpenCvSharp.CPlusPlus.Scalar(0, 0, 255, 255),
								3,
								OpenCvSharp.LineType.AntiAlias
							);
						}
					}
				}

				this.SetImg(this.disp_img.ToMemoryStream());
			}
		}

		private void DarknetClass(object sender, SelectionChangedEventArgs e)
		{
			ComboBox cur_class = (ComboBox)sender;
			FossilAPI.FOSSILDETECTInfo cookie_data = (FossilAPI.FOSSILDETECTInfo)cur_class.Tag;
			int class_no = MainWindow.UNDEFINED_GROUP;

			if (cur_class.SelectedIndex > 0)
			{
				string class_text = ((ComboBoxItem)cur_class.SelectedItem).Content.ToString();
				string[] class_str = class_text.Split('.');

				if (class_str[0][0] == 'c')
				{
					class_no = 0 - (Convert.ToInt32(class_str[0].Substring(2, class_str[0].Length - 2)) + 1);
				}
				else
				{
					class_no = Convert.ToInt32(class_str[0]) - this.start_order;
				}
			}

			for (int i = 0; i < this.cookie_list.Count; i++)
			{
				for (int j = 0; j < this.cookie_list[i].fossils.Count; j++)
				{
					if (this.cookie_list[i].fossils[j].fossil_id == cookie_data.fossil_id)
					{
						FossilAPI.FOSSILDETECTInfo cookie_buffer = this.cookie_list[i].fossils[j];
						cookie_buffer.specify_class = class_no;
						cookie_buffer.class_no = class_no;
						this.cookie_list[i].fossils[j] = cookie_buffer;
						return;
					}
				}
			}
		}

		private void SaveYolo(object sender, EventArgs e)
		{
			FileData cur_data = (FileData)LIST_FILE_ITEM.SelectedItem;

			using (StreamWriter save_writer = new StreamWriter(@"yolo\" + cur_data.name + ".txt"))
			{
				for (int i = 0; i < PANE_COOKIE_LIST.Children.Count; i++)
				{
					StackPanel item_pane = (StackPanel)PANE_COOKIE_LIST.Children[i];
					ComboBox cur_class = (ComboBox)((Border)item_pane.Children[1]).Child;
					double cookie_left = Convert.ToDouble(((Label)item_pane.Children[2]).Content);
					double cookie_top = Convert.ToDouble(((Label)item_pane.Children[3]).Content);
					double cookie_width = Convert.ToDouble(((Label)item_pane.Children[4]).Content);
					double cookie_height = Convert.ToDouble(((Label)item_pane.Children[5]).Content);
					int class_no = MainWindow.UNDEFINED_GROUP;

					if (cur_class.SelectedIndex > 0)
					{
						string class_text = ((ComboBoxItem)cur_class.SelectedItem).Content.ToString();
						string[] class_str = class_text.Split('.');

						if (class_str[0][0] == 'c')
						{
							class_no = 0 - (Convert.ToInt32(class_str[0].Substring(2, class_str[0].Length - 2)) + 1);
						}
						else
						{
							class_no = Convert.ToInt32(class_str[0]) - 1;
						}

						save_writer.Write(
							class_no.ToString() + " " +
							(cookie_left / cur_data.mat.Width * 100).ToString() + " " +
							(cookie_top / cur_data.mat.Height * 100).ToString() + " " +
							(cookie_width / cur_data.mat.Width * 100).ToString() + " " +
							(cookie_height / cur_data.mat.Height * 100).ToString()
						);
						save_writer.Write(System.Environment.NewLine);
					}
				}
			}
		}

		public void SortCookie(object sender, RoutedEventArgs e)
		{
			Button cur_header = (Button)sender;

			((Image)((StackPanel)BORDER_COOKIE_HEADER_CLASS.Content).Children[1]).Source =
			((Image)((StackPanel)BORDER_COOKIE_HEADER_LEFT.Content).Children[1]).Source =
			((Image)((StackPanel)BORDER_COOKIE_HEADER_TOP.Content).Children[1]).Source =
			((Image)((StackPanel)BORDER_COOKIE_HEADER_WIDTH.Content).Children[1]).Source =
			((Image)((StackPanel)BORDER_COOKIE_HEADER_HEIGHT.Content).Children[1]).Source =
			((Image)((StackPanel)BORDER_COOKIE_HEADER_SIZE.Content).Children[1]).Source =
					(ImageSource)new ImageSourceConverter().ConvertFromString("pack://application:,,,/resources/sorting.null.png");

			if (cur_header.Name == "BORDER_COOKIE_HEADER_CLASS")
			{
				this.sort_cookie = this.sort_cookie == 0 ? 1 : 0;
				((Image)((StackPanel)cur_header.Content).Children[1]).Source =
						(ImageSource)new ImageSourceConverter().ConvertFromString("pack://application:,,,/resources/" +
						(this.sort_cookie == 0 ? "sorting.up.on.png" : "sorting.down.on.png"));
			}
			else if (cur_header.Name == "BORDER_COOKIE_HEADER_LEFT")
			{
				this.sort_cookie = this.sort_cookie == 2 ? 3 : 2;
				((Image)((StackPanel)cur_header.Content).Children[1]).Source =
						(ImageSource)new ImageSourceConverter().ConvertFromString("pack://application:,,,/resources/" +
						(this.sort_cookie == 2 ? "sorting.up.on.png" : "sorting.down.on.png"));
			}
			else if (cur_header.Name == "BORDER_COOKIE_HEADER_TOP")
			{
				this.sort_cookie = this.sort_cookie == 4 ? 5 : 4;
				((Image)((StackPanel)cur_header.Content).Children[1]).Source =
						(ImageSource)new ImageSourceConverter().ConvertFromString("pack://application:,,,/resources/" +
						(this.sort_cookie == 4 ? "sorting.up.on.png" : "sorting.down.on.png"));
			}
			else if (cur_header.Name == "BORDER_COOKIE_HEADER_WIDTH")
			{
				this.sort_cookie = this.sort_cookie == 6 ? 7 : 6;
				((Image)((StackPanel)cur_header.Content).Children[1]).Source =
						(ImageSource)new ImageSourceConverter().ConvertFromString("pack://application:,,,/resources/" +
						(this.sort_cookie == 6 ? "sorting.up.on.png" : "sorting.down.on.png"));
			}
			else if (cur_header.Name == "BORDER_COOKIE_HEADER_HEIGHT")
			{
				this.sort_cookie = this.sort_cookie == 8 ? 9 : 8;
				((Image)((StackPanel)cur_header.Content).Children[1]).Source =
						(ImageSource)new ImageSourceConverter().ConvertFromString("pack://application:,,,/resources/" +
						(this.sort_cookie == 8 ? "sorting.up.on.png" : "sorting.down.on.png"));
			}
			else if (cur_header.Name == "BORDER_COOKIE_HEADER_SIZE")
			{
				this.sort_cookie = this.sort_cookie == 10 ? 11 : 10;
				((Image)((StackPanel)cur_header.Content).Children[1]).Source =
						(ImageSource)new ImageSourceConverter().ConvertFromString("pack://application:,,,/resources/" +
						(this.sort_cookie == 10 ? "sorting.up.on.png" : "sorting.down.on.png"));
			}

			this.SortCookie(this.sort_cookie, MainWindow.UNDEFINED_FOSSIL);
		}

		public void SortCookie(int sort_type, int fossil_id)
		{
			List<CookieData> cookie_list = new List<CookieData>();

			for (int i = 0; i < PANE_COOKIE_LIST.Children.Count; i++)
			{
				Border class_border = (Border)((StackPanel)PANE_COOKIE_LIST.Children[i]).Children[1];
				ComboBox specify_class = (ComboBox)class_border.Child;
				FossilAPI.FOSSILDETECTInfo cookie_data = (FossilAPI.FOSSILDETECTInfo)specify_class.Tag;
				CookieData list_data = new CookieData();
				list_data.data = cookie_data;
				list_data.item = class_border;
				cookie_list.Add(list_data);
			}

			if (sort_type == 0)
				cookie_list = cookie_list.OrderByDescending(o => o.data.class_no).ToList();
			else if (sort_type == 1)
				cookie_list = cookie_list.OrderBy(o => o.data.class_no).ToList();
			else if (sort_type == 2)
				cookie_list = cookie_list.OrderByDescending(o => o.data.rc.left).ToList();
			else if (sort_type == 3)
				cookie_list = cookie_list.OrderBy(o => o.data.rc.left).ToList();
			else if (sort_type == 4)
				cookie_list = cookie_list.OrderByDescending(o => o.data.rc.top).ToList();
			else if (sort_type == 5)
				cookie_list = cookie_list.OrderBy(o => o.data.rc.top).ToList();
			else if (sort_type == 6)
				cookie_list = cookie_list.OrderByDescending(o => (o.data.rc.right - o.data.rc.left)).ToList();
			else if (sort_type == 7)
				cookie_list = cookie_list.OrderBy(o => (o.data.rc.right - o.data.rc.left)).ToList();
			else if (sort_type == 8)
				cookie_list = cookie_list.OrderByDescending(o => (o.data.rc.bottom - o.data.rc.top)).ToList();
			else if (sort_type == 9)
				cookie_list = cookie_list.OrderBy(o => (o.data.rc.bottom - o.data.rc.top)).ToList();
			else if (sort_type == 10)
				cookie_list = cookie_list.OrderByDescending(o => ((o.data.rc.right - o.data.rc.left) * (o.data.rc.bottom - o.data.rc.top))).ToList();
			else if (sort_type == 11)
				cookie_list = cookie_list.OrderBy(o => ((o.data.rc.right - o.data.rc.left) * (o.data.rc.bottom - o.data.rc.top))).ToList();

			PANE_COOKIE_LIST.Dispatcher.Invoke(
				DispatcherPriority.Loaded,
				new Action(() =>
				{
					foreach (CookieData cookie_data in cookie_list)
					{
						Border class_border = cookie_data.item;
						StackPanel cookie_pane = (StackPanel)class_border.Parent;

						PANE_COOKIE_LIST.Children.Remove(cookie_pane);
						PANE_COOKIE_LIST.Children.Insert(0, cookie_pane);
					}

					if (fossil_id > MainWindow.UNDEFINED_FOSSIL)
					{
						double scroll_height = 0;

						foreach (StackPanel cookie_pane in PANE_COOKIE_LIST.Children)
						{
							ComboBox specify_class = (ComboBox)((Border)cookie_pane.Children[1]).Child;
							FossilAPI.FOSSILDETECTInfo cookie_data = (FossilAPI.FOSSILDETECTInfo)specify_class.Tag;

							if (cookie_data.fossil_id == fossil_id)
							{
								((ScrollViewer)PANE_COOKIE_LIST.Parent).ScrollToVerticalOffset(scroll_height);
								return;
							}

							scroll_height += cookie_pane.ActualHeight;
						}
					}
				})
			);
		}

		private void EnterItem(object sender, MouseEventArgs e, StackPanel img_pane, Border class_border, Label left_label, Label top_label,
				Label width_label, Label height_label, Label area_label)
		{
			img_pane.Background = class_border.Background = left_label.Background = top_label.Background = width_label.Background
					 = height_label.Background = area_label.Background = CommonUtil.GetBrush("#FF04AEDA");

			FileData cur_data = (FileData)LIST_FILE_ITEM.SelectedItem;
			if (cur_data == null) return;

			this.focus_pane = (StackPanel)img_pane.Parent;

			int img_left = Convert.ToInt32(left_label.Content);
			int img_top = Convert.ToInt32(top_label.Content);
			int img_width = Convert.ToInt32(width_label.Content);
			int img_height = Convert.ToInt32(height_label.Content);

			OpenCvSharp.CPlusPlus.Mat conv_img = this.disp_img.Clone();
			if (conv_img == null) return;

			conv_img.Rectangle(
				new OpenCvSharp.CPlusPlus.Rect(img_left, img_top, img_width, img_height),
				new OpenCvSharp.CPlusPlus.Scalar(218, 174, 4, 255),
				3,
				OpenCvSharp.LineType.AntiAlias
			);

			this.SetImg(conv_img.ToMemoryStream());
		}

		private void LeaveItem(object sender, MouseEventArgs e, StackPanel img_pane, Border class_border, Label left_label, Label top_label,
				Label width_label, Label height_label, Label area_label)
		{
			img_pane.Background = class_border.Background = top_label.Background = height_label.Background = CommonUtil.GetBrush("#FFa9a9a9");
			left_label.Background = width_label.Background = area_label.Background = CommonUtil.GetBrush("#FF969696");

			FileData cur_data = (FileData)LIST_FILE_ITEM.SelectedItem;
			if (cur_data == null) return;

			this.focus_pane = null;

			this.SetImg(this.disp_img.ToMemoryStream());
		}

		public void CookieList(int file_idx, int fossil_id)
		{
			PANE_COOKIE_LIST.Children.Clear();

			if (this.cookie_list.Count <= 0)
				return;

			foreach (FossilAPI.FOSSILDETECTInfoList fossil_list in this.cookie_list)
			{
				if (fossil_list.image_index == file_idx)
				{
					FileData file_data = this.GetFile(file_idx);
						
					foreach (FossilAPI.FOSSILDETECTInfo fossil_val in fossil_list.fossils)
						this.AddCookie(file_data, fossil_val, fossil_id);

					this.SortCookie(this.sort_cookie, fossil_id);
					break;
				}
			}
		}
	}
}
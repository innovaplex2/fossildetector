﻿using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Threading;
using System.Windows.Media;
using System.Collections.Generic;
using OpenCvSharp.CPlusPlus;

using System.Runtime.ExceptionServices;
using System.Runtime.InteropServices;
using log4net;

namespace FossilUI
{
	public partial class MainWindow : System.Windows.Window
	{
		private bool progress_anim = false;
		private void AnalyzeFile(object sender, EventArgs e)
		{
			if (this.opening_progress)
				return;

			this.stop_watch.Reset();
			this.stop_watch.Start();

			this.StatusMessage(CommonUtil.LangData("main_window", "analyze_progress"));
			this.fossil_cnt = 0;

			this.bbb = GRID_MAIN_WINDOW.RowDefinitions[2].Height;

			this.progress_anim = Convert.ToBoolean(CommonUtil.LoadJson("aa"));

			if (this.progress_anim == true)
			{
				GRID_MAIN_WINDOW.RowDefinitions[2].MinHeight = 0;
				GRID_MAIN_WINDOW.RowDefinitions[2].Height = new GridLength(0, GridUnitType.Pixel);
			}

			this.interrupt_code = 1;
			this.progress_win = new ProgressWindow(100, CommonUtil.LangData("main_window", "analyze_progress"));
			this.opening_progress = true;

			/*BackgroundWorker analyze_worker = new BackgroundWorker();
			analyze_worker.DoWork += new DoWorkEventHandler(this._tAnalyzeFile);
			analyze_worker.RunWorkerCompleted += (_sender, _e) => this.AnalyzeComplete(_sender, _e);
			analyze_worker.RunWorkerAsync();*/

			this.AnalyzeFile();
			this.AnalyzeComplete();
		}

		[HandleProcessCorruptedStateExceptions]
		//private void _tAnalyzeFile(object sender, DoWorkEventArgs e)
		private void AnalyzeFile()
		{
			int scroll_pos = 0;
			int fossil_cnt = 0;
			IntPtr fossil_detector = IntPtr.Zero;

			try
			{
				fossil_detector = FossilAPI.SetupFossilDetector("detect_darknet.conf");
				//fossil_detector = FossilAPI.SetupFossilDetector("");
			}
			catch (Exception ex)
			{
				LogManager.GetLogger("FileLogger").Error(ex);

				if (FossilAPI.dev_ver == false)
					LogManager.GetLogger("EmailLogger").Error(ex);
			}

			for (int i = 0; i < LIST_FILE_ITEM.Items.Count; i++)
			{
				FileData file_data = LIST_FILE_ITEM.Items[i] as FileData;

				while (this.interrupt_code <= 0)
				{
					if (this.interrupt_code < 0)
						break;
				}

				if (this.interrupt_code < 0)
					break;

				if (this.CheckFile(file_data.idx) == false || file_data.step >= ProcStep.analyze)
					continue;

				// set 'list item color'
				//////////////////////////////
				LIST_FILE_ITEM.Dispatcher.Invoke((Action)(() =>
				{
					ListBoxItem list_item = null;

					try
					{
						list_item = LIST_FILE_ITEM.ItemContainerGenerator.ContainerFromIndex(i) as ListBoxItem;
					}
					catch(Exception ex)
					{
					}

					if (list_item == null)
					{
						LIST_FILE_ITEM.UpdateLayout();
						LIST_FILE_ITEM.ScrollIntoView(LIST_FILE_ITEM.Items[i]);
						list_item = LIST_FILE_ITEM.ItemContainerGenerator.ContainerFromIndex(i) as ListBoxItem;
					}

					TextBlock file_label = CommonUtil.FindVisualChild<TextBlock>(list_item);

					file_label.Foreground = Brushes.Yellow;
				}));
				//////////////////////////////

				file_data.step = ProcStep.analyze;

				int max_cnt = 0;
				int detect_cnt = 0;

				try
				{
					FossilAPI.ProcessFossilDetector(fossil_detector, file_data.mat.Width, file_data.mat.Height,
							(int)file_data.mat.Step1(), file_data.mat.Data, ref max_cnt);
				}
				catch (Exception ex)
				{
					MessageBox.Show(file_data.path + " ==> " + ex.ToString());

					LogManager.GetLogger("FileLogger").Error(ex);

					if (FossilAPI.dev_ver == false)
						LogManager.GetLogger("EmailLogger").Error(ex);
				}

				FossilAPI.FOSSILDETECTInfoList fossil_list = new FossilAPI.FOSSILDETECTInfoList();
				fossil_list.image_index = file_data.idx;
				fossil_list.fossils = new List<FossilAPI.FOSSILDETECTInfo>();
				FossilAPI.FOSSILDETECTInfo[] fossil_data = new FossilAPI.FOSSILDETECTInfo[max_cnt];

				FossilAPI.GetFossilObject(fossil_detector, fossil_data, ref detect_cnt);

				this.fossil_cnt += detect_cnt;

				for (int j = 0; j < detect_cnt; j++)
				{
					fossil_data[j].image_index = file_data.idx;
					fossil_data[j].fossil_id = fossil_cnt;
					fossil_data[j].specify_class = MainWindow.UNDEFINED_GROUP;
					fossil_data[j].cate_class = MainWindow.UNDEFINED_GROUP;
					fossil_data[j].class_no = FossilAPI.light_ver == true ? MainWindow.UNDEFINED_GROUP : fossil_data[j].top_idx[0];

					fossil_list.fossils.Add(fossil_data[j]);

					fossil_cnt++;

					if (this.progress_anim == true)
					{
						PANE_COOKIE_LIST.Dispatcher.Invoke(
							DispatcherPriority.Input,
							new Action(() =>
							{
								OpenCvSharp.CPlusPlus.Rect cookie_rect = CommonUtil.SubRect(fossil_data[j].rc, file_data.mat.Width, file_data.mat.Height);
								this.AddCookie(file_data, fossil_data[j], MainWindow.UNDEFINED_FOSSIL);
								scroll_pos += cookie_rect.Height + 20;
							})
						);
					}
				}

				this.cookie_list.Add(fossil_list);

				if (this.progress_anim == true)
				{
					if (i % 5 == 0)
					{
						PANE_COOKIE_LIST.Dispatcher.Invoke(
							DispatcherPriority.Input,
							new Action(() =>
							{
								((ScrollViewer)PANE_COOKIE_LIST.Parent).ScrollToVerticalOffset(scroll_pos);
							})
						);
					}
				}

				this.progress_win.PROGRESS_PER.Dispatcher.Invoke(
					DispatcherPriority.Input,
					new Action(
						() => this.ProgressPer(this.cookie_list.Count * 100 / LIST_FILE_ITEM.Items.Count)
					)
				);
			}

			try
			{
				FossilAPI.EndupFossilDetector(fossil_detector);
			}
			catch (Exception ex)
			{
				LogManager.GetLogger("FileLogger").Error(ex);

				if (FossilAPI.dev_ver == false)
					LogManager.GetLogger("EmailLogger").Error(ex);
			}

			this.CheckAll(this.GetCheck());
		}

		//private void AnalyzeComplete(object sender, RunWorkerCompletedEventArgs e)
		private void AnalyzeComplete()
		{
			this.Activate();
			this.Topmost = true;
			this.Topmost = false;
			this.Focus();

			GRID_MAIN_WINDOW.RowDefinitions[2].MinHeight = 100;
			GRID_MAIN_WINDOW.RowDefinitions[2].Height = this.bbb;

			if (LIST_FILE_ITEM.Items.Count > 0)
			{
				LIST_FILE_ITEM.SelectedIndex = 0;
				LIST_FILE_ITEM.ScrollIntoView(LIST_FILE_ITEM.Items[0]);
				((ScrollViewer)PANE_COOKIE_LIST.Parent).ScrollToTop();
				this.SelFile();
			}

			this.progress_win.Close();
			this.opening_progress = false;
			this.ButtonStatus();
			this.StatusMessage(CommonUtil.LangData("main_window", "complete"));
			this.stop_watch.Stop();
			this.SetTitle(this.LIST_FILE_ITEM.Items.Count, this.fossil_cnt, "detection ::: " + this.stop_watch.Elapsed.ToString());
		}
	}
}

﻿namespace FossilUI
{
	public struct ProcStep
	{
		public static int init = 0;
		public static int load = 1;
		public static int analyze = 2;
		public static int category = 3;
		public static int recognize = 4;
	}
}

﻿using System;
using System.Windows.Interop;
using Microsoft.Win32.SafeHandles;
using System.Runtime.InteropServices;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.IO;
using System.Windows;
using System.Collections.Generic;

namespace FossilUI
{
	public class OverrideCursor : IDisposable
	{
		static Stack<Cursor> s_Stack = new Stack<Cursor>();

		public OverrideCursor(Cursor changeToCursor)
		{
			s_Stack.Push(changeToCursor);

			if (Mouse.OverrideCursor != changeToCursor)
				Mouse.OverrideCursor = changeToCursor;
		}

		public void Dispose()
		{
			s_Stack.Pop();

			Cursor cursor = s_Stack.Count > 0 ? s_Stack.Peek() : null;

			if (cursor != Mouse.OverrideCursor)
				Mouse.OverrideCursor = cursor;
		}
	}

	class SafeIconHandle : SafeHandleZeroOrMinusOneIsInvalid
	{
		[DllImport("user32.dll", SetLastError = true)]
		[return: MarshalAs(UnmanagedType.Bool)]
		internal static extern bool DestroyIcon(
			[In] IntPtr hIcon);

		private SafeIconHandle()
			: base(true)
		{
		}

		public SafeIconHandle(IntPtr hIcon)
			: base(true)
		{
			this.SetHandle(hIcon);
		}

		protected override bool ReleaseHandle()
		{
			return DestroyIcon(this.handle);
		}
	}

	public static class CursorHelper
	{
		private struct IconInfo
		{
			public bool fIcon;
			public int xHotspot;
			public int yHotspot;
			public IntPtr hbmMask;
			public IntPtr hbmColor;
		}

		[DllImport("user32.dll", SetLastError = true)]
		private static extern IntPtr CreateIconIndirect(ref IconInfo icon);
		[DllImport("user32.dll")]
		[return: MarshalAs(UnmanagedType.Bool)]
		private static extern bool GetIconInfo(IntPtr hIcon, ref IconInfo pIconInfo);

		private static Cursor InternalCreateCursor(System.Drawing.Bitmap bmp,
			int xHotSpot, int yHotSpot)
		{
			IconInfo tmp = new IconInfo();
			GetIconInfo(bmp.GetHicon(), ref tmp);
			tmp.xHotspot = xHotSpot;
			tmp.yHotspot = yHotSpot;
			tmp.fIcon = false;

			IntPtr ptr = CreateIconIndirect(ref tmp);
			//SafeFileHandle handle = new SafeFileHandle(ptr, true);
			SafeIconHandle handle = new SafeIconHandle(ptr);
			return CursorInteropHelper.Create(handle);
		}

		public static Cursor CreateCursor(UIElement element, int xHotSpot,
			int yHotSpot)
		{
			//if (Mouse.LeftButton == MouseButtonState.Pressed)
			//{
			element.Measure(new Size(double.PositiveInfinity,
			  double.PositiveInfinity));
			element.Arrange(new Rect(0, 0, element.DesiredSize.Width,
			  element.DesiredSize.Height));

			RenderTargetBitmap rtb =
			  new RenderTargetBitmap(Convert.ToInt32(element.DesiredSize.Width),
				Convert.ToInt32(element.DesiredSize.Height), 96, 96, PixelFormats.Pbgra32);
			rtb.Render(element);

			PngBitmapEncoder encoder = new PngBitmapEncoder();
			encoder.Frames.Add(BitmapFrame.Create(rtb));

			MemoryStream ms = new MemoryStream();
			encoder.Save(ms);

			System.Drawing.Bitmap bmp = new System.Drawing.Bitmap(ms);

			ms.Close();
			ms.Dispose();

			Cursor cur = InternalCreateCursor(bmp, xHotSpot, yHotSpot);

			bmp.Dispose();

			return cur;
			//}

			//return null;
		}
	}
}

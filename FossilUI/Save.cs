﻿using System;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Threading;
using OpenCvSharp.CPlusPlus;

namespace FossilUI
{
	public partial class MainWindow : System.Windows.Window
	{
		public string SaveDiag()
		{
			string init_dir = Convert.ToString(CommonUtil.LoadJson("save_dir"));
			System.Windows.Forms.SaveFileDialog save_diag = new System.Windows.Forms.SaveFileDialog();
			save_diag.InitialDirectory = init_dir;

			if (this.inst_id != "" && this.inst_depth != "")
				save_diag.FileName = this.inst_id + "_" + this.inst_depth + "_" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".xml";

			save_diag.Filter = "xml (*.xml)|*.xml";
			save_diag.FilterIndex = 1;

			if (save_diag.ShowDialog() != System.Windows.Forms.DialogResult.Cancel)
			{
				CommonUtil.SaveJson("save_dir", Path.GetDirectoryName(save_diag.FileName));
				return save_diag.FileName;
			}

			return "";
		}

		private void SaveAll(object sender, EventArgs e)
		{
			string file_name = this.SaveDiag();

			if (file_name != "")
			{
				this.SaveXml(file_name);
				MessageBox.Show(CommonUtil.LangData("common", "save_complete") + " (" + file_name + ")",
						CommonUtil.LangData("common", "save_complete"), MessageBoxButton.OK, MessageBoxImage.Exclamation);
			}
		}

		public void SaveXml(string file_name)
		{
			this.progress_win = new ProgressWindow(100, CommonUtil.LangData("common", "saving_progress"));
			this.progress_win.Owner = this;
			this.opening_progress = true;
			string save_path = System.IO.Path.GetDirectoryName(file_name);
			int save_cnt = 0;

			using (StreamWriter save_writer = new StreamWriter(file_name))
			{
				save_writer.Write(
					"<?xml version='1.0' encoding='UTF-8'?>" +
					"<data>" +
						"<inst id='" + this.inst_id + "' depth='" + this.inst_depth + "'/>"
				);

				foreach (FileData file_data in LIST_FILE_ITEM.Items)
				{
					save_writer.Write("<img idx='" + file_data.idx.ToString() + "' name='" + file_data.name + "' path='" + file_data.path + "' step='" + file_data.step.ToString() + "'>");

					if (Convert.ToBoolean(CommonUtil.LoadJson("save_img")) == true)
						save_writer.Write(Convert.ToBase64String(file_data.mat.ToMemoryStream().ToArray()));

					save_writer.Write("</img>");
				}

				foreach (FossilAPI.FOSSILDETECTInfoList sub_list in this.cookie_list)
				{
					foreach (FossilAPI.FOSSILDETECTInfo fossil_val in sub_list.fossils)
					{
						save_writer.Write(
							"<cut" +
								" img='" + fossil_val.image_index.ToString() + "'" +
								" idx='" + fossil_val.fossil_id.ToString() + "'" +
								" class='" + fossil_val.class_no.ToString() + "'" +
								" step='" + fossil_val.proc_step.ToString() + "'" +
								" pos='" + fossil_val.rc.left.ToString() + "," + fossil_val.rc.top.ToString() + "," + fossil_val.rc.right.ToString() + "," + fossil_val.rc.bottom.ToString() + "'" +
								" cnt='" + fossil_val.rgb_mean.Length.ToString() + "," + fossil_val.hu_moments.Length.ToString() + "," + fossil_val.descriptors.Length.ToString() + "'" +
								" sclass='" + fossil_val.specify_class.ToString() + "'" +
								" cclass='" + fossil_val.cate_class.ToString() + "'>" +
								"<rgb>"
						);

						for (int i = 0; i < fossil_val.rgb_mean.Length; i++)
							save_writer.Write((i == 0 ? "" : ",") + fossil_val.rgb_mean[i].ToString());

						save_writer.Write("</rgb><hu>");

						for (int i = 0; i < fossil_val.hu_moments.Length; i++)
							save_writer.Write((i == 0 ? "" : ",") + fossil_val.hu_moments[i].ToString());

						save_writer.Write("</hu><desc>");

						for (int i = 0; i < fossil_val.descriptors.Length; i++)
							save_writer.Write((i == 0 ? "" : ",") + fossil_val.descriptors[i].ToString());

						save_writer.Write("</desc><rank>");

						for (int i = 0; i < 5; i++)
							save_writer.Write((i == 0 ? "" : ",") + fossil_val.top_idx[i].ToString() + ":" + fossil_val.top_score[i].ToString());

						save_writer.Write("</rank></cut>");
					}

					save_cnt++;

					this.progress_win.PROGRESS_PER.Dispatcher.Invoke(
						DispatcherPriority.Background,
						new Action(
							() => this.ProgressPer(save_cnt * 100 / this.cookie_list.Count)
						)
					);
				}

				save_writer.Write("</data>");
			}

			this.progress_win.Close();
			this.opening_progress = false;
		}

		public void SaveXml(string file_name, StackPanel group_list, StackPanel undefined_list, CategoryWindow cate_win)
		{
			this.progress_win = new ProgressWindow(100, CommonUtil.LangData("common", "saving_progress"), cate_win);
			this.SaveXml(file_name, group_list, undefined_list);
		}

		public void SaveXml(string file_name, StackPanel group_list, StackPanel undefined_list, RecognizeWindow recog_win)
		{
			this.progress_win = new ProgressWindow(100, CommonUtil.LangData("common", "saving_progress"), recog_win);
			this.SaveXml(file_name, group_list, undefined_list);
		}

		public void SaveXml(string file_name, StackPanel group_list, StackPanel undefined_list)
		{
			this.opening_progress = true;
			string save_path = System.IO.Path.GetDirectoryName(file_name);
			string cut_path = System.IO.Path.GetFileName(file_name) + ".cuts";
			int group_idx = 1;
			int save_cnt = 0;
			int cookie_idx = 1;

			using (StreamWriter save_writer = new StreamWriter(file_name))
			{
				if (System.IO.Directory.Exists(save_path + "\\" + cut_path) == false)
					System.IO.Directory.CreateDirectory(save_path + "\\" + cut_path);

				save_writer.Write(
					"<?xml version='1.0' encoding='UTF-8'?>" +
					"<data>" +
						"<inst id='" + this.inst_id + "' depth='" + this.inst_depth + "'/>"
				);

				foreach (FileData file_item in this.LIST_FILE_ITEM.Items)
				{
					save_writer.Write("<img idx='" + file_item.idx.ToString() + "' name='" + file_item.name + "' path='" + file_item.path + "' step='" + file_item.step.ToString() + "'>");

					if (Convert.ToBoolean(CommonUtil.LoadJson("save_img")) == true)
						save_writer.Write(Convert.ToBase64String(file_item.mat.ToMemoryStream().ToArray()));

					save_writer.Write("</img>");
				}

				foreach (FossilAPI.FOSSILDETECTInfoList sub_list in this.cookie_list)
				{
					foreach (FossilAPI.FOSSILDETECTInfo fossil_val in sub_list.fossils)
					{
						save_writer.Write(
							"<cut" +
								" img='" + fossil_val.image_index.ToString() + "'" +
								" idx='" + fossil_val.fossil_id.ToString() + "'" +
								" class='" + fossil_val.class_no.ToString() + "'" +
								" step='" + fossil_val.proc_step.ToString() + "'" +
								" pos='" + fossil_val.rc.left.ToString() + "," + fossil_val.rc.top.ToString() + "," + fossil_val.rc.right.ToString() + "," + fossil_val.rc.bottom.ToString() + "'" +
								" cnt='" + fossil_val.rgb_mean.Length.ToString() + "," + fossil_val.hu_moments.Length.ToString() + "," + fossil_val.descriptors.Length.ToString() + "'>" +
								"<rgb>"
						);

						for (int i = 0; i < fossil_val.rgb_mean.Length; i++)
							save_writer.Write((i == 0 ? "" : ",") + fossil_val.rgb_mean[i].ToString());

						save_writer.Write("</rgb><hu>");

						for (int i = 0; i < fossil_val.hu_moments.Length; i++)
							save_writer.Write((i == 0 ? "" : ",") + fossil_val.hu_moments[i].ToString());

						save_writer.Write("</hu><desc>");

						for (int i = 0; i < fossil_val.descriptors.Length; i++)
							save_writer.Write((i == 0 ? "" : ",") + fossil_val.descriptors[i].ToString());

						save_writer.Write("</desc><rank>");

						for (int i = 0; i < 5; i++)
							save_writer.Write((i == 0 ? "" : ",") + fossil_val.top_idx[i].ToString() + ":" + fossil_val.top_score[i].ToString());

						save_writer.Write("</rank></cut>");
					}

					save_cnt++;

					this.progress_win.PROGRESS_PER.Dispatcher.Invoke(
						DispatcherPriority.Background,
						new Action(
							() => this.ProgressPer(save_cnt * 50 / this.cookie_list.Count)
						)
					);
				}

				save_cnt = 0;

				foreach (StackPanel group_pane in group_list.Children)
				{
					cookie_idx = 1;
					CustomCaretTextBox_grp group_name = (CustomCaretTextBox_grp)((StackPanel)group_pane.Children[0]).Children[0];
					ScrollViewer group_scroll = (ScrollViewer)group_pane.Children[2];
					StackPanel item_pane = (StackPanel)group_scroll.Content;
					int class_no = (int)item_pane.Tag;

					if (item_pane.Children.Count > 0)
					{
						foreach (Border item_border in item_pane.Children)
						{
							Image item_img = (Image)((Border)((StackPanel)item_border.Child).Children[0]).Child;
							FossilAPI.FOSSILDETECTInfo cookie_data = (FossilAPI.FOSSILDETECTInfo)item_img.Tag;
							FileData file_data = this.GetFile(cookie_data.image_index);

							Mat item_mat = file_data.mat.SubMat(
								cookie_data.rc.top,
								cookie_data.rc.bottom,
								cookie_data.rc.left,
								cookie_data.rc.right
							);

							if (System.IO.Directory.Exists(save_path + "\\" + cut_path + "\\" + group_name.txt_box.Text) == false)
								System.IO.Directory.CreateDirectory(save_path + "\\" + cut_path + "\\" + group_name.txt_box.Text);

							string cut_name = group_idx.ToString() + "_" + cookie_idx.ToString() + ".jpg";

							item_mat.SaveImage(save_path + "\\" + cut_path + "\\" + group_name.txt_box.Text + "\\" + cut_name);

							cookie_idx++;
						}
					}

					save_writer.Write("<group class='" + class_no.ToString() + "'>" + group_name.txt_box.Text + "</group>");

					save_cnt++;

					this.progress_win.PROGRESS_PER.Dispatcher.Invoke(
						DispatcherPriority.Background,
						new Action(
							() => this.ProgressPer(save_cnt * 50 / group_list.Children.Count + 50)
						)
					);
				}

				save_writer.Write("</data>");

				// undefined
				cookie_idx = 1;
				string undefined_label = CommonUtil.LangData("recognizer_window", "unidentified_group");

				foreach (Border item_border in undefined_list.Children)
				{
					Image item_img = (Image)((Border)((StackPanel)item_border.Child).Children[0]).Child;
					FossilAPI.FOSSILDETECTInfo cookie_data = (FossilAPI.FOSSILDETECTInfo)item_img.Tag;
					FileData file_data = this.GetFile(cookie_data.image_index);

					Mat item_mat = file_data.mat.SubMat(
						cookie_data.rc.top,
						cookie_data.rc.bottom,
						cookie_data.rc.left,
						cookie_data.rc.right
					);

					if (System.IO.Directory.Exists(save_path + "\\" + cut_path + "\\" + undefined_label) == false)
						System.IO.Directory.CreateDirectory(save_path + "\\" + cut_path + "\\" + undefined_label);

					string cut_name = group_idx.ToString() + "_" + cookie_idx.ToString() + ".jpg";

					item_mat.SaveImage(save_path + "\\" + cut_path + "\\" + undefined_label + "\\" + cut_name);

					cookie_idx++;
				}
			}

			this.progress_win.Close();
			this.opening_progress = false;

			MessageBox.Show(CommonUtil.LangData("common", "save_complete") + " (" + file_name + ")",
					CommonUtil.LangData("common", "save_complete"), MessageBoxButton.OK, MessageBoxImage.Exclamation);
		}
	}
}

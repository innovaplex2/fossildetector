﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Threading;
using System.IO;
using System.Threading.Tasks;
using OpenCvSharp.CPlusPlus;
using System.Windows.Media.Effects;

namespace FossilUI
{
	internal class RecognizeItem
	{
		private Border _item_border = null;
		private StackPanel _cur_pane = null;

		public Border item_border { get { return _item_border; } set { _item_border = value; } }
		public StackPanel cur_pane { get { return _cur_pane; } set { _cur_pane = value; } }

		private MainWindow main_win = null;
		private RecognizeWindow recog_win = null;
		private Image item_img = null;
		private StackPanel cookie_pane = null;
		private StackPanel group_pane = null;
		private int cookie_idx;
		private FossilAPI.FOSSILDETECTInfo cookie_data;
		private MemoryStream cookie_stream = null;
		private Brush act_brush = CommonUtil.GetBrush("#FFF1C40F");
		private bool view_score = false;

		public RecognizeItem(MainWindow main_win, RecognizeWindow recog_win, int cookie_idx, FossilAPI.FOSSILDETECTInfo cookie_data, StackPanel cookie_pane, StackPanel group_pane,
				StackPanel start_pane, bool view_score)
		{
			this.main_win = main_win;
			this.recog_win = recog_win;
			Mat img_cookie = null;

			if (recog_win.test_dir != "")
			{
				Mat test_cookie = new Mat(recog_win.test_img[cookie_data.image_index]);

				if (test_cookie.Width > 100)
					img_cookie = test_cookie.Resize(new OpenCvSharp.CPlusPlus.Size(100, test_cookie.Height * 100 / test_cookie.Width));
			}
			else
			{
				FileData file_data = this.main_win.GetFile(cookie_data.image_index);
				OpenCvSharp.CPlusPlus.Rect cookie_rect = CommonUtil.SubRect(cookie_data.rc, file_data.mat.Width,
						file_data.mat.Height);
				img_cookie = file_data.mat.SubMat(cookie_rect.Y, cookie_rect.Y + cookie_rect.Height,
						cookie_rect.X, cookie_rect.X + cookie_rect.Width);
			}

			this.InitCookie(recog_win, cookie_idx, cookie_data, cookie_pane, group_pane, start_pane, img_cookie, view_score);
		}

		public RecognizeItem(MainWindow main_win, RecognizeWindow recog_win, int cookie_idx, FossilAPI.FOSSILDETECTInfo cookie_data, StackPanel cookie_pane, StackPanel group_pane,
				StackPanel start_pane, string cookie_path)
		{
			this.main_win = main_win;
			this.recog_win = recog_win;
			Mat test_cookie = new Mat(cookie_path);
			Mat img_cookie;

			if (test_cookie.Width > 100)
				img_cookie = test_cookie.Resize(new OpenCvSharp.CPlusPlus.Size(100, test_cookie.Height * 100 / test_cookie.Width));
			else
				img_cookie = test_cookie.Resize(new OpenCvSharp.CPlusPlus.Size(32, 32));

			this.InitCookie(recog_win, cookie_idx, cookie_data, cookie_pane, group_pane, start_pane, img_cookie, true);
		}

		private void InitCookie(RecognizeWindow recog_win, int cookie_idx, FossilAPI.FOSSILDETECTInfo cookie_data, StackPanel cookie_pane, StackPanel group_pane,
				StackPanel start_pane, Mat img_cookie, bool view_score)
		{
			this.cookie_idx = cookie_idx;
			this.cookie_data = cookie_data;
			this.cookie_pane = cookie_pane;
			this.cookie_stream = img_cookie.ToMemoryStream();
			this.group_pane = group_pane;
			this.view_score = view_score;

			this.item_border = new Border
			{
				BorderBrush = Brushes.Transparent,
				BorderThickness = new Thickness(3),
				Margin = new Thickness(3),
				VerticalAlignment = VerticalAlignment.Top,
				HorizontalAlignment = HorizontalAlignment.Center
			};
			StackPanel item_frame = new StackPanel
			{
				Orientation = Orientation.Vertical
			};
			Border img_border = new Border
			{
				Width = img_cookie.Width,
				Height = img_cookie.Height,
				BorderBrush = Brushes.DimGray,
				BorderThickness = new Thickness(1)
			};
			this.item_img = new Image
			{
				Stretch = Stretch.Fill,
				Width = img_cookie.Width,
				Height = img_cookie.Height,
				Cursor = Cursors.SizeAll,
				Opacity = 0,
				Tag = cookie_data
			};

			this.item_img.Source = CommonUtil.MemImg(this.cookie_stream);
			this.item_img.DragEnter += new DragEventHandler(this.DragImage);
			this.item_img.MouseLeftButtonDown += new MouseButtonEventHandler(this.DownImage);
			this.item_img.MouseLeftButtonUp += new MouseButtonEventHandler(this.UpImage);
			this.item_img.MouseMove += new MouseEventHandler(this.MoveImage);

			img_border.Child = this.item_img;
			item_frame.Children.Add(img_border);
			this.item_border.Child = item_frame;

			if (this.view_score == true)
			{
				string item_score = "";

				for (int i = 0; i < 2; i++)
				{
					if (i > 0)
						item_score += System.Environment.NewLine;

					item_score += cookie_data.top_idx[i].ToString() + " : " + cookie_data.top_score[i].ToString();
				}

				Label score_label = new Label
				{
					Content = item_score,
					Foreground = Brushes.Yellow,
					HorizontalContentAlignment = System.Windows.HorizontalAlignment.Center,
					Margin = new Thickness(0, 5, 0, 0)
				};

				item_frame.Children.Add(score_label);
			}

			this.ToPane(start_pane, true);
		}

		public void ToPane(StackPanel to_pane)
		{
			this.ToPane(to_pane, false);
		}

		private void ToPane(StackPanel to_pane, bool init_pane)
		{
			double max_height = 150;
			int add_height = this.view_score == true ? 50 : 30;

			if (this.cur_pane != null)
			{
				this.cur_pane.Children.Remove(this.item_border);

				if ((int)this.cur_pane.Tag == MainWindow.UNDEFINED_GROUP)
					this.recog_win.LABEL_UNIDENTIFIED_COUNT.Content = this.cur_pane.Children.Count.ToString("#,##0");
				else
				{
					max_height = 150;

					foreach (Border item_border in this.cur_pane.Children)
					{
						Image item_img = (Image)((Border)((StackPanel)item_border.Child).Children[0]).Child;

						if (item_img.Height > max_height)
							max_height = item_img.Height;
					}

					this.cur_pane.Height = max_height + add_height;
					((ScrollViewer)this.cur_pane.Parent).Height = max_height + add_height + 10;
					((Label)((StackPanel)((ScrollViewer)this.cur_pane.Parent).Parent).Children[1]).Content = this.cur_pane.Children.Count.ToString("#,##0");
				}
			}

			this.cur_pane = to_pane;
			this.cur_pane.Children.Add(this.item_border);
			FossilAPI.FOSSILDETECTInfo cookie_data = (FossilAPI.FOSSILDETECTInfo)this.item_img.Tag;
			int class_no = (int)this.cur_pane.Tag;

			cookie_data.class_no = class_no;

			foreach (CookieData recog_data in this.recog_win.item_list)
			{
				if (recog_data.data.fossil_id == cookie_data.fossil_id)
				{
					recog_data.data = cookie_data;
					break;
				}
			}

			if (init_pane == true)
			{
				ScrollViewer item_scroll = (ScrollViewer)this.cur_pane.Parent;

				this.item_border.Dispatcher.Invoke(
					DispatcherPriority.Background,
					new Action(() => {
						DoubleAnimation opacity_anim = new DoubleAnimation(0, 1, TimeSpan.FromSeconds(0.5));
						this.item_img.BeginAnimation(UIElement.OpacityProperty, opacity_anim);
					}
				));

				item_scroll.ScrollToRightEnd();
				item_scroll.ScrollToBottom();
			}

			if ((int)this.cur_pane.Tag == MainWindow.UNDEFINED_GROUP)
			{
				this.recog_win.LABEL_UNIDENTIFIED_COUNT.Content = this.cur_pane.Children.Count.ToString("#,##0");
				return;
			}

			max_height = 150;

			foreach (Border item_border in this.cur_pane.Children)
			{
				Image item_img = (Image)((Border)((StackPanel)item_border.Child).Children[0]).Child;

				if (item_img.Height > max_height)
					max_height = item_img.Height;
			}

			this.cur_pane.Height = max_height + add_height;
			((ScrollViewer)this.cur_pane.Parent).Height = max_height + add_height + 10;
			((Label)((StackPanel)((ScrollViewer)this.cur_pane.Parent).Parent).Children[1]).Content = this.cur_pane.Children.Count.ToString("#,##0");
		}

		private void DragImage(object sender, DragEventArgs e)
		{
			if (Mouse.LeftButton == MouseButtonState.Pressed)
			{
				Image cur_img = (Image)sender;
				if (cur_img == null) return;

				FossilAPI.FOSSILDETECTInfo cookie_data = (FossilAPI.FOSSILDETECTInfo)cur_img.Tag;
				this.recog_win.StatusMessage(CommonUtil.GetPath(cookie_data.image_index));
			}
		}

		private void DownImage(object sender, MouseButtonEventArgs e)
		{
			Image cur_img = (Image)sender;
			if (cur_img == null) return;

			if (e.ClickCount == 2)
			{
				FossilAPI.FOSSILDETECTInfo cookie_data = (FossilAPI.FOSSILDETECTInfo)cur_img.Tag;
				this.main_win.SelFile(cookie_data);
			}
			else
			{
				((Border)((StackPanel)((Border)cur_img.Parent).Parent).Parent).BorderBrush = CommonUtil.GetBrush("#FFF1C40F");
				this.recog_win.cur_img = cur_img;
				this.recog_win.cur_cursor = null;

				this.recog_win.GroupFeedback(true);
			}
		}

		private async Task FocusCookie(FossilAPI.FOSSILDETECTInfo cookie_data)
		{
			this.recog_win.StatusMessage(CommonUtil.GetPath(cookie_data.image_index));
			this.main_win.SelFile(cookie_data);
		}

		private void UpImage(object sender, MouseButtonEventArgs e)
		{
			if (this.recog_win.cur_img != null)
			{
				((Border)((StackPanel)((Border)this.recog_win.cur_img.Parent).Parent).Parent).BorderBrush = Brushes.Transparent;
				this.recog_win.GroupFeedback(false);
				this.recog_win.cur_img = null;
				this.recog_win.cur_cursor = null;
			}
		}

		private void MoveImage(object sender, MouseEventArgs e)
		{
			if (Mouse.LeftButton == MouseButtonState.Pressed)
			{
				Image cur_img = (Image)sender;
				if (cur_img == null) return;
				Border cur_border = (Border)((StackPanel)((Border)cur_img.Parent).Parent).Parent;

				this.recog_win.cur_img = cur_img;
				DataObject drag_data = new DataObject("cookie_cut", this);
				DragDrop.DoDragDrop(cur_border, drag_data, DragDropEffects.Link);

				if (this.recog_win.cur_img != null)
				{
					((Border)((StackPanel)((Border)this.recog_win.cur_img.Parent).Parent).Parent).BorderBrush = Brushes.Transparent;
					this.recog_win.GroupFeedback(false);
					this.recog_win.cur_img = null;
					this.recog_win.cur_cursor = null;
				}
			}
		}
	}
}

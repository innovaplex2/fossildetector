﻿using System.Runtime.InteropServices.ComTypes;

namespace FossilUI
{
	public struct InnovaLicenseInfo
	{
		public InnovaPlexeLicenseType licenseType;
		public uint Options;
		public FILETIME TimeLimit;
	}
}

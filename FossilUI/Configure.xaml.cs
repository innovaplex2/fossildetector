﻿using System;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace FossilUI
{
    /// <summary>
    /// Interaction logic for Configure.xaml
    /// </summary>
    public partial class Configure : Window
    {
        public Configure()
        {
            InitializeComponent();
            this.ShowDialog();
        }

        private void OnOK(object sender, EventArgs e)
        {
            MessageBox.Show("Do you want to save config?", "Save config Changes", MessageBoxButton.YesNo, MessageBoxImage.Question);
            this.Close();
        }

        private void OnCancel(object sender, EventArgs e)
        {
            this.Close();
        }

    }
}

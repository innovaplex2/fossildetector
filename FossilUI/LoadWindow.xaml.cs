﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace FossilUI
{
	/// <summary>
	/// Interaction logic for LoadWindow.xaml
	/// </summary>
	public partial class LoadWindow : Window
	{
		private MainWindow main_win = null;
		private int load_type = 0;

		public LoadWindow()
		{
			InitializeComponent();

			//RADIO_LOAD_REMOTE.Content = CommonUtil.LangData("load_window", "load_remote");
			RADIO_LOAD_DIR.Content = CommonUtil.LangData("load_window", "load_dir");
			CHECK_SUB_DIR.Content = CommonUtil.LangData("load_window", "sub_dir");
			RADIO_LOAD_FILE.Content = CommonUtil.LangData("load_window", "load_file");
			CHECK_SHOW_INSTALL.Content = CommonUtil.LangData("load_window", "install_id_depth");
			LABEL_WELL_ID.Content = CommonUtil.LangData("load_window", "well_id");
			LABEL_DEPTH.Content = CommonUtil.LangData("load_window", "depth");
			BUTTON_LOAD_OK.Content = CommonUtil.LangData("common", "ok");
			BUTTON_LOAD_CANCEL.Content = CommonUtil.LangData("common", "cancel");

			this.main_win = (MainWindow)Application.Current.MainWindow;

			this.Owner = Application.Current.MainWindow;

			this.load_type = Convert.ToInt32(CommonUtil.LoadJson("load_type"));

			this.CheckItem();

			TEXT_INST_ID.txt_box.KeyDown += new KeyEventHandler(this.EnterKey);
			TEXT_INST_DEPTH.txt_box.KeyDown += new KeyEventHandler(this.EnterKey);

			TEXT_INST_ID.txt_box.Text = this.main_win.inst_id;
			TEXT_INST_DEPTH.txt_box.Text = this.main_win.inst_depth;

			CHECK_SHOW_INSTALL.IsChecked = Convert.ToBoolean(CommonUtil.LoadJson("load_install"));

			this.PreviewKeyDown += new KeyEventHandler(this.EscKey);

			this.ShowInstall();
			this.ShowDialog();
		}

		private void CheckItem(object sender, EventArgs e)
		{
			RadioButton cur_item = (RadioButton)sender;

			if (cur_item.Name == "RADIO_LOAD_REMOTE")
				this.load_type = 2;
			else if (cur_item.Name == "RADIO_LOAD_FILE")
				this.load_type = 1;
			else if (cur_item.Name == "RADIO_LOAD_DIR")
				this.load_type = 0;

			this.CheckItem();
		}

		private void CheckItem()
		{
			switch(this.load_type)
			{
				case 1:
					//RADIO_LOAD_REMOTE.IsChecked = false;
					RADIO_LOAD_DIR.IsChecked = false;
					CHECK_SUB_DIR.IsEnabled = false;
					RADIO_LOAD_FILE.IsChecked = true;
					break;
				case 2:
					//RADIO_LOAD_REMOTE.IsChecked = true;
					RADIO_LOAD_DIR.IsChecked = false;
					CHECK_SUB_DIR.IsEnabled = false;
					RADIO_LOAD_FILE.IsChecked = false;
					break;
				default:
					//RADIO_LOAD_REMOTE.IsChecked = false;
					RADIO_LOAD_DIR.IsChecked = true;
					CHECK_SUB_DIR.IsEnabled = true;
					RADIO_LOAD_FILE.IsChecked = false;
					break;
			}
		}

		private void EscKey(object sender, KeyEventArgs e)
		{
			if (e.Key == Key.Escape)
				this.Close();
		}

		private void EnterKey(object sender, KeyEventArgs e)
		{
			if (e.Key == Key.Return)
			{
				this.LoadRun();
				return;
			}
		}

		private void ShowInstall()
		{
			bool load_install = CHECK_SHOW_INSTALL.IsChecked == true ? true : false;
			TEXT_INST_ID.IsEnabled = TEXT_INST_DEPTH.IsEnabled = load_install;
			this.WindowStartupLocation = System.Windows.WindowStartupLocation.CenterScreen;
		}

		private void ShowInstall(object sender, EventArgs e)
		{
			this.ShowInstall();
		}

		private void LoadRun(object sender, EventArgs e)
		{
			this.LoadRun();
		}

		private void LoadRun()
		{
			if (CHECK_SHOW_INSTALL.IsChecked == true)
			{
				if (!CommonUtil.IsID(TEXT_INST_ID.txt_box.Text) || !CommonUtil.IsID(TEXT_INST_DEPTH.txt_box.Text))
				{
					MessageBox.Show(CommonUtil.LangData("load_window", "wrong_id_depth"), CommonUtil.LangData("common", "error"), MessageBoxButton.OK, MessageBoxImage.Error);
					return;
				}

				this.main_win.inst_id = TEXT_INST_ID.txt_box.Text.TrimStart();
				this.main_win.inst_depth = TEXT_INST_DEPTH.txt_box.Text.TrimEnd();
			}
			else
			{
				this.main_win.inst_id = "";
				this.main_win.inst_depth = "";
			}

			/*if (RADIO_LOAD_REMOTE.IsChecked == true)
			{
				//CommonUtil.SaveJson("load_type", 2);
			}*/
			if (RADIO_LOAD_FILE.IsChecked == true)
			{
				//CommonUtil.SaveJson("load_type", 1);
				this.main_win.LoadFile(this);
			}
			else
			{
				//CommonUtil.SaveJson("load_type", 0);
				bool sub_dir = CHECK_SUB_DIR.IsChecked == true ? true : false;
				this.main_win.LoadDir(this, sub_dir);
				//this.main_win.LoadDir(this, CHECK_SUB_DIR.IsChecked);
			}
		}

		private void ClosingLoad(object sender, EventArgs e)
		{
			bool load_install = CHECK_SHOW_INSTALL.IsChecked == true ? true : false;
			bool sub_dir = CHECK_SUB_DIR.IsChecked == true ? true : false;
			CommonUtil.SaveJson("load_type", this.load_type);
			CommonUtil.SaveJson("load_install", load_install);
			CommonUtil.SaveJson("sub_dir", sub_dir);
		}

		private void CloseLoad(object sender, MouseButtonEventArgs e)
		{
			this.Close();
		}

		private void CloseLoad(object sender, EventArgs e)
		{
			this.Close();
		}
	}
}

﻿using System.Linq;
using System.Windows;
using System.Windows.Controls;

namespace FossilUI
{
	/// <summary>
	/// Interaction logic for CustomCaretTextBox_grp.xaml
	/// </summary>
	public partial class CustomCaretTextBox_grp : UserControl
	{
		public TextBox txt_box = null;

		public CustomCaretTextBox_grp()
		{
			InitializeComponent();

			this.txt_box = this.TextBoxContainer.Children.OfType<TextBox>().FirstOrDefault();
			txt_box.SelectionChanged += (sender, e) => MoveCustomCaret();
			txt_box.LostFocus += (sender, e) => Caret.Visibility = Visibility.Collapsed;
			txt_box.GotFocus += (sender, e) => Caret.Visibility = Visibility.Visible;
		}

		private void MoveCustomCaret()
		{
			var caretLocation = this.txt_box.GetRectFromCharacterIndex(this.txt_box.CaretIndex).Location;

			if (!double.IsInfinity(caretLocation.X))
			{
				Canvas.SetLeft(Caret, caretLocation.X);
			}

			if (!double.IsInfinity(caretLocation.Y))
			{
				Canvas.SetTop(Caret, caretLocation.Y);
			}
		}
	}
}

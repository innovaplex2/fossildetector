﻿using System.Windows.Controls;

namespace FossilUI
{
	public class CookieData
	{
		public int idx { get; set; }
		public int id { get; set; }
		public FossilAPI.FOSSILDETECTInfo data { get; set; }
		public Border item { get; set; }
	}
}

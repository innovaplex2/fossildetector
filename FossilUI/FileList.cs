﻿using System;
using System.IO;
using System.Windows;
using System.Windows.Input;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;

namespace FossilUI
{
	public partial class MainWindow : System.Windows.Window
	{
		/*public struct ListMode
		{
			public static int clear = 0;
			public static int no = 1;
			public static int process = 2;
			public static int all = 3;
		}*/

		public void AddFile(int file_idx, FileData file_data)
		{
			if (LIST_FILE_ITEM.Dispatcher.CheckAccess())
			{
				LIST_FILE_ITEM.Items.Add(file_data);

				ListBoxItem list_item = LIST_FILE_ITEM.ItemContainerGenerator.ContainerFromIndex(LIST_FILE_ITEM.Items.Count - 1) as ListBoxItem;

				if (list_item == null)
				{
					LIST_FILE_ITEM.UpdateLayout();
					LIST_FILE_ITEM.ScrollIntoView(LIST_FILE_ITEM.Items[LIST_FILE_ITEM.Items.Count - 1]);
					list_item = LIST_FILE_ITEM.ItemContainerGenerator.ContainerFromIndex(LIST_FILE_ITEM.Items.Count - 1) as ListBoxItem;
				}

				Grid data_grid = CommonUtil.FindVisualChild<Grid>(list_item);
				data_grid.RowDefinitions[1].Height = new GridLength(0, this.list_type == 0 ? GridUnitType.Pixel : GridUnitType.Auto);
				Image file_img = CommonUtil.FindVisualChild<Image>(list_item, "IMAGE_FILE_LIST");
				BitmapImage file_bitmap = new BitmapImage();
				file_bitmap.BeginInit();
				file_bitmap.DecodePixelWidth = 500;
				file_bitmap.CacheOption = BitmapCacheOption.OnLoad;

				if (File.Exists(file_data.path) == true)
					file_bitmap.UriSource = new Uri(file_data.path);
				else
					file_bitmap.StreamSource = new MemoryStream(file_data.mat.ToBytes());

				file_bitmap.EndInit();
				file_img.Source = file_bitmap;

				/*bool tot_check = true;

				for (int i = 0; i < LIST_FILE_ITEM.Items.Count; i++)
				{
					list_item = LIST_FILE_ITEM.ItemContainerGenerator.ContainerFromIndex(i) as ListBoxItem;

					if (list_item == null)
					{
						LIST_FILE_ITEM.UpdateLayout();
						LIST_FILE_ITEM.ScrollIntoView(LIST_FILE_ITEM.Items[i]);
						list_item = LIST_FILE_ITEM.ItemContainerGenerator.ContainerFromIndex(i) as ListBoxItem;
					}

					CheckBox file_check = CommonUtil.FindVisualChild<CheckBox>(list_item);
					//TextBlock file_label = CommonUtil.FindVisualChild<TextBlock>(list_item);

					//file_label.Foreground = Brushes.Gray;

					if (file_check.IsChecked == false)
					{
						tot_check = false;
						break;
					}
				}

				CHECK_FILE_ALL.Tag = tot_check ? "3" : "0";
				CHECK_FILE_ALL.IsChecked = tot_check;*/
				this.CheckAll(this.GetCheck());
			}
			else
			{
				LIST_FILE_ITEM.Dispatcher.BeginInvoke(
					new Action(
						() => this.AddFile(file_idx, file_data)
					)
				);
			}
		}

		public void ApplyFile(int file_idx, string file_path, MemoryStream img_stream)
		{
			if (LIST_FILE_ITEM.Dispatcher.CheckAccess())
			{
				this.SetImg(img_stream);
			}
			else
			{
				LIST_FILE_ITEM.Dispatcher.BeginInvoke(
					new Action(
						() => this.ApplyFile(file_idx, file_path, img_stream)
					)
				);
			}
		}

		private void CheckAll(object sender, EventArgs e)
		{
			CheckBox all_check = (CheckBox)sender;

			if (all_check.Tag == "0")
				all_check.Tag = "1";
			else if (all_check.Tag == "1")
				all_check.Tag = "0";
			else if (all_check.Tag == "2")
				all_check.Tag = "3";
			else if (all_check.Tag == "3")
				all_check.Tag = "4";
			else if (all_check.Tag == "4")
				all_check.Tag = "5";
			else if (all_check.Tag == "5")
				all_check.Tag = "2";

			this.CheckAll(Convert.ToInt32(all_check.Tag));
		}

		private string GetCheck()
		{
			/*int checked_step = ProcStep.init;
			int unchecked_step = ProcStep.init;*/
			int step_buffer = -1;

			for (int i = 0; i < LIST_FILE_ITEM.Items.Count; i++)
			{
				ListBoxItem list_item = LIST_FILE_ITEM.ItemContainerGenerator.ContainerFromIndex(i) as ListBoxItem;

				if (list_item == null)
				{
					LIST_FILE_ITEM.UpdateLayout();
					LIST_FILE_ITEM.ScrollIntoView(LIST_FILE_ITEM.Items[i]);
					list_item = LIST_FILE_ITEM.ItemContainerGenerator.ContainerFromIndex(i) as ListBoxItem;
				}

				CheckBox file_check = CommonUtil.FindVisualChild<CheckBox>(list_item);
				FileData file_data = LIST_FILE_ITEM.Items[i] as FileData;

				if (step_buffer >= 0 && step_buffer != Math.Min(file_data.step, ProcStep.analyze))
					return "3";

				step_buffer = Math.Min(file_data.step, ProcStep.analyze);

				/*if (file_check.IsChecked == true)
				{
					if (checked_step < Math.Min(file_data.step, ProcStep.analyze))
						checked_step = Math.Min(file_data.step, ProcStep.analyze);
				}
				else if (file_check.IsChecked == false)
				{
					if (unchecked_step < Math.Min(file_data.step, ProcStep.analyze))
						unchecked_step = Math.Min(file_data.step, ProcStep.analyze);
				}*/
			}

			return "1";
		}

		private void CheckAll(string check_step)
		{
			//MessageBox.Show("check step : " + check_step);
			CHECK_FILE_ALL.Tag = check_step;
			this.CheckAll(Convert.ToInt32(check_step));
		}

		private void CheckAll(int check_step)
		{
			int proc_step = ProcStep.recognize + 1;

			for (int i = 0; i < LIST_FILE_ITEM.Items.Count; i++)
			{
				ListBoxItem list_item = LIST_FILE_ITEM.ItemContainerGenerator.ContainerFromIndex(i) as ListBoxItem;

				if (list_item == null)
				{
					LIST_FILE_ITEM.UpdateLayout();
					LIST_FILE_ITEM.ScrollIntoView(LIST_FILE_ITEM.Items[i]);
					list_item = LIST_FILE_ITEM.ItemContainerGenerator.ContainerFromIndex(i) as ListBoxItem;
				}

				CheckBox file_check = CommonUtil.FindVisualChild<CheckBox>(list_item);
				//file_check.IsChecked = all_check.IsChecked;
				FileData file_data = LIST_FILE_ITEM.Items[i] as FileData;
				//file_data.check = all_check.IsChecked == true ? true : false;

				if (check_step == 0 || check_step == 2)
					file_check.IsChecked = false;
				else if (check_step == 1 || check_step == 5)
					file_check.IsChecked = true;
				else if (check_step == 3)
					file_check.IsChecked = file_data.step >= ProcStep.analyze ? true : false;
				else if (check_step == 4)
					file_check.IsChecked = file_data.step >= ProcStep.analyze ? false : true;

				file_data.check = file_check.IsChecked == true ? true : false;

				if (file_check.IsChecked == false)
					continue;

				if (proc_step > file_data.step)
					proc_step = file_data.step;
			}

			this.ButtonStatus(proc_step <= ProcStep.recognize ? proc_step : 0);
		}

		private void CheckItem(object sender, EventArgs e)
		{
			CheckBox item_check = (CheckBox)sender;
			bool tot_check = true;
			int proc_step = ProcStep.recognize + 1;

			for (int i = 0; i < LIST_FILE_ITEM.Items.Count; i++)
			{
				ListBoxItem list_item = LIST_FILE_ITEM.ItemContainerGenerator.ContainerFromIndex(i) as ListBoxItem;
				CheckBox file_check = CommonUtil.FindVisualChild<CheckBox>(list_item);
				FileData file_data = LIST_FILE_ITEM.Items[i] as FileData;
				file_data.check = file_check.IsChecked == true ? true : false;

				if (file_check.IsChecked == false)
				{
					tot_check = false;
					continue;
				}

				if (proc_step > file_data.step)
					proc_step = file_data.step;
			}

			CHECK_FILE_ALL.Tag = tot_check ? "3" : "0";
			CHECK_FILE_ALL.IsChecked = tot_check;
			this.ButtonStatus(proc_step <= ProcStep.recognize ? proc_step : 0);
		}

		public bool CheckFile(int file_idx)
		{
			for (int i = 0; i < LIST_FILE_ITEM.Items.Count; i++)
			{
				FileData file_data = LIST_FILE_ITEM.Items[i] as FileData;

				if (file_data.idx == file_idx)
					return file_data.check;
			}

			return false;
		}

		public void SelFile(FossilAPI.FOSSILDETECTInfo cookie_data)
		{
			LIST_FILE_ITEM.SelectionChanged -= this.SelFile;

			for (int i = 0; i < LIST_FILE_ITEM.Items.Count; i++)
			{
				ListBoxItem list_item = LIST_FILE_ITEM.ItemContainerGenerator.ContainerFromIndex(i) as ListBoxItem;
				CheckBox file_check = CommonUtil.FindVisualChild<CheckBox>(list_item);

				if (Convert.ToInt32(file_check.Tag) == cookie_data.image_index)
				{
					list_item.IsSelected = true;
					break;
				}
			}

			LIST_FILE_ITEM.SelectionChanged += new SelectionChangedEventHandler(this.SelFile);

			this.SelFile(cookie_data.fossil_id);
		}

		public void SelFile()
		{
			this.SelFile(MainWindow.UNDEFINED_FOSSIL);
		}

		private void RemoveFile(object sender, EventArgs e)
		{
			Button cur_btn = (Button)sender;
			if (cur_btn == null) return;
			bool is_remove = false;

			if (Keyboard.Modifiers == ModifierKeys.Shift)
			{
				if (MessageBox.Show(CommonUtil.LangData("main_window", "remove_all"), CommonUtil.LangData("common", "confirm"),
						MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
					this.LoadInit();
			}
			else
			{
				for (int i = 0; i < LIST_FILE_ITEM.Items.Count; i++)
				{
					FileData file_data = LIST_FILE_ITEM.Items[i] as FileData;

					if (Convert.ToInt32(cur_btn.Tag) == file_data.idx)
					{
						LIST_FILE_ITEM.Items.RemoveAt(i);

						foreach (FossilAPI.FOSSILDETECTInfoList sub_list in this.cookie_list)
						{
							if (sub_list.image_index == file_data.idx)
							{
								this.cookie_list.Remove(sub_list);
								break;
							}
						}

						is_remove = true;
						break;
					}
				}

				if (LIST_FILE_ITEM.Items.Count == 0)
				{
					this.LoadInit();
					return;
				}

				if (is_remove)
					LIST_FILE_ITEM.SelectedIndex = 0;
			}
		}

		public void SelFile(int fossil_id)
		{
			FileData cur_data = (FileData)LIST_FILE_ITEM.SelectedItem;
			if (cur_data == null) return;
			bool chk_fossil = false;

			this.disp_img = cur_data.mat.Clone();

			OpenCvSharp.CPlusPlus.Rect cookie_rect;
			FossilAPI.FOSSILDETECTInfo fossil_buffer = new FossilAPI.FOSSILDETECTInfo();

			foreach (FossilAPI.FOSSILDETECTInfoList sub_list in this.cookie_list)
			{
				if (sub_list.fossils == null)
					continue;

				foreach (FossilAPI.FOSSILDETECTInfo fossil_val in sub_list.fossils)
				{
					if (fossil_val.image_index != cur_data.idx)
						continue;

					cookie_rect = CommonUtil.SubRect(fossil_val.rc, this.disp_img.Width, this.disp_img.Height);

					if (fossil_id >= 0 && fossil_val.fossil_id == fossil_id)
						fossil_buffer = fossil_val;

					this.disp_img.Rectangle
					(
						cookie_rect,
						new OpenCvSharp.CPlusPlus.Scalar(0, 0, 255, 255),
						3,
						OpenCvSharp.LineType.AntiAlias
					);

					chk_fossil = true;
				}

				if (chk_fossil == true)
					break;
			}

			if (fossil_id >= 0)
			{
				OpenCvSharp.CPlusPlus.Mat conv_img = this.disp_img.Clone();
				if (conv_img == null) return;

				cookie_rect = CommonUtil.SubRect(fossil_buffer.rc, this.disp_img.Width, this.disp_img.Height);

				conv_img.Rectangle(
					cookie_rect,
					new OpenCvSharp.CPlusPlus.Scalar(218, 174, 4, 255),
					3,
					OpenCvSharp.LineType.AntiAlias
				);

				this.SetImg(conv_img.ToMemoryStream());
			}
			else
			{
				this.SetImg(this.disp_img.ToMemoryStream());
			}

			this.CookieList(cur_data.idx, fossil_id);
		}
	}
}

﻿using System;
using System.Windows.Controls;
using System.Runtime.InteropServices;

namespace FossilUI
{
	public partial class MainWindow : System.Windows.Window
	{
		public enum eFOSSILE_TYPE { FOSSIL_TYPE_BLOB, FOSSIL_TYPE_LINE };
		public enum eFOSSILE_UPDATE_FLAG { FOSSIL_UPDATE_NONE, FOSSIL_UPDATE_UPDATE } ;

		public struct RECT
		{
			public int left;
			public int top;
			public int right;
			public int bottom;
		};

		public struct FOSSILDETECTInfo
		{
			public eFOSSILE_UPDATE_FLAG flag;		// Update flag

			public int fossil_id;
			public int image_index;
			public int class_no;
			public float class_distance;	// distance in class (low score is close)

			public RECT rc;
			public eFOSSILE_TYPE type;

			public float mean;			//normalize by global mean
			public float stddev;

			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 3)]
			public float[] rgb_mean;

			public float width;
			public float height;
			public float angle;
			public float area;

			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 7)]
			public double[] hu_moments;

            public int size_of_descriptor;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 1764)]
            public short[] descriptors;
		}

		[DllImport("FossilDetector.dll")]
		public static extern IntPtr SetupFossilDetector([MarshalAs(UnmanagedType.LPStr)]String szInputFile);
		[DllImport("FossilDetector.dll")]
		public static extern bool ProcessFossilDetector(IntPtr handle, int width, int height, int step, IntPtr image,
				ref int pMaxDetectCount);
		[DllImport("FossilDetector.dll")]
		public static extern bool GetFossilObject(IntPtr handle, [Out] FOSSILDETECTInfo[] info, ref int pDetectCount);
		[DllImport("FossilDetector.dll")]
		public static extern bool UpdateFossilObject(IntPtr handle, int nDetectCount, [Out] FOSSILDETECTInfo[] info);
		[DllImport("FossilDetector.dll")]
		public static extern bool ProcessUpdateFossilObject(IntPtr handle, int width, int height, int step, IntPtr image,
				int nDetectCount, [Out] FOSSILDETECTInfo[] info);
		[DllImport("FossilDetector.dll")]
		public static extern void EndupFossilDetector(IntPtr handle);

		/*[DllImport("FossilClassifier.dll")]
		public static extern IntPtr CreateClassifier();
		[DllImport("FossilClassifier.dll")]
		public static extern bool ProcessClassification2(IntPtr handle, int nTotalFossil, [In] FOSSILDETECTInfo[] in_info,
				[Out] FOSSILDETECTInfo[] out_info);
		[DllImport("FossilClassifier.dll")]
		public static extern void DestroyClassifier(IntPtr handle);*/
		[DllImport("FossilClassifier2.dll")]
		public static extern IntPtr CreateClassifier();
		[DllImport("FossilClassifier2.dll")]
		public static extern bool ProcessClassification2(IntPtr handle, int nTotalFossil, [In] FOSSILDETECTInfo[] in_info,
				[Out] FOSSILDETECTInfo[] out_info);
		[DllImport("FossilClassifier2.dll")]
		public static extern void DestroyClassifier(IntPtr handle);
	}
}

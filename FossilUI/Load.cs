﻿using System;
using System.IO;
using System.ComponentModel;
using System.Windows.Forms;
using System.Windows.Threading;
using System.Collections.Generic;
using OpenCvSharp;
using OpenCvSharp.CPlusPlus;

namespace FossilUI
{
	public partial class MainWindow : System.Windows.Window
	{
		private void LoadWindow(object sender, EventArgs e)
		{
			this.open_cookie = false;
			new LoadWindow();
		}

		private void LoadInit()
		{
			this.file_idx = 0;
			this.cookie_list.Clear();
			LIST_FILE_ITEM.Items.Clear();
			PANE_COOKIE_LIST.Children.Clear();
			IMG_CURRENT_FILE.Source = null;
			this.InitGroup();
		}

		public void LoadDir(LoadWindow load_win, bool sub_dir)
		{
			System.Windows.Forms.FolderBrowserDialog folder_diag = new System.Windows.Forms.FolderBrowserDialog();
			folder_diag.SelectedPath = Convert.ToString(CommonUtil.LoadJson("load_dir"));

			if (folder_diag.ShowDialog() != System.Windows.Forms.DialogResult.Cancel)
			{
				string[] sel_file = Directory.GetFiles(folder_diag.SelectedPath, "*.jpg", sub_dir == true ? SearchOption.AllDirectories : SearchOption.TopDirectoryOnly);

				if (sel_file.Length == 0)
					return;

				this.run_main = true;

				CommonUtil.SaveJson("load_dir", folder_diag.SelectedPath);
				this.SetTitle();
				this.StatusMessage(CommonUtil.LangData("load_window", "loading_progress"));
				load_win.Close();
				this.LoadInit();

				this.interrupt_code = 1;
				this.progress_win = new ProgressWindow(100, CommonUtil.LangData("load_window", "loading_progress"));
				this.opening_progress = true;

				BackgroundWorker load_worker = new BackgroundWorker();
				load_worker.DoWork += new DoWorkEventHandler(this._tLoadDir);
				load_worker.RunWorkerCompleted += (_sender, _e) => this.LoadComplete(_sender, _e, true);
				load_worker.RunWorkerAsync(sel_file);
			}
		}

		private void _tLoadDir(object sender, DoWorkEventArgs e)
		{
			string[] sel_file = (string[])e.Argument;

			foreach (string file_path in sel_file)
			{
				while (this.interrupt_code <= 0)
				{
					if (this.interrupt_code < 0)
					{
						e.Cancel = true;
						return;
					}

					System.Threading.Thread.Sleep(500);
				}

				FileData file_data = new FileData()
				{
					idx = this.file_idx,
					step = this.open_cookie == true ? ProcStep.analyze : ProcStep.load,
					name = Path.GetFileName(file_path),
					path = file_path,
					check = true,
					mat = new Mat(file_path, LoadMode.Color)
				};

				this.AddFile(this.file_idx, file_data);

				if (this.open_cookie == true)
				{
					FossilAPI.FOSSILDETECTInfoList fossil_list = new FossilAPI.FOSSILDETECTInfoList();
					fossil_list.image_index = this.file_idx;
					fossil_list.fossils = new List<FossilAPI.FOSSILDETECTInfo>();
					FossilAPI.FOSSILDETECTInfo[] fossil_data = new FossilAPI.FOSSILDETECTInfo[1];

					fossil_data[0].rc.left = fossil_data[0].rc.top = 0;
					fossil_data[0].rc.right = file_data.mat.Width;
					fossil_data[0].rc.bottom = file_data.mat.Height;
					fossil_data[0].image_index = this.file_idx;
					fossil_data[0].fossil_id = this.file_idx;
					fossil_data[0].specify_class = MainWindow.UNDEFINED_GROUP;
					fossil_data[0].cate_class = MainWindow.UNDEFINED_GROUP;
					fossil_data[0].class_no = MainWindow.UNDEFINED_GROUP;
					fossil_data[0].top_idx = new int[5];
					fossil_data[0].top_score = new float[5];

					fossil_list.fossils.Add(fossil_data[0]);
					this.cookie_list.Add(fossil_list);
				}

				this.file_idx++;

				this.progress_win.PROGRESS_PER.Dispatcher.Invoke(
					DispatcherPriority.Render,
					new Action(
						() => this.ProgressPer(LIST_FILE_ITEM.Items.Count * 100 / sel_file.Length)
					)
				);
			}
		}

		public void LoadFile(LoadWindow load_win)
		{
			System.Windows.Forms.OpenFileDialog open_diag = new System.Windows.Forms.OpenFileDialog();
			open_diag.InitialDirectory = CommonUtil.LoadJson("load_dir");
			open_diag.Multiselect = true;
			open_diag.Filter = "jpg (*.jpg)|*.jpg";
			open_diag.FilterIndex = 1;

			if (open_diag.ShowDialog() != System.Windows.Forms.DialogResult.Cancel)
			{
				string[] sel_file = open_diag.FileNames;

				if (sel_file.Length == 0)
					return;

				this.run_main = true;

				CommonUtil.SaveJson("load_dir", open_diag.InitialDirectory);
				this.SetTitle();
				this.StatusMessage(CommonUtil.LangData("load_window", "loading_progress"));
				load_win.Close();

				if (LIST_FILE_ITEM.Items.Count == 0 || this.open_cookie == true)
					this.LoadInit();

				this.interrupt_code = 1;
				this.progress_win = new ProgressWindow(100, CommonUtil.LangData("load_window", "loading_progress"));
				this.opening_progress = true;

				BackgroundWorker load_worker = new BackgroundWorker();
				load_worker.DoWork += new DoWorkEventHandler(this._tLoadFile);
				load_worker.RunWorkerCompleted += (_sender, _e) => this.LoadComplete(_sender, _e, false);
				load_worker.RunWorkerAsync(sel_file);
			}
		}

		private void _tLoadFile(object sender, DoWorkEventArgs e)
		{
			string[] sel_file = (string[])e.Argument;
			int file_cnt = 0;

			foreach (string file_path in sel_file)
			{
				while (this.interrupt_code <= 0)
				{
					if (this.interrupt_code < 0)
					{
						e.Cancel = true;
						return;
					}

					System.Threading.Thread.Sleep(500);
				}

				if (this.CheckPath(file_path))
				{
					FileData file_data = new FileData()
					{
						idx = this.file_idx,
						step = this.open_cookie == true ? ProcStep.analyze : ProcStep.load,
						name = Path.GetFileName(file_path),
						path = file_path,
						check = true,
						mat = new Mat(file_path, LoadMode.Color)
					};

					this.AddFile(this.file_idx, file_data);

					if (this.open_cookie == true)
					{
						FossilAPI.FOSSILDETECTInfoList fossil_list = new FossilAPI.FOSSILDETECTInfoList();
						fossil_list.image_index = this.file_idx;
						fossil_list.fossils = new List<FossilAPI.FOSSILDETECTInfo>();
						FossilAPI.FOSSILDETECTInfo[] fossil_data = new FossilAPI.FOSSILDETECTInfo[1];

						fossil_data[0].rc.left = fossil_data[0].rc.top = 0;
						fossil_data[0].rc.right = file_data.mat.Width;
						fossil_data[0].rc.bottom = file_data.mat.Height;
						fossil_data[0].image_index = this.file_idx;
						fossil_data[0].fossil_id = this.file_idx;
						fossil_data[0].specify_class = MainWindow.UNDEFINED_GROUP;
						fossil_data[0].cate_class = MainWindow.UNDEFINED_GROUP;
						fossil_data[0].class_no = MainWindow.UNDEFINED_GROUP;
						fossil_data[0].top_idx = new int[5];
						fossil_data[0].top_score = new float[5];

						fossil_list.fossils.Add(fossil_data[0]);
						this.cookie_list.Add(fossil_list);
					}

					this.file_idx++;
				}

				this.progress_win.PROGRESS_PER.Dispatcher.Invoke(
					DispatcherPriority.Render,
					new Action(
						() => this.ProgressPer(file_cnt * 100 / sel_file.Length)
					)
				);

				file_cnt++;
			}
		}

		private void LoadComplete(object sender, RunWorkerCompletedEventArgs e, bool init_item)
		{
			this.Activate();
			this.Topmost = true;
			this.Topmost = false;
			this.Focus();

			if (init_item == true && LIST_FILE_ITEM.Items.Count > 0)
			{
				LIST_FILE_ITEM.SelectedIndex = 0;
				LIST_FILE_ITEM.ScrollIntoView(LIST_FILE_ITEM.Items[0]);
			}

			this.progress_win.PROGRESS_PER.Dispatcher.Invoke(
				DispatcherPriority.Render,
				new Action(
					() => this.progress_win.Close()
				)
			);

			//this.progress_win.Close();
			this.opening_progress = false;
			this.StatusMessage(CommonUtil.LangData("main_window", "complete"));
			this.ButtonStatus();
		}
	}
}

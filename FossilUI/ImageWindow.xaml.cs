﻿using System;
using System.Linq;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Collections.Generic;
using System.Windows.Media.Effects;

using System.Runtime.ExceptionServices;
using System.Runtime.InteropServices;
using log4net;

namespace FossilUI
{
	/// <summary>
	/// Interaction logic for ImageWindow.xaml
	/// </summary>
	public partial class ImageWindow : Window
	{
		private MainWindow main_win = null;
		private FileData file_data = null;
		private int img_idx = 0;
		private Border focus_border = null;
		private Border cur_border = null;
		private Border last_border = null;
		private string cur_cursor = "";
		private double cursor_x = 0;
		private double cursor_y = 0;
		private double control_width = 0;
		private double control_height = 0;
		private bool show_tooltip = false;
		private bool is_changed = false;
		private Point? last_point;

		public ImageWindow(int img_idx, int x_centor, int y_centor)
		{
			InitializeComponent();

			this.Title = CommonUtil.LangData("image_window", "title");
			LABEL_WINDOW_TITLE.Content = CommonUtil.LangData("image_window", "title");

			this.show_tooltip = Convert.ToBoolean(CommonUtil.LoadJson("recognize_tooltip"));

			this.main_win = (MainWindow)Application.Current.MainWindow;
			this.img_idx = img_idx;
			this.file_data = this.main_win.GetFile(this.img_idx);

			((Image)BTN_RECOGNIZE_TOOLTIP.Content).Source =
				(ImageSource)new ImageSourceConverter().ConvertFromString(this.show_tooltip == true ? "pack://application:,,,/resources/recognize.tooltip.enable.png" : "pack://application:,,,/resources/recognize.tooltip.disable.png");

			IMG_FILE_SOURCE.Source = CommonUtil.MemImg(this.file_data.mat.ToMemoryStream());

			foreach (FossilAPI.FOSSILDETECTInfoList item_list in this.main_win.cookie_list)
			{
				foreach (FossilAPI.FOSSILDETECTInfo item_obj in item_list.fossils)
				{
					if (item_obj.image_index == this.img_idx)
						this.AddBorder(item_obj);
				}
			}

			this.PreviewKeyDown += new KeyEventHandler(this.SizingMove);
			this.KeyUp += new KeyEventHandler(this.RemoveCookie);
			this.KeyDown += new KeyEventHandler(this.TabCookie);

			IMG_FILE_SOURCE.MouseLeftButtonDown += new MouseButtonEventHandler(this.OnMouseDown);
			IMG_FILE_SOURCE.MouseLeftButtonUp += new MouseButtonEventHandler(this.OnMouseUp);
			IMG_FILE_SOURCE.MouseMove += new MouseEventHandler(this.OnMouseMove);

			ContextMenu img_context = new ContextMenu();
			MenuItem new_cookie = new MenuItem();
			new_cookie.Icon = new Image
			{
				Source = (ImageSource)new ImageSourceConverter().ConvertFromString(
						"pack://application:,,,/resources/context.new.cookie.png")
			};

			new_cookie.Header = CommonUtil.LangData("image_window", "new");
			new_cookie.Click += new RoutedEventHandler(this.NewCookie);
			img_context.Items.Add(new_cookie);

			MenuItem remove_all = new MenuItem();
			remove_all.Icon = new Image
			{
				Source = (ImageSource)new ImageSourceConverter().ConvertFromString(
						"pack://application:,,,/resources/remove.cookie.png")
			};
			remove_all.Header = CommonUtil.LangData("image_window", "remove_all");
			remove_all.Click += new RoutedEventHandler(this.RemoveAll);
			img_context.Items.Add(remove_all);

			IMG_FILE_SOURCE.ContextMenu = img_context;
			this.KeyUp += new KeyEventHandler(this.MainKey);

			SCROLL_IMAGE_VIEWER.ScrollToHorizontalOffset(x_centor - 400);
			SCROLL_IMAGE_VIEWER.ScrollToVerticalOffset(y_centor - 300);

			try
			{
				this.ShowDialog();
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.ToString());
			}
		}

		private void SizingMove(object sender, KeyEventArgs e)
		{
			if (e.Key != Key.Left && e.Key != Key.Up && e.Key != Key.Right && e.Key != Key.Down)
				return;

			if (this.cur_border == null)
				return;

			if (e.KeyboardDevice.Modifiers == ModifierKeys.Shift)
			{
				if (e.Key == Key.Left)
				{
					if (this.cur_border.Margin.Left >= 0 - IMG_FILE_SOURCE.ActualWidth && this.cur_border.ActualWidth - 1 >= 50)
					{
						this.cur_border.Margin = new Thickness(this.cur_border.Margin.Left - 1, this.cur_border.Margin.Top, 0, 0);
						this.cur_border.Width += 1;
					}
				}
				else if (e.Key == Key.Right)
				{
					if (this.cur_border.Margin.Left + this.cur_border.ActualWidth <= 0 && this.cur_border.ActualWidth + 1 >= 50)
						this.cur_border.Width += 1;
				}
				else if (e.Key == Key.Up)
				{
					if (this.cur_border.Margin.Top >= 0 && this.cur_border.ActualHeight + 1 >= 50)
					{
						this.cur_border.Margin = new Thickness(this.cur_border.Margin.Left, this.cur_border.Margin.Top - 1, 0, 0);
						this.cur_border.Height += 1;
					}
				}
				else if (e.Key == Key.Down)
				{
					if (this.cur_border.Margin.Top + this.cur_border.ActualHeight <= IMG_FILE_SOURCE.ActualHeight && this.cur_border.ActualHeight + 1 >= 50)
						this.cur_border.Height += 1;
				}
			}
			else if (e.KeyboardDevice.Modifiers == ModifierKeys.Control)
			{
				if (e.Key == Key.Left)
				{
					if (this.cur_border.Margin.Left + this.cur_border.ActualWidth <= 0 && this.cur_border.ActualWidth - 1 >= 50)
						this.cur_border.Width -= 1;
				}
				else if (e.Key == Key.Right)
				{
					if (this.cur_border.Margin.Left >= 0 - IMG_FILE_SOURCE.ActualWidth && this.cur_border.ActualWidth - 1 >= 50)
					{
						this.cur_border.Margin = new Thickness(this.cur_border.Margin.Left + 1, this.cur_border.Margin.Top, 0, 0);
						this.cur_border.Width -= 1;
					}
				}
				else if (e.Key == Key.Up)
				{
					if (this.cur_border.Margin.Top + this.cur_border.ActualHeight <= IMG_FILE_SOURCE.ActualHeight && this.cur_border.ActualHeight - 1 >= 50)
						this.cur_border.Height -= 1;
				}
				else if (e.Key == Key.Down)
				{
					if (this.cur_border.Margin.Top >= 0 && this.cur_border.ActualHeight - 1 >= 50)
					{
						this.cur_border.Margin = new Thickness(this.cur_border.Margin.Left, this.cur_border.Margin.Top + 1, 0, 0);
						this.cur_border.Height -= 1;
					}
				}
			}
			else
			{
				if (e.Key == Key.Left)
					this.cur_border.Margin = new Thickness((this.cur_border.Margin.Left - 1 <= 0 - IMG_FILE_SOURCE.ActualWidth ? 0 - IMG_FILE_SOURCE.ActualWidth : this.cur_border.Margin.Left - 1), this.cur_border.Margin.Top, 0, 0);
				else if (e.Key == Key.Up)
					this.cur_border.Margin = new Thickness(this.cur_border.Margin.Left, (this.cur_border.Margin.Top - 1 <= 0 ? 0 : this.cur_border.Margin.Top - 1), 0, 0);
				else if (e.Key == Key.Right)
					this.cur_border.Margin = new Thickness((this.cur_border.Margin.Left + 1 > 0 - this.cur_border.Width ? 0 - this.cur_border.Width : this.cur_border.Margin.Left + 1), this.cur_border.Margin.Top, 0, 0);
				else if (e.Key == Key.Down)
					this.cur_border.Margin = new Thickness(this.cur_border.Margin.Left, (this.cur_border.Margin.Top + 1 > IMG_FILE_SOURCE.ActualHeight - this.cur_border.ActualHeight ? IMG_FILE_SOURCE.ActualHeight - this.cur_border.ActualHeight : this.cur_border.Margin.Top + 1), 0, 0);
			}

			this.is_changed = true;
		}

		private void MainKey(object sender, KeyEventArgs e)
		{
			if (e.Key == Key.Escape)
			{
				e.Handled = true;
				this.Close();
			}
		}

		private void OnMouseDown(object sender, MouseButtonEventArgs e)
		{
			IMG_FILE_SOURCE.Cursor = Cursors.Hand;
			var mouse = e.GetPosition(SCROLL_IMAGE_VIEWER);
			mouse.Y += 40;
			this.last_point = mouse;

			Mouse.Capture(IMG_FILE_SOURCE);

			if (this.cur_border != null)
			{
				this.cur_border.BorderBrush = Brushes.Red;
				((StackPanel)this.cur_border.Child).Opacity = 0;
				Panel.SetZIndex(this.cur_border, 0);
				this.cur_border = null;
			}
		}

		private void OnMouseUp(object sender, MouseButtonEventArgs e)
		{
			IMG_FILE_SOURCE.Cursor = Cursors.Arrow;
			IMG_FILE_SOURCE.ReleaseMouseCapture();

			this.last_point = null;
		}

		private void OnMouseMove(object sender, MouseEventArgs e)
		{
			if (this.cur_border == null)
			{
				if (this.last_point.HasValue)
				{
					Point posNowByScroll = e.GetPosition(SCROLL_IMAGE_VIEWER);

					if (posNowByScroll == this.last_point)
						return;

					posNowByScroll.Y += 40;

					double dX = posNowByScroll.X - this.last_point.Value.X;
					double dY = posNowByScroll.Y - this.last_point.Value.Y;

					this.last_point = posNowByScroll;

					SCROLL_IMAGE_VIEWER.ScrollToHorizontalOffset(SCROLL_IMAGE_VIEWER.HorizontalOffset - dX);
					SCROLL_IMAGE_VIEWER.ScrollToVerticalOffset(SCROLL_IMAGE_VIEWER.VerticalOffset - dY);
				}
			}
			else
			{
				this.cur_border.BorderBrush = Brushes.Cyan;

				if (e.LeftButton == MouseButtonState.Pressed)
					this.SizingApply(this.cur_border, e);
			}
		}

		private void AddBorder(FossilAPI.FOSSILDETECTInfo fossil_val)
		{
			Border fossil_border = new Border
			{
				BorderBrush = Brushes.Red,
				BorderThickness = new Thickness(5),
				Background = Brushes.Transparent,
				HorizontalAlignment = System.Windows.HorizontalAlignment.Left,
				VerticalAlignment = System.Windows.VerticalAlignment.Top,
				Width = fossil_val.rc.right - fossil_val.rc.left,
				Height = fossil_val.rc.bottom - fossil_val.rc.top,
				Margin = new Thickness(fossil_val.rc.left - this.file_data.mat.Width, fossil_val.rc.top, 0, 0),
				Tag = fossil_val
			};

			StackPanel fossil_pane = new StackPanel
			{
				HorizontalAlignment = HorizontalAlignment.Stretch,
				VerticalAlignment = VerticalAlignment.Stretch,
				Background = Brushes.White,
				Opacity = 0
			};

			Image fossil_remove = new Image
			{
				Width = 15,
				Height = 15,
				HorizontalAlignment = System.Windows.HorizontalAlignment.Right,
				Cursor = Cursors.Hand,
				Source = (ImageSource)new ImageSourceConverter().ConvertFromString(
						"pack://application:,,,/resources/remove.cookie.png")
			};

			fossil_pane.Children.Add(fossil_remove);
			fossil_border.Child = fossil_pane;

			fossil_remove.MouseEnter += new MouseEventHandler(this.RemoveOver);
			fossil_remove.MouseLeave += new MouseEventHandler(this.RemoveOut);
			fossil_remove.MouseUp += new MouseButtonEventHandler(this.RemoveCookie);
			fossil_border.MouseEnter += new MouseEventHandler(this.SizingOver);
			fossil_border.MouseMove += new MouseEventHandler(this.SizingMove);
			fossil_border.MouseLeave += new MouseEventHandler(this.SizingOut);
			fossil_border.MouseDown += new MouseButtonEventHandler(this.SizingStart);
			fossil_border.MouseLeftButtonUp += new MouseButtonEventHandler(this.RecognizeArea);

			PANE_FILE_SOURCE.Children.Add(fossil_border);
		}

		private void CloseTooltip()
		{
			if (this.last_border != null)
			{
				ToolTip cookie_tooltip = (ToolTip)this.last_border.ToolTip;

				if (cookie_tooltip != null)
				{
					cookie_tooltip.IsOpen = false;
					cookie_tooltip.ToolTip = null;
					this.last_border.ToolTip = null;
				}
			}
		}

		private void RecognizeArea(object sender, MouseButtonEventArgs e)
		{
			Border cur_border = (Border)sender;
			if (cur_border == null) return;

			this.CloseTooltip();

			this.last_border = cur_border;

			string caffe_proto = Convert.ToString(CommonUtil.LoadJson("caffe_proto"));
			string caffe_model = Convert.ToString(CommonUtil.LoadJson("caffe_model"));
			string caffe_mean = Convert.ToString(CommonUtil.LoadJson("caffe_mean"));
			int data_size = Convert.ToInt32(CommonUtil.LoadJson("data_size"));
			int batch_size = 0;
			FossilAPI.RECT img_rect;
			int[] top_idx = new int[1];
			float[] top_score = new float[1];

			img_rect.left =
					(Convert.ToInt32(cur_border.Margin.Left) + this.file_data.mat.Width >= 0) ?
							Convert.ToInt32(cur_border.Margin.Left) + this.file_data.mat.Width : 0;
			img_rect.top =
					(Convert.ToInt32(cur_border.Margin.Top) >= 0) ?
							Convert.ToInt32(cur_border.Margin.Top) : 0;
			img_rect.right =
					(img_rect.left + Convert.ToInt32(cur_border.ActualWidth) <= this.file_data.mat.Width) ?
							img_rect.left + Convert.ToInt32(cur_border.ActualWidth) :
							this.file_data.mat.Width;
			img_rect.bottom =
					(img_rect.top + Convert.ToInt32(cur_border.ActualHeight) <= this.file_data.mat.Height) ?
							img_rect.top + Convert.ToInt32(cur_border.ActualHeight) :
							this.file_data.mat.Height;

			OpenCvSharp.CPlusPlus.Mat item_mat = this.file_data.mat.SubMat(
				img_rect.top,
				img_rect.bottom,
				img_rect.left,
				img_rect.right
			);

			if (this.show_tooltip == true)
			{
				OpenCvSharp.CPlusPlus.Mat batch_mat = item_mat.Resize(new OpenCvSharp.CPlusPlus.Size(data_size, data_size));

				IntPtr caffe_handle = FossilAPI.SetupCaffeClassification(
					FossilAPI.ePROCESS_TYPE.PROCESS_CPU,
					caffe_proto,
					caffe_model,
					caffe_mean,
					IntPtr.Zero,
					ref batch_size
				);

				int recognizer_res = FossilAPI.ProcessCaffeClassificationMegredImages(
					caffe_handle,
					data_size,
					1,
					batch_mat.Data,
					top_idx,
					top_score
				);

				FossilAPI.EndupCaffeClassification(caffe_handle);

				foreach (GroupData group_data in this.main_win.group_list)
				{
					if (top_idx[0] != group_data.idx)
						continue;

					StackPanel tooltip_pane = new StackPanel
					{
						Orientation = System.Windows.Controls.Orientation.Vertical,
						Background = Brushes.Black
					};

					StackPanel score_pane = new StackPanel
					{
						Orientation = System.Windows.Controls.Orientation.Horizontal
					};

					score_pane.Children.Add(
						new Label
						{
							Content = CommonUtil.LangData("image_window", "score") + " : ",
							Foreground = Brushes.LightGray
						}
					);

					score_pane.Children.Add(
						new Label
						{
							Content = top_score[0].ToString(),
							Foreground = Brushes.Yellow
						}
					);

					tooltip_pane.Children.Add(score_pane);

					StackPanel classify_pane = new StackPanel
					{
						Orientation = System.Windows.Controls.Orientation.Horizontal,
						Margin = new Thickness(0, -10, 0, 0)
					};

					classify_pane.Children.Add(
						new Label
						{
							Content = CommonUtil.LangData("image_window", "expected_class") + " '",
							Foreground = Brushes.LightGray
						}
					);

					classify_pane.Children.Add(
						new Label
						{
							Content = top_score[0] >= group_data.rmin ? (group_data.idx + this.main_win.start_order).ToString() + ". " + group_data.name : CommonUtil.LangData("image_window", "unidentified_group"),
							Foreground = Brushes.Lime
						}
					);

					classify_pane.Children.Add(
						new Label
						{
							Content = "'.",
							Foreground = Brushes.LightGray
						}
					);

					tooltip_pane.Children.Add(classify_pane);

					ToolTip recognize_tooltip = new ToolTip
					{
						Content = tooltip_pane,
						StaysOpen = true,
						IsOpen = true,
						BorderThickness = new Thickness(0)
					};

					cur_border.ToolTip = recognize_tooltip;

					return;
				}
			}

			this.cur_border = null;
		}

		private void NewCookie(object sender, EventArgs e)
		{
			FossilAPI.FOSSILDETECTInfo new_cookie = new FossilAPI.FOSSILDETECTInfo();
			new_cookie.fossil_id = MainWindow.UNDEFINED_FOSSIL;
			new_cookie.image_index = this.img_idx;
			new_cookie.class_no = MainWindow.UNDEFINED_GROUP;
			new_cookie.update_flag = FossilAPI.eFOSSILE_UPDATE_FLAG.FOSSIL_UPDATE_UPDATE;
			new_cookie.rc.left =
					(Convert.ToInt32(Mouse.GetPosition(IMG_FILE_SOURCE).X) >= this.file_data.mat.Width - 50) ?
							this.file_data.mat.Width - 50 :
							Convert.ToInt32(Mouse.GetPosition(IMG_FILE_SOURCE).X);
			new_cookie.rc.top =
					(Convert.ToInt32(Mouse.GetPosition(IMG_FILE_SOURCE).Y) >= this.file_data.mat.Height - 50) ?
							this.file_data.mat.Height - 50 :
							Convert.ToInt32(Mouse.GetPosition(IMG_FILE_SOURCE).Y);
			new_cookie.rc.right = new_cookie.rc.left + 100;
			new_cookie.rc.bottom = new_cookie.rc.top + 100;

			this.AddBorder(new_cookie);

			this.is_changed = true;
		}

		private void RemoveAll(object sender, EventArgs e)
		{
			if (MessageBox.Show(CommonUtil.LangData("image_window", "confirm_remove"), CommonUtil.LangData("common", "confirm"),
					MessageBoxButton.YesNo, MessageBoxImage.Question) != MessageBoxResult.Yes)
				return;

			for (int i = PANE_FILE_SOURCE.Children.Count - 1; i >= 0; i--)
			{
				object node_child = PANE_FILE_SOURCE.Children[i];

				if (!(node_child is Border))
					continue;

				Border cookie_border = (Border)node_child;
				PANE_FILE_SOURCE.Children.RemoveAt(i);
				this.is_changed = true;
			}

			this.focus_border = null;
		}

		private void RemoveOver(object sender, MouseEventArgs e)
		{
			Image cur_remove = (Image)sender;
			if (cur_remove == null) return;
			cur_remove.Source = (ImageSource)new ImageSourceConverter().ConvertFromString(
					"pack://application:,,,/resources/remove.cookie.over.png");
		}

		private void RemoveOut(object sender, MouseEventArgs e)
		{
			Image cur_remove = (Image)sender;
			if (cur_remove == null) return;
			cur_remove.Source = (ImageSource)new ImageSourceConverter().ConvertFromString(
					"pack://application:,,,/resources/remove.cookie.png");
		}

		private void TabCookie(object sender, KeyEventArgs e)
		{
			if (e.Key == Key.Tab)
			{
				bool app_border = false;
				Border cookie_border = null;
				Border first_border = null;
				Border cur_border = null;

				foreach (object node_child in PANE_FILE_SOURCE.Children)
				{
					if (!(node_child is Border))
						continue;

					cookie_border = (Border)node_child;

					if (first_border == null)
						first_border = cookie_border;

					FossilAPI.FOSSILDETECTInfo fossil_info = (FossilAPI.FOSSILDETECTInfo)cookie_border.Tag;

					if (this.cur_border == null)
					{
						this.FocusBorder(cookie_border);
						return;
					}

					if (this.cur_border == cookie_border)
					{
						app_border = true;
						continue;
					}

					if (app_border == true)
					{
						cur_border = cookie_border;
						this.FocusBorder(cur_border);
						break;
					}
				}

				if (app_border == true && cur_border == null)
				{
					this.FocusBorder(first_border);
				}
			}
		}

		private void RemoveCookie(object sender, KeyEventArgs e)
		{
			if (this.focus_border == null) return;

			if (e.Key == Key.Delete || e.Key == Key.D)
			{
				e.Handled = true;
				this.RemoveCookie(this.focus_border);
			}
		}

		private void RemoveCookie(object sender, RoutedEventArgs e)
		{
			e.Handled = true;
			MenuItem cur_item = (MenuItem)sender;
			if (cur_item == null) return;
			Border cur_border = (Border)((ContextMenu)cur_item.Parent).PlacementTarget;
			if (cur_border == null) return;
			this.RemoveCookie(cur_border);
		}

		private void RemoveCookie(object sender, MouseButtonEventArgs e)
		{
			e.Handled = true;
			Image cur_remove = (Image)sender;
			if (cur_remove == null) return;
			Border cur_border = (Border)((StackPanel)cur_remove.Parent).Parent;
			if (cur_border == null) return;
			this.RemoveCookie(cur_border);
		}

		private void RemoveCookie(Border cur_border)
		{
			this.CloseTooltip();

			try
			{
				((DockPanel)cur_border.Parent).Children.Remove(cur_border);
			}
			catch (Exception ex)
			{
				Console.WriteLine(ex.ToString());
			}
			finally
			{
				this.is_changed = true;
			}
		}

		private void FocusBorder(Border focus_border)
		{
			if (this.cur_border != null)
			{
				this.cur_border.BorderBrush = Brushes.Red;
				((StackPanel)this.cur_border.Child).Opacity = 0;
				Panel.SetZIndex(this.cur_border, 0);
			}

			this.cur_border = focus_border;
			if (this.cur_border == null) return;
			this.cur_border.BorderBrush = Brushes.Cyan;
			((StackPanel)this.cur_border.Child).Opacity = 0.2;
			Panel.SetZIndex(this.cur_border, 1);
		}

		private void SizingStart(object sender, MouseButtonEventArgs e)
		{
			this.CloseTooltip();

			Border focus_border = (Border)sender;
			if (focus_border == null) return;

			this.FocusBorder(focus_border);

			this.cursor_x = e.GetPosition(this.cur_border).X;
			this.cursor_y = e.GetPosition(this.cur_border).Y;
			this.control_width = this.cur_border.ActualWidth;
			this.control_height = this.cur_border.ActualHeight;

			if (e.RightButton == MouseButtonState.Pressed && this.cur_cursor == "all.all")
			{
				ContextMenu cookie_context = new ContextMenu();
				MenuItem remove_cookie = new MenuItem();
				remove_cookie.Icon = new Image
				{
					Source = (ImageSource)new ImageSourceConverter().ConvertFromString(
							"pack://application:,,,/resources/context.remove.cookie.png")
				};
				remove_cookie.Header = CommonUtil.LangData("image_window", "remove");
				remove_cookie.Click += new RoutedEventHandler(this.RemoveCookie);
				cookie_context.Items.Add(remove_cookie);
				this.cur_border.ContextMenu = cookie_context;
				((StackPanel)this.cur_border.Child).Opacity = 0;
				Panel.SetZIndex(this.cur_border, 0);
				this.cur_border = null;
			}

			if (this.cur_border != null)
			{
				FossilAPI.FOSSILDETECTInfo fossil_info = (FossilAPI.FOSSILDETECTInfo)this.cur_border.Tag;
				fossil_info.update_flag = FossilAPI.eFOSSILE_UPDATE_FLAG.FOSSIL_UPDATE_UPDATE;
				this.cur_border.Tag = fossil_info;
			}
		}

		private void SizingOver(object sender, MouseEventArgs e)
		{
			this.focus_border = (Border)sender;

			if (e.LeftButton == MouseButtonState.Pressed)
			{
			}
			else
			{
				Border cur_border = (Border)sender;
				if (cur_border == null) return;
				cur_border.BorderBrush = Brushes.Cyan;

				this.MouseArrow((Border)sender, e);
			}
		}

		private void SizingMove(object sender, MouseEventArgs e)
		{
			if (e.LeftButton == MouseButtonState.Pressed)
				this.SizingApply(this.cur_border, e);
			else
				this.MouseArrow((Border)sender, e);
		}

		private void SizingOut(object sender, MouseEventArgs e)
		{
			this.focus_border = null;

			if (e.LeftButton == MouseButtonState.Pressed)
			{
			}
			else
			{
				Border cur_border = (Border)sender;
				if (cur_border == null) return;
				cur_border.BorderBrush = Brushes.Red;
			}
		}

		private void SizingApply(Border cur_border, MouseEventArgs e)
		{
			if (cur_border == null) return;

			double pos_diff = 0;

			if (this.cur_cursor == "")
				return;

			if (this.cur_cursor.Substring(0, 3) == "all")
			{
				if (this.cursor_x > e.GetPosition(cur_border).X)
				{
					pos_diff = this.cursor_x - e.GetPosition(cur_border).X;

					if (cur_border.Margin.Left - pos_diff >= 0 - IMG_FILE_SOURCE.ActualWidth + 0)
						cur_border.Margin = new Thickness(cur_border.Margin.Left - pos_diff, cur_border.Margin.Top, 0, 0);
				}
				else
				{
					pos_diff = e.GetPosition(cur_border).X - this.cursor_x;

					if (cur_border.Margin.Left + cur_border.ActualWidth + pos_diff <= 0)
						cur_border.Margin = new Thickness(cur_border.Margin.Left + pos_diff, cur_border.Margin.Top, 0, 0);
				}

				if (this.cursor_y > e.GetPosition(cur_border).Y)
				{
					pos_diff = this.cursor_y - e.GetPosition(cur_border).Y;

					if (cur_border.Margin.Top - pos_diff >= 0)
						cur_border.Margin = new Thickness(cur_border.Margin.Left, cur_border.Margin.Top - pos_diff, 0, 0);
				}
				else
				{
					pos_diff = e.GetPosition(cur_border).Y - this.cursor_y;

					if (cur_border.Margin.Top + cur_border.ActualHeight + pos_diff <= IMG_FILE_SOURCE.ActualHeight)
						cur_border.Margin = new Thickness(cur_border.Margin.Left, cur_border.Margin.Top + pos_diff, 0, 0);
				}
			}
			else
			{
				if (this.cur_cursor.Substring(0, 4) == "left")
				{
					pos_diff = this.cursor_x - e.GetPosition(cur_border).X;

					if (cur_border.Margin.Left >= 0 - IMG_FILE_SOURCE.ActualWidth && cur_border.Width + pos_diff >= 50)
					{
						cur_border.Margin = new Thickness(cur_border.Margin.Left - pos_diff, cur_border.Margin.Top, 0, 0);
						cur_border.Width += pos_diff;
					}
				}
				else if (this.cur_cursor.Substring(0, 5) == "right")
				{
					pos_diff = e.GetPosition(cur_border).X - this.cursor_x;

					if (cur_border.Margin.Left + cur_border.ActualWidth <= 0 && this.control_width + pos_diff >= 50)
						cur_border.Width = this.control_width + pos_diff;
				}

				if (this.cur_cursor.Substring(this.cur_cursor.Length - 3) == "top")
				{
					pos_diff = this.cursor_y - e.GetPosition(cur_border).Y;

					if (cur_border.Margin.Top >= 0 && cur_border.Height + pos_diff >= 50)
					{
						cur_border.Margin = new Thickness(cur_border.Margin.Left, cur_border.Margin.Top - pos_diff, 0, 0);
						cur_border.Height += pos_diff;
					}
				}
				else if (this.cur_cursor.Substring(this.cur_cursor.Length - 6) == "bottom")
				{
					pos_diff = e.GetPosition(cur_border).Y - this.cursor_y;

					if (cur_border.Margin.Top + cur_border.ActualHeight <= IMG_FILE_SOURCE.ActualHeight &&
							this.control_height + pos_diff >= 50)
						cur_border.Height = this.control_height + pos_diff;
				}
			}

			this.is_changed = true;
		}

		private void MouseArrow(Border cur_border, MouseEventArgs e)
		{
			if (cur_border == null) return;

			int cursor_padding = 10;

			if (e.GetPosition(cur_border).X < cursor_padding && e.GetPosition(cur_border).Y < cursor_padding)
			{
				cur_border.Cursor = Cursors.SizeNWSE;
				this.cur_cursor = "left.top";
			}
			else if (cur_border.ActualWidth - e.GetPosition(cur_border).X < cursor_padding &&
					e.GetPosition(cur_border).Y < cursor_padding)
			{
				cur_border.Cursor = Cursors.SizeNESW;
				this.cur_cursor = "right.top";
			}
			else if (e.GetPosition(cur_border).X < cursor_padding &&
					cur_border.ActualHeight - e.GetPosition(cur_border).Y < cursor_padding)
			{
				cur_border.Cursor = Cursors.SizeNESW;
				this.cur_cursor = "left.bottom";
			}
			else if (cur_border.ActualWidth - e.GetPosition(cur_border).X < cursor_padding &&
					cur_border.ActualHeight - e.GetPosition(cur_border).Y < cursor_padding)
			{
				cur_border.Cursor = Cursors.SizeNWSE;
				this.cur_cursor = "right.bottom";
			}
			else if (e.GetPosition(cur_border).X < cursor_padding)
			{
				cur_border.Cursor = Cursors.SizeWE;
				this.cur_cursor = "left.left";
			}
			else if (e.GetPosition(cur_border).Y < cursor_padding)
			{
				cur_border.Cursor = Cursors.SizeNS;
				this.cur_cursor = "top.top";
			}
			else if (cur_border.ActualWidth - e.GetPosition(cur_border).X < cursor_padding)
			{
				cur_border.Cursor = Cursors.SizeWE;
				this.cur_cursor = "right.right";
			}
			else if (cur_border.ActualHeight - e.GetPosition(cur_border).Y < cursor_padding)
			{
				cur_border.Cursor = Cursors.SizeNS;
				this.cur_cursor = "bottom.bottom";
			}
			else
			{
				cur_border.Cursor = Cursors.SizeAll;
				this.cur_cursor = "all.all";
			}
		}

		private void MoveWindow(object sender, MouseButtonEventArgs e)
		{
			if (CommonUtil.GetFocus() == false)
			{
				if (e.ChangedButton == MouseButton.Left)
					this.DragMove();
			}
		}

		private void CloseWindow(object sender, RoutedEventArgs e)
		{
			e.Handled = true;
			this.Close();
		}

		[HandleProcessCorruptedStateExceptions]
		//private void CloseWindow(object sender, RoutedEventArgs e)
		private void ClosingWindow(object sender, System.ComponentModel.CancelEventArgs e)
		{
			//e.Handled = true;

			this.CloseTooltip();

			//Check image list change.

			if (!this.is_changed)
			{
				main_win.SelFile();
				e.Cancel = false;
				return;
			}

			MessageBoxResult message_result = MessageBox.Show(CommonUtil.LangData("image_window", "save_changed"), CommonUtil.LangData("common", "confirm"),
					MessageBoxButton.YesNoCancel, MessageBoxImage.Question);

			if (message_result == MessageBoxResult.Cancel)
			{
				e.Cancel = true;
				return;
			}

			if (message_result != MessageBoxResult.Yes)
			{
				main_win.SelFile();
				e.Cancel = false;
				return;
			}

			//this.main_win.load_step = 2;

			FossilAPI.FOSSILDETECTInfo[] pTotalFossilInfos = new FossilAPI.FOSSILDETECTInfo[PANE_FILE_SOURCE.Children.Count];
			int totalFossil = 0;

			foreach (object node_child in PANE_FILE_SOURCE.Children)
			{
				if (!(node_child is Border))
					continue;

				Border cookie_border = (Border)node_child;
				pTotalFossilInfos[totalFossil] = (FossilAPI.FOSSILDETECTInfo)cookie_border.Tag;

				if (pTotalFossilInfos[totalFossil].update_flag == FossilAPI.eFOSSILE_UPDATE_FLAG.FOSSIL_UPDATE_UPDATE)
				{
					pTotalFossilInfos[totalFossil].rc.left =
							(Convert.ToInt32(cookie_border.Margin.Left) + this.file_data.mat.Width >= 0) ?
									Convert.ToInt32(cookie_border.Margin.Left) + this.file_data.mat.Width : 0;
					pTotalFossilInfos[totalFossil].rc.top =
							(Convert.ToInt32(cookie_border.Margin.Top) >= 0) ?
									Convert.ToInt32(cookie_border.Margin.Top) : 0;
					pTotalFossilInfos[totalFossil].rc.right =
							(pTotalFossilInfos[totalFossil].rc.left + Convert.ToInt32(cookie_border.ActualWidth) <= this.file_data.mat.Width) ?
									pTotalFossilInfos[totalFossil].rc.left + Convert.ToInt32(cookie_border.ActualWidth) :
									this.file_data.mat.Width;
					pTotalFossilInfos[totalFossil].rc.bottom =
							(pTotalFossilInfos[totalFossil].rc.top + Convert.ToInt32(cookie_border.ActualHeight) <= this.file_data.mat.Height) ?
									pTotalFossilInfos[totalFossil].rc.top + Convert.ToInt32(cookie_border.ActualHeight) :
									this.file_data.mat.Height;
				}

				totalFossil++;
			}

			//Update re-process fossil information and update.
			IntPtr fossil_detector = IntPtr.Zero;

			try
			{
				fossil_detector = FossilAPI.SetupFossilDetector("detect_darknet.conf");
			}
			catch (Exception ex)
			{
				LogManager.GetLogger("FileLogger").Error(ex);

				if (FossilAPI.dev_ver == false)
					LogManager.GetLogger("EmailLogger").Error(ex);
			}

			try
			{
				FossilAPI.ProcessUpdateFossilObject2(fossil_detector, this.file_data.mat.Width,
						this.file_data.mat.Height, (int)this.file_data.mat.Step1(),
						this.file_data.mat.Data, totalFossil, pTotalFossilInfos, pTotalFossilInfos);
			}
			catch (Exception ex)
			{
				LogManager.GetLogger("FileLogger").Error(ex);

				if (FossilAPI.dev_ver == false)
					LogManager.GetLogger("EmailLogger").Error(ex);
			}

			try
			{
				FossilAPI.EndupFossilDetector(fossil_detector);
			}
			catch (Exception ex)
			{
				LogManager.GetLogger("FileLogger").Error(ex);

				if (FossilAPI.dev_ver == false)
					LogManager.GetLogger("EmailLogger").Error(ex);
			}

			FossilAPI.FOSSILDETECTInfoList FossilList = new FossilAPI.FOSSILDETECTInfoList();
			FossilList.fossils = new List<FossilAPI.FOSSILDETECTInfo>();
			FossilList.image_index = this.img_idx;

			int fossil_id = this.main_win.cookie_list.DefaultIfEmpty().Max(c => c.fossils.DefaultIfEmpty().Max(f => f.fossil_id)) + 1;

			for (int i = 0; i < totalFossil; i++)
			{
				if (pTotalFossilInfos[i].fossil_id <= MainWindow.UNDEFINED_FOSSIL)
				{
					pTotalFossilInfos[i].fossil_id = fossil_id;
					fossil_id++;
				}

				pTotalFossilInfos[i].image_index = FossilList.image_index;
				FossilList.fossils.Add(pTotalFossilInfos[i]);
			}

			bool list_update = false;

			for (int i = 0; i < this.main_win.cookie_list.Count; i++)
			{
				if (this.main_win.cookie_list[i].image_index != this.img_idx)
					continue;

				this.main_win.cookie_list[i] = FossilList;
				list_update = true;
				break;
			}

			if (list_update == false)
				this.main_win.cookie_list.Add(FossilList);

			this.main_win.SelFile();
			e.Cancel = false;
		}

		private void SizeWindow(object sender, RoutedEventArgs e)
		{
			e.Handled = true;

			if (this.WindowState == WindowState.Maximized)
			{
				((Image)BTN_SIZE_WINDOW.Content).Source =
						(ImageSource)new ImageSourceConverter().ConvertFromString("pack://application:,,,/resources/max.win.png");
				this.WindowState = WindowState.Normal;
			}
			else
			{
				((Image)BTN_SIZE_WINDOW.Content).Source =
						(ImageSource)new ImageSourceConverter().ConvertFromString("pack://application:,,,/resources/min.win.png");
				this.WindowState = WindowState.Maximized;
			}
		}

		private void ResizeWindow(object sender, SizeChangedEventArgs e)
		{
		}

		private void RecognizeTooltip(object sender, RoutedEventArgs e)
		{
			e.Handled = true;

			if (this.show_tooltip == true)
			{
				((Image)BTN_RECOGNIZE_TOOLTIP.Content).Source =
						(ImageSource)new ImageSourceConverter().ConvertFromString("pack://application:,,,/resources/recognize.tooltip.disable.png");
				this.show_tooltip = false;
			}
			else
			{
				((Image)BTN_RECOGNIZE_TOOLTIP.Content).Source =
						(ImageSource)new ImageSourceConverter().ConvertFromString("pack://application:,,,/resources/recognize.tooltip.enable.png");
				this.show_tooltip = true;
			}

			CommonUtil.SaveJson("recognize_tooltip", this.show_tooltip);
		}
	}
}

﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using OpenCvSharp.CPlusPlus;
using System.Threading;
using System.Windows.Threading;

using System.Runtime.ExceptionServices;
using System.Runtime.InteropServices;
using log4net;

namespace FossilUI
{
	/// <summary>
	/// Interaction logic for CategoryWindow.xaml
	/// </summary>
	public partial class CategoryWindow : System.Windows.Window
	{
		private List<GroupPane> _group_list = new List<GroupPane>(); // Group Panel
		private List<CookieData> _item_list = new List<CookieData>();
		private Image _cur_img = null;
		private Cursor _cur_cursor = null;

		public List<GroupPane> group_list { get { return _group_list; } set { _group_list = value; } }
		public List<CookieData> item_list { get { return _item_list; } set { _item_list = value; } }
		public Image cur_img { get { return _cur_img; } set { _cur_img = value; } }
		public Cursor cur_cursor { get { return _cur_cursor; } set { _cur_cursor = value; } }

		private MainWindow main_win = null;
		private bool view_score = false;
		private ScrollViewer item_scroll = null;
		private int move_cnt = 0;
		private bool move_stop = false;
		private Thread move_thread = null;

		public CategoryWindow()
		{
			InitializeComponent();

			this.Title = CommonUtil.LangData("main_window", "category");
			LABEL_WINDOW_TITLE.Content = CommonUtil.LangData("main_window", "category");
			LABEL_SORT_BY.Content = CommonUtil.LangData("category_window", "sort_by");
			RADIO_SORT_SHAPE.Content = CommonUtil.LangData("category_window", "sort_shape");
			RADIO_SORT_SIZE.Content = CommonUtil.LangData("category_window", "sort_size");
			BUTTON_SAVE_CATEGORY.Content = CommonUtil.LangData("category_window", "save");
			BUTTON_NEW_LABEL.Content = CommonUtil.LangData("category_window", "new_label");
			LABEL_UNIDENTIFIED_GROUP.Content = CommonUtil.LangData("category_window", "unidentified_group");

			PANE_UNDEFINED_LIST.Tag = MainWindow.UNDEFINED_GROUP;

			this.main_win = (MainWindow)Application.Current.MainWindow;

			this.main_win.stop_watch.Reset();
			this.main_win.stop_watch.Start();

			this.Width = (System.Windows.Forms.Screen.PrimaryScreen.Bounds.Width * 0.7);
			this.Height = (System.Windows.Forms.Screen.PrimaryScreen.Bounds.Height * 0.7);
			this.view_score = Convert.ToBoolean(CommonUtil.LoadJson("view_score"));

			Mouse.OverrideCursor = Cursors.Wait;

			this.PreviewKeyDown += new KeyEventHandler(this.EscKey);
			this.ContentRendered += new EventHandler(this.ContentRender);

			Mouse.OverrideCursor = null;

			this.Show();
		}

		private void ContentRender(object sender, EventArgs e)
		{
			this.RunClassifier();
		}

		private void EscKey(object sender, KeyEventArgs e)
		{
			if (e.Key == Key.Escape)
				this.Close();
		}

		private void MoveWindow(object sender, MouseButtonEventArgs e)
		{
			if (e.ChangedButton == MouseButton.Left)
			{
				e.Handled = true;
				this.DragMove();
			}
		}

		private void CloseWindow(object sender, RoutedEventArgs e)
		{
			e.Handled = true;

			if (this.main_win.opening_progress)
				return;

			this.Close();
		}

		private void SizeWindow(object sender, RoutedEventArgs e)
		{
			e.Handled = true;

			if (this.WindowState == WindowState.Maximized)
			{
				((Image)BTN_SIZE_WINDOW.Content).Source =
						(ImageSource)new ImageSourceConverter().ConvertFromString("pack://application:,,,/resources/max.win.png");
				this.WindowState = WindowState.Normal;
			}
			else
			{
				((Image)BTN_SIZE_WINDOW.Content).Source =
						(ImageSource)new ImageSourceConverter().ConvertFromString("pack://application:,,,/resources/min.win.png");
				this.WindowState = WindowState.Maximized;
			}
		}

		[HandleProcessCorruptedStateExceptions]
		private void RunClassifier()
		{
			this.group_list.Clear();
			PANE_GROUP_LIST.Children.Clear();

			List<GroupData> group_list = this.main_win.group_list.OrderBy(o => o.order).ToList();

			if (FossilAPI.light_ver == false)
			{
				foreach (GroupData group_data in group_list)
					this.group_list.Add(new GroupPane(PANE_UNDEFINED_LIST, PANE_GROUP_LIST, group_data.name, group_data.idx, this));
			}

			// classifier
			if (this.main_win.cookie_list.Count == 0)
				return;

			int proc_step = 0;
			int tot_cnt = 0;

			foreach (FossilAPI.FOSSILDETECTInfoList sub_list in this.main_win.cookie_list)
			{
				if (this.main_win.CheckFile(sub_list.image_index) == false)
					continue;

				tot_cnt += sub_list.fossils.Count;

				FileData file_data = this.main_win.GetFile(sub_list.image_index);

				if (file_data.step > proc_step)
					proc_step = file_data.step;
			}

			if (tot_cnt == 0)
				return;

			if (proc_step < ProcStep.category)
			{
				FossilAPI.FOSSILDETECTInfo[] fossil_buffer = new FossilAPI.FOSSILDETECTInfo[tot_cnt];

				int k = 0;
				foreach (FossilAPI.FOSSILDETECTInfoList sub_list in this.main_win.cookie_list)
				{
					if (this.main_win.CheckFile(sub_list.image_index) == false)
						continue;

					foreach (FossilAPI.FOSSILDETECTInfo fossil_val in sub_list.fossils)
						fossil_buffer[k++] = fossil_val;
				}

				IntPtr fossil_classifier = IntPtr.Zero;

				try
				{
					fossil_classifier = FossilAPI.CreateClassifier();
				}
				catch (Exception ex)
				{
					LogManager.GetLogger("FileLogger").Error(ex);

					if (FossilAPI.dev_ver == false)
						LogManager.GetLogger("EmailLogger").Error(ex);
				}

				try
				{
					FossilAPI.ProcessClassification2(fossil_classifier, tot_cnt, fossil_buffer, fossil_buffer);
				}
				catch (Exception ex)
				{
					LogManager.GetLogger("FileLogger").Error(ex);

					if (FossilAPI.dev_ver == false)
						LogManager.GetLogger("EmailLogger").Error(ex);
				}

				//Update List 
				k = 0;
				for (int i = 0; i < this.main_win.cookie_list.Count; i++)
				{
					if (this.main_win.CheckFile(this.main_win.cookie_list[i].image_index) == false)
						continue;

					FossilAPI.FOSSILDETECTInfoList fossil_res = this.main_win.cookie_list[i];

					for (int j = 0; j < this.main_win.cookie_list[i].fossils.Count; j++)
						fossil_res.fossils[j] = fossil_buffer[k++];
				}

				try
				{
					FossilAPI.DestroyClassifier(fossil_classifier);
				}
				catch (Exception ex)
				{
					LogManager.GetLogger("FileLogger").Error(ex);

					if (FossilAPI.dev_ver == false)
						LogManager.GetLogger("EmailLogger").Error(ex);
				}
			}

			for (int i = 0; i < this.main_win.cookie_list.Count; i++)
			{
				if (this.main_win.CheckFile(this.main_win.cookie_list[i].image_index) == false)
					continue;

				FileData file_data = this.main_win.GetFile(this.main_win.cookie_list[i].image_index);

				for (int j = 0; j < this.main_win.cookie_list[i].fossils.Count; j++)
				{
					CookieData cate_buffer = new CookieData();
					FossilAPI.FOSSILDETECTInfo cookie_buffer = this.main_win.cookie_list[i].fossils[j];
					cate_buffer.idx = i;
					cate_buffer.id = j;
					cate_buffer.data = cookie_buffer;
					this.item_list.Add(cate_buffer);
				}

				if (file_data.step < ProcStep.category)
					file_data.step = ProcStep.category;
			}

			foreach (CookieData cate_data in this.item_list)
				this.AddCookie(cate_data);

			CategoryComplete();
		}

		private void CategoryComplete()
		{
			((ScrollViewer)PANE_UNDEFINED_LIST.Parent).ScrollToTop();

			foreach (GroupPane group_pane in this.group_list)
			{
				ScrollViewer item_scroll = (ScrollViewer)group_pane.box_pane.Children[2];
				item_scroll.ScrollToLeftEnd();
			}

			this.main_win.SelFile();

			this.Activate();
			this.Topmost = true;
			this.Topmost = false;
			this.Focus();
			this.main_win.stop_watch.Stop();
			this.main_win.SetTitle(this.main_win.LIST_FILE_ITEM.Items.Count, this.main_win.fossil_cnt, "category ::: " + this.main_win.stop_watch.Elapsed.ToString());
			this.main_win.opening_progress = false;
			this.SortItem(RADIO_SORT_SHAPE, null);
			this.StatusMessage(CommonUtil.LangData("main_window", "complete"));
		}

		public void SortItem(object sender, EventArgs e)
		{
			RadioButton cur_btn = (RadioButton)sender;
			if (cur_btn == null) return;
			int cur_sort = Convert.ToInt32(cur_btn.Tag);

			List<CookieData> item_list = (cur_sort == 1) ?
					this.item_list.OrderByDescending(o => ((o.data.rc.right - o.data.rc.left) * (o.data.rc.bottom - o.data.rc.top))).ToList() :
					this.item_list.OrderBy(o => o.data.top_score[0]).ToList();

			foreach (CookieData cate_data in item_list)
			{
				if (cate_data.data.class_no == MainWindow.UNDEFINED_GROUP)
					continue;

				StackPanel item_pane = (StackPanel)cate_data.item.Parent;

				item_pane.Children.Remove(cate_data.item);
				item_pane.Children.Insert(0, cate_data.item);
			}
		}

		public void AddCookie(CookieData cate_data)
		{
			if (cate_data.data.class_no == MainWindow.UNDEFINED_GROUP)
			{
				cate_data.item = (new CategoryItem(this, cate_data.data, PANE_UNDEFINED_LIST, PANE_GROUP_LIST, PANE_UNDEFINED_LIST, this.view_score)).item_border;
				return;
			}

			foreach (GroupPane group_pane in this.group_list)
			{
				if (group_pane.class_no == cate_data.data.class_no)
				{
					cate_data.item = (new CategoryItem(this, cate_data.data, PANE_UNDEFINED_LIST, PANE_GROUP_LIST, group_pane.item_pane, this.view_score)).item_border;
					return;
				}
			}

			GroupPane new_group = new GroupPane(PANE_UNDEFINED_LIST, PANE_GROUP_LIST,
					cate_data.data.class_no < MainWindow.UNDEFINED_GROUP ? "Custom Group #"
					+ Math.Abs(cate_data.data.class_no + 1).ToString() : "Group #" + (FossilAPI.light_ver == true ? cate_data.data.class_no : cate_data.data.class_no + 1).ToString(),
					cate_data.data.class_no, this);
			this.group_list.Add(new_group);
			cate_data.item = (new CategoryItem(this, cate_data.data, PANE_UNDEFINED_LIST, PANE_GROUP_LIST, new_group.item_pane, this.view_score)).item_border;
		}

		private void SaveXml(object sender, EventArgs e)
		{
			string file_name = this.main_win.SaveDiag();

			if (file_name != "")
				this.main_win.SaveXml(file_name, PANE_GROUP_LIST, PANE_UNDEFINED_LIST, this);
		}

		private void NewLabel(object sender, EventArgs e)
		{
			int min_idx = this.main_win.group_list.DefaultIfEmpty().Min(l => l.idx) - 1;

			if (min_idx >= MainWindow.UNDEFINED_GROUP)
				min_idx = MainWindow.UNDEFINED_GROUP - 1;

			int max_order = this.main_win.group_list.DefaultIfEmpty().Max(l => l.order) + 1;
			string group_name = "Custom Group #" + Math.Abs(min_idx + 1).ToString();
			this.group_list.Add(new GroupPane(PANE_UNDEFINED_LIST, PANE_GROUP_LIST, group_name, min_idx, this));
			this.main_win.group_list.Add(
				new GroupData()
				{
					idx = min_idx,
					order = max_order,
					rmin = 0,
					amin = 0,
					name = group_name
				}
			);
		}

		private void EnterControl(object sender, MouseButtonEventArgs e)
		{
			move_stop = false;
			move_cnt = 0;
			System.Windows.Point mouse_pos;
			bool move_way = false;
			bool move_buffer = false;
			this.item_scroll = (ScrollViewer)sender;

			move_thread = new Thread(() =>
			{
				while (!move_stop)
				{
					this.item_scroll.Dispatcher.BeginInvoke(new Action(() =>
					{
						mouse_pos = Mouse.GetPosition(this.item_scroll);
						move_way = (mouse_pos.Y <= this.item_scroll.ActualHeight / 2) ? true : false;
						if (move_way != move_buffer) move_cnt = 0;
						move_buffer = move_way;

						if (System.Windows.Input.Mouse.RightButton == MouseButtonState.Pressed)
						{
							move_cnt++;
							this.item_scroll.ScrollToVerticalOffset(this.item_scroll.ContentVerticalOffset + (move_way ? -Math.Pow(move_cnt, 2) : Math.Pow(move_cnt, 2)));
						}
						else
						{
							move_cnt -= 2;

							if (move_cnt <= 0)
							{
								this.item_scroll.ScrollToVerticalOffset(this.item_scroll.ContentVerticalOffset + (move_way ? -1 : 1));
								move_stop = true;
								move_thread.Join();
							}
							else if (move_cnt <= 4)
							{
								this.item_scroll.ScrollToVerticalOffset(this.item_scroll.ContentVerticalOffset + (move_way ? 1 : -1));
							}
							else
							{
								this.item_scroll.ScrollToVerticalOffset(this.item_scroll.ContentVerticalOffset + (move_way ? -Math.Pow(move_cnt, 2) : Math.Pow(move_cnt, 2)));
							}
						}
					}));

					Thread.Sleep(35);
				}
			});

			move_thread.Start();
		}

		private void DropPane(object sender, DragEventArgs e)
		{
			StackPanel cur_pane = (StackPanel)sender;
			if (cur_pane == null) return;

			CategoryItem cur_cookie = (e.Data.GetData("cookie_cut") as CategoryItem);
			if (cur_cookie == null) return;

			StackPanel item_pane = cur_cookie.cur_pane;
			Border cur_border = cur_cookie.item_border;
			if (cur_border == null) return;
			if (cur_pane == item_pane) return;

			Image item_img = (Image)((Border)((StackPanel)cur_border.Child).Children[0]).Child;
			FossilAPI.FOSSILDETECTInfo cookie_data = (FossilAPI.FOSSILDETECTInfo)item_img.Tag;
			cookie_data.class_no = MainWindow.UNDEFINED_GROUP;

			for (int i = 0; i < this.main_win.cookie_list.Count; i++)
			{
				for (int j = 0; j < this.main_win.cookie_list[i].fossils.Count; j++)
				{
					if (this.main_win.cookie_list[i].fossils[j].fossil_id == cookie_data.fossil_id)
					{
						this.main_win.cookie_list[i].fossils[j] = cookie_data;
						cur_cookie.ToPane(cur_pane);
						return;
					}
				}
			}
		}

		public void StatusMessage(string msg_txt)
		{
			this.LABEL_CATEGORY_STATUS.Content = msg_txt;
		}

		public void GroupFeedback(bool set_event)
		{
			this.PANE_UNDEFINED_LIST.GiveFeedback -= this.DragFeedback;
			if (set_event) this.PANE_UNDEFINED_LIST.GiveFeedback += this.DragFeedback;

			foreach (GroupPane group_pane in this.group_list)
			{
				group_pane.item_pane.GiveFeedback -= this.DragFeedback;
				if (set_event) group_pane.item_pane.GiveFeedback += this.DragFeedback;
			}
		}

		public void DragFeedback(object sender, GiveFeedbackEventArgs e)
		{
			if (this.cur_img != null)
			{
				try
				{
					if (this.cur_cursor == null)
						this.cur_cursor = CursorHelper.CreateCursor(this.cur_img, 0, 0);

					Mouse.SetCursor(this.cur_cursor);

					e.UseDefaultCursors = false;
					e.Handled = true;
				}
				finally { }
			}
		}

		private void ClosingWindow(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (MessageBox.Show(CommonUtil.LangData("common", "closing_window"), CommonUtil.LangData("common", "confirm"),
					MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
			{
				this.main_win.SelFile();

				e.Cancel = false;
			}
			else
				e.Cancel = true;
		}
	}
}

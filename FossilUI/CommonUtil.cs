﻿using System;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Runtime.InteropServices;
using System.Web.Script.Serialization;
using System.Linq;

namespace FossilUI
{
	static class CommonUtil
	{
		// event vars
		private static bool focus_control = false;
		private static int click_timestamp = 1000;

		// color vars
		private static Brush enable_color = (Brush)(new BrushConverter()).ConvertFromString("#FF04AEDA");
		private static Brush disable_color = (Brush)(new BrushConverter()).ConvertFromString("#FF777777");

		public static Brush GetBrush(string color_str)
		{
			return (Brush)(new BrushConverter()).ConvertFromString(color_str);
		}

		public static void SetFocus(bool focus_control)
		{
			CommonUtil.focus_control = focus_control;
		}

		public static bool GetFocus()
		{
			return CommonUtil.focus_control;
		}

		public static void SetTimestamp(int click_timestamp)
		{
			CommonUtil.click_timestamp = click_timestamp;
		}

		public static int GetTimestamp()
		{
			return CommonUtil.click_timestamp;
		}

		public static ImageSource MemImg(MemoryStream img_stream)
		{
			BitmapImage bitmap_img = new BitmapImage();
			bitmap_img.BeginInit();
			bitmap_img.StreamSource = img_stream;
			bitmap_img.EndInit();
			return bitmap_img;
		}

		public static OpenCvSharp.CPlusPlus.Rect SubRect(FossilAPI.RECT org_rect, int img_width, int img_height)
		{
			OpenCvSharp.CPlusPlus.Rect buff_rect;
			OpenCvSharp.CPlusPlus.Rect fixed_rect;

			buff_rect.X = fixed_rect.X = org_rect.left;
			buff_rect.Y = fixed_rect.Y = org_rect.top;
			buff_rect.Width = fixed_rect.Width = org_rect.right - org_rect.left;
			buff_rect.Height = fixed_rect.Height = org_rect.bottom - org_rect.top;

			if (buff_rect.X + buff_rect.Width > img_width)
				fixed_rect.Width = img_width - buff_rect.X;

			if (buff_rect.Y + buff_rect.Height > img_height)
				fixed_rect.Height = img_height - buff_rect.Y;

			return fixed_rect;
		}

		public static void SaveImg(System.Windows.Controls.Image save_img, string save_path)
		{
			RenderTargetBitmap render_bmp = new RenderTargetBitmap(Convert.ToInt32(save_img.Width), Convert.ToInt32(save_img.Height), 90, 90, PixelFormats.Default);
			render_bmp.Render(save_img);

			PngBitmapEncoder png_enc = new PngBitmapEncoder();
			png_enc.Frames.Add(BitmapFrame.Create(render_bmp));

			Stream img_stream = new MemoryStream();
			png_enc.Save(img_stream);

			img_stream.Position = 0;

			System.Drawing.Image res_img = System.Drawing.Image.FromStream(img_stream);
			res_img.Save(save_path);
		}

		public static OpenCvSharp.CPlusPlus.Mat StrMat(int file_idx, string img_type, string img_src)
		{
			string file_path = Path.GetTempPath() + file_idx.ToString() + "." + img_type + ".jpg";
			byte[] img_binary = Convert.FromBase64String(img_src);

			File.WriteAllBytes(file_path, img_binary);
			OpenCvSharp.CPlusPlus.Mat img_mat = new OpenCvSharp.CPlusPlus.Mat(file_path, OpenCvSharp.LoadMode.Color);

			return img_mat;
		}

		public static OpenCvSharp.CPlusPlus.Mat GetMat(int img_idx)
		{
			OpenCvSharp.CPlusPlus.Mat cur_mat = null;

			foreach (FileData file_data in ((MainWindow)Application.Current.MainWindow).LIST_FILE_ITEM.Items)
			{
				if (file_data.idx == img_idx)
				{
					cur_mat = new OpenCvSharp.CPlusPlus.Mat(file_data.path, OpenCvSharp.LoadMode.Color);
					break;
				}
			}

			return cur_mat;
		}

		public static string GetPath(int img_idx)
		{
			string file_path = "";

			foreach (FileData file_data in ((MainWindow)Application.Current.MainWindow).LIST_FILE_ITEM.Items)
			{
				if (file_data.idx == img_idx)
				{
					file_path = file_data.path;
					break;
				}
			}

			return file_path;
		}

		public static bool IsID(string inst_id)
		{
			if (inst_id.Trim() == "")
				return false;

			foreach (char id_unit in inst_id)
			{
				if (id_unit == '\\' || id_unit == '/' || id_unit == ':' || id_unit == '*' || id_unit == '?'
						|| id_unit == '"' || id_unit == '<' || id_unit == '>' || id_unit == '|')
					return false;
			}

			return true;
		}

		public static dynamic UiData(string data_name)
		{
			MainWindow main_win = (MainWindow)Application.Current.MainWindow;
			return main_win.ui_json[data_name];
		}

		public static dynamic LangData(string data_name1, string data_name2)
		{
			MainWindow main_win = (MainWindow)Application.Current.MainWindow;

			try
			{
				return main_win.lang_json[data_name1][data_name2];
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.ToString());
				return "";
			}
		}

		public static dynamic LoadJson(string data_name)
		{
			string ui_str = System.IO.File.ReadAllText(@"ui.json");
			dynamic ui_json = null;

			try
			{
				ui_json = (new JavaScriptSerializer()).Deserialize<dynamic>(ui_str);
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.ToString());
			}

			if (ui_json != null)
				return ui_json[data_name];

			return null;
		}

		public static void SaveJson(string data_name, dynamic data_value)
		{
			string ui_str = System.IO.File.ReadAllText(@"ui.json");
			dynamic ui_json = (new JavaScriptSerializer()).Deserialize<dynamic>(ui_str);
			string save_str = "{" + System.Environment.NewLine;
			string json_str = "";
			bool data_exist = false;

			foreach (var json_data in ui_json)
			{
				if (json_str != "")
					json_str += "," + System.Environment.NewLine;

				json_str += "\t\"" + json_data.Key + "\": ";

				if (data_name == Convert.ToString(json_data.Key))
				{
					if (data_value.GetType() == typeof(string))
						json_str += "\"";

					if (data_value.GetType() == typeof(bool))
						json_str += Convert.ToString(data_value).ToLower();
					else
					{
						if (data_name == "test_dir" || data_name == "load_dir" || data_name == "save_dir")
							json_str += data_value.ToString().Replace("\\", "\\\\");
						else
							json_str += data_value;
					}

					if (data_value.GetType() == typeof(string))
						json_str += "\"";

					data_exist = true;
				}
				else
				{
					if (json_data.Value.GetType() == typeof(string))
						json_str += "\"";

					if (json_data.Value.GetType() == typeof(bool))
						json_str += Convert.ToString(json_data.Value).ToLower();
					else
					{
						if (Convert.ToString(json_data.Key) == "test_dir" || Convert.ToString(json_data.Key) == "load_dir"
								|| Convert.ToString(json_data.Key) == "save_dir")
							json_str += json_data.Value.ToString().Replace("\\", "\\\\");
						else
							json_str += json_data.Value;
					}

					if (json_data.Value.GetType() == typeof(string))
						json_str += "\"";
				}
			}

			if (data_exist == false)
			{
				if (json_str != "")
					json_str += "," + System.Environment.NewLine;

				json_str += "\t\"" + data_name + "\": ";

				if (data_value.GetType() == typeof(string))
					json_str += "\"";

				if (data_value.GetType() == typeof(bool))
					json_str += Convert.ToString(data_value).ToLower();
				else
				{
					if (data_name == "test_dir" || data_name == "load_dir" || data_name == "save_dir")
						json_str += data_value.ToString().Replace("\\", "\\\\");
					else
						json_str += data_value;
				}

				if (data_value.GetType() == typeof(string))
					json_str += "\"";
			}

			save_str += json_str + System.Environment.NewLine + "}";

			using (StreamWriter json_writer = new StreamWriter(@"ui.json"))
			{
				json_writer.Write(save_str);
			};
		}

		public static void GroupName(int class_no, string group_name)
		{
			MainWindow main_win = (MainWindow)Application.Current.MainWindow;

			foreach (GroupData group_data in main_win.group_list)
			{
				if (group_data.idx == class_no)
				{
					group_data.name = group_name;
					return;
				}
			}

			main_win.group_list.Add(
				new GroupData
				{
					idx = class_no,
					order = 0,
					name = group_name
				}
			);
		}

		public static void GroupName(string group_name, int class_no)
		{
			CommonUtil.GroupName(class_no, group_name);
		}

		public static void WaitThread(int term_sec)
		{
			MainWindow main_win = (MainWindow)Application.Current.MainWindow;
			bool worker_completed = false;

			while (!worker_completed)
			{
				try
				{
					worker_completed = (from main_worker in main_win.worker_list
										where main_worker.IsBusy
										select main_worker).ToList().Count == 0;
				}
				catch (Exception ex)
				{
					worker_completed = false;
				}

				System.Threading.Thread.Sleep(term_sec);
			}
		}

		public static bool IsWindowOpen<T>(string name = "") where T : Window
		{
			return string.IsNullOrEmpty(name)
			   ? Application.Current.Windows.OfType<T>().Any()
			   : Application.Current.Windows.OfType<T>().Any(w => w.Name.Equals(name));
		}

		public static T FindVisualChild<T>(DependencyObject dep_obj) where T : DependencyObject
		{
			if (dep_obj != null)
			{
				for (int i = 0; i < VisualTreeHelper.GetChildrenCount(dep_obj); i++)
				{
					DependencyObject obj_child = VisualTreeHelper.GetChild(dep_obj, i);

					if (obj_child != null && obj_child is T)
						return (T)obj_child;

					T child_item = FindVisualChild<T>(obj_child);

					if (child_item != null)
						return child_item;
				}
			}

			return null;
		}

		public static T FindVisualChild<T>(DependencyObject parent, string childName) where T : DependencyObject
		{
			if (parent == null)
				return null;

			T foundChild = null;

			int childrenCount = VisualTreeHelper.GetChildrenCount(parent);
			for (int i = 0; i < childrenCount; i++)
			{
				var child = VisualTreeHelper.GetChild(parent, i);
				T childType = child as T;
				if (childType == null)
				{
					foundChild = FindVisualChild<T>(child, childName);

					if (foundChild != null) break;
				}
				else if (!string.IsNullOrEmpty(childName))
				{
					var frameworkElement = child as FrameworkElement;
					if (frameworkElement != null && frameworkElement.Name == childName)
					{
						foundChild = (T)child;
						break;
					}
				}
				else
				{
					foundChild = (T)child;
					break;
				}
			}

			return foundChild;
		}
	}
}

﻿using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Threading;

namespace FossilUI
{
	/// <summary>
	/// Interaction logic for ProgressWindow.xaml
	/// </summary>
	public partial class ProgressWindow : Window
	{
		private MainWindow main_win = null;
		private int progress_cnt = 0;

		public ProgressWindow(double progress_max, string progress_text)
		{
			InitializeComponent();

			this.main_win = (MainWindow)Application.Current.MainWindow;

			LABEL_PROGRESS_CONTENT.Content = progress_text;
			PROGRESS_PER.Maximum = progress_max;

			this.Owner = Application.Current.MainWindow;
			this.WindowStartupLocation = WindowStartupLocation.CenterOwner;
			this.ShowInTaskbar = false;
			this.Show();
		}

		public ProgressWindow(double progress_max, string progress_text, bool cancel_btn)
		{
			InitializeComponent();

			this.main_win = (MainWindow)Application.Current.MainWindow;

			LABEL_PROGRESS_CONTENT.Content = progress_text;
			PROGRESS_PER.Maximum = progress_max;

			if (cancel_btn == false)
			{
				BUTTON_PROGRESS_CANCEL.Visibility = System.Windows.Visibility.Hidden;
				PROGRESS_PER.Width = 280;
			}

			this.Owner = Application.Current.MainWindow;
			this.WindowStartupLocation = WindowStartupLocation.CenterOwner;
			this.ShowInTaskbar = false;
			this.Show();
		}

		public ProgressWindow(double progress_max, string progress_text, CategoryWindow cate_win)
		{
			InitializeComponent();

			this.main_win = (MainWindow)Application.Current.MainWindow;

			LABEL_PROGRESS_CONTENT.Content = progress_text;
			PROGRESS_PER.Maximum = progress_max;

			this.Owner = cate_win;
			this.WindowStartupLocation = WindowStartupLocation.CenterOwner;
			this.ShowInTaskbar = false;
			this.Show();
		}

		public ProgressWindow(double progress_max, string progress_text, RecognizeWindow recog_win)
		{
			InitializeComponent();

			this.main_win = (MainWindow)Application.Current.MainWindow;

			LABEL_PROGRESS_CONTENT.Content = progress_text;
			PROGRESS_PER.Maximum = progress_max;

			this.Owner = recog_win;
			this.WindowStartupLocation = WindowStartupLocation.CenterOwner;
			this.ShowInTaskbar = false;
			this.Show();
		}

		public void ClickControl(object sender, MouseButtonEventArgs e)
		{
			CommonUtil.SetFocus(true);
		}

		public void FinalizeEvent()
		{
			CommonUtil.SetFocus(false);
		}

		public void CancelProgress(object sender, MouseButtonEventArgs e)
		{
			this.Dispatcher.Invoke(
				DispatcherPriority.Background,
				new Action(() => {
					this.main_win.interrupt_code = 0;

					if (MessageBox.Show(CommonUtil.LangData("common", "cancel_progress"), CommonUtil.LangData("common", "confirm"),
							MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
						this.main_win.interrupt_code = -1;
					else
						this.main_win.interrupt_code = 1;

					this.FinalizeEvent();
				}
			));
		}

		public void SetVal(object sender, ProgressChangedEventArgs e)
		{
			if (PROGRESS_PER.Value < e.ProgressPercentage)
				PROGRESS_PER.Value = e.ProgressPercentage;
		}

		static int CompareFossilList(FossilAPI.FOSSILDETECTInfoList a, FossilAPI.FOSSILDETECTInfoList b)
		{
			return a.image_index - b.image_index;
		}

		public void SetText(string text_val)
		{
			LABEL_PROGRESS_CONTENT.Content = text_val;
		}

		public void CloseWin(object sender, RunWorkerCompletedEventArgs e, int max_cnt, string process_step)
		{
			this.progress_cnt++;

			if (this.progress_cnt >= max_cnt)
			{
				if (process_step == "analyze")
				{
					CommonUtil.WaitThread(1000);
					this.main_win.worker_list.Clear();
					this.main_win.opening_progress = false;
					this.main_win.ButtonStatus();
					this.SelFirst(0);
					this.main_win.StatusMessage("Complete.");
					this.main_win.SetTitle(this.main_win.LIST_FILE_ITEM.Items.Count, this.main_win.fossil_cnt);
					this.Close();
				}
			}
		}

		public void CloseWin(object sender, RunWorkerCompletedEventArgs e, int max_cnt, string process_step, RecognizeWindow recog_win)
		{
			this.progress_cnt++;

			if (this.progress_cnt >= max_cnt)
			{
				CommonUtil.WaitThread(1000);
				this.main_win.worker_list.Clear();
				recog_win.SortItem(recog_win.RADIO_SORT_SHAPE, null);
				this.Close();
				this.main_win.opening_progress = false;

				this.main_win.stop_watch.Stop();
				this.main_win.SetTitle(this.main_win.LIST_FILE_ITEM.Items.Count, this.main_win.fossil_cnt, this.main_win.stop_watch.Elapsed.ToString());
			}
		}

		private void SelFirst(int sleep_sec)
		{
			System.Threading.Thread.Sleep(sleep_sec);

			this.main_win.LIST_FILE_ITEM.Items.MoveCurrentToFirst();
			ListBoxItem first_item = (ListBoxItem)this.main_win.LIST_FILE_ITEM.Items.CurrentItem;

			if (first_item != null)
			{
				((ListBoxItem)this.main_win.LIST_FILE_ITEM.Items.CurrentItem).IsSelected = true;
				this.main_win.SelFile();
			}
		}
	}
}

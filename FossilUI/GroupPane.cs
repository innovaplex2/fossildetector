﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Threading;
using System.Windows.Threading;
using System.Windows.Shapes;

namespace FossilUI
{
	public class GroupPane
	{
		private StackPanel _box_pane = null;
		private StackPanel _item_pane = null;
		private int _class_no = MainWindow.UNDEFINED_GROUP;
		private CategoryWindow cate_win = null;
		private RecognizeWindow recog_win = null;
		private int item_type = 0;
		private Brush bg_color = CommonUtil.GetBrush("#FF777777");
		private Brush act_color = CommonUtil.GetBrush("#FFF1C40F");
		private StackPanel label_pane = null;
		private ScrollViewer item_scroll = null;
		private StackPanel undefined_pane = null;
		private StackPanel group_pane = null;
		private TextBox item_label = null;
		private Image item_top = null, item_up = null, item_down = null, item_bottom = null, item_remove = null;
		private String pane_name = "";
		private MainWindow main_win = null;
		private int move_cnt = 0;
		private bool move_stop = false;
		private Thread move_thread = null;
		private bool is_focus = false;

		public StackPanel box_pane { get { return _box_pane; } set { _box_pane = value; } }
		public StackPanel item_pane { get { return _item_pane; } set { _item_pane = value; } }
		public int class_no { get { return _class_no; } set { _class_no = value; } }

		public GroupPane(StackPanel undefined_pane, StackPanel group_pane, String pane_name, int class_no, CategoryWindow cate_win)
		{
			this.cate_win = cate_win;
			this.item_type = 0;
			this.InitPane(undefined_pane, group_pane, pane_name, class_no);
		}

		public GroupPane(StackPanel undefined_pane, StackPanel group_pane, String pane_name, int class_no, RecognizeWindow recog_win)
		{
			this.recog_win = recog_win;
			this.item_type = 1;
			this.InitPane(undefined_pane, group_pane, pane_name, class_no);
		}

		private void InitPane(StackPanel undefined_pane, StackPanel group_pane, String pane_name, int class_no)
		{
			this.main_win = (MainWindow)Application.Current.MainWindow;
			this.undefined_pane = undefined_pane;
			this.group_pane = group_pane;
			this.pane_name = pane_name;
			this.class_no = class_no;

			this.box_pane = new StackPanel
			{
				Orientation = Orientation.Vertical,
				Margin = new Thickness(0, 5, 0, 0),
				HorizontalAlignment = HorizontalAlignment.Stretch,
				Background = Brushes.Transparent
			};
			this.label_pane = new StackPanel
			{
				Orientation = Orientation.Horizontal,
				HorizontalAlignment = HorizontalAlignment.Stretch
			};
			CustomCaretTextBox_grp label_box = new CustomCaretTextBox_grp
			{
				HorizontalAlignment = HorizontalAlignment.Left,
				BorderThickness = new Thickness(0),
				Margin = new Thickness(0, 5, 0, 0),
				Padding = new Thickness(5, 3, 5, 3),
				FontSize = 14.5,
				FontWeight = FontWeights.Bold,
				Foreground = Brushes.Yellow,
				Background = Brushes.Transparent,
				MaxWidth = 385
			};
			this.item_label = label_box.txt_box;
			this.item_label.Text = this.pane_name;
			this.item_top = new Image
			{
				HorizontalAlignment = HorizontalAlignment.Right,
				Source = (ImageSource)new ImageSourceConverter().ConvertFromString(
						"pack://application:,,,/resources/group.top.png"),
				Margin = new Thickness(0, 7, 5, 5),
				Cursor = Cursors.Hand,
				Visibility = Visibility.Hidden
			};
			this.item_up = new Image
			{
				HorizontalAlignment = HorizontalAlignment.Right,
				Source = (ImageSource)new ImageSourceConverter().ConvertFromString(
						"pack://application:,,,/resources/group.up.png"),
				Margin = new Thickness(0, 7, 5, 5),
				Cursor = Cursors.Hand,
				Visibility = Visibility.Hidden
			};
			this.item_down = new Image
			{
				HorizontalAlignment = HorizontalAlignment.Right,
				Source = (ImageSource)new ImageSourceConverter().ConvertFromString(
						"pack://application:,,,/resources/group.down.png"),
				Margin = new Thickness(0, 7, 5, 5),
				Cursor = Cursors.Hand,
				Visibility = Visibility.Hidden
			};
			this.item_bottom = new Image
			{
				HorizontalAlignment = HorizontalAlignment.Right,
				Source = (ImageSource)new ImageSourceConverter().ConvertFromString(
						"pack://application:,,,/resources/group.bottom.png"),
				Margin = new Thickness(0, 7, 5, 5),
				Cursor = Cursors.Hand,
				Visibility = Visibility.Hidden
			};
			this.item_remove = new Image
			{
				HorizontalAlignment = HorizontalAlignment.Right,
				Source = (ImageSource)new ImageSourceConverter().ConvertFromString(
						"pack://application:,,,/resources/group.remove.png"),
				Margin = new Thickness(0, 7, 5, 5),
				Cursor = Cursors.Hand,
				Visibility = Visibility.Hidden
			};
			Label item_cnt = new Label
			{
				HorizontalAlignment = HorizontalAlignment.Right,
				Foreground = Brushes.White,
				Margin = new Thickness(0, -28, 0, 0),
				Content = "0"
			};
			Border scroll_border = new Border
			{
				Height = 1,
				Background = new ImageBrush
				{
					ImageSource = (ImageSource)new ImageSourceConverter().ConvertFromString(
							"pack://application:,,,/resources/list.line.png"),
					AlignmentY = AlignmentY.Top,
					Stretch = Stretch.UniformToFill,
					TileMode = TileMode.FlipX,
					Viewport = new System.Windows.Rect(0, 0, 4, 1),
					ViewportUnits = BrushMappingMode.Absolute
				}
			};
			this.item_scroll = new ScrollViewer
			{
				HorizontalAlignment = HorizontalAlignment.Stretch,
				Margin = new Thickness(0, 0, 0, 1),
				HorizontalScrollBarVisibility = ScrollBarVisibility.Hidden,
				VerticalScrollBarVisibility = ScrollBarVisibility.Disabled,
				Background = Brushes.Transparent
			};
			this.item_pane = new StackPanel
			{
				Orientation = Orientation.Horizontal,
				AllowDrop = true,
				MinHeight = 100,
				Background = Brushes.Transparent,
				VerticalAlignment = VerticalAlignment.Top,
				HorizontalAlignment = HorizontalAlignment.Stretch,
				CanHorizontallyScroll = true,
				Tag = class_no
			};

			bool start_group = false;

			foreach (GroupData group_data in this.main_win.group_data)
			{
				if (group_data.idx == this.class_no)
				{
					start_group = true;
					break;
				}
			}

			this.item_top.MouseDown += new MouseButtonEventHandler(this.ClickControl);
			this.item_up.MouseDown += new MouseButtonEventHandler(this.ClickControl);
			this.item_down.MouseDown += new MouseButtonEventHandler(this.ClickControl);
			this.item_bottom.MouseDown += new MouseButtonEventHandler(this.ClickControl);
			this.item_remove.MouseDown += new MouseButtonEventHandler(this.ClickControl);

			this.item_top.MouseUp += new MouseButtonEventHandler(this.MoveTop);
			this.item_up.MouseUp += new MouseButtonEventHandler(this.MoveUp);
			this.item_down.MouseUp += new MouseButtonEventHandler(this.MoveDown);
			this.item_bottom.MouseUp += new MouseButtonEventHandler(this.MoveBottom);
			this.item_remove.MouseUp += new MouseButtonEventHandler(this.RemoveGroup);

			this.box_pane.MouseMove += new MouseEventHandler(this.MouseMovePane);
			this.box_pane.MouseEnter += new MouseEventHandler(this.MouseEnterPane);
			this.box_pane.MouseLeave += new MouseEventHandler(this.MouseLeavePane);
			this.item_scroll.PreviewMouseWheel += new MouseWheelEventHandler(this.MouseWheel);
			this.item_scroll.MouseDown += new MouseButtonEventHandler(this.MoveScroll);
			this.item_pane.DragOver += new DragEventHandler(this.DragOverPane);
			this.item_pane.DragEnter += new DragEventHandler(this.DragEnterPane);
			this.item_pane.DragLeave += new DragEventHandler(this.DragLeavePane);
			this.item_pane.Drop += this.item_type == 0 ? new DragEventHandler(this.DropPaneCategory)
					: new DragEventHandler(this.DropPaneRecognize);

			this.item_label.KeyUp += new KeyEventHandler(this.ChangeName);

			this.item_scroll.Content = this.item_pane;

			this.label_pane.Children.Add(label_box);
			this.label_pane.Children.Add(this.item_top);
			this.label_pane.Children.Add(this.item_up);
			this.label_pane.Children.Add(this.item_down);
			this.label_pane.Children.Add(this.item_bottom);

			if (start_group == false)
				this.label_pane.Children.Add(this.item_remove);

			this.box_pane.Children.Add(this.label_pane);
			this.box_pane.Children.Add(item_cnt);
			this.box_pane.Children.Add(this.item_scroll);
			this.box_pane.Children.Add(scroll_border);
			this.group_pane.Children.Add(this.box_pane);
		}

		private void EnterScroll(object sender, MouseEventArgs e)
		{
			Point mouse_pos = Mouse.GetPosition(this.item_pane);
			ToolTip scroll_tooltip = (ToolTip)this.item_scroll.ToolTip;

			scroll_tooltip.HorizontalOffset = mouse_pos.X - this.item_scroll.HorizontalOffset;
			scroll_tooltip.VerticalOffset = mouse_pos.Y;
			scroll_tooltip.IsOpen = true;
		}

		private void TooltipScroll(object sender, MouseEventArgs e)
		{
			Point mouse_pos = Mouse.GetPosition(this.item_pane);
			ToolTip scroll_tooltip = (ToolTip)this.item_scroll.ToolTip;

			scroll_tooltip.HorizontalOffset = mouse_pos.X;
			scroll_tooltip.VerticalOffset = mouse_pos.Y;
		}

		private void LeaveScroll(object sender, MouseEventArgs e)
		{
			ToolTip scroll_tooltip = (ToolTip)this.item_scroll.ToolTip;
			scroll_tooltip.IsOpen = false;
		}

		private void MoveScroll(object sender, MouseButtonEventArgs e)
		{
			move_stop = false;
			move_cnt = 0;
			Point mouse_pos;
			bool move_way = false;
			bool move_buffer = false;

			move_thread = new Thread(() =>
			{
				while (!move_stop)
				{
					this.item_scroll.Dispatcher.BeginInvoke(new Action(() =>
					{
						mouse_pos = Mouse.GetPosition(this.box_pane);
						move_way = (mouse_pos.X <= this.box_pane.ActualWidth / 2) ? true : false;
						if (move_way != move_buffer) move_cnt = 0;
						move_buffer = move_way;

						if (System.Windows.Input.Mouse.RightButton == MouseButtonState.Pressed)
						{
							move_cnt++;
							this.item_scroll.ScrollToHorizontalOffset(this.item_scroll.ContentHorizontalOffset
									+ (move_way ? -Math.Pow(move_cnt, 2) : Math.Pow(move_cnt, 2)));
						}
						else
						{
							move_cnt -= 2;

							if (move_cnt <= 0)
							{
								this.item_scroll.ScrollToHorizontalOffset(this.item_scroll.ContentHorizontalOffset + (move_way ? -1 : 1));
								move_stop = true;
								move_thread.Join();
							}
							else if (move_cnt <= 4)
							{
								this.item_scroll.ScrollToHorizontalOffset(this.item_scroll.ContentHorizontalOffset + (move_way ? 1 : -1));
							}
							else
							{
								this.item_scroll.ScrollToHorizontalOffset(this.item_scroll.ContentHorizontalOffset
										+ (move_way ? -Math.Pow(move_cnt, 2) : Math.Pow(move_cnt, 2)));
							}
						}
					}));

					Thread.Sleep(35);
				}
			});

			move_thread.Start();
		}

		private void ClickControl(object sender, MouseButtonEventArgs e)
		{
			CommonUtil.SetFocus(true);
		}

		private void ChangeName(object sender, KeyEventArgs e)
		{
			TextBox item_label = (TextBox)sender;
			this.pane_name = item_label.Text;
			CommonUtil.GroupName(this.class_no, this.pane_name);
		}

		private void MoveTop(object sender, MouseButtonEventArgs e)
		{
			Image cur_btn = (Image)sender;
			if (cur_btn == null) return;
			StackPanel label_pane = (StackPanel)cur_btn.Parent;
			StackPanel box_pane = (StackPanel)label_pane.Parent;
			StackPanel group_pane = (StackPanel)box_pane.Parent;
			group_pane.Children.Remove(box_pane);
			this.group_pane.Children.Insert(0, this.box_pane);
			this.SetOrder();
		}

		private void MoveUp(object sender, MouseButtonEventArgs e)
		{
			Image cur_btn = (Image)sender;
			if (cur_btn == null) return;
			StackPanel label_pane = (StackPanel)cur_btn.Parent;
			StackPanel box_pane = (StackPanel)label_pane.Parent;
			StackPanel group_pane = (StackPanel)box_pane.Parent;

			int cur_idx = group_pane.Children.IndexOf(box_pane);
			StackPanel target_pane = null;

			if (cur_idx - 1 >= 0)
				target_pane = (StackPanel)group_pane.Children[cur_idx - 1];
			else
				return;

			group_pane.Children.Remove(box_pane);
			this.group_pane.Children.Insert((cur_idx - 1 >= 0 ? cur_idx - 1 : 0), this.box_pane);

			TranslateTransform up_trans = new TranslateTransform();
			target_pane.RenderTransform = up_trans;
			DoubleAnimation up_anim = new DoubleAnimation(-target_pane.ActualHeight, 0, TimeSpan.FromSeconds(0.5));
			//up_anim.AutoReverse = true;
			//up_anim.RepeatBehavior = RepeatBehavior.Forever;
			up_trans.BeginAnimation(TranslateTransform.YProperty, up_anim);

			TranslateTransform down_trans = new TranslateTransform();
			box_pane.RenderTransform = down_trans;
			DoubleAnimation down_anim = new DoubleAnimation(target_pane.ActualHeight, 0, TimeSpan.FromSeconds(0.5));
			down_trans.BeginAnimation(TranslateTransform.YProperty, down_anim);

			this.SetOrder();
		}

		private void MoveDown(object sender, MouseButtonEventArgs e)
		{
			Image cur_btn = (Image)sender;
			if (cur_btn == null) return;
			StackPanel label_pane = (StackPanel)cur_btn.Parent;
			StackPanel box_pane = (StackPanel)label_pane.Parent;
			StackPanel group_pane = (StackPanel)box_pane.Parent;

			int cur_idx = group_pane.Children.IndexOf(box_pane);
			StackPanel target_pane = null;

			if (cur_idx + 1 < group_pane.Children.Count)
				target_pane = (StackPanel)group_pane.Children[cur_idx + 1];
			else
				return;

			group_pane.Children.Remove(box_pane);
			this.group_pane.Children.Insert((cur_idx + 1 <= group_pane.Children.Count ? cur_idx + 1 : group_pane.Children.Count), this.box_pane);

			TranslateTransform up_trans = new TranslateTransform();
			box_pane.RenderTransform = up_trans;
			DoubleAnimation up_anim = new DoubleAnimation(-target_pane.ActualHeight, 0, TimeSpan.FromSeconds(0.5));
			up_trans.BeginAnimation(TranslateTransform.YProperty, up_anim);

			TranslateTransform down_trans = new TranslateTransform();
			target_pane.RenderTransform = down_trans;
			DoubleAnimation down_anim = new DoubleAnimation(target_pane.ActualHeight, 0, TimeSpan.FromSeconds(0.5));
			down_trans.BeginAnimation(TranslateTransform.YProperty, down_anim);

			this.SetOrder();
		}

		private void MoveBottom(object sender, MouseButtonEventArgs e)
		{
			Image cur_btn = (Image)sender;
			if (cur_btn == null) return;
			StackPanel label_pane = (StackPanel)cur_btn.Parent;
			StackPanel box_pane = (StackPanel)label_pane.Parent;
			StackPanel group_pane = (StackPanel)box_pane.Parent;
			group_pane.Children.Remove(box_pane);
			this.group_pane.Children.Add(this.box_pane);
			this.SetOrder();
		}

		private void RemoveGroup(object sender, MouseButtonEventArgs e)
		{
			Image cur_btn = (Image)sender;
			if (cur_btn == null) return;

			this.is_focus = true;

			if (MessageBox.Show(CommonUtil.LangData("recognizer_window", "remove_label"), CommonUtil.LangData("common", "confirm"),
					MessageBoxButton.YesNo, MessageBoxImage.Question) != MessageBoxResult.Yes)
			{
				this.is_focus = false;
				this.MouseLeavePane(null, null);
				return;
			}

			for (int i = this.item_pane.Children.Count - 1; i >= 0; i--)
			{
				Border cookie_border = (Border)this.item_pane.Children[i];
				Image item_img = (Image)((StackPanel)cookie_border.Child).Children[0];
				FossilAPI.FOSSILDETECTInfo cookie_data = (FossilAPI.FOSSILDETECTInfo)item_img.Tag;
				cookie_data.class_no = MainWindow.UNDEFINED_GROUP;
				this.item_pane.Children.Remove(cookie_border);
				this.undefined_pane.Children.Add(cookie_border);
			}

			foreach (GroupData group_data in this.main_win.group_list)
			{
				if (group_data.idx == this.class_no)
				{
					this.main_win.group_list.Remove(group_data);
					break;
				}
			}

			if (this.item_type == 0)
				this.cate_win.group_list.Remove(this);
			else
				this.recog_win.group_list.Remove(this);

			this.group_pane.Children.Remove(this.box_pane);
		}

		private void SetOrder()
		{
			int group_order = 0;

			foreach (StackPanel box_pane in this.group_pane.Children)
			{
				int group_idx = (int)((StackPanel)((ScrollViewer)box_pane.Children[2]).Content).Tag;

				foreach (GroupData group_data in this.main_win.group_list)
				{
					if (group_data.idx == group_idx)
					{
						group_data.order = (group_order++);
						break;
					}
				}
			}
		}

		private void MouseMovePane(object sender, MouseEventArgs e)
		{
		}

		private void MouseEnterPane(object sender, MouseEventArgs e)
		{
			this.item_scroll.Height = this.item_scroll.ActualHeight;
			this.item_scroll.HorizontalScrollBarVisibility = ScrollBarVisibility.Auto;
			this.item_top.Visibility = this.item_up.Visibility = this.item_down.Visibility = this.item_bottom.Visibility
					= this.item_remove.Visibility = Visibility.Visible;
		}

		private void MouseLeavePane(object sender, MouseEventArgs e)
		{
			if (this.is_focus == true)
				return;

			this.item_scroll.Height = this.item_scroll.ActualHeight;
			this.item_scroll.HorizontalScrollBarVisibility = ScrollBarVisibility.Hidden;
			this.item_top.Visibility = this.item_up.Visibility = this.item_down.Visibility = this.item_bottom.Visibility
					= this.item_remove.Visibility = Visibility.Hidden;
		}

		private void MouseWheel(object sender, MouseWheelEventArgs e)
		{
			ScrollViewer group_scroll = ((ScrollViewer)this.group_pane.Parent);

			if (e.Delta < 0)
				group_scroll.LineDown();
			else
				group_scroll.LineUp();

			e.Handled = true;
		}

		private void DragOverPane(object sender, DragEventArgs e)
		{
			StackPanel cur_pane = (StackPanel)sender;
			if (cur_pane == null) return;
		}

		private void DragEnterPane(object sender, DragEventArgs e)
		{
		}

		private void DragLeavePane(object sender, DragEventArgs e)
		{
			StackPanel cur_pane = (StackPanel)sender;
			if (cur_pane == null) return;
		}

		private void DropPaneCategory(object sender, DragEventArgs e)
		{
			StackPanel cur_pane = (StackPanel)sender;
			if (cur_pane == null) return;

			CategoryItem cur_cookie = (e.Data.GetData("cookie_cut") as CategoryItem);
			if (cur_cookie == null) return;

			StackPanel item_pane = cur_cookie.cur_pane;
			Border cur_border = cur_cookie.item_border;
			if (cur_border == null) return;
			if (cur_pane == item_pane) return;

			bool item_changed = false;
			Image item_img = (Image)((Border)((StackPanel)cur_border.Child).Children[0]).Child;
			FossilAPI.FOSSILDETECTInfo cookie_data = (FossilAPI.FOSSILDETECTInfo)item_img.Tag;
			cookie_data.class_no = this.class_no;

			foreach (FossilAPI.FOSSILDETECTInfoList item_list in this.main_win.cookie_list)
			{
				foreach (FossilAPI.FOSSILDETECTInfo item_obj in item_list.fossils)
				{
					if (item_obj.fossil_id == cookie_data.fossil_id)
					{
						item_list.fossils.Remove(item_obj);
						item_list.fossils.Add(cookie_data);
						item_changed = true;
						break;
					}
				}

				if (item_changed)
					break;
			}

			cur_cookie.ToPane(cur_pane);
		}

		private void DropPaneRecognize(object sender, DragEventArgs e)
		{
			StackPanel cur_pane = (StackPanel)sender;
			if (cur_pane == null) return;

			RecognizeItem cur_cookie = (e.Data.GetData("cookie_cut") as RecognizeItem);
			if (cur_cookie == null) return;

			StackPanel item_pane = cur_cookie.cur_pane;
			Border cur_border = cur_cookie.item_border;
			if (cur_border == null) return;
			if (cur_pane == item_pane) return;

			Image item_img = (Image)((Border)((StackPanel)cur_border.Child).Children[0]).Child;
			FossilAPI.FOSSILDETECTInfo cookie_data = (FossilAPI.FOSSILDETECTInfo)item_img.Tag;
			cookie_data.class_no = this.class_no;

			foreach (FossilAPI.FOSSILDETECTInfoList item_list in this.main_win.cookie_list)
			{
				foreach (FossilAPI.FOSSILDETECTInfo cookie_buffer in item_list.fossils)
				{
					if (cookie_buffer.fossil_id == cookie_data.fossil_id)
					{
						item_list.fossils.Remove(cookie_buffer);
						item_list.fossils.Add(cookie_data);
						cur_cookie.ToPane(cur_pane);
						return;
					}
				}
			}
		}
	}
}

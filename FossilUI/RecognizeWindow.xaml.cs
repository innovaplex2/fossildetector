﻿using System;
using System.IO;
using System.ComponentModel;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using OpenCvSharp.CPlusPlus;
using System.Threading;
using System.Windows.Threading;
using System.Xml;

using System.Runtime.ExceptionServices;
using System.Runtime.InteropServices;
using log4net;

namespace FossilUI
{
	/// <summary>
	/// Interaction logic for CategoryWindow.xaml
	/// </summary>
	public partial class RecognizeWindow : System.Windows.Window
	{
		private List<GroupPane> _group_list = new List<GroupPane>();
		private List<CookieData> _item_list = new List<CookieData>();
		private Image _cur_img = null;
		private Cursor _cur_cursor = null;
		private IntPtr _caffe_handle;
		private IntPtr _softmax_handle;
		private string _test_dir = "";
		private string[] _test_img;

		public List<GroupPane> group_list { get { return _group_list; } set { _group_list = value; } }
		public List<CookieData> item_list { get { return _item_list; } set { _item_list = value; } }
		public Image cur_img { get { return _cur_img; } set { _cur_img = value; } }
		public Cursor cur_cursor { get { return _cur_cursor; } set { _cur_cursor = value; } }
		public IntPtr caffe_handle { get { return _caffe_handle; } set { _caffe_handle = value; } }
		public IntPtr softmax_handle { get { return _softmax_handle; } set { _softmax_handle = value; } }
		public string test_dir { get { return _test_dir; } set { _test_dir = value; } }
		public string[] test_img { get { return _test_img; } set { _test_img = value; } }

		private MainWindow main_win = null;
		private bool view_score = false;
		private FossilAPI.ePROCESS_TYPE prc_type = FossilAPI.ePROCESS_TYPE.PROCESS_CPU;
		private ProgressWindow progress_win = null;
		private int prc_cnt = Environment.ProcessorCount;
		//private int batch_size = 600;
		private ScrollViewer item_scroll = null;
		private int move_cnt = 0;
		private bool move_stop = false;
		private Thread move_thread = null;

		public RecognizeWindow()
		{
			InitializeComponent();

			this.Title = CommonUtil.LangData("main_window", "recognizer");
			LABEL_WINDOW_TITLE.Content = CommonUtil.LangData("main_window", "recognizer");
			LABEL_SORT_BY.Content = CommonUtil.LangData("recognizer_window", "sort_by");
			RADIO_SORT_SHAPE.Content = CommonUtil.LangData("recognizer_window", "sort_shape");
			RADIO_SORT_SIZE.Content = CommonUtil.LangData("recognizer_window", "sort_size");
			BUTTON_SAVE_RECOGNIZER.Content = CommonUtil.LangData("recognizer_window", "save");
			BUTTON_NEW_LABEL.Content = CommonUtil.LangData("recognizer_window", "new_label");
			LABEL_UNIDENTIFIED_GROUP.Content = CommonUtil.LangData("recognizer_window", "unidentified_group");

			PANE_UNDEFINED_LIST.Tag = MainWindow.UNDEFINED_GROUP;

			this.main_win = (MainWindow)Application.Current.MainWindow;

			this.main_win.stop_watch.Reset();
			this.main_win.stop_watch.Start();

			this.Width = (System.Windows.Forms.Screen.PrimaryScreen.Bounds.Width * 0.7);
			this.Height = (System.Windows.Forms.Screen.PrimaryScreen.Bounds.Height * 0.7);

			this.view_score = Convert.ToBoolean(CommonUtil.LoadJson("view_score"));
			this.prc_type = Convert.ToString(CommonUtil.LoadJson("prc_type")).ToLower() == "gpu" ? FossilAPI.ePROCESS_TYPE.PROCESS_GPU : FossilAPI.ePROCESS_TYPE.PROCESS_CPU;

			Mouse.OverrideCursor = Cursors.Wait;

			this.test_dir = Convert.ToString(CommonUtil.LoadJson("test_dir"));

			this.PreviewKeyDown += new KeyEventHandler(this.EscKey);
			this.ContentRendered += new EventHandler(this.ContentRender);

			Mouse.OverrideCursor = null;

			this.Show();
		}

		private void ContentRender(object sender, EventArgs e)
		{
			if (this.test_dir != "")
				this.TestFolder();

			this.RunClassifier();
		}

		private void EscKey(object sender, KeyEventArgs e)
		{
			if (e.Key == Key.Escape)
				this.Close();
		}

		private void TestFolder()
		{
			this.main_win.cookie_list.Clear();
			FossilAPI.FOSSILDETECTInfoList t1 = new FossilAPI.FOSSILDETECTInfoList();
			t1.fossils = new List<FossilAPI.FOSSILDETECTInfo>();

			this.test_img = Directory.GetFiles(System.IO.Path.GetFullPath(this.test_dir), "*.jpg", SearchOption.TopDirectoryOnly);

			for (int i = 0; i < this.test_img.Count(); i++)
			{
				FossilAPI.FOSSILDETECTInfo fossil_list = new FossilAPI.FOSSILDETECTInfo();
				fossil_list.image_index = i;
				fossil_list.class_no = MainWindow.UNDEFINED_GROUP;
				t1.fossils.Add(fossil_list);
			}

			this.main_win.cookie_list.Add(t1);
		}

		private void MoveWindow(object sender, MouseButtonEventArgs e)
		{
			if (e.ChangedButton == MouseButton.Left)
			{
				e.Handled = true;
				this.DragMove();
			}
		}

		private void CloseWindow(object sender, RoutedEventArgs e)
		{
			e.Handled = true;

			if (this.main_win.opening_progress)
				return;

			this.Close();
		}

		private void SizeWindow(object sender, RoutedEventArgs e)
		{
			e.Handled = true;

			if (this.WindowState == WindowState.Maximized)
			{
				((Image)BTN_SIZE_WINDOW.Content).Source =
						(ImageSource)new ImageSourceConverter().ConvertFromString("pack://application:,,,/resources/max.win.png");
				this.WindowState = WindowState.Normal;
			}
			else
			{
				((Image)BTN_SIZE_WINDOW.Content).Source =
						(ImageSource)new ImageSourceConverter().ConvertFromString("pack://application:,,,/resources/min.win.png");
				this.WindowState = WindowState.Maximized;
			}
		}

		private void RunClassifier()
		{
			this.group_list.Clear();
			PANE_GROUP_LIST.Children.Clear();

			List<GroupData> group_list = this.main_win.group_list.OrderBy(o => o.order).ToList();

			foreach (GroupData group_data in group_list)
				this.group_list.Add(new GroupPane(PANE_UNDEFINED_LIST, PANE_GROUP_LIST, group_data.name, group_data.idx, this));

			if (this.main_win.cookie_list.Count > 0)
			{
				this.StatusMessage(CommonUtil.LangData("recognizer_window", "recognize_progress"));

				this.main_win.interrupt_code = 1;
				this.progress_win = new ProgressWindow(100, CommonUtil.LangData("recognizer_window", "recognize_progress"), this);
				this.main_win.opening_progress = true;

				BackgroundWorker recognize_worker = new BackgroundWorker();
				recognize_worker.DoWork += new DoWorkEventHandler(this._tRunClassifier);
				recognize_worker.RunWorkerCompleted += (_sender, _e) => this.RecognizeComplete(_sender, _e);
				recognize_worker.RunWorkerAsync();
			}
		}

		[HandleProcessCorruptedStateExceptions]
		private void _tRunClassifier(object sender, DoWorkEventArgs e)
		{
			int per_cnt = 0;
			int batch_start = 0;
			int batch_end = 0;
			int batch_cnt = 0;
			List<CookieData> item_list = new List<CookieData>();
			string caffe_proto = Convert.ToString(CommonUtil.LoadJson("caffe_proto"));
			string caffe_model = Convert.ToString(CommonUtil.LoadJson("caffe_model"));
			string caffe_mean = Convert.ToString(CommonUtil.LoadJson("caffe_mean"));
			int data_size = Convert.ToInt32(CommonUtil.LoadJson("data_size"));

			for (int i = 0; i < this.main_win.cookie_list.Count; i++)
			{
				while (this.main_win.interrupt_code <= 0)
				{
					if (this.main_win.interrupt_code < 0)
						return;
				}

				if (this.main_win.CheckFile(this.main_win.cookie_list[i].image_index) == false)
					continue;

				for (int j = 0; j < this.main_win.cookie_list[i].fossils.Count; j++)
				{
					CookieData recog_buffer = new CookieData();
					FossilAPI.FOSSILDETECTInfo cookie_buffer = this.main_win.cookie_list[i].fossils[j];
					recog_buffer.idx = i;
					recog_buffer.id = j;
					recog_buffer.data = cookie_buffer;

					if (this.main_win.cookie_list[i].fossils[j].proc_step < ProcStep.recognize)
						item_list.Add(recog_buffer);

					this.item_list.Add(recog_buffer);

					this.progress_win.PROGRESS_PER.Dispatcher.Invoke(
						DispatcherPriority.Render,
						new Action(
							() => this.ProgressPer(i * 10 / this.main_win.cookie_list.Count)
						)
					);
				}
			}

			if (this.main_win.interrupt_code <= 0)
				return;

			while (true)
			{
				while (this.main_win.interrupt_code <= 0)
				{
					if (this.main_win.interrupt_code < 0)
						return;
				}

				int batch_size = 0;

				try
				{
					this.caffe_handle = FossilAPI.SetupCaffeClassification(this.prc_type, caffe_proto, caffe_model, caffe_mean, IntPtr.Zero, ref batch_size);
				}
				catch (Exception ex)
				{
					LogManager.GetLogger("FileLogger").Error(ex);

					if (FossilAPI.dev_ver == false)
						LogManager.GetLogger("EmailLogger").Error(ex);
				}

				if (this.main_win.use_cudnn == false && Convert.ToInt32(CommonUtil.LoadJson("batch_count")) > 0)
					batch_size = Convert.ToInt32(CommonUtil.LoadJson("batch_count"));

				batch_end = item_list.Count() < batch_start + batch_size ? item_list.Count() : batch_start + batch_size;
				batch_cnt = batch_end - batch_start;
				int start_pos = 0;
				Mat batch_mat = new Mat(data_size * batch_cnt, data_size, OpenCvSharp.CPlusPlus.MatType.CV_8UC3);
				int[] top_idx = new int[batch_cnt];
				float[] top_score = new float[batch_cnt];
				int[] softmax_idx = new int[batch_cnt];
				float[] softmax_score = new float[batch_cnt];

				for (int i = batch_start; i < batch_end; i++)
				{
					FileData file_data = this.main_win.GetFile(item_list[i].data.image_index);

					Mat item_mat = file_data.mat.SubMat(
						item_list[i].data.rc.top,
						item_list[i].data.rc.bottom,
						item_list[i].data.rc.left,
						item_list[i].data.rc.right
					);

					Mat resized_mat = item_mat.Resize(new OpenCvSharp.CPlusPlus.Size(data_size, data_size));
					resized_mat.CopyTo(batch_mat.SubMat(new OpenCvSharp.CPlusPlus.Rect(0, start_pos, data_size, data_size)));

					start_pos = start_pos + data_size;

					this.progress_win.PROGRESS_PER.Dispatcher.Invoke(
						DispatcherPriority.Render,
						new Action(
							() => this.ProgressPer(per_cnt * 10 / item_list.Count + 10)
						)
					);

					per_cnt++;
				}

				/*Cv2.ImShow("batch image", batch_mat);
				Cv2.WaitKey();*/

				if (per_cnt == 0)
					break;

				try
				{
					FossilAPI.ProcessCaffeClassificationMegredImages(
						this.caffe_handle,
						data_size,
						batch_cnt,
						batch_mat.Data,
						top_idx,
						top_score
					);
				}
				catch (Exception ex)
				{
					LogManager.GetLogger("FileLogger").Error(ex);

					if (FossilAPI.dev_ver == false)
						LogManager.GetLogger("EmailLogger").Error(ex);
				}

				try
				{
					FossilAPI.EndupCaffeClassification(this.caffe_handle);
				}
				catch (Exception ex)
				{
					LogManager.GetLogger("FileLogger").Error(ex);

					if (FossilAPI.dev_ver == false)
						LogManager.GetLogger("EmailLogger").Error(ex);
				}

				int fossil_cnt = 0;

				for (int i = batch_start; i < batch_end; i++)
				{
					FossilAPI.FOSSILDETECTInfo cookie_data = this.main_win.cookie_list[item_list[i].idx].fossils[item_list[i].id];
					GroupData group_data = this.main_win.GetGroup(top_idx[fossil_cnt]);

					cookie_data.cate_class = top_score[fossil_cnt] >= group_data.rmin ? top_idx[fossil_cnt] : MainWindow.UNDEFINED_GROUP;

					if (cookie_data.specify_class == MainWindow.UNDEFINED_GROUP)
						cookie_data.class_no = cookie_data.cate_class;

					cookie_data.top_idx[0] = top_idx[fossil_cnt];
					cookie_data.top_score[0] = top_score[fossil_cnt];
					cookie_data.proc_step = ProcStep.recognize;
					this.main_win.cookie_list[item_list[i].idx].fossils[item_list[i].id] = cookie_data;
					item_list[i].data = cookie_data;

					foreach (CookieData recog_data in this.item_list)
					{
						if (recog_data.idx == item_list[i].idx && recog_data.id == item_list[i].id)
						{
							recog_data.data = cookie_data;
							break;
						}
					}

					fossil_cnt++;

					this.progress_win.PROGRESS_PER.Dispatcher.Invoke(
						DispatcherPriority.Render,
						new Action(
							() => this.ProgressPer(per_cnt * 10 / item_list.Count + 10)
						)
					);

					per_cnt++;
				}

				batch_start += batch_size;

				if (batch_start > item_list.Count)
					break;
			}

			if (this.main_win.interrupt_code <= 0)
				return;

			for (int i = 0; i < this.item_list.Count; i++)
			{
				while (this.main_win.interrupt_code <= 0)
				{
					if (this.main_win.interrupt_code < 0)
						return;
				}

				this.AddCookie(this.item_list[i]);

				this.progress_win.PROGRESS_PER.Dispatcher.Invoke(
					DispatcherPriority.Render,
					new Action(
						() => this.ProgressPer(i * 70 / this.item_list.Count + 30)
					)
				);
			}
		}

		private void RecognizeComplete(object sender, RunWorkerCompletedEventArgs e)
		{
			((ScrollViewer)PANE_UNDEFINED_LIST.Parent).ScrollToTop();

			foreach (GroupPane group_pane in this.group_list)
			{
				ScrollViewer item_scroll = (ScrollViewer)group_pane.box_pane.Children[2];
				item_scroll.ScrollToLeftEnd();
			}

			this.main_win.SelFile();

			this.Activate();
			this.Topmost = true;
			this.Topmost = false;
			this.Focus();
			this.main_win.stop_watch.Stop();
			this.main_win.SetTitle(this.main_win.LIST_FILE_ITEM.Items.Count, this.main_win.fossil_cnt, "recognize ::: " + this.main_win.stop_watch.Elapsed.ToString());
			this.progress_win.Close();
			this.main_win.opening_progress = false;
			this.SortItem(RADIO_SORT_SHAPE, null);
			this.StatusMessage(CommonUtil.LangData("main_window", "complete"));
		}

		private void ProgressPer(int progress_per)
		{
			this.progress_win.PROGRESS_PER.Value = progress_per;
		}

		public void SortItem(object sender, EventArgs e)
		{
			RadioButton cur_btn = (RadioButton)sender;
			if (cur_btn == null) return;
			int cur_sort = Convert.ToInt32(cur_btn.Tag);

			List<CookieData> item_list = (cur_sort == 1) ?
					this.item_list.OrderByDescending(o => ((o.data.rc.right - o.data.rc.left) * (o.data.rc.bottom - o.data.rc.top))).ToList() :
					this.item_list.OrderBy(o => o.data.top_score[0]).ToList();

			foreach (CookieData recog_data in item_list)
			{
				if (recog_data.item == null || recog_data.data.class_no == MainWindow.UNDEFINED_GROUP)
					continue;

				StackPanel item_pane = (StackPanel)recog_data.item.Parent;

				item_pane.Children.Remove(recog_data.item);
				item_pane.Children.Insert(0, recog_data.item);
			}
		}

		public void AddCookie(CookieData recog_data)
		{
			this.Dispatcher.Invoke(
				DispatcherPriority.Render,
				new Action(() => {
					StackPanel start_pane = null;

					if (recog_data.data.class_no == MainWindow.UNDEFINED_GROUP)
						start_pane = PANE_UNDEFINED_LIST;

					if (start_pane == null)
					{
						foreach (GroupPane group_pane in this.group_list)
						{
							if (group_pane.class_no == recog_data.data.class_no)
							{
								start_pane = group_pane.item_pane;
								break;
							}
						}
					}

					if (start_pane == null)
					{
						GroupPane new_group = new GroupPane(PANE_UNDEFINED_LIST, PANE_GROUP_LIST,
								recog_data.data.class_no < MainWindow.UNDEFINED_GROUP ? "Custom Group #"
								+ Math.Abs(recog_data.data.class_no + 1).ToString() : "Group #" + (recog_data.data.class_no + 1).ToString(),
								recog_data.data.class_no, this);
						this.group_list.Add(new_group);
						start_pane = new_group.item_pane;
					}

					if (start_pane != null)
						recog_data.item = (new RecognizeItem(this.main_win, this, recog_data.idx, recog_data.data, PANE_UNDEFINED_LIST, PANE_GROUP_LIST, start_pane, this.view_score)).item_border;
				})
			);
		}

		private void SaveXml(object sender, EventArgs e)
		{
			string file_name = this.main_win.SaveDiag();

			if (file_name != "")
				this.main_win.SaveXml(file_name, PANE_GROUP_LIST, PANE_UNDEFINED_LIST, this);
		}

		private void NewLabel(object sender, EventArgs e)
		{
			int min_idx = this.main_win.group_list.DefaultIfEmpty().Min(l => l.idx) - 1;

			if (min_idx >= MainWindow.UNDEFINED_GROUP)
				min_idx = MainWindow.UNDEFINED_GROUP - 1;

			int max_order = this.main_win.group_list.DefaultIfEmpty().Max(l => l.order) + 1;
			string group_name = "Custom Group #" + Math.Abs(min_idx + 1).ToString();
			this.group_list.Add(new GroupPane(PANE_UNDEFINED_LIST, PANE_GROUP_LIST, group_name, min_idx, this));
			this.main_win.group_list.Add(
				new GroupData()
				{
					idx = min_idx,
					order = max_order,
					rmin = 0,
					amin = 0,
					name = group_name
				}
			);
		}

		private void RefreshItem(object sender, EventArgs e)
		{
			try
			{
				this.RunClassifier();
			}
			finally
			{
				Mouse.OverrideCursor = null;
			}
		}

		private void EnterControl(object sender, MouseButtonEventArgs e)
		{
			move_stop = false;
			move_cnt = 0;
			System.Windows.Point mouse_pos;
			bool move_way = false;
			bool move_buffer = false;
			this.item_scroll = (ScrollViewer)sender;

			move_thread = new Thread(() =>
			{
				while (!move_stop)
				{
					this.item_scroll.Dispatcher.BeginInvoke(new Action(() =>
					{
						mouse_pos = Mouse.GetPosition(this.item_scroll);
						move_way = (mouse_pos.Y <= this.item_scroll.ActualHeight / 2) ? true : false;
						if (move_way != move_buffer) move_cnt = 0;
						move_buffer = move_way;

						if (System.Windows.Input.Mouse.RightButton == MouseButtonState.Pressed)
						{
							move_cnt++;
							this.item_scroll.ScrollToVerticalOffset(this.item_scroll.ContentVerticalOffset + (move_way ? -Math.Pow(move_cnt, 2) : Math.Pow(move_cnt, 2)));
						}
						else
						{
							move_cnt -= 2;

							if (move_cnt <= 0)
							{
								this.item_scroll.ScrollToVerticalOffset(this.item_scroll.ContentVerticalOffset + (move_way ? -1 : 1));
								move_stop = true;
								move_thread.Join();
							}
							else if (move_cnt <= 4)
							{
								this.item_scroll.ScrollToVerticalOffset(this.item_scroll.ContentVerticalOffset + (move_way ? 1 : -1));
							}
							else
							{
								this.item_scroll.ScrollToVerticalOffset(this.item_scroll.ContentVerticalOffset + (move_way ? -Math.Pow(move_cnt, 2) : Math.Pow(move_cnt, 2)));
							}
						}
					}));

					Thread.Sleep(35);
				}
			});

			move_thread.Start();
		}

		private void DropPane(object sender, DragEventArgs e)
		{
			StackPanel cur_pane = (StackPanel)sender;
			if (cur_pane == null) return;

			RecognizeItem cur_cookie = (e.Data.GetData("cookie_cut") as RecognizeItem);
			if (cur_cookie == null) return;

			StackPanel item_pane = cur_cookie.cur_pane;
			Border cur_border = cur_cookie.item_border;
			if (cur_border == null) return;
			if (cur_pane == item_pane) return;

			cur_cookie.ToPane(cur_pane);

			Image item_img = (Image)((Border)((StackPanel)cur_border.Child).Children[0]).Child;
			FossilAPI.FOSSILDETECTInfo cookie_data = (FossilAPI.FOSSILDETECTInfo)item_img.Tag;
			cookie_data.class_no = MainWindow.UNDEFINED_GROUP;

			for (int i = 0; i < this.main_win.cookie_list.Count; i++)
			{
				for (int j = 0; j < this.main_win.cookie_list[i].fossils.Count; j++)
				{
					if (this.main_win.cookie_list[i].fossils[j].fossil_id == cookie_data.fossil_id)
					{
						this.main_win.cookie_list[i].fossils[j] = cookie_data;
						return;
					}
				}
			}
		}

		public void StatusMessage(string msg_txt)
		{
			this.LABEL_CATEGORY_STATUS.Content = msg_txt;
		}

		public void GroupFeedback(bool set_event)
		{
			this.PANE_UNDEFINED_LIST.GiveFeedback -= this.DragFeedback;
			if (set_event) this.PANE_UNDEFINED_LIST.GiveFeedback += this.DragFeedback;

			foreach (GroupPane group_pane in this.group_list)
			{
				group_pane.item_pane.GiveFeedback -= this.DragFeedback;
				if (set_event) group_pane.item_pane.GiveFeedback += this.DragFeedback;
			}
		}

		public void DragFeedback(object sender, GiveFeedbackEventArgs e)
		{
			if (this.cur_img != null)
			{
				try
				{
					if (this.cur_cursor == null)
						this.cur_cursor = CursorHelper.CreateCursor(this.cur_img, 0, 0);

					Mouse.SetCursor(this.cur_cursor);

					e.UseDefaultCursors = false;
					e.Handled = true;
				}
				finally { }
			}
		}

		private void ClosingWindow(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (MessageBox.Show(CommonUtil.LangData("common", "closing_window"), CommonUtil.LangData("common", "confirm"),
					MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
			{
				this.main_win.SelFile();

				using (StreamWriter save_writer = new StreamWriter("custom.group.xml"))
				{
					XmlDocument xml_doc = new XmlDocument();
					xml_doc.LoadXml("<?xml version=\"1.0\" encoding=\"UTF-8\"?><data></data>");
					XmlNode root_node = xml_doc.DocumentElement;

					foreach (GroupData group_data in this.main_win.group_list)
					{
						XmlElement group_node = xml_doc.CreateElement("group");
						group_node.SetAttribute("idx", group_data.idx.ToString());
						group_node.SetAttribute("order", group_data.order.ToString());
						group_node.SetAttribute("rmin", group_data.rmin.ToString());
						group_node.SetAttribute("amin", group_data.amin.ToString());
						group_node.SetAttribute("name", group_data.name.ToString());
						root_node.AppendChild(group_node);
					}

					xml_doc.Save(save_writer);
				}

				e.Cancel = false;
			}
			else
				e.Cancel = true;
		}
	}
}

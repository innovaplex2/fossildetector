﻿using System;
using System.Windows;
using System.Windows.Threading;
using System.Windows.Controls;
using System.Windows.Media;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using System.Xml;
using System.IO;
using OpenCvSharp;
using OpenCvSharp.CPlusPlus;

namespace FossilUI
{
	public partial class MainWindow : System.Windows.Window
	{
		private int cut_cnt = 0;

		private void OpenAnalysis(object sender, EventArgs e)
		{
			System.Windows.Forms.OpenFileDialog open_diag = new System.Windows.Forms.OpenFileDialog();
			open_diag.InitialDirectory = Convert.ToString(Environment.SpecialFolder.Desktop);
			open_diag.Filter = "xml (*.xml)|*.xml";
			open_diag.FilterIndex = 1;

			if (open_diag.ShowDialog() != System.Windows.Forms.DialogResult.Cancel)
				this.OpenAnalysis1(open_diag.FileName);
		}

		private void OpenAnalysis1(string file_name)
		{
			int img_idx = 0;
			string img_name = "";
			string img_path = "";
			Mat img_mat = null;
			int img_step = 0;
			int img_cnt = 0;
			int proc_step = ProcStep.recognize + 1;

			this.run_main = true;

			this.StatusMessage(CommonUtil.LangData("main_window", "analysis_progress"));
			this.progress_win = new ProgressWindow(100, CommonUtil.LangData("main_window", "analysis_check"), false);
			this.opening_progress = true;

			using (StreamReader stream_reader = new StreamReader(file_name, Encoding.UTF8))
			{
				this.load_content = stream_reader.ReadToEnd();
			}

			using (XmlReader xml_reader = XmlReader.Create(new StringReader(this.load_content)))
			{
				if (xml_reader.ReadToDescendant("img"))
				{
					do
					{
						img_path = xml_reader.GetAttribute("path");
						img_step = Convert.ToInt32(xml_reader.GetAttribute("step"));

						xml_reader.ReadStartElement("img");

						if (FossilAPI.light_ver == true)
						{
							if (img_step > ProcStep.category)
							{
								this.progress_win.Close();
								MessageBox.Show(CommonUtil.LangData("main_window", "wrong_xml2"), CommonUtil.LangData("common", "error"),
										MessageBoxButton.OK, MessageBoxImage.Error);
								this.progress_win.Close();
								this.opening_progress = false;
								return;
							}
						}
						else
						{
							if (img_step == ProcStep.category)
							{
								this.progress_win.Close();
								MessageBox.Show(CommonUtil.LangData("main_window", "wrong_xml2"), CommonUtil.LangData("common", "error"),
										MessageBoxButton.OK, MessageBoxImage.Error);
								this.progress_win.Close();
								this.opening_progress = false;
								return;
							}
						}

						if (xml_reader.Value.Trim() != "")
							continue;

						if (File.Exists(img_path) == false)
						{
							this.progress_win.Close();
							MessageBox.Show(CommonUtil.LangData("main_window", "no_file") + " (" + img_path + ")",
									CommonUtil.LangData("common", "error"), MessageBoxButton.OK, MessageBoxImage.Error);
							this.progress_win.Close();
							this.opening_progress = false;
							return;
						}
					}
					while (xml_reader.ReadToFollowing("img"));
				}
			}

			using (XmlReader xml_reader = XmlReader.Create(new StringReader(this.load_content)))
			{
				if (xml_reader.ReadToDescendant("inst"))
				{
					this.inst_id = xml_reader.GetAttribute("id");
					this.inst_depth = xml_reader.GetAttribute("depth");
				}
				else
				{
					MessageBox.Show(CommonUtil.LangData("main_window", "wrong_xml"), CommonUtil.LangData("common", "error"),
							MessageBoxButton.OK, MessageBoxImage.Error);
					this.progress_win.Close();
					this.opening_progress = false;
					return;
				}

				while (xml_reader.ReadToFollowing("img"))
					img_cnt++;
			}

			using (XmlReader xml_reader = XmlReader.Create(new StringReader(this.load_content)))
			{
				if (xml_reader.ReadToDescendant("cut"))
				{
					while (xml_reader.ReadToFollowing("cut"))
						this.cut_cnt++;
				}
			}

			this.SetTitle(file_name);
			this.LoadInit();
			this.progress_win.SetText(CommonUtil.LangData("main_window", "analysis_progress"));

			using (XmlReader xml_reader = XmlReader.Create(new StringReader(this.load_content)))
			{
				if (xml_reader.ReadToDescendant("img"))
				{
					do
					{
						img_idx = Convert.ToInt32(xml_reader.GetAttribute("idx"));
						img_name = xml_reader.GetAttribute("name");
						img_path = xml_reader.GetAttribute("path");
						img_step = Convert.ToInt32(xml_reader.GetAttribute("step"));

						if (File.Exists(img_path) == true)
						{
							img_mat = new Mat(img_path, LoadMode.Color);
						}
						else
						{
							xml_reader.ReadStartElement("img");

							img_path = Path.GetTempPath() + img_name;
							img_mat = Mat.FromImageData(Convert.FromBase64String(xml_reader.Value), LoadMode.Color);
							img_mat.SaveImage(img_path);
						}

						this.AddFile(
							this.file_idx,
							new FileData()
							{
								idx = img_idx,
								step = img_step,
								name = Path.GetFileName(img_path),
								path = img_path,
								check = true,
								mat = img_mat
							}
						);

						if (img_idx >= this.file_idx)
							this.file_idx = img_idx + 1;

						if (proc_step > img_step)
							proc_step = img_step;

						int item_order = -1;

						if (img_step >= ProcStep.analyze)
						{
							foreach (FileData file_data in LIST_FILE_ITEM.Items)
							{
								item_order++;

								if (file_data.idx != img_idx)
									continue;

								LIST_FILE_ITEM.Dispatcher.Invoke((Action)(() =>
								{
									ListBoxItem list_item = null;

									try
									{
										list_item = LIST_FILE_ITEM.ItemContainerGenerator.ContainerFromIndex(item_order) as ListBoxItem;
									}
									catch (Exception ex)
									{
									}

									if (list_item == null)
									{
										LIST_FILE_ITEM.UpdateLayout();
										LIST_FILE_ITEM.ScrollIntoView(LIST_FILE_ITEM.Items[item_order]);
										list_item = LIST_FILE_ITEM.ItemContainerGenerator.ContainerFromIndex(item_order) as ListBoxItem;
									}

									TextBlock file_label = CommonUtil.FindVisualChild<TextBlock>(list_item);

									file_label.Foreground = Brushes.Yellow;
								}));

								break;
							}
						}

						this.progress_win.PROGRESS_PER.Dispatcher.Invoke(
							DispatcherPriority.Render,
							new Action(
								() => this.ProgressPer(LIST_FILE_ITEM.Items.Count * 50 / img_cnt)
							)
						);
					}
					while (xml_reader.ReadToFollowing("img"));

					if (LIST_FILE_ITEM.Items.Count > 0)
					{
						LIST_FILE_ITEM.SelectedIndex = 0;
						LIST_FILE_ITEM.ScrollIntoView(LIST_FILE_ITEM.Items[0]);
					}

					this.ButtonStatus(proc_step <= ProcStep.recognize ? proc_step : -1);
					this.OpenAnalysis2();
				}
			}
		}

		public void OpenAnalysis2()
		{
			int prc_cnt = 0;
			int img_idx = 0;
			int class_no = MainWindow.UNDEFINED_GROUP;
			int proc_step = 0;
			int pos_top = 0;
			int pos_right = 0;
			int pos_bottom = 0;
			int pos_left = 0;
			int rgb_cnt = 0;
			int hu_cnt = 0;
			int desc_cnt = 0;
			int specify_class = MainWindow.UNDEFINED_GROUP;
			int cate_class = MainWindow.UNDEFINED_GROUP;
			string group_name = "";
			int prev_img = -1;

			using (XmlReader xml_reader = XmlReader.Create(new StringReader(this.load_content)))
			{
				if (xml_reader.ReadToDescendant("cut"))
				{
					do
					{
						FossilAPI.FOSSILDETECTInfo fossil_data = new FossilAPI.FOSSILDETECTInfo();

						img_idx = Convert.ToInt32(xml_reader.GetAttribute("img"));
						class_no = Convert.ToInt32(xml_reader.GetAttribute("class"));
						proc_step = Convert.ToInt32(xml_reader.GetAttribute("step"));
						specify_class = Convert.ToInt32(xml_reader.GetAttribute("sclass"));
						cate_class = Convert.ToInt32(xml_reader.GetAttribute("cclass"));
						string[] fossil_pos = xml_reader.GetAttribute("pos").Split(',');
						pos_left = Convert.ToInt32(fossil_pos[0]);
						pos_top = Convert.ToInt32(fossil_pos[1]);
						pos_right = Convert.ToInt32(fossil_pos[2]);
						pos_bottom = Convert.ToInt32(fossil_pos[3]);
						string[] val_cnt = xml_reader.GetAttribute("cnt").Split(',');
						rgb_cnt = Convert.ToInt32(val_cnt[0]);
						hu_cnt = Convert.ToInt32(val_cnt[1]);
						desc_cnt = Convert.ToInt32(val_cnt[2]);

						fossil_data.fossil_id = prc_cnt;
						fossil_data.image_index = img_idx;
						fossil_data.specify_class = specify_class;
						fossil_data.cate_class = cate_class;
						fossil_data.class_no = class_no;
						fossil_data.rc.left = pos_left;
						fossil_data.rc.top = pos_top;
						fossil_data.rc.right = pos_right;
						fossil_data.rc.bottom = pos_bottom;

						fossil_data.rgb_mean = new float[rgb_cnt];
						fossil_data.hu_moments = new double[hu_cnt];
						fossil_data.descriptors = new short[desc_cnt];
						fossil_data.top_idx = new int[5];
						fossil_data.top_score = new float[5];

						if (xml_reader.ReadToFollowing("rgb"))
						{
							xml_reader.ReadStartElement("rgb");
							string[] val_rgb = xml_reader.Value.Split(',');

							for (int i = 0; i < val_rgb.Length; i++)
								fossil_data.rgb_mean[i] = Convert.ToSingle(val_rgb[i]);
						}

						if (xml_reader.ReadToFollowing("hu"))
						{
							xml_reader.ReadStartElement("hu");
							string[] val_hu = xml_reader.Value.Split(',');

							for (int i = 0; i < val_hu.Length; i++)
								fossil_data.hu_moments[i] = Convert.ToDouble(val_hu[i]);
						}

						if (xml_reader.ReadToFollowing("desc"))
						{
							xml_reader.ReadStartElement("desc");
							string[] val_desc = xml_reader.Value.Split(',');

							for (int i = 0; i < val_desc.Length; i++)
								fossil_data.descriptors[i] = Convert.ToInt16(val_desc[i]);
						}

						if (xml_reader.ReadToFollowing("rank"))
						{
							xml_reader.ReadStartElement("rank");
							string[] val_rank = xml_reader.Value.Split(',');

							for (int i = 0; i < val_rank.Length; i++)
							{
								string[] top_rank = val_rank[i].Split(':');
								fossil_data.top_idx[i] = Convert.ToInt32(top_rank[0]);
								fossil_data.top_score[i] = Convert.ToSingle(top_rank[1]);
							}
						}

						bool list_update = false;

						foreach (FossilAPI.FOSSILDETECTInfoList fossil_list in this.cookie_list)
						{
							if (fossil_list.image_index == img_idx)
							{
								fossil_list.fossils.Add(fossil_data);
								list_update = true;
								break;
							}
						}

						if (list_update == false)
						{
							FossilAPI.FOSSILDETECTInfoList fossil_list = new FossilAPI.FOSSILDETECTInfoList();
							fossil_list.image_index = img_idx;
							fossil_list.fossils = new List<FossilAPI.FOSSILDETECTInfo>();
							fossil_list.fossils.Add(fossil_data);
							this.cookie_list.Add(fossil_list);
						}

						prc_cnt++;

						this.progress_win.PROGRESS_PER.Dispatcher.Invoke(
							new Action(
								() => this.ProgressPer(prc_cnt * 50 / this.cut_cnt + 50)
							),
							System.Windows.Threading.DispatcherPriority.Background
						);
					}
					while (xml_reader.ReadToFollowing("cut"));
				}
			}

			using (XmlReader xml_reader = XmlReader.Create(new StringReader(this.load_content)))
			{
				if (xml_reader.ReadToDescendant("group"))
				{
					do
					{
						class_no = Convert.ToInt32(xml_reader.GetAttribute("class"));
						xml_reader.ReadStartElement("group");
						group_name = xml_reader.Value;
						CommonUtil.GroupName(class_no, group_name);
					}
					while (xml_reader.ReadToFollowing("group"));
				}
			}

			this.SelFile();
			this.progress_win.Close();
			this.opening_progress = false;
			this.StatusMessage(CommonUtil.LangData("main_window", "complete"));
		}
	}
}

﻿using System;
using System.Windows.Controls;
using System.Runtime.InteropServices;
using System.Collections.Generic;

namespace FossilUI
{
	public class FossilAPI
	{
		public const bool light_ver = false;
		public const bool dev_ver = false;
		public enum eFOSSILE_TYPE { FOSSIL_TYPE_BLOB, FOSSIL_TYPE_LINE };
		public enum eFOSSILE_UPDATE_FLAG { FOSSIL_UPDATE_NONE, FOSSIL_UPDATE_UPDATE } ;
		public enum ePROCESS_TYPE { PROCESS_CPU, PROCESS_GPU } ;

		public struct RECT
		{
			public int left;
			public int top;
			public int right;
			public int bottom;
		};

		public struct FOSSILDETECTInfo
		{
			public eFOSSILE_UPDATE_FLAG update_flag;		// Update flag

			public int proc_step;
			public int fossil_id;
			public int image_index;
			public int specify_class;
			public int cate_class;
			public int class_no;
			public float class_distance;	// distance in class (low score is close)

			public RECT rc;
			public eFOSSILE_TYPE type;

			public float mean;			//normalize by global mean
			public float stddev;

			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 3)]
			public float[] rgb_mean;

			public float width;
			public float height;
			public float angle;
			public float area;

			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 7)]
			public double[] hu_moments;

			public int size_of_descriptor;
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 1764)]
			public short[] descriptors;

			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 5)]
			public int[] top_idx;
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 5)]
			public float[] top_score;

			public int num_of_feature;
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 2)]
			public float[] keypoints;
		}

		public struct FOSSILDETECTInfoList
		{
			public int image_index;
			public List<FOSSILDETECTInfo> fossils;
		}

		[DllImport(light_ver ? "FossilDetector.dll" : "FossilDetectorDK.dll")]
		public static extern IntPtr SetupFossilDetector([MarshalAs(UnmanagedType.LPStr)]String szInputFile);
		[DllImport(light_ver ? "FossilDetector.dll" : "FossilDetectorDK.dll")]
		public static extern bool ProcessFossilDetector(IntPtr handle, int width, int height, int step, IntPtr image,
				ref int pMaxDetectCount);
		[DllImport(light_ver ? "FossilDetector.dll" : "FossilDetectorDK.dll")]
		public static extern bool GetFossilObject(IntPtr handle, [Out] FOSSILDETECTInfo[] info, ref int pDetectCount);
		[DllImport(light_ver ? "FossilDetector.dll" : "FossilDetectorDK.dll")]
		public static extern bool UpdateFossilObject2(IntPtr handle, int nDetectCount, [In] FOSSILDETECTInfo[] in_info,
				[Out] FOSSILDETECTInfo[] out_info);
		[DllImport(light_ver ? "FossilDetector.dll" : "FossilDetectorDK.dll")]
		public static extern bool ProcessUpdateFossilObject2(IntPtr handle, int width, int height, int step, IntPtr image,
				int nDetectCount, [In] FOSSILDETECTInfo[] in_info, [Out] FOSSILDETECTInfo[] out_info);
		[DllImport(light_ver ? "FossilDetector.dll" : "FossilDetectorDK.dll")]
		public static extern void EndupFossilDetector(IntPtr handle);

		[DllImport(light_ver ? "FossilClassifier.dll" : "FossilClassifier2.dll")]
		public static extern IntPtr CreateClassifier();
		[DllImport(light_ver ? "FossilClassifier.dll" : "FossilClassifier2.dll")]
		public static extern bool ProcessClassification(IntPtr handle, int nTotalFossil, [In] FOSSILDETECTInfo[] in_info);
		[DllImport(light_ver ? "FossilClassifier.dll" : "FossilClassifier2.dll")]
		public static extern bool ProcessClassification2(IntPtr handle, int nTotalFossil, [In] FOSSILDETECTInfo[] in_info,
				[Out] FOSSILDETECTInfo[] out_info);
		[DllImport(light_ver ? "FossilClassifier.dll" : "FossilClassifier2.dll")]
		public static extern void DestroyClassifier(IntPtr handle);

		// license module
		[DllImport("InnovaLicenseGenLib.dll", CallingConvention = CallingConvention.Cdecl)]
		public static extern int VailedInnovaLicense(
							[MarshalAs(UnmanagedType.LPStr)]String szLicense,
							ref int nLicenseCnt,
							[Out] InnovaLicenseInfo[] pLicenseInfo,
							[MarshalAs(UnmanagedType.LPStr)]String SystemMACAddr
							);

		/*[DllImport("Caffe.dll")]
		public static extern int Fossil_Recognizer(
			ePROCESS_TYPE prc_type,
			[MarshalAs(UnmanagedType.LPStr)]String Prototxt,
			[MarshalAs(UnmanagedType.LPStr)]String Caffemodel,
			//[MarshalAs(UnmanagedType.LPStr)]String LabelTXT,
			int NumOfLable,
			int InputDataSize,
			IntPtr InputImg,
			int[] Top5_Idx,
			float[] Top5_Score
		);*/

		/*[DllImport("libcaffe.dll")]
		public static extern void InitCaffeClassification();*/
		[DllImport(light_ver ? "libcaffe_cpu.dll" : "libcaffe.dll")]
		public static extern IntPtr SetupCaffeClassification(
			ePROCESS_TYPE type,
			[MarshalAs(UnmanagedType.LPStr)]String Prototxt,
			[MarshalAs(UnmanagedType.LPStr)]String Caffemodel,
			[MarshalAs(UnmanagedType.LPStr)]String MeanBinaryFile,
			IntPtr PixelValue,
			ref int max_net_count
		);
		[DllImport(light_ver ? "libcaffe_cpu.dll" : "libcaffe.dll")]
		public static extern void EndupCaffeClassification(IntPtr handle);
		[DllImport(light_ver ? "libcaffe_cpu.dll" : "libcaffe.dll")]
		public static extern int ProcessCaffeClassification(
			/*ePROCESS_TYPE type,
			[MarshalAs(UnmanagedType.LPStr)]String Prototxt,
			[MarshalAs(UnmanagedType.LPStr)]String Caffemodel,
			[MarshalAs(UnmanagedType.LPStr)]String MeanBinaryFile,*/
			IntPtr handle,
			int InputDataSize,
			IntPtr InputImg,
			int[] Top5_Idx,
			float[] Top5_Score
		);
		[DllImport(light_ver ? "libcaffe_cpu.dll" : "libcaffe.dll")]
		public static extern int ProcessCaffeClassificationMegredImages(
			/*ePROCESS_TYPE type,
			[MarshalAs(UnmanagedType.LPStr)]String Prototxt,
			[MarshalAs(UnmanagedType.LPStr)]String Caffemodel,
			[MarshalAs(UnmanagedType.LPStr)]String MeanBinaryFile,*/
			IntPtr handle,
			int InputDataSize,
			int nScoreCnt,
			IntPtr InputImg,
			int[] Top5_Idx,
			float[] Top5_Score
		);
		/*[DllImport("libcaffe2.dll")]
		public static extern IntPtr SetupCaffeClassification2(
			ePROCESS_TYPE type,
			[MarshalAs(UnmanagedType.LPStr)]String Prototxt,
			[MarshalAs(UnmanagedType.LPStr)]String Caffemodel,
			[MarshalAs(UnmanagedType.LPStr)]String MeanBinaryFile,
			int file_cnt
		);
		[DllImport("libcaffe2.dll")]
		public static extern void EndupCaffeClassification2(IntPtr handle);
		[DllImport("libcaffe2.dll")]
		public static extern int ProcessCaffeClassification2(
			IntPtr handle,
			int InputDataSize,
			IntPtr InputImg,
			int nScoreCnt,
			int[] Top5_Idx,
			float[] Top5_Score
		);*/
	}
}

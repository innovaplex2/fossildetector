﻿using System;
using System.Windows;
using System.Threading;
using log4net;
using log4net.Config;

namespace FossilUI
{
	/// <summary>
	/// Interaction logic for App.xaml
	/// </summary>
	public partial class App : Application
	{
		private static Mutex cur_mutex = null;
		public bool app_handled { get; set; }

		protected override void OnStartup(StartupEventArgs e)
		{
			const string app_name = "Innova Plex Fossil Detector";
			bool new_instance = false;

			cur_mutex = new Mutex(true, app_name, out new_instance);

			if (new_instance == false)
				Application.Current.Shutdown();

			base.OnStartup(e);
		}

		private void AppStart(object sender, StartupEventArgs e)
		{
			log4net.Config.XmlConfigurator.Configure();
			this.app_handled = false;
			AppDomain.CurrentDomain.UnhandledException += new UnhandledExceptionEventHandler(LogException);
		}

		void LogException(object sender, UnhandledExceptionEventArgs e)
		{
			Exception ex = e.ExceptionObject as Exception;
			LogManager.GetLogger("FileLogger").Error(ex);

			if (FossilAPI.dev_ver == false)
				LogManager.GetLogger("EmailLogger").Error(ex);
		}

		private void LogException(object sender, System.Windows.Threading.DispatcherUnhandledExceptionEventArgs e)
		{
			Exception ex = e.Exception;
			LogManager.GetLogger("FileLogger").Error(ex);

			if (FossilAPI.dev_ver == false)
				LogManager.GetLogger("EmailLogger").Error(ex);

			if (this.app_handled == true)
				e.Handled = true;
			else
				e.Handled = false;
		}
	}
}

﻿using OpenCvSharp.CPlusPlus;

namespace FossilUI
{
	public class FileData
	{
		public int idx { get; set; }
		public int step { get; set; }
		public string name { get; set; }
		public string path { get; set; }
		public bool check { get; set; }
		public Mat mat { get; set; }
	}
}

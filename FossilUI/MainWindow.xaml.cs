﻿using System;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.ComponentModel;
using System.Web.Script.Serialization;
using System.Diagnostics;
using OpenCvSharp.CPlusPlus;

namespace FossilUI
{
	/// <summary>
	/// Interaction logic for MainWindow.xaml
	/// </summary>
	/// 
	public partial class MainWindow : System.Windows.Window
	{
		public static int UNDEFINED_FOSSIL = -1;
		public static int UNDEFINED_GROUP = -1;

		private List<FossilAPI.FOSSILDETECTInfoList> _cookie_list = new List<FossilAPI.FOSSILDETECTInfoList>(); // Cookie Cut
		private List<GroupData> _group_data = new List<GroupData>(); // Group Panel
		private List<GroupData> _group_list = new List<GroupData>(); // Group Panel
		private int _file_idx = 0;
		private Mat _disp_img = null;
		private string _inst_id = "";
		private string _inst_depth = "";
		private dynamic _ui_json = null;
		private dynamic _lang_json = null;
		private int _fossil_id = 0;
		private int[][] _top_idx;
		private float[][] _top_score;
		private IList<BackgroundWorker> _worker_list = null;
		private bool _opening_progress = false;
		private int _fossil_cnt = 0;
		private int _list_type = 0;
		private ProgressWindow _progress_win = null;
		private Stopwatch _stop_watch = new Stopwatch();
		private int _interrupt_code = 0;
		private int _start_order = 0;
		private bool _use_cudnn = false;
		private int _list_status = 0;
		private bool _open_cookie = false;
		private IntPtr _fossil_detector = IntPtr.Zero;

		public List<FossilAPI.FOSSILDETECTInfoList> cookie_list { get { return _cookie_list; } set { _cookie_list = value; } }
		public List<GroupData> group_data { get { return _group_data; } set { _group_data = value; } }
		public List<GroupData> group_list { get { return _group_list; } set { _group_list = value; } }
		public int file_idx { get { return _file_idx; } set { _file_idx = value; } }
		public Mat disp_img { get { return _disp_img; } set { _disp_img = value; } }
		public string inst_id { get { return _inst_id; } set { _inst_id = value; } }
		public string inst_depth { get { return _inst_depth; } set { _inst_depth = value; } }
		public dynamic ui_json { get { return _ui_json; } set { _ui_json = value; } }
		public dynamic lang_json { get { return _lang_json; } set { _lang_json = value; } }
		public int fossil_id { get { return _fossil_id; } set { _fossil_id = value; } }
		public int[][] top_idx { get { return _top_idx; } set { _top_idx = value; } }
		public float[][] top_score { get { return _top_score; } set { _top_score = value; } }
		public IList<BackgroundWorker> worker_list { get { return _worker_list; } set { _worker_list = value; } }
		public bool opening_progress { get { return _opening_progress; } set { _opening_progress = value; } }
		public int fossil_cnt { get { return _fossil_cnt; } set { _fossil_cnt = value; } }
		public int list_type { get { return _list_type; } set { _list_type = value; } }
		public ProgressWindow progress_win { get { return _progress_win; } set { _progress_win = value; } }
		public Stopwatch stop_watch { get { return _stop_watch; } set { _stop_watch = value; } }
		public int interrupt_code { get { return _interrupt_code; } set { _interrupt_code = value; } }
		public int start_order { get { return _start_order; } set { _start_order = value; } }
		public bool use_cudnn { get { return _use_cudnn; } set { _use_cudnn = value; } }
		public int list_status { get { return _list_status; } set { _list_status = value; } }
		public bool open_cookie { get { return _open_cookie; } set { _open_cookie = value; } }
		public IntPtr fossil_detector { get { return _fossil_detector; } set { _fossil_detector = value; } }
		public GridLength bbb;

		private string load_content = "";
		private int sort_cookie = 10;
		private bool run_main = false;
		private StackPanel focus_pane = null;
		//private ListMode list_node;

		public MainWindow()
		{
			InitializeComponent();

			try
			{
				this.ui_json = (new JavaScriptSerializer()).Deserialize<dynamic>(System.IO.File.ReadAllText(@"ui.json"));
			}
			catch (Exception ex)
			{
				MessageBox.Show("There are no required file. : " + ex.ToString(), "Error", MessageBoxButton.OK, MessageBoxImage.Error);
				Application.Current.Shutdown();
			}

			try
			{
				this.lang_json = (new JavaScriptSerializer()).Deserialize<dynamic>(System.IO.File.ReadAllText(@"lang." + ui_json["app_lang"] + ".json"));
			}
			catch (Exception ex)
			{
				MessageBox.Show("There are no required file. : " + ex.ToString(), "Error", MessageBoxButton.OK, MessageBoxImage.Error);
				Application.Current.Shutdown();
			}

			//string StartupDirEndingWithSlash = System.IO.Path.GetDirectoryName(System.Diagnostics.Process.GetCurrentProcess().MainModule.FileName) + "\\";
			//string ResolvedDomainTimeFileName = StartupDirEndingWithSlash + "ABCLib_Resolved.dll";
			this.use_cudnn = Convert.ToBoolean(CommonUtil.LoadJson("use_cudnn"));

			try
			{
				File.Delete("libcaffe.dll");
			}
			catch(Exception ex)
			{
				MessageBox.Show("There are no permission to write file. : " + ex.ToString(), "Error", MessageBoxButton.OK, MessageBoxImage.Error);
				Application.Current.Shutdown();
			}

			try
			{
				File.Copy(this.use_cudnn == true ? "libcaffe_cudnn.dll" : "libcaffe_normal.dll", "libcaffe.dll");
			}
			catch(Exception ex)
			{
				MessageBox.Show("There are no permission to write file. : " + ex.ToString(), "Error", MessageBoxButton.OK, MessageBoxImage.Error);
				Application.Current.Shutdown();
			}

			if (File.Exists("Innova_license.lic") == false)
			{
				ActivationWindow activation_window = new ActivationWindow();
				activation_window.ShowDialog();
				//MessageBox.Show("There are no license file.", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
				Application.Current.Shutdown();
				return;
			}

			string license_text = System.IO.File.ReadAllText("Innova_license.lic");
			string mac_addr = "";
			//IntPtr mac_addr = IntPtr.Zero;
			int license_count = 0;
			InnovaLicenseInfo[] license_buffer = new InnovaLicenseInfo[16];
			//InnovaLicenseInfo license_info;
			bool license_exist = false;

			if (FossilAPI.VailedInnovaLicense(license_text, ref license_count, license_buffer, mac_addr) == 1)
			{
				for (int i = 0; i < license_count; i++){
					if (license_buffer[i].licenseType == InnovaPlexeLicenseType.INNOVA_LICENSE_FOSSIL_DETECTOR) {
						//license_info = license_buffer[i];
						license_exist = true;
						break;
					}
				}
			}

			if (license_exist == false)
			{
				MessageBox.Show("There are no license data.", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
				Application.Current.Shutdown();
				return;
			}

			BTN_MAIN_LOAD.Content = CommonUtil.LangData("main_window", "load");
			BTN_MAIN_ANALYZE.Content = CommonUtil.LangData("main_window", "analyze");
			BTN_MAIN_STAT.Content = CommonUtil.LangData("main_window", "statistic");
			BTN_MAIN_CATEGORY.Content = CommonUtil.LangData("main_window", "category");
			BTN_MAIN_RECOGNIZER.Content = CommonUtil.LangData("main_window", "recognizer");
			BTN_MAIN_ANALYSIS.Content = CommonUtil.LangData("main_window", "open_analysis");
			BTN_MAIN_SAVE.Content = CommonUtil.LangData("main_window", "save_all");
			BTN_MAIN_CONFIG.Content = CommonUtil.LangData("main_window", "configure");
			BTN_MAIN_OPENC.Content = CommonUtil.LangData("main_window", "open_cookie");

			((Label)((StackPanel)((Border)PANE_COOKIE_HEADER.Children[0]).Child).Children[0]).Content = CommonUtil.LangData("main_window", "fossil_list_cut");
			((Label)((StackPanel)((Button)PANE_COOKIE_HEADER.Children[1]).Content).Children[0]).Content = CommonUtil.LangData("main_window", "fossil_list_class");
			((Label)((StackPanel)((Button)PANE_COOKIE_HEADER.Children[2]).Content).Children[0]).Content = CommonUtil.LangData("main_window", "fossil_list_left");
			((Label)((StackPanel)((Button)PANE_COOKIE_HEADER.Children[3]).Content).Children[0]).Content = CommonUtil.LangData("main_window", "fossil_list_top");
			((Label)((StackPanel)((Button)PANE_COOKIE_HEADER.Children[4]).Content).Children[0]).Content = CommonUtil.LangData("main_window", "fossil_list_width");
			((Label)((StackPanel)((Button)PANE_COOKIE_HEADER.Children[5]).Content).Children[0]).Content = CommonUtil.LangData("main_window", "fossil_list_height");
			((Label)((StackPanel)((Button)PANE_COOKIE_HEADER.Children[6]).Content).Children[0]).Content = CommonUtil.LangData("main_window", "fossil_list_size");

			if (Convert.ToSingle(CommonUtil.LoadJson("window_width")) < this.MinWidth
					|| Convert.ToSingle(CommonUtil.LoadJson("window_width")) > System.Windows.Forms.Screen.PrimaryScreen.Bounds.Width * 0.8)
				this.Width = (System.Windows.Forms.Screen.PrimaryScreen.Bounds.Width * 0.75);
			else
				this.Width = Convert.ToSingle(CommonUtil.LoadJson("window_width"));

			if (Convert.ToSingle(CommonUtil.LoadJson("window_height")) < this.MinHeight
					|| Convert.ToSingle(CommonUtil.LoadJson("window_height")) > System.Windows.Forms.Screen.PrimaryScreen.Bounds.Height * 0.8)
				this.Height = (System.Windows.Forms.Screen.PrimaryScreen.Bounds.Height * 0.75);
			else
				this.Height = Convert.ToSingle(CommonUtil.LoadJson("window_height"));

			this.ButtonStatus(-1);

			if (Convert.ToSingle(CommonUtil.LoadJson("splitter_left")) > 0)
			{
				GRID_MAIN_WINDOW.ColumnDefinitions[0].Width = new GridLength(Convert.ToSingle(CommonUtil.LoadJson("splitter_left")), GridUnitType.Star);
				GRID_MAIN_WINDOW.ColumnDefinitions[1].Width = new GridLength(100 - Convert.ToSingle(CommonUtil.LoadJson("splitter_left")), GridUnitType.Star);
			}

			if (Convert.ToSingle(CommonUtil.LoadJson("splitter_top")) > 0)
			{
				GRID_MAIN_WINDOW.RowDefinitions[2].Height = new GridLength(Convert.ToSingle(CommonUtil.LoadJson("splitter_top")), GridUnitType.Star);
				GRID_MAIN_WINDOW.RowDefinitions[3].Height = new GridLength(100 - Convert.ToSingle(CommonUtil.LoadJson("splitter_top")), GridUnitType.Star);
			}

			this.list_type = Convert.ToInt32(CommonUtil.LoadJson("list_type"));
			this.ListType();

			if (FossilAPI.light_ver == true)
				BTN_MAIN_RECOGNIZER.Visibility = Visibility.Hidden;
			else
				BTN_MAIN_CATEGORY.Visibility = Visibility.Hidden;

			this.KeyUp += new KeyEventHandler(this.MainKey);
			LIST_FILE_ITEM.SelectionChanged += new SelectionChangedEventHandler(this.SelFile);
			this.ContentRendered += new EventHandler(ContentRender);

			this.StatusMessage(CommonUtil.LangData("main_window", "ready"));
		}

		private void MainKey(object sender, KeyEventArgs e)
		{
			if (e.Key == Key.Delete)
			{
				e.Handled = true;
				if (this.focus_pane == null) return;
				this.RemoveCookie(this.focus_pane);
			}
			else if (e.Key == Key.Escape)
			{
				e.Handled = true;
				this.Close();
			}
		}

		private void ContentRender(object sender, EventArgs e)
		{
			if (File.Exists("group.xml") == false)
			{
				MessageBox.Show("There are no required file. : group.xml", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
				Application.Current.Shutdown();
				return;
			}

			using (StreamReader stream_reader = new StreamReader("group.xml", Encoding.UTF8))
			{
				using (XmlReader xml_reader = XmlReader.Create(new StringReader(stream_reader.ReadToEnd())))
				{
					if (xml_reader.ReadToDescendant("group"))
					{
						do
						{
							this.group_data.Add(
								new GroupData
								{
									idx = Convert.ToInt32(xml_reader.GetAttribute("idx")),
									order = Convert.ToInt32(xml_reader.GetAttribute("order")),
									rmin = Convert.ToSingle(xml_reader.GetAttribute("rmin")),
									amin = Convert.ToSingle(xml_reader.GetAttribute("amin")),
									name = xml_reader.GetAttribute("name")
								}
							);
						}
						while (xml_reader.ReadToFollowing("group"));
					}
				}
			}

			this.InitGroup();

			if (Convert.ToBoolean(CommonUtil.LoadJson("use_session")) == true && File.Exists("session.xml") == true)
			{
				if (MessageBox.Show(CommonUtil.LangData("main_window", "open_last"), CommonUtil.LangData("common", "confirm"),
						MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
					this.OpenAnalysis1("session.xml");
			}
		}

		private void MoveWindow(object sender, MouseButtonEventArgs e)
		{
			if (e.ChangedButton == MouseButton.Left)
			{
				e.Handled = true;
				this.DragMove();
			}
		}

		private void ClosingWindow(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (this.run_main == true)
			{
				if (MessageBox.Show(CommonUtil.LangData("common", "closing_window"), CommonUtil.LangData("common", "confirm"),
						MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
				{
					if (Convert.ToBoolean(CommonUtil.LoadJson("use_session")) == true)
					{
						if (LIST_FILE_ITEM.Items.Count > 0)
							this.SaveXml("session.xml");
						else
							File.Delete("session.xml");
					}

					CommonUtil.SaveJson("window_width", this.ActualWidth);
					CommonUtil.SaveJson("window_height", this.ActualHeight);

					string grid_left = GRID_MAIN_WINDOW.ColumnDefinitions[0].Width.ToString();
					grid_left = grid_left.Replace("*", "");
					grid_left = grid_left == "" ? "0" : grid_left;

					if (Convert.ToSingle(grid_left) > 100)
					{
						double splitter_left = Convert.ToSingle(grid_left) * 100 / this.ActualWidth;
						CommonUtil.SaveJson("splitter_left", splitter_left);
					}

					string grid_top = GRID_MAIN_WINDOW.RowDefinitions[2].Height.ToString();
					grid_top = grid_top.Replace("*", "");
					grid_top = grid_top == "" ? "0" : grid_top;

					if (Convert.ToSingle(grid_top) > 100)
					{
						double splitter_top = Convert.ToSingle(grid_top) * 100 / (this.ActualHeight - 120);
						CommonUtil.SaveJson("splitter_top", splitter_top);
					}

					CommonUtil.SaveJson("list_type", this.list_type);

					e.Cancel = false;
				}
				else
				{
					e.Cancel = true;
				}
			}
			else
			{
				e.Cancel = false;
			}
		}

		private void CloseWindow(object sender, RoutedEventArgs e)
		{
			e.Handled = true;

			if (Application.Current.Windows.Count > 1)
				return;

			this.Close();
		}

		private void SizeWindow(object sender, RoutedEventArgs e)
		{
			e.Handled = true;

			if (this.WindowState == WindowState.Maximized)
			{
				((Image)BTN_SIZE_WINDOW.Content).Source =
						(ImageSource)new ImageSourceConverter().ConvertFromString("pack://application:,,,/resources/max.win.png");
				this.WindowState = WindowState.Normal;
			}
			else
			{
				((Image)BTN_SIZE_WINDOW.Content).Source =
						(ImageSource)new ImageSourceConverter().ConvertFromString("pack://application:,,,/resources/min.win.png");
				this.WindowState = WindowState.Maximized;
			}
		}

		private void ResizeWindow(object sender, SizeChangedEventArgs e)
		{
			LABEL_PROGRAM_TITLE.MaxWidth = this.Width - 120;
		}

		public void StatusMessage(string msg_txt)
		{
			((MainWindow)Application.Current.MainWindow).LABEL_PROGRAM_STATUS.Content = msg_txt;
		}

		private void ResizeCookie(object sender, SizeChangedEventArgs e)
		{
			this.ResizeCookie();
		}

		private void ResizeCookie()
		{
			double list_width = PANE_COOKIE_LIST.ActualWidth;
			int child_cnt = 0;

			((Border)PANE_COOKIE_HEADER.Children[0]).Width = (list_width * 0.20);
			((Button)PANE_COOKIE_HEADER.Children[1]).Width = (list_width * 0.20);
			((Button)PANE_COOKIE_HEADER.Children[2]).Width = (list_width * 0.12);
			((Button)PANE_COOKIE_HEADER.Children[3]).Width = (list_width * 0.12);
			((Button)PANE_COOKIE_HEADER.Children[4]).Width = (list_width * 0.12);
			((Button)PANE_COOKIE_HEADER.Children[5]).Width = (list_width * 0.12);
			((Button)PANE_COOKIE_HEADER.Children[6]).Width = (list_width * 0.12);

			foreach (object node_child in PANE_COOKIE_LIST.Children)
			{
				if (node_child is StackPanel)
				{
					StackPanel list_pane = (StackPanel)node_child;

					((StackPanel)list_pane.Children[0]).Width = (list_width * 0.20);
					((Border)list_pane.Children[1]).Width = (list_width * 0.20);
					((Label)list_pane.Children[2]).Width = (list_width * 0.12);
					((Label)list_pane.Children[3]).Width = (list_width * 0.12);
					((Label)list_pane.Children[4]).Width = (list_width * 0.12);
					((Label)list_pane.Children[5]).Width = (list_width * 0.12);
					((Label)list_pane.Children[6]).Width = (list_width * 0.12);
				}

				child_cnt++;
			}
		}

		public void SelFile(object sender, SelectionChangedEventArgs e)
		{
			this.SelFile();
		}

		public void SetImg(String file_path)
		{
			if (IMG_CURRENT_FILE.Dispatcher.CheckAccess())
			{
				IMG_CURRENT_FILE.Source = (ImageSource)new ImageSourceConverter().ConvertFromString(file_path);
			}
			else
			{
				IMG_CURRENT_FILE.Dispatcher.BeginInvoke(
					new Action(
						() => this.SetImg(file_path)
					)
				);
			}
		}

		public void SetImg(MemoryStream img_stream)
		{
			IMG_CURRENT_FILE.Source = CommonUtil.MemImg(img_stream);
		}

		private void CategoryWindow(object sender, EventArgs e)
		{
			if (CommonUtil.IsWindowOpen<CategoryWindow>() == false)
				new CategoryWindow();
		}

		private void RecognizeWindow(object sender, EventArgs e)
		{
			if (CommonUtil.IsWindowOpen<RecognizeWindow>() == false)
				new RecognizeWindow();
		}

		private void ShowImageWindow(object sender, RoutedEventArgs e)
		{
			FileData cur_data = (FileData)LIST_FILE_ITEM.SelectedItem;
			if (cur_data == null) return;

			if (cur_data.step >= ProcStep.analyze)
			{
				new ImageWindow(
					cur_data.idx,
					Convert.ToInt32(((Mouse.GetPosition(IMG_CURRENT_FILE).X) * cur_data.mat.Width) / IMG_CURRENT_FILE.ActualWidth),
					Convert.ToInt32(((Mouse.GetPosition(IMG_CURRENT_FILE).Y) * cur_data.mat.Height) / IMG_CURRENT_FILE.ActualHeight)
				);
			}
		}

		private void OnConfigure(object sender, EventArgs e)
		{
			new Configure();
		}

		public void ButtonStatus()
		{
			int proc_step = ProcStep.recognize + 1;

			foreach (FileData file_data in LIST_FILE_ITEM.Items)
			{
				if (this.CheckFile(file_data.idx))
				{
					if (proc_step > file_data.step)
						proc_step = file_data.step;
				}
			}

			this.ButtonStatus(proc_step <= ProcStep.recognize ? proc_step : 0);
		}

		public void ButtonStatus(float load_step)
		{
			if (load_step < 0)
			{
				BTN_MAIN_ANALYZE.IsEnabled = BTN_MAIN_STAT.IsEnabled = BTN_MAIN_CATEGORY.IsEnabled = BTN_MAIN_RECOGNIZER.IsEnabled =
						BTN_MAIN_SAVE.IsEnabled = BTN_MAIN_CONFIG.IsEnabled = false;
			}
			else if (load_step < 1)
			{
				BTN_MAIN_ANALYZE.IsEnabled = BTN_MAIN_STAT.IsEnabled = BTN_MAIN_CATEGORY.IsEnabled = BTN_MAIN_RECOGNIZER.IsEnabled =
						BTN_MAIN_CONFIG.IsEnabled = false;
			}
			else if (load_step < 2)
			{
				BTN_MAIN_ANALYZE.IsEnabled = true;
				BTN_MAIN_STAT.IsEnabled = false;
				BTN_MAIN_CATEGORY.IsEnabled = false;
				BTN_MAIN_RECOGNIZER.IsEnabled = false;
				BTN_MAIN_SAVE.IsEnabled = true;
			}
			else if (load_step < 3)
			{
				BTN_MAIN_ANALYZE.IsEnabled = false;
				BTN_MAIN_STAT.IsEnabled = true;
				BTN_MAIN_CATEGORY.IsEnabled = true;
				BTN_MAIN_RECOGNIZER.IsEnabled = true;
				BTN_MAIN_SAVE.IsEnabled = true;
			}
			else
			{
				BTN_MAIN_ANALYZE.IsEnabled = false;
				BTN_MAIN_STAT.IsEnabled = true;
				BTN_MAIN_CATEGORY.IsEnabled = true;
				BTN_MAIN_RECOGNIZER.IsEnabled = true;
				BTN_MAIN_SAVE.IsEnabled = true;
			}
		}

		private void ProgressPer(int progress_per)
		{
			this.progress_win.PROGRESS_PER.Value = progress_per;
		}

		private void ProgressPer(ProgressWindow progress_win, int progress_per)
		{
			progress_win.PROGRESS_PER.Value = progress_per;
		}

		public void SetTitle()
		{
			string window_title = "Innova Plex";
			window_title += (this.inst_id != "" && this.inst_depth != "") ? " [ " + this.inst_id + "__" + this.inst_depth + " ]" : "";
			this.LABEL_PROGRAM_TITLE.Foreground = (this.inst_id != "" && this.inst_depth != "") ? Brushes.Yellow : CommonUtil.GetBrush("#FFDDDDDD");
			this.LABEL_PROGRAM_TITLE.Content = window_title;
		}

		public void SetTitle(string file_name)
		{
			string window_title = "Innova Plex";
			window_title += " - " + file_name;
			this.LABEL_PROGRAM_TITLE.Foreground = CommonUtil.GetBrush("#FFDDDDDD");
			this.LABEL_PROGRAM_TITLE.Content = window_title;
		}

		public void SetTitle(int file_cnt, int fossil_cnt)
		{
			string window_title = "Innova Plex";
			window_title += (this.inst_id != "" && this.inst_depth != "") ? " [ " + this.inst_id + "__" + this.inst_depth + " ]" : "";
			window_title += " [ " + file_cnt.ToString() + " file(s) : " + fossil_cnt.ToString() + " fossil(s) ]";
			this.LABEL_PROGRAM_TITLE.Foreground = (this.inst_id != "" && this.inst_depth != "") ? Brushes.Yellow : CommonUtil.GetBrush("#FFDDDDDD");
			this.LABEL_PROGRAM_TITLE.Content = window_title;
		}

		public void SetTitle(int file_cnt, int fossil_cnt, string check_time)
		{
			string window_title = "Innova Plex";
			window_title += (this.inst_id != "" && this.inst_depth != "") ? " [ " + this.inst_id + "__" + this.inst_depth + " ]" : "";
			window_title += " [ " + file_cnt.ToString() + " file(s) : " + fossil_cnt.ToString() + " fossil(s) ] / " + check_time;
			this.LABEL_PROGRAM_TITLE.Foreground = (this.inst_id != "" && this.inst_depth != "") ? Brushes.Yellow : CommonUtil.GetBrush("#FFDDDDDD");
			this.LABEL_PROGRAM_TITLE.Content = window_title;
		}

		public bool CheckPath(string file_path)
		{
			bool is_dup = false;

			foreach (FileData file_data in LIST_FILE_ITEM.Items)
			{
				if (file_data.path == file_path)
				{
					is_dup = true;
					break;
				}
			}

			return !is_dup;
		}

		public FileData GetFile(int file_idx)
		{
			foreach (FileData file_data in LIST_FILE_ITEM.Items)
			{
				if (file_data.idx == file_idx)
					return file_data;
			}

			return null;
		}

		public GroupData GetGroup(int class_no)
		{
			foreach (GroupData group_data in this.group_list)
			{
				if (group_data.idx == class_no)
					return group_data;
			}

			return null;
		}

		private void ListType(object sender, RoutedEventArgs e)
		{
			Button list_btn = (Button)sender;
			if (list_btn == null) return;
			this.ListType(list_btn.Name);
		}

		private void ListType(string list_name)
		{
			this.list_type = list_name == "BTN_LIST_TYPE_TXT" ? 0 : 1;

			((Image)BTN_LIST_TYPE_TXT.Content).Source = (ImageSource)new ImageSourceConverter().ConvertFromString(
					this.list_type == 0 ? "pack://application:,,,/resources/list.txt.sel.png" : "pack://application:,,,/resources/list.txt.png");
			((Image)BTN_LIST_TYPE_IMG.Content).Source = (ImageSource)new ImageSourceConverter().ConvertFromString(
					this.list_type == 0 ? "pack://application:,,,/resources/list.img.png" : "pack://application:,,,/resources/list.img.sel.png");

			for (int i = 0; i < LIST_FILE_ITEM.Items.Count; i++)
			{
				ListBoxItem list_item = LIST_FILE_ITEM.ItemContainerGenerator.ContainerFromIndex(i) as ListBoxItem;

				if (list_item == null)
				{
					LIST_FILE_ITEM.UpdateLayout();
					LIST_FILE_ITEM.ScrollIntoView(LIST_FILE_ITEM.Items[i]);
					list_item = LIST_FILE_ITEM.ItemContainerGenerator.ContainerFromIndex(i) as ListBoxItem;
				}

				Grid data_grid = CommonUtil.FindVisualChild<Grid>(list_item);
				data_grid.RowDefinitions[1].Height = new GridLength(0, this.list_type == 0 ? GridUnitType.Pixel : GridUnitType.Auto);
				Image file_img = CommonUtil.FindVisualChild<Image>(list_item, "IMAGE_FILE_LIST");
				file_img.Visibility = this.list_type == 0 ? Visibility.Hidden : Visibility.Visible;
			}
		}

		private void ListType()
		{
			this.ListType(this.list_type == 0 ? "BTN_LIST_TYPE_TXT" : "BTN_LIST_TYPE_IMG");
		}

		public void InitGroup()
		{
			this.group_list.Clear();

			using (StreamReader stream_reader = new StreamReader(File.Exists("custom.group.xml") == true ? "custom.group.xml" : "group.xml", Encoding.UTF8))
			{
				using (XmlReader xml_reader = XmlReader.Create(new StringReader(stream_reader.ReadToEnd())))
				{
					if (xml_reader.ReadToDescendant("group"))
					{
						do
						{
							this.group_list.Add(
								new GroupData
								{
									idx = Convert.ToInt32(xml_reader.GetAttribute("idx")),
									order = Convert.ToInt32(xml_reader.GetAttribute("order")),
									rmin = Convert.ToSingle(xml_reader.GetAttribute("rmin")),
									amin = Convert.ToSingle(xml_reader.GetAttribute("amin")),
									name = xml_reader.GetAttribute("name")
								}
							);
						}
						while (xml_reader.ReadToFollowing("group"));
					}
				}
			}
		}
	}
}

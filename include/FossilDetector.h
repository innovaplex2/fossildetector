#ifndef __FOSSIL_DETECTOR_H__
#define __FOSSIL_DETECTOR_H__
#include <windows.h>

typedef enum{
	FOSSIL_TYPE_BLOB,
	FOSSIL_TYPE_LINE,
}eFOSSILE_TYPE;

typedef enum{
	FOSSIL_UPDATE_NONE,
	FOSSIL_UPDATE_UPDATE,	//FOSSIL_UPDATE_ADD
	FOSSIL_UPDATE_PROC,		//Process by FossilDetector
	FOSSIL_UPDATE_PROC_CNN,	//Process by FossilDetectorML

}eFOSSILE_UPDATE_FLAG;

#define	H_BIN_WIDTH	30
#define	S_BIN_WIDTH	32

typedef struct{
	int pos_x;
	int pos_y;
	float size;
	float angle;
	float response;
	int octave;
	int class_id;
}KEYPOINT;

typedef struct{
	int size;
	float data[128];
}SURFDESCRIPTOR;


typedef struct{
	eFOSSILE_UPDATE_FLAG flag;		// Update flag

	int				proc_step;
	int				fossil_id;		//fossil index
	int				image_index;
	int				specify_class;
	int				cate_class;
	int				class_no;		// classification result
	float			class_distance;	// distance in class (low score is close)
		
	RECT			rc;
	eFOSSILE_TYPE	type;

	float			mean;			//normalize by global mean
	float			stddev;

	float			rgb_mean[3];		

	//Moment for sort
	float			width;
	float			height;
	float			angle;
	float			area;
	
	double			hu_moments[7];	//

	int				size_of_descriptor;
	short			descriptors[1764];

	int				top_idx[5];
	float			top_score[5];

	int				num_of_feature;
	float			keypoints[2];
}FOSSILDETECTInfo;


class IFossilDetector
{
public:
	virtual BOOL	Open(const char *szConfigXML) = 0;
	virtual BOOL	ProcessFossilDetector( int width, int height, int step, BYTE *pData, int* pDetectCount ) = 0;
	virtual BOOL	GetFossilObject( FOSSILDETECTInfo *pOjbectInfo, int* pDetectCount ) = 0;
	virtual BOOL	UpdateFossilObject( int nDetectCount, FOSSILDETECTInfo *pOjbectInfo ) = 0;
	virtual BOOL	ProcessUpdateFossilObject( int width, int height, int step, BYTE *pData, int nDetectCount, FOSSILDETECTInfo *pOjbectInfo) = 0;

	virtual void	DestorySelf() = 0;
};


#ifdef __cplusplus
extern "C" {
#endif

HANDLE	__stdcall	SetupFossilDetector(const char* szConfigXML);
void	__stdcall	EndupFossilDetector(HANDLE handle);

BOOL	__stdcall	ProcessFossilDetector(HANDLE handle, int width, int height, int step, BYTE *pData, int* pDetectCount );
BOOL	__stdcall	GetFossilObject(HANDLE handle, FOSSILDETECTInfo *pOjbectInfo, int* pDetectCount );
BOOL	__stdcall	UpdateFossilObject(HANDLE handle, int nDetectCount, FOSSILDETECTInfo *pOjbectInfo);
BOOL	__stdcall	ProcessUpdateFossilObject(HANDLE handle, int width, int height, int step, BYTE *pData, int nDetectCount, FOSSILDETECTInfo *pOjbectInfo);

BOOL	__stdcall	UpdateFossilObject2(HANDLE handle, int nDetectCount, FOSSILDETECTInfo* pInFossilInfos, FOSSILDETECTInfo* pOutFossilInfos);
BOOL	__stdcall	ProcessUpdateFossilObject2(HANDLE handle, int width, int height, int step, BYTE *pData, 
											   int nDetectCount, FOSSILDETECTInfo* pInFossilInfos, FOSSILDETECTInfo* pOutFossilInfos);

IFossilDetector*	__stdcall	CreateFossilDetector();

#ifdef __cplusplus
}
#endif


#endif
#ifndef __FOSSIL_RECOGNITION_H__
#define __FOSSIL_RECOGNITION_H__

#define FOSSILTYPE_NOT_DEFINE		-1

typedef struct{
	int		width;
	int		height;
	unsigned char *pData;
	int		type;
}FOSSILDETECTInfo;


HANDLE	SetupFossilRecognition(char* szConfigXML);
void		EndupFossilRecognition(HANDLE handle);

BOOL		TrainFossil(HANDLE handle, int nObjCnt, FOSSILEOBJECTInfo *pOjbectInfo);
BOOL		GetFossilType(HANDLE handle, FOSSILEOBJECTInfo *pOjbectInfo);

#endif
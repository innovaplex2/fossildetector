#ifndef __FOSSIL_CLASSIFIER_H__
#define __FOSSIL_CLASSIFIER_H__
#include "FossilDetector.h"

#ifdef __cplusplus
extern "C" {
#endif

HANDLE	__stdcall CreateClassifier();
void	__stdcall DestroyClassifier(HANDLE handle);

BOOL	__stdcall ProcessClassification(HANDLE handle, int nTotalFossil, FOSSILDETECTInfo* FossilInfo);
BOOL	__stdcall ProcessClassification2(HANDLE handle, int nTotalFossil, FOSSILDETECTInfo* pInFossilInfos, FOSSILDETECTInfo* pOutFossilInfos);
#ifdef __cplusplus
}
#endif

#endif
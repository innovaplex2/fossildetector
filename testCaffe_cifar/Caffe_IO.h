#pragma once


int ComputeImageMean(char* InputDB, char* MeanFileName) ;
int Convert_Fossil_Image(char* InputFolder, char* ListFile, char* DBName, int Shuffle);
int Convert_Cifar10Dataset(char* InputFile, char* Name) ;
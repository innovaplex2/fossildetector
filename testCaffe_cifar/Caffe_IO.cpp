#include "stdafx.h"
#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <algorithm>
#include <leveldb/db.h>

#include <leveldb/db.h>
#include <leveldb/write_batch.h>
#include <google/protobuf\message.h>
#include <glog/logging.h>

#include <proto/caffe.pb.h>
#include <opencv/cv.h>
#include <opencv/highgui.h>
#pragma comment(lib, "libglog.lib")


#ifdef _DEBUG
#pragma comment(lib, "leveldbd")
#pragma comment(lib, "libprotobufd")
#pragma comment(lib, "opencv_core2410d")
#pragma comment(lib, "opencv_highgui2410d")
#pragma comment(lib, "opencv_imgproc2410d")
#else
#pragma comment(lib, "leveldb.lib")
#pragma comment(lib, "libprotobuf.lib")
#pragma comment(lib, "opencv_core2410.lib")
#pragma comment(lib, "opencv_highgui2410.lib")
#pragma comment(lib, "opencv_imgproc2410")
#endif

using namespace std;
using namespace caffe;
using google::protobuf::Message;

#define snprintf _snprintf

bool ReadImageToDatum(const string& filename, const int label, const int height, const int width, Datum* datum) 
{
  cv::Mat cv_img;
  if (height > 0 && width > 0) {
    cv::Mat cv_img_origin = cv::imread(filename, CV_LOAD_IMAGE_COLOR);
    cv::resize(cv_img_origin, cv_img, cv::Size(height, width));
  } else {
    cv_img = cv::imread(filename, CV_LOAD_IMAGE_COLOR);
  }
  if (!cv_img.data) {
    LOG(ERROR) << "Could not open or find file " << filename;
    return false;
  }
  datum->set_channels(3);
  datum->set_height(cv_img.rows);
  datum->set_width(cv_img.cols);
  datum->set_label(label);
  datum->clear_data();
  datum->clear_float_data();
  string* datum_string = datum->mutable_data();
  for (int c = 0; c < 3; ++c) 
  {
    for (int h = 0; h < cv_img.rows; ++h) 
	{
      for (int w = 0; w < cv_img.cols; ++w) 
	  {
        datum_string->push_back(
            static_cast<char>(cv_img.at<cv::Vec3b>(h, w)[c]));
      }
    }
  }
  return true;
}

inline bool ReadImageToDatum(const string& filename, const int label, Datum* datum) 
{
	return ReadImageToDatum(filename, label, 0, 0, datum);
}

void WriteProtoToBinaryFile(const Message& proto, const char* filename) 
{
	fstream output(filename, ios::out | ios::trunc | ios::binary);
	CHECK(proto.SerializeToOstream(&output));
}


const int kCIFARSize = 32;
const int kCIFARImageNBytes = 3072;
const int kCIFARBatchSize = 10000;
const int kCIFARTrainBatches = 5;

void read_image(std::ifstream* file, int* label, char* buffer) 
{
	char label_char;
	file->read(&label_char, 1);
	*label = label_char;
	file->read(buffer, kCIFARImageNBytes);
  
	return;
}


void convert_dataset(const string& input_folder, const string& output_folder) 
{
	// Leveldb options
	leveldb::Options options;
	options.create_if_missing = true;
	options.error_if_exists = true;
  
	// Data buffer
	int label;
	char str_buffer[kCIFARImageNBytes];
	string value;
	caffe::Datum datum;
	datum.set_channels(3);
	datum.set_height(kCIFARSize);
	datum.set_width(kCIFARSize);

	LOG(INFO) << "Writing Training data";
	leveldb::DB* train_db;
	leveldb::Status status;
	status = leveldb::DB::Open(options, output_folder + "/cifar-train-leveldb", &train_db);
	CHECK(status.ok()) << "Failed to open leveldb.";

	for (int fileid = 0; fileid < kCIFARTrainBatches; ++fileid) 
	{
		// Open files
		LOG(INFO) << "Training Batch " << fileid + 1;
		snprintf(str_buffer, kCIFARImageNBytes, "/data_batch_%d.bin", fileid + 1);
		std::ifstream data_file((input_folder + str_buffer).c_str(),
        std::ios::in | std::ios::binary);
		CHECK(data_file) << "Unable to open train file #" << fileid + 1;
    
		for (int itemid = 0; itemid < kCIFARBatchSize; ++itemid) 
		{
			read_image(&data_file, &label, str_buffer);
			datum.set_label(label);
			datum.set_data(str_buffer, kCIFARImageNBytes);
			datum.SerializeToString(&value);
      
			snprintf(str_buffer, kCIFARImageNBytes, "%05d", fileid * kCIFARBatchSize + itemid);
			train_db->Put(leveldb::WriteOptions(), string(str_buffer), value);
		}
	}

	LOG(INFO) << "Writing Testing data";
	leveldb::DB* test_db;
	CHECK(leveldb::DB::Open(options, output_folder + "/cifar-test-leveldb", &test_db).ok()) << "Failed to open leveldb.";
  
	// Open files
	std::ifstream data_file((input_folder + "/test_batch.bin").c_str(), std::ios::in | std::ios::binary);
	CHECK(data_file) << "Unable to open test file.";
  
	for(int itemid = 0; itemid < kCIFARBatchSize; ++itemid) 
	{
		read_image(&data_file, &label, str_buffer);
		datum.set_label(label);
		datum.set_data(str_buffer, kCIFARImageNBytes);
		datum.SerializeToString(&value);
		snprintf(str_buffer, kCIFARImageNBytes, "%05d", itemid);
		test_db->Put(leveldb::WriteOptions(), string(str_buffer), value);
	}

	delete train_db;
	delete test_db;
}


int ComputeImageMean(char* InputDB, char* MeanFileName) 
{
	leveldb::DB* db;
	leveldb::Options options;
	options.create_if_missing = false;

	LOG(INFO) << "Opening leveldb " << InputDB;
	leveldb::Status status = leveldb::DB::Open(options, InputDB, &db);
	CHECK(status.ok()) << "Failed to open leveldb " << InputDB;

	leveldb::ReadOptions read_options;
	read_options.fill_cache = false;
	leveldb::Iterator* it = db->NewIterator(read_options);
	it->SeekToFirst();
  
	Datum datum;
	BlobProto sum_blob;
	int count = 0;
  
	datum.ParseFromString(it->value().ToString());
	sum_blob.set_num(1);
	sum_blob.set_channels(datum.channels());
	sum_blob.set_height(datum.height());
	sum_blob.set_width(datum.width());
  
	const int data_size = datum.channels() * datum.height() * datum.width();
	int size_in_datum = std::max<int>(datum.data().size(), datum.float_data_size());
	
	for (int i = 0; i < size_in_datum; ++i) 
	{
		sum_blob.add_data(0.);
	}
	LOG(INFO) << "Starting Iteration";
  
	for(it->SeekToFirst(); it->Valid(); it->Next()) 
	{
		// just a dummy operation
		datum.ParseFromString(it->value().ToString());
    
		const string& data = datum.data();
		size_in_datum = std::max<int>(datum.data().size(), datum.float_data_size());
		CHECK_EQ(size_in_datum, data_size) << "Incorrect data field size " << size_in_datum;
    
		if (data.size() != 0) 
		{
			for (int i = 0; i < size_in_datum; ++i) 
			{
				sum_blob.set_data(i, sum_blob.data(i) + (uint8_t)data[i]);
			}
		} 
		else 
		{
			for (int i = 0; i < size_in_datum; ++i) 
			{
				sum_blob.set_data(i, sum_blob.data(i) + static_cast<float>(datum.float_data(i)));
			}
		}
		++count;
    
		if (count % 10000 == 0) 
		{
			LOG(ERROR) << "Processed " << count << " files.";
		}
	}
  
	if (count % 10000 != 0) 
	{
		LOG(ERROR) << "Processed " << count << " files.";
	}
  
	for (int i = 0; i < sum_blob.data_size(); ++i) 
	{
		sum_blob.set_data(i, sum_blob.data(i) / count);
	}
  
	// Write to disk
	LOG(INFO) << "Write to " << MeanFileName;
	WriteProtoToBinaryFile(sum_blob, MeanFileName);

	//delete db;
  
	return 0;
}


int Convert_Fossil_Image(char* InputFolder, char* ListFile, char* DBName, int Shuffle)
{
	std::ifstream infile(ListFile);
	std::vector<std::pair<string, int> > lines;
	string filename;

	int label;

	while (infile >> filename >> label)
	{
		lines.push_back(std::make_pair(filename, label));
	}
	if (Shuffle == '1')
	{
		// randomly shuffle data
		std::random_shuffle(lines.begin(), lines.end());
	}

	leveldb::DB* db;
	leveldb::Options options;
	options.error_if_exists = true;
	options.create_if_missing = true;
	options.write_buffer_size = 268435456;

	leveldb::Status status = leveldb::DB::Open(options, DBName, &db);

	string root_folder(InputFolder);
	Datum datum;
	int count = 0;
	const int kMaxKeyLength = 256;
	char key_cstr[kMaxKeyLength];
	leveldb::WriteBatch* batch = new leveldb::WriteBatch();
	int data_size;
	bool data_size_initialized = false;

	for (int line_id = 0; line_id < lines.size(); ++line_id)
	{
		if (!ReadImageToDatum(root_folder + lines[line_id].first, lines[line_id].second, &datum))
		{
			continue;
		}
		if (!data_size_initialized)
		{
			data_size = datum.channels() * datum.height() * datum.width();
			data_size_initialized = true;
		}
		else
		{
			const string& data = datum.data();
		}
		// sequential
		snprintf(key_cstr, kMaxKeyLength, "%08d_%s", line_id, lines[line_id].first.c_str());
		string value;
		// get the value
		datum.SerializeToString(&value);
		batch->Put(string(key_cstr), value);

		if (++count % 1000 == 0)
		{
			db->Write(leveldb::WriteOptions(), batch);

			delete batch;
			batch = new leveldb::WriteBatch();
		}
	}
	// write the last batch
	if (count % 1000 != 0)
	{
		db->Write(leveldb::WriteOptions(), batch);
	}

	delete batch;
	delete db;

	return 1;
}


int Convert_Cifar10Dataset(char* InputFile, char* Name) 
{
	convert_dataset(string(InputFile), string(Name));
 
	return 0;
}
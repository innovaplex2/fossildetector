// testCaffe_cifar.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "Caffe_IO.h"
#include "MainCaller.h"
#include <Windows.h>

const int COOKIE_CUT_SIZE = 32;

using namespace cv;
#pragma comment(lib, "caffe")


void LoadPath(char* FilePath)
{
	OPENFILENAMEA ofn;
	char lpstrFile[MAX_PATH] = "";

	memset(&ofn, 0, sizeof(OPENFILENAME));
	ofn.lStructSize = sizeof(OPENFILENAME);
	ofn.hwndOwner = NULL;
	ofn.lpstrFilter = "모든파일(*.*)\0*.*\0";
	ofn.lpstrFile = lpstrFile;
	ofn.lpstrInitialDir = ".\\Images";
	ofn.Flags = OFN_NOCHANGEDIR;
	ofn.nMaxFile = MAX_PATH;
	char name[300] = { 0 };
	

	if (GetOpenFileNameA(&ofn) != 0)
	{
		memcpy(FilePath, lpstrFile, sizeof(char)*MAX_PATH);
	}
	if (ofn.lpstrFile[0] == 0)
	{
		exit(0);
	}
}
//#define TRAINING_STEP


int _tmain(int argc, _TCHAR* argv[])
{
	int Ret;
	char FilePath[MAX_PATH];
	/*
	Training_CNN				dll_Training_CNN;
	Fossil_Recognizer			dll_Fossil_Recognizer;

	HINSTANCE hinstlib = LoadLibrary("Caffe.dll");

	if (hinstlib == NULL){
		printf("오류: DLL을 불러올 수 없습니다.\n");
	}

	dll_Training_CNN			= (Training_CNN)GetProcAddress(hinstlib, "Training_CNN");
	dll_Fossil_Recognizer		= (Fossil_Recognizer)GetProcAddress(hinstlib, "Fossil_Recognizer");

	if (dll_Training_CNN == NULL){
		printf("오류: Training_CNN.\n");
	}
	if (dll_Fossil_Recognizer == NULL){
		printf("오류: Fossil_Recognizer.\n");
	}
	*/

#if 1
	// Make LevelDB from Cifar10 Dataset ======================================================================================
	Convert_Cifar10Dataset("./cifar-10-batches-bin", "./cifar10-leveldb");
	// ========================================================================================================================

	// Make MeanImage from cifar10 LevelDB ====================================================================================
	ComputeImageMean("./cifar10-leveldb\\cifar-train-leveldb", "mean.binaryproto");
	// ========================================================================================================================

	// Train and Test cifar10 dataset =========================================================================================
	Training_CNN("cifar10_quick_solver.prototxt", NULL);
	// ========================================================================================================================
#else

	
	// Fossile Recognizer Test ================================================================================================
	LoadPath(FilePath);
	
	Mat InputImage = imread(FilePath, 1);
	
	Mat ResizedInput;
	BYTE *ResizedByteInput = new BYTE[32*32*3];
	imshow("Input Image", InputImage);
	resize(InputImage, ResizedInput, Size(32,32));
	memcpy(ResizedByteInput, ResizedInput.data, sizeof(BYTE)*32*32*3);
	imshow("Resize Image", ResizedInput);
	
	int* Top5_Index = new int[5];
	float* Top5_Score = new float[5];
	
	// Recognizer Start
 	int ret = Fossil_Recognizer(PROCESS_GPU, "cifar10_quick.prototxt", "cifar10_quick_iter_4000", 10, 32, ResizedByteInput, Top5_Index, Top5_Score);

	printf("1st: %d, Score: %f\n", Top5_Index[0], Top5_Score[0]);
	printf("2nd: %d, Score: %f\n", Top5_Index[1], Top5_Score[1]);
	printf("3rd: %d, Score: %f\n", Top5_Index[2], Top5_Score[2]);
	printf("4th: %d, Score: %f\n", Top5_Index[3], Top5_Score[3]);
	printf("5th: %d, Score: %f\n", Top5_Index[4], Top5_Score[4]);

	printf("return value : %d \n", ret);
	
	cvWaitKey(0);
#endif	
	// ===========================================================================================================================
	return 0;
}



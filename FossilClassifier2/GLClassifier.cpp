#include "GLClassifier.h"
#include <stdio.h>
#include <math.h>
#include <vector>
#include <algorithm>
#include <assert.h>
#include <emmintrin.h>

using namespace std;

#pragma comment(lib, "winmm.lib") 
//#define REPORT_RESULT 

//const float SIZE_THRESHOLD			= (float)0.5;
const float AREA_THRESHOLD			= (float)0.6;
const float BRIGHTNESS_THRESHOLD	= (float)0.5;
const float COLOR_THRESHOLD			= (float)0.10;
const float SHAPE_THRESHOLD			= (float)0.6;
//const float SHAPE_THRESHOLD			= (float)1.5;
const float SHAPE_THRESHOLD_LINE	= (float)1.8;
const float HOG_THRESHOLD			= (float)0.4;
const float TOTAL_THRESHOLD			= (float)0.8;
//const float TOTAL_THRESHOLD			= (float)1.5;
//const float TOTAL_THRESHOLD_LINE	= (float)0.35;
const float GRUOP_THRESHOLD			= (float)0.5;
const float KEY_GRUOP_THRESHOLD		= (float)0.5;
//const float MATCH_THERSHOLD		= (float)0.38;
const float MATCH_THERSHOLD			= (float)0.38;
const int	MAX_CLUSTER_CNT			= 40;
const float	MAX_DIFFERNT_VALUE		= 100.0f;




inline double DiffShapes( const double * ma, const double * mb)
{
    int i, sma, smb;
    double eps = 1.e-5;
    double result = 0;
	    
	for( i = 0; i < 7; i++ )
    {
        double ama = fabs( ma[i] );
        double amb = fabs( mb[i] );

        if( ma[i] > 0 )
            sma = 1;
        else if( ma[i] < 0 )
            sma = -1;
        else
            sma = 0;
        if( mb[i] > 0 )
            smb = 1;
        else if( mb[i] < 0 )
            smb = -1;
        else
            smb = 0;

        if( ama > eps && amb > eps )
        {
            ama = 1. / (sma * log10( ama ));
            amb = 1. / (smb * log10( amb ));
            result += fabs( -ama + amb );
        }
    }
	return result;	
}

inline float DiffColor(float RGBMean1[], float RGBMean2[])
{

	float DiffRG_1 = (RGBMean1[0]-RGBMean1[1])/255;
	float DiffRB_1 = (RGBMean1[0]-RGBMean1[2])/255;
	float DiffGB_1 = (RGBMean1[1]-RGBMean1[2])/255;

	float DiffRG_2 = (RGBMean2[0]-RGBMean2[1])/255;
	float DiffRB_2 = (RGBMean2[0]-RGBMean2[2])/255;
	float DiffGB_2 = (RGBMean2[1]-RGBMean2[2])/255;

	return (abs(DiffRG_1 - DiffRG_2) + abs(DiffRB_1-DiffRB_2) + abs(DiffGB_1-DiffGB_2));
	
}

inline float DiffBrightness(float RGBMean1[], float RGBMean2[])
{
	return (abs(RGBMean1[0]-RGBMean2[0])/255 + abs(RGBMean1[1]-RGBMean2[1])/255 + abs(RGBMean1[2]-RGBMean2[2])/255);	
}

//angle을 고려한 ratio (width/height)도 고려의 대상이 된다.
inline float DiffArea(float area1, float area2)
{
	return ((area1 > area2)?((area1 - area2)/area1) : ((area2 - area1)/area2));
}


inline float DiffSize(RECT rc1, RECT rc2)
{
	float size1 = (rc1.right - rc1.left) *(rc1.bottom - rc1.top);
	float size2 = (rc2.right - rc2.left) *(rc2.bottom - rc2.top);

	return ((size1 > size2)?((size1 - size2)/size1) : ((size2 - size1)/size2));
}


#ifdef SUPPORT_AVX2
inline float DiffHoG(short* desciption1, short* desciption2, int size)
{
	int loop = size / 16;

	float total_diff = 0;
	__m256i	desc1, desc2, diff, temp;
	__m256i *pDesc1 = (__m256i *)desciption1;
	__m256i *pDesc2 = (__m256i *)desciption2;
			
	diff = _mm256_setzero_si256();
	for (int i = 0; i < loop ; i++){
		desc1	= _mm256_loadu_si256(pDesc1 + i);
		desc2	= _mm256_loadu_si256(pDesc2 + i);

		temp	= _mm256_subs_epi16(desc1, desc2);
		temp	= _mm256_abs_epi16(temp);
		diff	= _mm256_add_epi16(diff, temp);
	}

	total_diff = 0;
	for (int i = 0; i < 16; i++) total_diff += diff.m256i_u16[i];
	return diff/1000;
}
#else 

inline float DiffHoG_SSE2(short* desciption1, short* desciption2, int size)
{
	int loop = size / 8;

	float total_diff = 0;
	__m128i	desc1, desc2, diff, temp;
	__m128i *pDesc1 = (__m128i *)desciption1;
	__m128i *pDesc2 = (__m128i *)desciption2;

	diff = _mm_setzero_si128();
	for (int i = 0; i < loop; i++){
		desc1 = _mm_loadu_si128(pDesc1 + i);
		desc2 = _mm_loadu_si128(pDesc2 + i);

 		temp = _mm_subs_epi16(desc1, desc2);
		temp = _mm_abs_epi16(temp);
		diff = _mm_add_epi16(diff, temp);
	}

	total_diff = 0;
	for (int i = 0; i < 8; i++) total_diff += diff.m128i_i16[i];

	return float(total_diff / 1000000.0);
}

#endif


inline float DiffHoG(short* desciption1, short* desciption2, int size)
{
	float total_diff = 0;
	for (int i = 0; i < size; i++){
		total_diff += abs(desciption1[i] - desciption2[i]);
	}
	return float(total_diff / 1000000.0);
}


typedef struct
{
	int		index;
	float	diff;	//color diff + area diff + shape diff
	int		cluster_id;
	float	avr_distance;	//

	//information
	//float	area_diff;
	//float	color_diff;
	//float	brightness_diff;
	//float	shape_diff;
	//float	hog_diff;

}FOSSILDistanceInfo;

GLClassifier::GLClassifier()
{
	m_NumOfFossil = 0;
	
}
GLClassifier::~GLClassifier()
{
	
}


bool FossilCompareObj( FOSSILDistanceInfo first, FOSSILDistanceInfo second )
{
 return first.diff < second.diff;
}


typedef struct
{
	int		index;
	float	diff;	//color diff + area diff + shape diff
	float	avr_distance;	//
}ClusterInfo;


BOOL GLClassifier::GLClassificationStart(int nTotalFossil, FOSSILDETECTInfo* pFossilInfos)
{
	m_NumOfFossil = nTotalFossil;
	float* pFossilDistance = (float* )malloc(m_NumOfFossil*m_NumOfFossil*sizeof(float));
	int* pFossilBesMatchIndex = (int*)malloc(m_NumOfFossil * sizeof(int));
	
	int i, j;
	float size_diff, area_diff, color_diff, brightness_diff, shape_diff, total_diff, hog_diff;
	DWORD tick1, tick2;
	float distance, min_distance = MATCH_THERSHOLD;
	int find_index, first_index, second_index, cluster_id = 0;

	tick1 = timeGetTime();
	for ( i = 0; i < m_NumOfFossil; i++)
	{
		pFossilDistance[i*m_NumOfFossil + i] = pFossilDistance[i + i*m_NumOfFossil] = MAX_DIFFERNT_VALUE;
		for ( j = i+1; j < m_NumOfFossil; j++)
		{
			pFossilDistance[i*m_NumOfFossil + j] = pFossilDistance[i + j*m_NumOfFossil] = MAX_DIFFERNT_VALUE;

			if (pFossilInfos[i].type != pFossilInfos[j].type) continue;

//			size_diff = DiffSize(pFossilInfos[i].rc, pFossilInfos[j].rc);	
//			if (size_diff > SIZE_THRESHOLD) continue;
			
			area_diff = DiffArea(pFossilInfos[i].area, pFossilInfos[j].area);
			if (area_diff > AREA_THRESHOLD) continue;

			if(pFossilInfos[i].type == FOSSIL_TYPE_BLOB){
				color_diff = DiffColor(pFossilInfos[i].rgb_mean, pFossilInfos[j].rgb_mean);
				if (color_diff > COLOR_THRESHOLD) continue;

				brightness_diff = DiffBrightness(pFossilInfos[i].rgb_mean, pFossilInfos[j].rgb_mean);
				if (brightness_diff > BRIGHTNESS_THRESHOLD) continue;

				shape_diff = (float)DiffShapes(pFossilInfos[i].hu_moments, pFossilInfos[j].hu_moments);
				if (shape_diff > SHAPE_THRESHOLD) continue;
				if (shape_diff < 0.05)shape_diff = 0.05;
			
				//hog_diff = DiffHoG((short *)pFossilInfos[i].descriptors, (short *)pFossilInfos[j].descriptors, pFossilInfos[j].size_of_descriptor);
				hog_diff = DiffHoG_SSE2((short *)pFossilInfos[i].descriptors, (short *)pFossilInfos[j].descriptors, pFossilInfos[j].size_of_descriptor);
				if (hog_diff > HOG_THRESHOLD) continue;

				total_diff = shape_diff + hog_diff * 2;
				//total_diff = shape_diff ;
				//if (total_diff > TOTAL_THRESHOLD) continue;
			}else{
				shape_diff = (float)DiffShapes(pFossilInfos[i].hu_moments, pFossilInfos[j].hu_moments);
				if(shape_diff > SHAPE_THRESHOLD_LINE) continue;

				hog_diff = DiffHoG_SSE2((short *)pFossilInfos[i].descriptors, (short *)pFossilInfos[j].descriptors, pFossilInfos[j].size_of_descriptor);
				total_diff = hog_diff * 7;
				//if (total_diff > TOTAL_THRESHOLD) continue;
				
			}

			pFossilDistance[i*m_NumOfFossil + j] = pFossilDistance[i + j*m_NumOfFossil] = total_diff;
		}
		
		min_distance = MATCH_THERSHOLD;
		pFossilInfos[i].class_no = pFossilBesMatchIndex[i] = -1;
		//find best matched fossil 
		for ( j = 0; j < m_NumOfFossil; j++){
			if( i == j ) continue;
			distance = pFossilDistance[i*m_NumOfFossil + j];
			if(min_distance > distance){
				pFossilBesMatchIndex[i] = j;
				min_distance = distance;
			}
		}
	}


	tick2 = timeGetTime();
	printf("Process1 Take Time : %d, \n", tick2 - tick1);
/*
	//sort 
	tick1 = timeGetTime();
	for ( i = 0; i < m_NumOfFossil; i++){
		sort(pFossilDistance[i].begin(), pFossilDistance[i].end(), FossilCompareObj);
	}
	tick2 = timeGetTime();
	printf("Process2 Take Time : %d, \n", tick2 - tick1);
*/

	//first find best matched and cluster..
	vector<vector <int>>  ClusterFossils;
	bool bFindExistInCluster;
	int n, m;

	tick1 = timeGetTime();
	//1. init cluster seed 
	while (1){
		min_distance = MATCH_THERSHOLD;
		find_index = -1;
		for (i = 0; i < m_NumOfFossil; i++){
			if(pFossilBesMatchIndex[i] == -1) continue;

			distance = pFossilDistance[i * m_NumOfFossil + pFossilBesMatchIndex[i]];
			if ( ( min_distance > distance ) && (pFossilInfos[i].class_no == -1) ){
				min_distance = distance;
				find_index = i;
			}
		}

		if(find_index == -1) break;
		second_index = pFossilBesMatchIndex[find_index];

		//1. check exit cluster 
		bFindExistInCluster = false;
		for( n = 0 ; n < ClusterFossils.size() ; n++){
			first_index = ClusterFossils[n][0];
			for( m = 0 ; m < ClusterFossils[n].size() ; m++){

				int search_index = ClusterFossils[n][m];
				if(second_index == search_index || find_index == search_index){
					bFindExistInCluster = true;
					break;
				}
								
				if( (pFossilDistance[find_index*m_NumOfFossil + search_index]  < KEY_GRUOP_THRESHOLD) && 
					(pFossilDistance[find_index*m_NumOfFossil + first_index] < MAX_DIFFERNT_VALUE)){
					bFindExistInCluster = true;
					break;
				}
								 
				if( (pFossilDistance[second_index*m_NumOfFossil + search_index] < KEY_GRUOP_THRESHOLD) && 
					(pFossilDistance[second_index*m_NumOfFossil + first_index] < MAX_DIFFERNT_VALUE) ){
					bFindExistInCluster = true;
					break;
				}
			}
			if(bFindExistInCluster) break;
		}


		if(bFindExistInCluster){
			if(pFossilInfos[find_index].class_no == -1){
				ClusterFossils[n].push_back(find_index);
				pFossilInfos[find_index].class_no = i;
			}

			if(pFossilInfos[second_index].class_no == -1){
				ClusterFossils[n].push_back(second_index);
				pFossilInfos[second_index].class_no = i;
			}
			continue;
		}
		
		//Add new seed 
		vector <int>	FossilGroup;
		FossilGroup.push_back(find_index);
		FossilGroup.push_back(second_index);
		ClusterFossils.push_back(FossilGroup);
		pFossilInfos[find_index].class_no =  pFossilInfos[second_index].class_no = cluster_id;

		cluster_id++;
		if(cluster_id > MAX_CLUSTER_CNT) break;
	}

	tick2 = timeGetTime();
	printf("Process3 Take Time : %d, \n", tick2 - tick1);


	int find_cluster;
	
	tick1 = timeGetTime();
	for ( i = 0; i < m_NumOfFossil; i++){
		
		min_distance = MATCH_THERSHOLD;
		find_cluster = -1;
		bFindExistInCluster = false;

		for( n = 0 ; n < ClusterFossils.size() ; n++){
			first_index	= ClusterFossils[n][0];
			if((pFossilDistance[i*m_NumOfFossil + first_index]  == MAX_DIFFERNT_VALUE)) continue;

			for( m = 0 ; m < ClusterFossils[n].size() ; m++){
				distance = pFossilDistance[i*m_NumOfFossil + ClusterFossils[n][m]];
				if( (min_distance > distance) ){
					min_distance = distance;
					find_cluster = n;
				}
			}
		}

		if(find_cluster != -1){
			ClusterFossils[find_cluster].push_back(i);
		}
	}

	tick2 = timeGetTime();
	printf("Process4 Take Time : %d, \n", tick2 - tick1);
	
	for( n = 0 ; n < ClusterFossils.size() ; n++){
		first_index	= ClusterFossils[n][0];
		for( m = 0 ; m < ClusterFossils[n].size() ; m++){
			find_index	= ClusterFossils[n][m];
			
			pFossilInfos[find_index].class_no = n+1;
			pFossilInfos[find_index].class_distance = pFossilDistance[first_index*m_NumOfFossil + find_index];
		}
	}
	

	free( pFossilDistance );
	free( pFossilBesMatchIndex );
		

	return TRUE;
}



#include "../Include/FossilClassifier.h"
#include "GLClassifier.h"

#define	FOSSIL_CLASSIFIER_THRESHOLD	1.2

HANDLE __stdcall CreateClassifier()
{
	return (HANDLE) new GLClassifier;
}

void __stdcall DestroyClassifier(HANDLE handle)
{
	if(handle){
		delete (GLClassifier *)handle;
	}
}

BOOL __stdcall ProcessClassification(HANDLE handle, int nTotalFossil, FOSSILDETECTInfo* pFossilInfos)
{
	if(handle != NULL)
	{
		GLClassifier* pClassifier = (GLClassifier*)handle;

		BOOL bProcessCNN = FALSE;
		
		for (int i = 0; i < nTotalFossil; i++){
			if (pFossilInfos->flag == FOSSIL_UPDATE_PROC_CNN){
				if (pFossilInfos[i].top_score[0] > FOSSIL_CLASSIFIER_THRESHOLD){
					if (pFossilInfos[i].cate_class == -1) pFossilInfos[i].cate_class = pFossilInfos[i].top_idx[0];
					if (pFossilInfos[i].specify_class == -1 && pFossilInfos[i].class_no == -1) pFossilInfos[i].class_no = pFossilInfos[i].top_idx[0];
					pFossilInfos[i].class_distance = 1.0 / pFossilInfos[i].top_score[0];
				}
				else
				{
					if (pFossilInfos[i].specify_class == -1 && pFossilInfos[i].class_no == -1) pFossilInfos[i].class_no = -1;
				}
				bProcessCNN = TRUE;
			}
		}
		
		if (bProcessCNN) return TRUE;
		return pClassifier->GLClassificationStart(nTotalFossil, pFossilInfos);
	}

	return FALSE;
}


BOOL __stdcall ProcessClassification2(HANDLE handle, int nTotalFossil, FOSSILDETECTInfo* pInFossilInfos, FOSSILDETECTInfo* pOutFossilInfos)
{
	if (handle != NULL)
	{
		GLClassifier* pClassifier = (GLClassifier*)handle;
		memcpy(pOutFossilInfos, pInFossilInfos, sizeof(FOSSILDETECTInfo)*nTotalFossil);

		BOOL bProcessCNN = FALSE;

		for (int i = 0; i < nTotalFossil; i++){
			if (pInFossilInfos->flag == FOSSIL_UPDATE_PROC_CNN){
				if (pInFossilInfos[i].top_score[0] > FOSSIL_CLASSIFIER_THRESHOLD){
					if (pOutFossilInfos[i].cate_class == -1) pOutFossilInfos[i].cate_class = pInFossilInfos[i].top_idx[0];
					if (pOutFossilInfos[i].specify_class == -1 && pOutFossilInfos[i].class_no == -1) pOutFossilInfos[i].class_no = pInFossilInfos[i].top_idx[0];
					pOutFossilInfos[i].class_distance = 1.0 / pInFossilInfos[i].top_score[0];
				}
				else
				{
					if (pOutFossilInfos[i].specify_class == -1 && pOutFossilInfos[i].class_no == -1) pOutFossilInfos[i].class_no = -1;
				}
				bProcessCNN = TRUE;
			}
		}
		
		if (bProcessCNN) return TRUE;
		return pClassifier->GLClassificationStart(nTotalFossil, pOutFossilInfos);
	}

	return FALSE;
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Xml;
using System.IO;

namespace XmlConverter
{
	/// <summary>
	/// Interaction logic for MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window
	{
		private string[] sel_file = { };

		public MainWindow()
		{
			InitializeComponent();
		}

		private void ExitApp(object sender, EventArgs e)
		{
			Application.Current.Shutdown();
		}

		private void OpenFile(object sender, EventArgs e)
		{
			System.Windows.Forms.OpenFileDialog open_diag = new System.Windows.Forms.OpenFileDialog();
			open_diag.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
			open_diag.Multiselect = true;
			open_diag.Filter = "xml (*.xml)|*.xml";
			open_diag.FilterIndex = 1;

			if (open_diag.ShowDialog() == System.Windows.Forms.DialogResult.Cancel)
				return;

			if (open_diag.FileNames.Length == 0)
				return;

			this.sel_file = open_diag.FileNames;

			TEXTBOX_INPUT_LIST.Dispatcher.BeginInvoke(
				new Action(
					() => {
						TEXTBOX_INPUT_LIST.Text = "";
					}
				)
			);

			foreach (string file_path in this.sel_file)
			{
				TEXTBOX_INPUT_LIST.Dispatcher.BeginInvoke(
					new Action(
						() =>
						{
							TEXTBOX_INPUT_LIST.Text += file_path;
							TEXTBOX_INPUT_LIST.Text += Environment.NewLine;
						}
					)
				);
			}
		}

		private void ConvertFile(object sender, EventArgs e)
		{
			if (this.sel_file.Length == 0)
			{
				MessageBox.Show("파일이 없습니다.", "오류", MessageBoxButton.OK, MessageBoxImage.Error);
				return;
			}

			string load_content = "";
			int img_idx = 0;
			int img_cnt = 0;
			string img_name = "";
			string img_path = "";
			List<ImageData> img_list = new List<ImageData>();
			Encoding file_encode = System.Text.Encoding.GetEncoding("ks_c_5601-1987");

			foreach (string file_path in this.sel_file)
			{
				//using (StreamReader stream_reader = new StreamReader(file_path, Encoding.UTF8))
				using (StreamReader stream_reader = new StreamReader(file_path, file_encode))
				{
					load_content = stream_reader.ReadToEnd();
					stream_reader.Close();
				}

				using (XmlReader xml_reader = XmlReader.Create(new StringReader(load_content)))
				{
					if (xml_reader.ReadToDescendant("img"))
					{
						do
						{
							img_path = xml_reader.GetAttribute("path");

							xml_reader.ReadStartElement("img");

							if (xml_reader.Value.Trim() != "")
								continue;

							if (File.Exists(img_path) == false)
							{
								MessageBox.Show("이미지 파일이 없습니다.", "오류", MessageBoxButton.OK, MessageBoxImage.Error);
								return;
							}
						}
						while (xml_reader.ReadToFollowing("img"));
					}

					xml_reader.Close();
				}

				using (XmlReader xml_reader = XmlReader.Create(new StringReader(load_content)))
				{
					if (xml_reader.ReadToDescendant("img"))
					{
						do
						{
							bool img_exist = false;

							//img_idx = Convert.ToInt32(xml_reader.GetAttribute("idx"));
							img_name = xml_reader.GetAttribute("name");
							img_path = xml_reader.GetAttribute("path");

							foreach (ImageData img_data in img_list)
							{
								if (img_data.path == img_path)
								{
									img_exist = true;
                                    MessageBox.Show("Same File Exist : " + img_path + " : " + img_name);
									break;
								}
							}

							if (img_exist == true)
								continue;

							img_list.Add(
								new ImageData
								{
									idx = img_idx,
									name = img_name,
									path = img_path
								}
							);

							img_idx++;
							img_cnt++;
						}
						while (xml_reader.ReadToFollowing("img"));
					}

					xml_reader.Close();
				}
			}

			if (Directory.Exists(@"output") == false)
				Directory.CreateDirectory(@"output");

			TEXTBOX_OUTPUT_LIST.Dispatcher.BeginInvoke(
				new Action(
					() =>
					{
						TEXTBOX_OUTPUT_LIST.Text = "";
					}
				)
			);

            //save training_list text
            string train_list_file = @"output\" + "training_list.txt";
            using (StreamWriter train_list_writer = new StreamWriter(train_list_file))
            {
                foreach (ImageData img_data in img_list)
                {
                    train_list_writer.Write(img_data.path);
                    train_list_writer.Write(System.Environment.NewLine);
                }

				train_list_writer.Close();
			}
               

			foreach (ImageData img_data in img_list)
			{
                
                string output_file = @"output\" + System.IO.Path.GetFileNameWithoutExtension(img_data.name) + ".txt";
                
                //check same fil exist
                //if (File.Exists(output_file))
                //{
                //    MessageBox.Show("File Exist : " + output_file);
                //}


				using (StreamWriter save_writer = new StreamWriter(output_file))
				{
					System.Drawing.Bitmap img_val = new System.Drawing.Bitmap(img_data.path);

					foreach (string file_path in this.sel_file)
					{
						using (StreamReader stream_reader = new StreamReader(file_path, Encoding.UTF8))
						{
							load_content = stream_reader.ReadToEnd();
							stream_reader.Close();
						}

						string[] path_list = new string[img_cnt];

						using (XmlReader xml_reader = XmlReader.Create(new StringReader(load_content)))
						{
							if (xml_reader.ReadToDescendant("img"))
							{
								do
								{
									img_idx = Convert.ToInt32(xml_reader.GetAttribute("idx"));
									img_path = xml_reader.GetAttribute("path");
									path_list[img_idx] = img_path;
								}
								while (xml_reader.ReadToFollowing("img"));
							}

							xml_reader.Close();
						}

						using (XmlReader xml_reader = XmlReader.Create(new StringReader(load_content)))
						{
							if (xml_reader.ReadToDescendant("cut"))
							{
								do
								{
									img_idx = Convert.ToInt32(xml_reader.GetAttribute("img"));

									if (img_data.path != path_list[img_idx])
										continue;

                                    int class_no = Convert.ToInt32(xml_reader.GetAttribute("class"));

                                    if (class_no >= 1) 
                                        class_no = 1;
                                    else 
                                        class_no = 0;
                                    
									string[] fossil_pos = xml_reader.GetAttribute("pos").Split(',');
                                    double cookie_left = Convert.ToDouble(fossil_pos[0]) / Convert.ToDouble(img_val.Width);
									double cookie_top = Convert.ToDouble(fossil_pos[1]) / Convert.ToDouble(img_val.Height);
                                    double cookie_right = Convert.ToDouble(fossil_pos[2]) / Convert.ToDouble(img_val.Width);
                                    double cookie_bottom = Convert.ToDouble(fossil_pos[3]) / Convert.ToDouble(img_val.Height);

                                    double cookie_width = cookie_right - cookie_left;
                                    double cookie_height = cookie_bottom - cookie_top;
                                    double cookie_center_x = cookie_left + cookie_width / 2;
                                    double cookie_center_y = cookie_top + cookie_height / 2;
                                        
                                    save_writer.Write(
										class_no.ToString() + " " +
                                        cookie_center_x.ToString() + " " +
                                        cookie_center_y.ToString() + " " +
										cookie_width.ToString() + " " +
										cookie_height.ToString()
									);

									save_writer.Write(System.Environment.NewLine);
                                 }
								while (xml_reader.ReadToFollowing("cut"));
							}

							xml_reader.Close();
						}
					}

					save_writer.Close();
				}

				TEXTBOX_OUTPUT_LIST.Dispatcher.BeginInvoke(
					new Action(
						() =>
						{
							TEXTBOX_OUTPUT_LIST.Text += output_file;
							TEXTBOX_OUTPUT_LIST.Text += Environment.NewLine;
						}
					)
				);
			}

			MessageBox.Show("변환이 완료되었습니다.", "완료", MessageBoxButton.OK, MessageBoxImage.Information);
		}
	}
}

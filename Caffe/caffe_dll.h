#ifndef __CAFFE_CLASSSIFICATION_H__
#define __CAFFE_CLASSSIFICATION_H__
#include <windows.h>

typedef enum  {
	PROCESS_CPU,
	PROCESS_GPU
}ePROCESS_TYPE;


#ifdef __cplusplus
extern "C" {
#endif

	HANDLE	__stdcall	SetupCaffeClassification(ePROCESS_TYPE type, const char* Prototxt, const char* Caffemodel, const char * MeanBinaryFile, const float* PixelValue, int* max_net_count);
	void	__stdcall	EndupCaffeClassification(HANDLE handle);

	int		__stdcall	ProcessCaffeClassification(HANDLE handle, int InputDataSize, unsigned char* CurImage, int* nTopIdx, float* fScoreValue);
	int		__stdcall	ProcessCaffeClassificationMegredImages(HANDLE handle, int InputDataSize, int InputImageCount, unsigned char* pMergedImage, int* nTopIdx, float* fScoreValue);

#ifdef __cplusplus
}
#endif


//For C++ 
#include <vector>
typedef std::pair<int, float> Prediction;
int		__stdcall	ProcessCaffeClassificationImageArray(HANDLE handle, std::vector<cv::Mat>& input_images, std::vector<Prediction>& output);


#endif

// testF	ossilClassification.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"


#include "opencv/cv.h"
#include "opencv/highgui.h"

#ifdef _DEBUG
#pragma comment(lib, "opencv_core2410d.lib")
#pragma comment(lib, "opencv_highgui2410d.lib")
#pragma comment(lib, "opencv_imgproc2410d.lib")
#pragma comment(lib, "opencv_objdetect2410d.lib")


#else
#pragma comment(lib, "opencv_core2410.lib")
#pragma comment(lib, "opencv_highgui2410.lib")
#pragma comment(lib, "opencv_imgproc2410.lib")
#pragma comment(lib, "opencv_objdetect2410.lib")

#endif

double DiffShapes( const double * ma, const double * mb, int method)
{
    int i, sma, smb;
    double eps = 1.e-5;
    double mmm;
    double result = 0;
	    
    switch (method)
    {
    case 1:
        {
            for( i = 0; i < 7; i++ )
            {
                double ama = fabs( ma[i] );
                double amb = fabs( mb[i] );

                if( ma[i] > 0 )
                    sma = 1;
                else if( ma[i] < 0 )
                    sma = -1;
                else
                    sma = 0;
                if( mb[i] > 0 )
                    smb = 1;
                else if( mb[i] < 0 )
                    smb = -1;
                else
                    smb = 0;

                if( ama > eps && amb > eps )
                {
                    ama = 1. / (sma * log10( ama ));
                    amb = 1. / (smb * log10( amb ));
                    result += fabs( -ama + amb );
                }
            }
            break;
        }

    case 2:
        {
            for( i = 0; i < 7; i++ )
            {
                double ama = fabs( ma[i] );
                double amb = fabs( mb[i] );

                if( ma[i] > 0 )
                    sma = 1;
                else if( ma[i] < 0 )
                    sma = -1;
                else
                    sma = 0;
                if( mb[i] > 0 )
                    smb = 1;
                else if( mb[i] < 0 )
                    smb = -1;
                else
                    smb = 0;

                if( ama > eps && amb > eps )
                {
                    ama = sma * log10( ama );
                    amb = smb * log10( amb );
                    result += fabs( -ama + amb );
                }
            }
            break;
        }

    case 3:
        {
            for( i = 0; i < 7; i++ )
            {
                double ama = fabs( ma[i] );
                double amb = fabs( mb[i] );

                if( ma[i] > 0 )
                    sma = 1;
                else if( ma[i] < 0 )
                    sma = -1;
                else
                    sma = 0;
                if( mb[i] > 0 )
                    smb = 1;
                else if( mb[i] < 0 )
                    smb = -1;
                else
                    smb = 0;

                if( ama > eps && amb > eps )
                {
                    ama = sma * log10( ama );
                    amb = smb * log10( amb );
                    mmm = fabs( (ama - amb) / ama );
                    if( result < mmm )
                        result = mmm;
                }
            }
            break;
        }
    }

    return result;
}

#define SHOW_DEBUG 1

using namespace cv;
#define MAX_TEST	13

#define	VARIANCE_THRESHOLD	20

typedef struct{

	int	average;	//
	int	variance;	//1차 분류 기준이 됨 variance가 적을 경우 egde로 
	
}FOSSILINFO;

typedef enum{
	FOSSIL_TYPE_BLOB,
	FOSSIL_TYPE_LINE,
}eFOSSILE_TYPE;





#ifdef HUMOMENT_TEST
int _tmain(int argc, _TCHAR* argv[])
{
	cv::Mat img_input, img_gray, img_binary, img_temp, img_temp2;
	char strFileName[256];
	char strPutFileName[256];
	char strPutFileName_c[256];
	Moments moment;
	double huMoment[MAX_TEST][7], Diff[3] ;

	sprintf(strPutFileName, "D:\\work2\\InnovaPlex\\test\\result.csv");
	sprintf(strPutFileName_c, "D:\\work2\\InnovaPlex\\test\\result_c.csv");
	FILE *fp = fopen(strPutFileName,"w+t");
	FILE *fp_c = fopen(strPutFileName_c,"w+t");

	eFOSSILE_TYPE type = FOSSIL_TYPE_BLOB;

	FOSSILINFO fossil_info[13];
	int border_percent ; 
	for(int i = 1 ; i < 13; i++){
		sprintf(strFileName, "D:\\work2\\InnovaPlex\\test\\test%d.jpg", i);	
		
		img_input = cv::imread(strFileName);
		
		cvtColor(img_input, img_gray, CV_RGB2GRAY );

		Scalar avr, var;

		//1. get average and variance 
		meanStdDev(img_gray, avr, var);

		fossil_info[i].average = avr[0];
		fossil_info[i].variance= var[0];

		printf("average : %d, variance : %d \n", fossil_info[i].average, fossil_info[i].variance);


		if( fossil_info[i].variance < VARIANCE_THRESHOLD ){
			type = FOSSIL_TYPE_LINE;
		}else{
			type = FOSSIL_TYPE_BLOB;
		}

		if( FOSSIL_TYPE_BLOB == type ){
			border_percent = 15;
			threshold(img_gray, img_binary, 128, 255, THRESH_BINARY|THRESH_OTSU);

			Mat element(2, 2, CV_8U, cv::Scalar(1));
			morphologyEx( img_binary, img_binary, CV_MOP_OPEN, element);

			//Mat element2(3, 3, CV_8U, cv::Scalar(1));
			//morphologyEx( img_binary, img_binary, CV_MOP_ERODE, element2);

		}else{
			border_percent = 10;
			int ddepth = CV_16S;
			Laplacian( img_gray, img_temp, ddepth, 3, 1, 0, BORDER_DEFAULT );
			convertScaleAbs( img_temp, img_binary);

			GaussianBlur( img_binary, img_temp, Size(3,3), 2);
			threshold(img_temp, img_binary, 128, 255, THRESH_BINARY|THRESH_OTSU);


//			Mat element(2, 2, CV_8U, cv::Scalar(1));
//			morphologyEx( img_binary, img_binary, CV_MOP_OPEN, element);
		}

		//Canny(img_gray, img_binary, 50, 150);
		
		//GaussianBlur( img_gray, img_temp, Size(3,3), 2);
		//GaussianBlur( img_gray, img_temp2, Size(3,3), 21);
		
		//subtract(img_temp ,img_temp2, img_binary );
		//img_binary = img_temp - img_temp2;

		//Laplacian( img_gray, img_temp, ddepth, 3, 1, 0, BORDER_DEFAULT );
		//convertScaleAbs( img_temp, img_binary);
		
		//threshold(img_binary, img_binary, 128, 255, THRESH_BINARY|THRESH_OTSU);

		

#if 1		
		img_binary.copyTo(img_gray);
		img_binary.copyTo(img_temp);
		
		// Find contours
		vector<vector<Point> > contours;
		findContours( img_temp, contours, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_SIMPLE);

		// FInd center of contours if remove unwanted boundary small blob 
		Rect validRect;
		img_input.cols;
		
		//clean bounday.
		validRect.x		= 0; 
		validRect.y		= 0;
		validRect.width = img_input.cols; 	
		validRect.height= img_input.rows;
		rectangle(img_binary, validRect, Scalar(0,0,0), 1);
		
		validRect.x		= (img_input.cols*border_percent)/100; 
		validRect.y		= (img_input.rows*border_percent)/100;
		validRect.width = img_input.cols - 2*validRect.x; 	
		validRect.height= img_input.rows - 2*validRect.y;

#ifdef SHOW_DEBUG
		rectangle(img_binary, validRect, Scalar(128,0,0), 1);
#endif

		Point pt;
		for ( int i = 0 ; i < contours.size() ; i++){
			Scalar m = mean(contours[i]);
			pt.x = m.val[1];
			pt.y = m.val[0];

			if(validRect.contains(pt) == false){
				drawContours( img_binary, contours, i, Scalar(0,0,0), CV_FILLED);
			}
		}
#endif		
		//adaptiveThreshold(img_gray, img_binary, 255, ADAPTIVE_THRESH_GAUSSIAN_C, THRESH_BINARY , 5, -1);
		//Canny(img_gray, img_binary, 20, 60);
#ifdef SHOW_DEBUG
		/// Show in a window
		imshow("Input", img_input);	
		imshow("gray", img_gray);	
		imshow( "binary", img_binary );
#endif
	

		moment = moments(img_binary, true);
		HuMoments(moment, huMoment[i]);
		
//		fprintf(fp, "spatial moments : %8.3f %8.3f %8.3f %8.3f %8.3f %8.3f %8.3f %8.3f %8.3f %8.3f \n",
//			moment.m00, moment.m10, moment.m01, moment.m20, moment.m11, moment.m02, moment.m30, moment.m21, moment.m12, moment.m03);
//		fprintf(fp, "central moments : %8.3f %8.3f %8.3f %8.3f %8.3f %8.3f %8.3f \n",
//			moment.mu20, moment.mu11, moment.mu02, moment.mu30, moment.mu21, moment.mu12, moment.mu03);
		fprintf(fp, "central normalized moments, %8.3f, %8.3f, %8.3f, %8.3f, %8.3f, %8.3f, %8.3f \n",
			moment.nu20, moment.nu11, moment.nu02, moment.nu30, moment.nu21, moment.nu12, moment.nu03);
		
		//fprintf(fp, "Humoments, %8.3f, %8.3f, %8.3f, %8.3f, %8.3f, %8.3f, %8.3f \n",
		//	huMoment[i][0], huMoment[i][1], huMoment[i][2], huMoment[i][3], huMoment[i][4], huMoment[i][5], huMoment[i][6]);

				
		Diff[0] = DiffShapes( huMoment[1], huMoment[i], 0);
		Diff[1] = DiffShapes( huMoment[1], huMoment[i], 1);
		Diff[2] = DiffShapes( huMoment[1], huMoment[i], 2);
		

		fprintf(fp_c, "Diff [1-%d], %8.3f, %8.3f, %8.3f \n", i, Diff[0], Diff[1], Diff[2]);

#ifdef SHOW_DEBUG
		cv::waitKey(0);
#endif
	}
	fclose(fp);
	fclose(fp_c);

	return 0;
}


#else


Mat get_hogdescriptor_visual_image(Mat& origImg, vector<float>& descriptorValues, Size winSize, Size cellSize, int scaleFactor, double viz_factor);

//find object angle
static float IC_Angle(const Mat& image, const int half_k, Point2f pt, const vector<int> & u_max)
{
	int m_01 = 0, m_10 = 0;
	const uchar* center = &image.at<uchar>(cvRound(pt.y), cvRound(pt.x));

	// Treat the center line differently, v=0
	for (int u = -half_k; u <= half_k; ++u)
		m_10 += u * center[u];

	// Go line by line in the circular patch
	int step = (int)image.step1();
	for (int v = 1; v <= half_k; ++v)
	{
		// Proceed over the two lines
		int v_sum = 0;
		int d = u_max[v];
		for (int u = -d; u <= d; ++u)
		{
			int val_plus = center[u + v*step], val_minus = center[u - v*step];
			v_sum += (val_plus - val_minus);
			m_10 += u * (val_plus + val_minus);
		}
		m_01 += v * v_sum;
	}

	return fastAtan2((float)m_01, (float)m_10);
}


double compare_descript(vector<float>& descriptor1, vector<float>& descriptor2)
{
	double N1Distance = 0;
	for (int i = 0; i < descriptor1.size(); i++){
		N1Distance += fabs(descriptor1[i] - descriptor2[i]);
	}
	return	N1Distance;
}


double compare_descript2(vector<float>& descriptor1, vector<float>& descriptor2)
{
	double N1Distance = 0;
	for (int i = 0; i < descriptor1.size(); i++){
		N1Distance += (descriptor1[i] - descriptor2[i])*(descriptor1[i] - descriptor2[i]);
	}
	return	sqrt(N1Distance);
}


int _tmain(int argc, _TCHAR* argv[])
{
	cv::Mat img_input, img_gray, hog_image;
	char strFileName[256];

	cv::Size FossilNoramlizeSize(64, 64);

	HOGDescriptor HoGD(FossilNoramlizeSize, cv::Size(16, 16), cv::Size(8, 8), cv::Size(8, 8), 9);
	float angle;
		

	// pre-compute the end of a row in a circular patch

	int halfPatchSize = 28;	//64 * 0.8 /2 
	vector<int> umax(halfPatchSize + 2);

	int v, v0, vmax = cvFloor(halfPatchSize * sqrt(2.f) / 2 + 1);
	int vmin = cvCeil(halfPatchSize * sqrt(2.f) / 2);
	for (v = 0; v <= vmax; ++v)
		umax[v] = cvRound(sqrt((double)halfPatchSize * halfPatchSize - v * v));

	// Make sure we are symmetric
	for (v = halfPatchSize, v0 = 0; v >= vmin; --v)
	{
		while (umax[v0] == umax[v0 + 1])
			++v0;
		umax[v] = v0;
		++v0;
	}
	
	Point2f center_pt(31, 31);

	vector<vector<float>> FossildescriptorsValues;

	for (int i = 1; i < 13; i++){
		sprintf(strFileName, "D:\\work2\\InnovaPlex\\test\\test%d.jpg", i);

		img_input = cv::imread(strFileName);
		resize(img_input, img_input, FossilNoramlizeSize);

		cvtColor(img_input, img_gray, CV_RGB2GRAY);
						
		vector<float> descriptorsValues;
		HoGD.compute(img_gray, descriptorsValues);

		//need computer center using moment. 
		angle = IC_Angle(img_gray, halfPatchSize, center_pt, umax);
		printf("Angle %f \n", angle);
		
		//
		hog_image = get_hogdescriptor_visual_image(img_input, descriptorsValues, FossilNoramlizeSize, cv::Size(8, 8), 4, 2);
		
		imshow("Input", hog_image);
		imshow("gray", img_gray);
		

		FossildescriptorsValues.push_back(descriptorsValues);
		cv::waitKey(0);
	}

	
	FILE *fp = fopen("D:\\work2\\InnovaPlex\\test\\result_hog.csv", "w+t");
	for (int i = 0; i < 12; i++){
		for (int j = 0; j < 12; j++){
			double distance = compare_descript(FossildescriptorsValues[i], FossildescriptorsValues[j]);
			double distance2 = compare_descript2(FossildescriptorsValues[i], FossildescriptorsValues[j]);
			fprintf(fp, "%d, %d, %8.3f, %8.3f\n", i, j, distance, distance2);
		}
	}


	fclose(fp);
	//Compare HOG descriptor


	return 0;
}



// HOGDescriptor visual_imagealizer
// adapted for arbitrary size of feature sets and training images
Mat get_hogdescriptor_visual_image(Mat& origImg, vector<float>& descriptorValues, Size winSize, Size cellSize, int scaleFactor, double viz_factor)
{
	Mat visual_image;
	resize(origImg, visual_image, Size(origImg.cols*scaleFactor, origImg.rows*scaleFactor));

	int gradientBinSize = 9;
	// dividing 180° into 9 bins, how large (in rad) is one bin?
	float radRangeForOneBin = 3.14 / (float)gradientBinSize;

	// prepare data structure: 9 orientation / gradient strenghts for each cell
	int cells_in_x_dir = winSize.width / cellSize.width;
	int cells_in_y_dir = winSize.height / cellSize.height;
	int totalnrofcells = cells_in_x_dir * cells_in_y_dir;
	float*** gradientStrengths = new float**[cells_in_y_dir];
	int** cellUpdateCounter = new int*[cells_in_y_dir];
	for (int y = 0; y<cells_in_y_dir; y++)
	{
		gradientStrengths[y] = new float*[cells_in_x_dir];
		cellUpdateCounter[y] = new int[cells_in_x_dir];
		for (int x = 0; x<cells_in_x_dir; x++)
		{
			gradientStrengths[y][x] = new float[gradientBinSize];
			cellUpdateCounter[y][x] = 0;

			for (int bin = 0; bin<gradientBinSize; bin++)
				gradientStrengths[y][x][bin] = 0.0;
		}
	}

	// nr of blocks = nr of cells - 1
	// since there is a new block on each cell (overlapping blocks!) but the last one
	int blocks_in_x_dir = cells_in_x_dir - 1;
	int blocks_in_y_dir = cells_in_y_dir - 1;

	// compute gradient strengths per cell
	int descriptorDataIdx = 0;
	int cellx = 0;
	int celly = 0;

	for (int blockx = 0; blockx<blocks_in_x_dir; blockx++)
	{
		for (int blocky = 0; blocky<blocks_in_y_dir; blocky++)
		{
			// 4 cells per block ...
			for (int cellNr = 0; cellNr<4; cellNr++)
			{
				// compute corresponding cell nr
				int cellx = blockx;
				int celly = blocky;
				if (cellNr == 1) celly++;
				if (cellNr == 2) cellx++;
				if (cellNr == 3)
				{
					cellx++;
					celly++;
				}

				for (int bin = 0; bin<gradientBinSize; bin++)
				{
					float gradientStrength = descriptorValues[descriptorDataIdx];
					descriptorDataIdx++;

					gradientStrengths[celly][cellx][bin] += gradientStrength;

				} // for (all bins)


				// note: overlapping blocks lead to multiple updates of this sum!
				// we therefore keep track how often a cell was updated,
				// to compute average gradient strengths
				cellUpdateCounter[celly][cellx]++;

			} // for (all cells)


		} // for (all block x pos)
	} // for (all block y pos)


	// compute average gradient strengths
	for (int celly = 0; celly<cells_in_y_dir; celly++)
	{
		for (int cellx = 0; cellx<cells_in_x_dir; cellx++)
		{

			float NrUpdatesForThisCell = (float)cellUpdateCounter[celly][cellx];

			// compute average gradient strenghts for each gradient bin direction
			for (int bin = 0; bin<gradientBinSize; bin++)
			{
				gradientStrengths[celly][cellx][bin] /= NrUpdatesForThisCell;
			}
		}
	}


	//cout << "descriptorDataIdx = " << descriptorDataIdx << endl;

	// draw cells
	for (int celly = 0; celly<cells_in_y_dir; celly++)
	{
		for (int cellx = 0; cellx<cells_in_x_dir; cellx++)
		{
			int drawX = cellx * cellSize.width;
			int drawY = celly * cellSize.height;

			int mx = drawX + cellSize.width / 2;
			int my = drawY + cellSize.height / 2;

			rectangle(visual_image,
				Point(drawX*scaleFactor, drawY*scaleFactor),
				Point((drawX + cellSize.width)*scaleFactor,
				(drawY + cellSize.height)*scaleFactor),
				CV_RGB(100, 100, 100),
				1);

			// draw in each cell all 9 gradient strengths
			for (int bin = 0; bin<gradientBinSize; bin++)
			{
				float currentGradStrength = gradientStrengths[celly][cellx][bin];

				// no line to draw?
				if (currentGradStrength == 0)
					continue;

				float currRad = bin * radRangeForOneBin + radRangeForOneBin / 2;

				float dirVecX = cos(currRad);
				float dirVecY = sin(currRad);
				float maxVecLen = cellSize.width / 2;
				float scale = viz_factor; // just a visual_imagealization scale,
				// to see the lines better

				// compute line coordinates
				float x1 = mx - dirVecX * currentGradStrength * maxVecLen * scale;
				float y1 = my - dirVecY * currentGradStrength * maxVecLen * scale;
				float x2 = mx + dirVecX * currentGradStrength * maxVecLen * scale;
				float y2 = my + dirVecY * currentGradStrength * maxVecLen * scale;

				// draw gradient visual_imagealization
				line(visual_image,
					Point(x1*scaleFactor, y1*scaleFactor),
					Point(x2*scaleFactor, y2*scaleFactor),
					CV_RGB(0, 0, 255),
					1);

			} // for (all bins)

		} // for (cellx)
	} // for (celly)


	// don't forget to free memory allocated by helper data structures!
	for (int y = 0; y<cells_in_y_dir; y++)
	{
		for (int x = 0; x<cells_in_x_dir; x++)
		{
			delete[] gradientStrengths[y][x];
		}
		delete[] gradientStrengths[y];
		delete[] cellUpdateCounter[y];
	}
	delete[] gradientStrengths;
	delete[] cellUpdateCounter;

	return visual_image;

}

#endif

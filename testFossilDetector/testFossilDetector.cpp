﻿// testFossilDetector.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "../include/FossilDetector.h"
#include "../include/FossilClassifier.h"


#include "opencv/cv.h"
#include "opencv/highgui.h"

#pragma comment(lib, "winmm.lib") 


#ifdef _DEBUG
#pragma comment(lib, "opencv_core2410d.lib")
#pragma comment(lib, "opencv_highgui2410d.lib")
#pragma comment(lib, "opencv_imgproc2410d.lib")

#ifdef _WIN64 
//#pragma comment(lib, "../x64/Debug//FossilDetectorML")
#pragma comment(lib, "../x64/Debug/FossilDetectorDK")
#pragma comment(lib, "../x64/Debug//FossilClassifier2")
#else
#pragma comment(lib, "../Win32/Debug//FossilDetectorML")
#pragma comment(lib, "../Win32/Debug//FossilClassifier2")
#endif

#else

#pragma comment(lib, "opencv_core2410.lib")
#pragma comment(lib, "opencv_highgui2410.lib")
#pragma comment(lib, "opencv_imgproc2410.lib")

#ifdef _WIN64 
//#pragma comment(lib, "../x64/Release/FossilDetectorML")
//#pragma comment(lib, "../x64/Release/FossilDetector")
#pragma comment(lib, "../x64/Release/FossilDetectorDK")

#pragma comment(lib, "../x64/Release/FossilClassifier2")
#else
#pragma comment(lib, "../Win32/Release/FossilDetector")
#pragma comment(lib, "../Win32/Release/FossilClassifier2")

#endif


#endif

#define TEST_DETECTOR_ONLY	


char* g_szConf= (	"{\"object_size_level\" : [68, 50, 38, 28, 22],\
					\"theshold_level\" : [200, 140, 102, 90, 80],\
					\"level_weight\" : [0.6,0.72,0.86,1.0,1.2],\
					\"min_distance\" : 1.2,\
					\"foreground_thread_low\" : 14,\
					\"foreground_thread_high\" : 12,\
					\"max_width\" : 76,\
					\"threadhold_fossil_occapy_ratio\" : 4,\
					\"threadhold_fossil_distance_ratio\" : 4 }"
 );


//char* g_szConf= (	"{ \"min_distance\" : 1.2 } " );


#ifdef TEST_DETECTOR_ONLY


int _tmain(int argc, _TCHAR* argv[])
{
	cv::Mat img_input;
	//MessageBoxA(NULL, "0000", "0000", MB_OK);
	
	//img_input = cv::imread("D:\\un-slide_p005z4t1c1.jpg");
	//img_input = cv::imread("D:\\un-slide_p006z4t1c1.jpg");
	//img_input = cv::imread("D:\\un-slide_p003z4t1c1.jpg");
	//img_input = cv::imread("D:\\un-slide_p001z4t1c1.jpg");
	//img_input = cv::imread("D:\\work2\\InnovaPlex\\unmarked sample\\multirotation_p05z3t1c1.jpg");
	//img_input = cv::imread("D:\\work2\\InnovaPlex\\unmarked sample\\multirotation_p37z3t1c1.jpg");
	//img_input = cv::imread("D:\\work2\\InnovaPlex\\ODP_Processed\\792E_10R_40_41_A_Processed.jpg_Files\\792E_10R_40_41_A_Processed_p000.jpg");
	
	
	int nMaxDetectCount, nDetectCount;
	FOSSILDETECTInfo *pDETECTInfo = NULL;
	//MessageBoxA(NULL, "aaaa", "aaaa", MB_OK);

	//const char* aa = "C:\\Users\\bughyuk\\Dropbox\\Projects\\vs_2013\\fossil_detector\\x64\\Release\\detect_darknet.conf";
	//MessageBoxA(NULL, aa, aa, MB_OK);
	//HANDLE handle = SetupFossilDetector("detect_darknet.conf");
	HANDLE handle = SetupFossilDetector("C:\\Users\\bughyuk\\Dropbox\\Projects\\vs_2013\\fossil_detector\\x64\\Debug\\detect_darknet.conf");
	if (handle == NULL){
		printf("can not setup fossile detecotor \n");
		return -1;
	}
	//HANDLE handle = SetupFossilDetector(0);

	char strInputName[256];
	int i;
	for (i = 2; i < 3; i++)
	{
		//sprintf(strInputName, "D:\\ODP_Processed\\792E_10R_40_41_A_Processed.jpg_Files\\792E_10R_40_41_A_Processed_p%03d.jpg", i);
		sprintf(strInputName, "C:\\11\\792E_10R_40_41_A_Processed_p%03d.jpg", i);
		//MessageBoxA(NULL, strInputName, strInputName, MB_OK);
		img_input = cv::imread(strInputName);
	
		nMaxDetectCount = nDetectCount = 0; 
		DWORD tick = timeGetTime();
		char temp_str1[10];
		char temp_str2[10];
		itoa(img_input.cols, temp_str1, 10);
		itoa(img_input.rows, temp_str2, 10);
		MessageBoxA(NULL, temp_str1, temp_str2, MB_OK);
		ProcessFossilDetector( handle, img_input.cols, img_input.rows, img_input.step.p[0], img_input.data, &nMaxDetectCount);
		if (nMaxDetectCount){
			pDETECTInfo = (FOSSILDETECTInfo *)malloc(nMaxDetectCount * sizeof(FOSSILDETECTInfo));
			GetFossilObject(handle, pDETECTInfo, &nDetectCount);
		}
	
		DWORD tick2 = timeGetTime();
		printf("Take Time : %d, \n", tick2-tick);

		//Draw result 
		cv::Scalar color(0, 0, 255);
		cv::Rect rc;
		char d_count[10];
		itoa(nDetectCount, d_count, 10);
		MessageBoxA(NULL, d_count, "detect count", MB_OK);

		for( int i = 0 ; i < nDetectCount ; i++)
		{
			rc.x = pDETECTInfo[i].rc.left;
			rc.y = pDETECTInfo[i].rc.top;
			rc.width = pDETECTInfo[i].rc.right - pDETECTInfo[i].rc.left;
			rc.height= pDETECTInfo[i].rc.bottom - pDETECTInfo[i].rc.top;

			if (pDETECTInfo[i].flag == FOSSIL_UPDATE_PROC_CNN){
				char strTemp[32];
				cv::Point pt(rc.x + 10, rc.y+20);
				sprintf(strTemp, "%d, %5.3f", pDETECTInfo[i].top_idx[0], pDETECTInfo[i].top_score[0]);
				cv::putText(img_input, strTemp, pt, cv::FONT_HERSHEY_SIMPLEX, 0.5, (255, 255, 255), 2);
			}
			rectangle(img_input, rc, color, 2); 
		}
	
		imshow(strInputName, img_input);

		cv::waitKey(0);
		//Draw result 
		if (pDETECTInfo){
			free(pDETECTInfo);
		}
	}

	EndupFossilDetector( handle );
	
	return 0;
}

#else

#include <list>
using namespace std;

int _tmain(int argc, _TCHAR* argv[])
{
	cv::Mat img_input;
	cv::Mat img_fossil;
	char strTemp[MAX_PATH];
	char strFossil[MAX_PATH];
	
	HANDLE hDetector = SetupFossilDetector("detect.conf");
	vector<vector<FOSSILDETECTInfo>> TotalFossilInfoList;
	int nTotalDETECTInfo = 0;
	int fossil_num = 0;
	DWORD tick = timeGetTime();
<<<<<<< HEAD
	for (int i = 1; i <= 10; i++){
	//for (int i = 1; i <= 156; i++){
		//sprintf(strTemp, "D:\\work2\\InnovaPlex\\new_fossil\\unmarked sample\\un-slide_p%03dz4t1c1.jpg", 1);
		sprintf(strTemp, "C:\\Users\\bughyuk\\Desktop\\test_images\\multirotation_p01z3t1c1.jpg", 1);
=======
	//for (int i = 1; i <= 10; i++){
	for (int i = 1; i <= 156; i++){
		sprintf(strTemp, "D:\\work2\\InnovaPlex\\new_fossil\\unmarked sample\\un-slide_p%03dz4t1c1.jpg", i);
>>>>>>> af8ae3091a2144d21c1781b0ee0393e41a0a1fbb
		img_input = cv::imread(strTemp);

		int nMaxDetectCount, nDetectCount;
		FOSSILDETECTInfo *pDETECTInfo = NULL;
		vector<FOSSILDETECTInfo> FossilInfoList;
				
		ProcessFossilDetector(hDetector, img_input.cols, img_input.rows, img_input.step.p[0], img_input.data, &nMaxDetectCount);
		if (nMaxDetectCount){
			pDETECTInfo = (FOSSILDETECTInfo *)malloc(nMaxDetectCount * sizeof(FOSSILDETECTInfo));
			GetFossilObject(hDetector, pDETECTInfo, &nDetectCount);
		}

		for (int j = 0; j < nDetectCount; j++){
			pDETECTInfo[j].image_index	= i;
			pDETECTInfo[j].class_no		= -1;
			FossilInfoList.push_back(pDETECTInfo[j]);
		}

		TotalFossilInfoList.push_back(FossilInfoList);
		nTotalDETECTInfo += nDetectCount;


		cv::Scalar color(0, 0, 255);
		cv::Rect rc;

		for (int i = 0; i < nDetectCount; i++)
		{
			rc.x = pDETECTInfo[i].rc.left;
			rc.y = pDETECTInfo[i].rc.top;
			rc.width = pDETECTInfo[i].rc.right - pDETECTInfo[i].rc.left;
			rc.height = pDETECTInfo[i].rc.bottom - pDETECTInfo[i].rc.top;

			if (pDETECTInfo[i].flag == FOSSIL_UPDATE_PROC_CNN){
				char strTemp[32];
				cv::Point pt(rc.x + 10, rc.y + 20);
				sprintf(strTemp, "%d, %5.3f", pDETECTInfo[i].top_idx[0], pDETECTInfo[i].top_score[0]);
				cv::putText(img_input, strTemp, pt, cv::FONT_HERSHEY_SIMPLEX, 0.5, (255, 255, 255), 2);
			}
			rectangle(img_input, rc, color, 2);
		}

		/*
		for (int j = 0; j < nDetectCount; j++){		
			cv::Rect FossilRect(pDETECTInfo[j].rc.left, pDETECTInfo[j].rc.top, pDETECTInfo[j].rc.right-pDETECTInfo[j].rc.left, pDETECTInfo[j].rc.bottom-pDETECTInfo[j].rc.top);
			img_fossil = img_input(FossilRect);
			sprintf(strFossil, "D:\\work2\\InnovaPlex\\fossil\\fossil_%02d.jpg", fossil_num++);
			imwrite(strFossil, img_fossil); 
		}
		*/

		if (pDETECTInfo){
			free(pDETECTInfo);
		}

		imshow("Restul", img_input);
		cv::waitKey(0);

	}
	DWORD tick2 = timeGetTime();
	printf("Detector Take Time : %d, \n", tick2 - tick);
	EndupFossilDetector(hDetector);
		
		
	//ouptu fissli 
	FOSSILDETECTInfo *pTotalDETECTInfo = (FOSSILDETECTInfo *)malloc(nTotalDETECTInfo * sizeof(FOSSILDETECTInfo));
	int k = 0;
	for (int i = 0; i < TotalFossilInfoList.size() ; i++){
		for (int j = 0; j < TotalFossilInfoList[i].size(); j++){
			memcpy(&pTotalDETECTInfo[k++], &TotalFossilInfoList[i][j], sizeof(FOSSILDETECTInfo));
			
		}
	}

	tick = timeGetTime();
	HANDLE	hClassifier = CreateClassifier();
	ProcessClassification(hClassifier, nTotalDETECTInfo, pTotalDETECTInfo);
	tick2 = timeGetTime();
	printf("Classification Take Time : %d, \n", tick2 - tick);

	DestroyClassifier(hClassifier);
	
	//Update result
	k = 0;
	for (int i = 0; i < TotalFossilInfoList.size(); i++){
		for (int j = 0; j < TotalFossilInfoList[i].size(); j++){
			TotalFossilInfoList[i][j].class_no = pTotalDETECTInfo[k].class_no;
			k++;
		}
	}


	free(pTotalDETECTInfo);

	return 0;
}

#endif
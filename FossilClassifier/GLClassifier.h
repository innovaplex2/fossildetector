#pragma once
#include <windows.h>
#include "../Include/FossilClassifier.h"

#define HUMOMENT_THRESHOLD		3.0f
#define COLORCOST_THRESHOLD		10.0f

class GLClassifier
{
public:
	GLClassifier();
	~GLClassifier();

	BOOL	GLClassificationStart(int nTotalFossil, FOSSILDETECTInfo* FossilInfo);
	
private:
	float HuMomentCostComputation(double HuMoment1[], double HuMoment2[], int ComputeType);
	float EuclideanDistance(float Vec1[], float Vec2[], int VecSize);
	float ColorCostComputation(float RGBMean1[], float RGBMean2[], int ComputeType);
	int FossilSizeComparison(FOSSILDETECTInfo Fossil_1, FOSSILDETECTInfo Fossil_2);
	int LocalFeatureMatching(FOSSILDETECTInfo Fossil_1, FOSSILDETECTInfo Fossil_2);
};
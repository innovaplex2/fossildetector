#include "../Include/FossilClassifier.h"
#include "GLClassifier.h"

HANDLE __stdcall CreateClassifier()
{
	return (HANDLE) new GLClassifier;
}

void __stdcall DestroyClassifier(HANDLE handle)
{
	if(handle){
		delete (GLClassifier *)handle;
	}
}

BOOL __stdcall ProcessClassification(HANDLE handle, int nTotalFossil, FOSSILDETECTInfo* pFossilInfos)
{
	if(handle != NULL)
	{
		GLClassifier* pClassifier = (GLClassifier*)handle;
		return pClassifier->GLClassificationStart(nTotalFossil, pFossilInfos);
	}

	return FALSE;
}


BOOL __stdcall ProcessClassification2(HANDLE handle, int nTotalFossil, FOSSILDETECTInfo* pInFossilInfos, FOSSILDETECTInfo* pOutFossilInfos)
{
	if (handle != NULL)
	{
		GLClassifier* pClassifier = (GLClassifier*)handle;
		memcpy(pOutFossilInfos, pInFossilInfos, sizeof(FOSSILDETECTInfo)*nTotalFossil);
		return pClassifier->GLClassificationStart(nTotalFossil, pOutFossilInfos);
	}

	return FALSE;
}
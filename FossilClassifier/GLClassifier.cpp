#include "GLClassifier.h"
#include <stdio.h>
#include <math.h>

//#define REPORT_RESULT 

GLClassifier::GLClassifier()
{
	
}
GLClassifier::~GLClassifier()
{
	
}

BOOL GLClassifier::GLClassificationStart(int nTotalFossil, FOSSILDETECTInfo* pFossilInfos)
{
	int* FossilTypeFlag	= new int[nTotalFossil];
	memset(FossilTypeFlag,	0, sizeof(int)*nTotalFossil);

	for(int n = 0; n < nTotalFossil; n++){
		if(pFossilInfos[n].type == FOSSIL_TYPE_BLOB){
			FossilTypeFlag[n] = 0;	// Blob type == 0
		}
		else if(pFossilInfos[n].type == FOSSIL_TYPE_LINE){
			FossilTypeFlag[n] = 1;	// Line type == 1
		}
	}

	int Index = 2;
#ifdef REPORT_RESULT
	FILE* file = fopen("Matching_result2.csv", "at");
#endif

	for(int n = 0; n < nTotalFossil; n++){
		int Flag				= 0;
		int LocalFeatureCost	= 0;
		int FossilSize			= 0;
		float HuCost			= 0;
		float ColorCost			= 0;
		char buffer[256] = {};

		if(FossilTypeFlag[n] == 0){
			for(int i = n+1; i < nTotalFossil; i++){
				if(FossilTypeFlag[i] == 0){
					HuCost = HuMomentCostComputation(pFossilInfos[n].hu_moments, pFossilInfos[i].hu_moments, 0);
					ColorCost = ColorCostComputation(pFossilInfos[n].rgb_mean, pFossilInfos[i].rgb_mean, 0);
					LocalFeatureCost = LocalFeatureMatching(pFossilInfos[n], pFossilInfos[i]);
					FossilSize = FossilSizeComparison(pFossilInfos[n], pFossilInfos[i]);

					//sprintf(buffer, "flag 0 ==> %f : %d : %f : %d", ColorCost, LocalFeatureCost, HuCost, FossilSize);
					//MessageBoxA(NULL, buffer, "111", MB_OK);

					if (ColorCost < COLORCOST_THRESHOLD
							//&& LocalFeatureCost == 1
							&& HuCost < HUMOMENT_THRESHOLD
							&& FossilSize == 1
					){
						FossilTypeFlag[n] = Index;
						FossilTypeFlag[i] = Index;
						Flag = 1;
					}
				}
			}
		}
		else if(FossilTypeFlag[n] == 1){
			for(int i = n+1; i < nTotalFossil; i++){
				if(FossilTypeFlag[i] == 1){
					HuCost = HuMomentCostComputation(pFossilInfos[n].hu_moments, pFossilInfos[i].hu_moments, 0);
					ColorCost = ColorCostComputation(pFossilInfos[n].rgb_mean, pFossilInfos[i].rgb_mean, 0);
					LocalFeatureCost = LocalFeatureMatching(pFossilInfos[n], pFossilInfos[i]);
					FossilSize = FossilSizeComparison(pFossilInfos[n], pFossilInfos[i]);

					//sprintf(buffer, "flag 1 ==> %f : %d : %f : %d", ColorCost, LocalFeatureCost, HuCost, FossilSize);
					//MessageBoxA(NULL, buffer, "111", MB_OK);

					if(ColorCost < COLORCOST_THRESHOLD
							//&& LocalFeatureCost == 1
							&& HuCost < HUMOMENT_THRESHOLD
							&& FossilSize == 1
					){
						FossilTypeFlag[n] = Index;
						FossilTypeFlag[i] = Index;
						Flag = 1;
					}
				}
			}
		}
		
		if(Flag == 1){
			Index++;
		}
	}

	for(int i = 0; i < nTotalFossil; i++){
		if(FossilTypeFlag[i] == 0 || FossilTypeFlag[i] == 1){
			FossilTypeFlag[i] = 0;
		}
	}
	for(int i = 0; i < nTotalFossil; i++){
		float FossilRatio = (float)abs(pFossilInfos[i].rc.bottom-pFossilInfos[i].rc.top)
							/ (float)abs(pFossilInfos[i].rc.right-pFossilInfos[i].rc.left);

		if(FossilRatio < 0.5 || FossilRatio > 2.0){
			FossilTypeFlag[i] = 0;
		}
		pFossilInfos[i].class_no = FossilTypeFlag[i] > 1 ? FossilTypeFlag[i] - 1 : FossilTypeFlag[i];
	}
	
#ifdef REPORT_RESULT
	for(int i = 0; i < nTotalFossil; i++){
//		fprintf(file, "%d, %f, %f\n", FossilTypeFlag[i], FossilHuCost[i], FossilColorCost[i]);
	}
	fclose(file);
#endif

	delete[] FossilTypeFlag;

	return TRUE;
}

float GLClassifier::HuMomentCostComputation(double HuMoment1[], double HuMoment2[], int MatchType)
{
	float Result = 0.0f;
	float Temp[7] = {0.0};

	for(int n = 0; n < 7; n++)
	{
		float I = 0.0f;
		float Mn1 = sin(HuMoment1[n] + 0.05f) * log(HuMoment1[n] + 0.05f);
		float Mn2 = sin(HuMoment2[n] + 0.05f) * log(HuMoment2[n] + 0.05f);

		if(MatchType == 0)
		{
			I = abs(1.0f/Mn1 - 1.0f/Mn2);
			Result += I;
		}
		else if(MatchType == 1)
		{
			I = abs(Mn1 + Mn2);
			Result += I;
		}
		else if(MatchType == 2)
		{
			float MaxValue = 0.0f;
			Temp[n] = abs(Mn1 - Mn2) / abs(Mn1);
			if(n == 6){
				for(int t = 0; t < 7; t++){
					if(Temp[t] > MaxValue){
						MaxValue = Temp[t];
					}
				}
				Result = MaxValue;
			}
		}
	}

	return Result;
}
float GLClassifier::EuclideanDistance(float Vec1[], float Vec2[], int VecSize)
{
	float Result = 0.0f;

	for(int n = 0; n < VecSize; n++)
	{
		Result += (float)((Vec1[n] - Vec2[n]) * (Vec1[n] - Vec2[n]));
	}

	return sqrt(Result);
}
float GLClassifier::ColorCostComputation(float RGBMean1[], float RGBMean2[], int ComputeType)
{
	float DiffRG_1 = RGBMean1[0]-RGBMean1[1];
	float DiffRB_1 = RGBMean1[0]-RGBMean1[2];
	float DiffGB_1 = RGBMean1[1]-RGBMean1[2];

	float DiffRG_2 = RGBMean2[0]-RGBMean2[1];
	float DiffRB_2 = RGBMean2[0]-RGBMean2[2];
	float DiffGB_2 = RGBMean2[1]-RGBMean2[2];

	float Result = abs(DiffRG_1 - DiffRG_2) + abs(DiffRB_1-DiffRB_2) + abs(DiffGB_1-DiffGB_2);

	return Result;
}
int GLClassifier::LocalFeatureMatching(FOSSILDETECTInfo Fossil_1, FOSSILDETECTInfo Fossil_2)
{
	int Score = 0;
	int NumOfFeature = 0;
	int Fossil1_sizeX = abs(Fossil_1.rc.right - Fossil_1.rc.left);
	int Fossil1_sizeY = abs(Fossil_1.rc.bottom - Fossil_1.rc.top);
	int Fossil2_sizeX = abs(Fossil_2.rc.right - Fossil_2.rc.left);
	int Fossil2_sizeY = abs(Fossil_2.rc.bottom - Fossil_2.rc.top);
	float Descriptor1[64];
	float Descriptor2[64];
	
	for(int j = 0; j < Fossil_1.num_of_feature; j++)
	{
		if(Fossil_1.keypoints[0] > Fossil1_sizeX * 0.1 && Fossil_1.keypoints[0] < Fossil1_sizeX * 0.9
				&& Fossil_1.keypoints[1] > Fossil1_sizeY * 0.1 && Fossil_1.keypoints[1] < Fossil1_sizeY * 0.9)
		{
			NumOfFeature++;

			int flag = 0;

			float* CostTable = new float[Fossil_2.num_of_feature];
			memset(CostTable, 0, Fossil_2.num_of_feature);
			memcpy(Descriptor1, &Fossil_1.descriptors[j*64], 64*sizeof(float)); 

			for(int i = 0; i < Fossil_2.num_of_feature; i++)
			{
				memcpy(Descriptor2, &Fossil_2.descriptors[i*64], 64*sizeof(float)); 
				float cost = EuclideanDistance(Descriptor1, Descriptor2, 64);
				
				CostTable[i] = cost;
			}

			float MinCost = 9999.0f;
			for(int i = 0; i < Fossil_2.num_of_feature; i++)
			{
				if(CostTable[i] < MinCost){
					MinCost = CostTable[i];
				}
			}
			for(int i = 0; i < Fossil_2.num_of_feature; i++)
			{
				CostTable[i] = (float)MinCost / (float)CostTable[i];
			}

			for(int i = 0; i < Fossil_2.num_of_feature; i++)
			{
				if(CostTable[i] > 0.9){
				//if(MinCost < 3.0){
					Score++;
				}

			}
		}
	}

	if(Score > NumOfFeature * 0.3)
	//if(Score > 10)
	{
		return 1;
	}
	else
	{
		return 0;
	}
}
int GLClassifier::FossilSizeComparison(FOSSILDETECTInfo Fossil_1, FOSSILDETECTInfo Fossil_2)
{
	int Fossil1_sizeX = abs(Fossil_1.rc.right - Fossil_1.rc.left);
	int Fossil1_sizeY = abs(Fossil_1.rc.bottom - Fossil_1.rc.top);
	int Fossil2_sizeX = abs(Fossil_2.rc.right - Fossil_2.rc.left);
	int Fossil2_sizeY = abs(Fossil_2.rc.bottom - Fossil_2.rc.top);
	int Fossil1_Size = Fossil1_sizeX*Fossil1_sizeY;
	int Fossil2_Size = Fossil2_sizeX*Fossil2_sizeY;

	float Result = (float)Fossil1_Size / (float)Fossil2_Size;
	if(Result < 1.4 && Result > 0.6)
	{
		return 1;
	}
	else{
		return 0;
	}
}
﻿using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Media;
using System.Windows.Controls;


namespace FossilDetectorApp
{
	public class ProgressWin : Window
	{
		MainWindow main_win = null;
		ProgressBar progress_bar = null;
		int progress_cnt = 0;

		public ProgressWin(double progress_max, string progress_text)
		{
			this.main_win = (MainWindow)Application.Current.Windows[0];

			this.MinWidth = this.MaxWidth = 600;
			this.MinHeight = this.MaxHeight = 50;
			//this.Background = Brushes.Lime;
			this.WindowStyle = WindowStyle.None;

			StackPanel win_pane = new StackPanel
			{
				Orientation = Orientation.Horizontal
				//Background = Brushes.Pink
			};
			Label progress_label = new Label
			{
				Width = 200,
				HorizontalContentAlignment = HorizontalAlignment.Center,
				VerticalAlignment = System.Windows.VerticalAlignment.Center,
				Content = progress_text
			};
			this.progress_bar = new ProgressBar
			{
				Width = this.MinWidth - 250,
				Height = 15,
				Minimum = 0,
				Maximum = progress_max
			};

			win_pane.Children.Add(progress_label);
			win_pane.Children.Add(this.progress_bar);

			this.Content = win_pane;

			this.WindowStartupLocation = System.Windows.WindowStartupLocation.CenterOwner;
			this.Owner = Application.Current.MainWindow;
			this.Show();
		}

		public void SetVal(object sender, ProgressChangedEventArgs e)
		{
			if (this.progress_bar.Value < e.ProgressPercentage)
				this.progress_bar.Value = e.ProgressPercentage;
		}

		public void CloseWin(object sender, RunWorkerCompletedEventArgs e, int max_cnt, string process_step)
		{
			this.progress_cnt++;

			if (this.progress_cnt >= max_cnt)
			{
				if (process_step == "load")
				{
					this.main_win.btn_an.IsEnabled = true;

					int file_idx = 0;

					foreach (string file_path in main_win.sel_files)
					{
						main_win.list_pane.AddFile(file_idx, file_path);
						file_idx ++;
					}

					this.SelFirst(0);
				}
				else if (process_step == "an")
				{
					this.main_win.btn_an.IsEnabled = false;
					this.main_win.btn_ca.IsEnabled = true;
					this.SelFirst(500);
				}

				this.Close();
			}
		}

		private void SelFirst(int sleep_sec)
		{
			System.Threading.Thread.Sleep(sleep_sec);

			this.main_win.list_pane.file_list.Items.MoveCurrentToFirst();
			ListBoxItem first_item = (ListBoxItem)this.main_win.list_pane.file_list.Items.CurrentItem;

			if (first_item != null)
			{
				((ListBoxItem)this.main_win.list_pane.file_list.Items.CurrentItem).IsSelected = true;
				this.main_win.list_pane.SelFile();
			}
		}
	}
}

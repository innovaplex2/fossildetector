﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.IO;
using OpenCvSharp.CPlusPlus;


namespace FossilDetectorApp
{
	class CookieCut
	{
		Border item_border = null;
		Image item_img = null;
		StackPanel cookie_pane = null;
		StackPanel group_pane = null;
		StackPanel cur_pane = null;
		MainWindow.FOSSILDETECTInfo cookie_data;
		OpenCvSharp.CPlusPlus.Rect cookie_rect;
		MemoryStream cookie_stream = null;

		public CookieCut(Mat cur_img, int cookie_idx, MainWindow.FOSSILDETECTInfo cookie_data, StackPanel cookie_pane, StackPanel group_pane, StackPanel start_pane)
		{
			this.cookie_rect = CommonUtil.SubRect(cookie_data.rc, cur_img.Width, cur_img.Height);
			Mat img_cookie = cur_img.SubMat(this.cookie_rect.Y, this.cookie_rect.Y + this.cookie_rect.Height, this.cookie_rect.X, this.cookie_rect.X + this.cookie_rect.Width);

			this.cookie_data = cookie_data;
			this.cookie_stream = img_cookie.ToMemoryStream();
			this.cookie_pane = cookie_pane;
			this.group_pane = group_pane;

			this.item_border = new Border
			{
				BorderBrush = new SolidColorBrush(Colors.Transparent),
				CornerRadius = new CornerRadius(10),
				BorderThickness = new Thickness(2),
				Margin = new Thickness(5),
				VerticalAlignment = VerticalAlignment.Top,
				HorizontalAlignment = HorizontalAlignment.Center
			};
			this.item_img = new Image
			{
				Stretch = Stretch.Fill,
				Width = this.cookie_rect.Width,
				Height = this.cookie_rect.Height,
				Tag = cookie_idx
			};

			this.item_border.Child = this.item_img;

			this.item_img.Source = CommonUtil.MemImg(this.cookie_stream);
			this.item_img.MouseDown += new MouseButtonEventHandler(this.DownImage);
			this.item_img.MouseMove += new MouseEventHandler(this.MoveImage);

			this.item_border.Width = this.item_img.Width;
			this.item_border.Height = this.item_img.Height;

			this.ToPane(start_pane);
		}

		public void ToPane(StackPanel to_pane)
		{
			if (this.cur_pane != null)
				this.cur_pane.Children.Remove(this.item_border);

			this.cur_pane = to_pane;

			this.cur_pane.Children.Add(this.item_border);

			// auto new label
			if (false)
			{
				for (int i = this.group_pane.Children.Count - 1; i >= 0; i --)
				{
					StackPanel box_pane = (StackPanel)this.group_pane.Children[i];
					//StackPanel item_pane = (StackPanel)((ScrollViewer)box_pane.Children[1]).Content;
					StackPanel item_pane = (StackPanel)((ScrollViewer)((Border)box_pane.Children[1]).Child).Content;

					if (item_pane.Children.Count == 0)
						this.group_pane.Children.Remove(box_pane);
				}

				new GroupPane(this.cookie_pane, this.group_pane, "New Group");
			}
		}

		public Border GetBorder()
		{
			return this.item_border;
		}

		public StackPanel GetPane()
		{
			return this.cur_pane;
		}

		private void DownImage(object sender, MouseButtonEventArgs e)
		{
			Image cur_img = (Image)sender;
			if (cur_img == null) return;
		}

		private void MoveImage(object sender, MouseEventArgs e)
		{
			Image cur_img = (Image)sender;
			if (cur_img == null) return;
			Border cur_border = (Border)cur_img.Parent;
			//cur_border.BorderBrush = new SolidColorBrush(Colors.Black);
			DataObject drag_data = new DataObject("cookie_cut", this);
			DragDrop.DoDragDrop(cur_border, drag_data, DragDropEffects.Link);
		}

		private void LeaveImage(object sender, MouseEventArgs e)
		{
			Image cur_img = (Image)sender;
			if (cur_img == null) return;
			Border cur_border = (Border)cur_img.Parent;
			cur_border.BorderBrush = new SolidColorBrush(Colors.Transparent);
		}
	}
}

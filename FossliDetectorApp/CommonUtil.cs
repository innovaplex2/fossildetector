﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Runtime.InteropServices;
using System.IO;
using System.Windows;
using OpenCvSharp;
using OpenCvSharp.CPlusPlus;


namespace FossilDetectorApp
{
	static class CommonUtil
	{
		public static ImageSource MemImg(MemoryStream img_stream)
		{
			try
			{
				BitmapImage bitmap_img = new BitmapImage();
				bitmap_img.BeginInit();
				bitmap_img.StreamSource = img_stream;
				bitmap_img.EndInit();
				return bitmap_img;
			}
			catch (Exception ex)
			{
				string ex_msg = ex.Message;
				return null;
			}
		}

		public static OpenCvSharp.CPlusPlus.Rect SubRect(MainWindow.RECT org_rect, int img_width, int img_height)
		{
			OpenCvSharp.CPlusPlus.Rect buff_rect;
			OpenCvSharp.CPlusPlus.Rect fixed_rect;

			buff_rect.X = fixed_rect.X = org_rect.left;
			buff_rect.Y = fixed_rect.Y = org_rect.top;
			buff_rect.Width = fixed_rect.Width = org_rect.right - org_rect.left;
			buff_rect.Height = fixed_rect.Height = org_rect.bottom - org_rect.top;

			if (buff_rect.X + buff_rect.Width > img_width)
				fixed_rect.Width = img_width - buff_rect.X;

			if (buff_rect.Y + buff_rect.Height > img_height)
				fixed_rect.Height = img_height - buff_rect.Y;

			return fixed_rect;
		}

		public static bool ChkWin(System.Windows.Window chk_win)
		{
			foreach (System.Windows.Window cur_win in Application.Current.Windows)
			{
				if (cur_win == chk_win)
					return true;
			}

			return false;
		}

		public static void SaveImg(System.Windows.Controls.Image save_img, string save_path)
		{
			RenderTargetBitmap render_bmp = new RenderTargetBitmap((int)save_img.Width, (int)save_img.Height, 90, 90, PixelFormats.Default);
			render_bmp.Render(save_img);

			PngBitmapEncoder png_enc = new PngBitmapEncoder();
			png_enc.Frames.Add(BitmapFrame.Create(render_bmp));

			Stream img_buff = new MemoryStream();
			png_enc.Save(img_buff);

			img_buff.Position = 0;

			System.Drawing.Image res_img = System.Drawing.Image.FromStream(img_buff);
			res_img.Save(save_path);
		}
	}
}

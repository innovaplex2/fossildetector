﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Runtime.InteropServices;
using System.IO;
using System.Diagnostics;
using OpenCvSharp;
using OpenCvSharp.CPlusPlus;


namespace FossilDetectorApp
{
	/// <summary>
	/// Interaction logic for MainWindow.xaml
	/// </summary>
	public partial class MainWindow : System.Windows.Window
	{
        public enum eFOSSILE_TYPE { FOSSIL_TYPE_BLOB, FOSSIL_TYPE_LINE };
        public enum eFOSSILE_UPDATE_FLAG { FOSSIL_UPDATE_NONE, FOSSIL_UPDATE_UPDATE } ;

        public struct RECT
        {
            public int left;
            public int top;
            public int right;
            public int bottom;
        };

        public struct FOSSILDETECTInfo
        {
            public eFOSSILE_UPDATE_FLAG flag;		// Update flag

            public int fossil_id;
            public int image_index;
            public int class_no;
            public float class_distance;	// distance in class (low score is close)

            public RECT rc;
            public eFOSSILE_TYPE type;

            public float mean;			//normalize by global mean
            public float stddev;

            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 3)]
            public float[] rgb_mean;

            public float width;
            public float height;
            public float angle;
            public float area;

            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 7)]
            public double[] hu_moments;

            public int size_of_descriptor;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 1764)]
            public short[] descriptors;
        }

        [DllImport("FossilDetector.dll")]
        public static extern IntPtr SetupFossilDetector([MarshalAs(UnmanagedType.LPStr)]String szInputFile);

        [DllImport("FossilDetector.dll")]
        public static extern bool ProcessFossilDetector(IntPtr handle, int width, int height, int step,
                IntPtr image, ref int pMaxDetectCount);

        [DllImport("FossilDetector.dll")]
        public static extern bool GetFossilObject(IntPtr handle, [Out] FOSSILDETECTInfo[] info, ref int pDetectCount);

        [DllImport("FossilDetector.dll")]
        public static extern bool UpdateFossilObject(IntPtr handle, int nDetectCount, [Out] FOSSILDETECTInfo[] info);

        [DllImport("FossilDetector.dll")]
        public static extern bool ProcessUpdateFossilObject(IntPtr handle, int width, int height, int step,
                IntPtr image, int nDetectCount, [Out] FOSSILDETECTInfo[] info);

        [DllImport("FossilDetector.dll")]
        public static extern void EndupFossilDetector(IntPtr handle);


        [DllImport("FossilClassifier2.dll")]
        public static extern IntPtr CreateClassifier();

        [DllImport("FossilClassifier2.dll")]
        public static extern bool ProcessClassification2(IntPtr handle, int nTotalFossil, [In] FOSSILDETECTInfo[] in_info, [Out] FOSSILDETECTInfo[] out_info);

        [DllImport("FossilClassifier2.dll")]
        public static extern void DestroyClassifier(IntPtr handle);

		private static int MAX_FILE = 512;

		public List<List<FOSSILDETECTInfo>> cookie_list = new List<List<FOSSILDETECTInfo>>(); // Cookie Cut
		//List<FOSSILDETECTInfo> cookie_list = new List<FOSSILDETECTInfo>();

		//public string[] file_path = new string[MAX_GROUP];
		//private string[] sel_files;

		public string[] sel_files;

		List<string> file_list = new List<string>();
		public Mat[] img_list = new Mat[MAX_FILE];
		public Mat[] disp_list = new Mat[MAX_FILE];
		public FOSSILDETECTInfo[] cookie_data = new FOSSILDETECTInfo[4 * 1024];

		public ListPane list_pane = null;
		private int tot_cnt = 0;

		public static Brush bg_color = Brushes.LightSteelBlue;
		public static Brush fg_color = Brushes.Lavender;
		public static Brush pt_color = Brushes.LightSlateGray;

		public MainWindow()
		{
			InitializeComponent();

			// font
			//this.FontFamily = new FontFamily("Liberation Sans");
			//this.FontSize = 10.5;

			// init buttons
			btn_an.IsEnabled = btn_st.IsEnabled = btn_sa.IsEnabled = btn_ca.IsEnabled = false;

			// background
			menu_pane.Background = bg_color;

			// main
			this.list_pane = new ListPane(main_pane);
			//this.list_pane.SetImg("img\\test.jpg");
		}

		private void SizeWin(object sender, EventArgs e)
		{
			System.Windows.Window main_win = (System.Windows.Window)sender;
			this.main_pane.Width = main_win.ActualWidth;
			this.main_pane.Height = main_win.ActualHeight - 120;

			try { this.list_pane.ResizePane(); }
			catch { }

			/*if(main_pane.Children.GetType() == typeof(ListPane)) {
			}*/
		}

		private void CloseWin(object sender, EventArgs e)
		{
		}

		private void LoadWin(object sender, RoutedEventArgs e)
		{
			System.Windows.Window open_win = new System.Windows.Window
			{
				MinWidth = 300,
				MinHeight = 300,
				MaxWidth = 300,
				MaxHeight = 300,
				WindowStyle = WindowStyle.ToolWindow,
				Title = "Load Images"
			};
			StackPanel win_pane = new StackPanel
			{
				Orientation = System.Windows.Controls.Orientation.Vertical,
				Width = 300
			};
			StackPanel radio_pane = new StackPanel
			{
				Orientation = System.Windows.Controls.Orientation.Vertical,
				Width = 300,
				Height = 90
			};
			StackPanel input_pane = new StackPanel
			{
				Orientation = System.Windows.Controls.Orientation.Vertical,
				Width = 300,
				Height = 125
			};
			StackPanel button_pane = new StackPanel
			{
				Orientation = System.Windows.Controls.Orientation.Horizontal,
				HorizontalAlignment = System.Windows.HorizontalAlignment.Right,
				Width = 175,
				Height = 47
			};
			System.Windows.Controls.RadioButton load_remote = new System.Windows.Controls.RadioButton
			{
				Margin = new Thickness(80, 10, 0, 0),
				Content = "Load from Remote."
			};
			/*RadioButton load_local = new RadioButton
			{
				IsChecked = true,
				Margin = new Thickness(80, 10, 0, 0),
				Content = "Load from Local."
			};*/
			System.Windows.Controls.RadioButton load_dir = new System.Windows.Controls.RadioButton
			{
				IsChecked = true,
				Margin = new Thickness(80, 10, 0, 0),
				Content = "Load from Dir."
			};
			System.Windows.Controls.RadioButton load_file = new System.Windows.Controls.RadioButton
			{
				IsChecked = false,
				Margin = new Thickness(80, 10, 0, 0),
				Content = "Load from File(s)."
			};
			System.Windows.Controls.Label path_label = new System.Windows.Controls.Label
			{
				HorizontalContentAlignment = System.Windows.HorizontalAlignment.Center,
				Height = 30
			};
			StackPanel wellid_pane = new StackPanel
			{
				Orientation = System.Windows.Controls.Orientation.Horizontal,
				Width = 300,
				Height = 30
			};
			System.Windows.Controls.Label wellid_label = new System.Windows.Controls.Label
			{
				Width = 55,
				Height = 25,
				Margin = new Thickness(70, 0, 0, 0),
				HorizontalContentAlignment = System.Windows.HorizontalAlignment.Right,
				Content = "Well ID :"
			};
			System.Windows.Controls.TextBox wellid_textbox = new System.Windows.Controls.TextBox
			{
				Width = 100,
				Height = 20,
				Margin = new Thickness(5, 0, 0, 0)
			};
			StackPanel depth_pane = new StackPanel
			{
				Orientation = System.Windows.Controls.Orientation.Horizontal,
				Width = 300,
				Height = 30
			};
			System.Windows.Controls.Label depth_label = new System.Windows.Controls.Label
			{
				Width = 55,
				Height = 25,
				Margin = new Thickness(70, 0, 0, 0),
				HorizontalContentAlignment = System.Windows.HorizontalAlignment.Right,
				Content = "Depth :"
			};
			System.Windows.Controls.TextBox depth_textbox = new System.Windows.Controls.TextBox
			{
				Width = 100,
				Height = 20,
				Margin = new Thickness(5, 0, 0, 0)
			};
			System.Windows.Controls.Label date_label = new System.Windows.Controls.Label
			{
				//VerticalAlignment = VerticalAlignment.Top,
				HorizontalContentAlignment = System.Windows.HorizontalAlignment.Center,
				Height = 30,
				Content = "Current Time : " + DateTime.Now.ToString("M/dd/yyyy HH:mm tt")
			};
			System.Windows.Controls.Button load_cancel = new System.Windows.Controls.Button
			{
				VerticalAlignment = VerticalAlignment.Center,
				Width = 70,
				Height = 28,
				Margin = new Thickness(10, 0, 0, 0),
				Content = "Cancel"
			};
			System.Windows.Controls.Button load_ok = new System.Windows.Controls.Button
			{
				VerticalAlignment = VerticalAlignment.Center,
				Width = 70,
				Height = 28,
				Content = "OK"
			};

			//open_win.Closing += new System.ComponentModel.CancelEventHandler(this.MainClose);
			load_ok.Click += new RoutedEventHandler(this.LoadRun);
			load_cancel.Click += new RoutedEventHandler(this.LoadCancel);
			//DockPanel.SetDock(load_cancel, Dock.Right);
			//DockPanel.SetDock(load_ok, Dock.Right);

			radio_pane.Children.Add(load_remote);
			radio_pane.Children.Add(load_dir);
			radio_pane.Children.Add(load_file);

			wellid_pane.Children.Add(wellid_label);
			wellid_pane.Children.Add(wellid_textbox);
			depth_pane.Children.Add(depth_label);
			depth_pane.Children.Add(depth_textbox);
			input_pane.Children.Add(path_label);
			input_pane.Children.Add(wellid_pane);
			input_pane.Children.Add(depth_pane);
			input_pane.Children.Add(date_label);

			button_pane.Children.Add(load_ok);
			button_pane.Children.Add(load_cancel);
			win_pane.Children.Add(radio_pane);
			win_pane.Children.Add(input_pane);
			win_pane.Children.Add(button_pane);
			open_win.Content = win_pane;
			open_win.ShowDialog();
		}

		private void LoadRun(object sender, EventArgs e)
		{
			System.Windows.Window open_win =
					(System.Windows.Window)((StackPanel)((StackPanel)((System.Windows.Controls.Button)sender).Parent).Parent).Parent;
			if (open_win == null) return;

			System.Windows.Forms.FolderBrowserDialog folder_diag = new System.Windows.Forms.FolderBrowserDialog();

			if (folder_diag.ShowDialog() != System.Windows.Forms.DialogResult.Cancel)
			{
				this.sel_files = Directory.GetFiles(folder_diag.SelectedPath, "*.jpg", SearchOption.TopDirectoryOnly);

				if (this.sel_files.Count() > 0)
				{
					this.tot_cnt = 0;
					open_win.Close();
					this.file_list.Clear();
					this.cookie_list.Clear();
					this.list_pane.InitList();
					ProgressWin progress_win = new ProgressWin(100, "Load Files");
					BackgroundWorker main_worker = null;
					int file_idx = 0;

					foreach (string file_path in this.sel_files)
					{
						string[] worker_args = { this.sel_files.Count().ToString(), file_idx.ToString(), file_path };
						main_worker = new BackgroundWorker();
						//main_worker.DoWork += (_sender, _e) => this._tLoadRun(_sender, _e, this.file_idx, file_path);
						main_worker.DoWork += new DoWorkEventHandler(this._tLoadRun);
						main_worker.ProgressChanged += new ProgressChangedEventHandler(progress_win.SetVal);
						main_worker.RunWorkerCompleted += (_sender, _e) => progress_win.CloseWin(_sender, _e, this.sel_files.Count(), "load");
						main_worker.WorkerReportsProgress = true;
						main_worker.RunWorkerAsync(worker_args);
						file_idx++;
					}
				}
			}
		}

		private void _tLoadRun(object sender, DoWorkEventArgs e)
		{
			string[] worker_args = (string[])e.Argument;
			int sel_cnt = Convert.ToInt32(worker_args[0]);
			int file_idx = Convert.ToInt32(worker_args[1]);
			string file_path = worker_args[2];

			System.Threading.Thread.Sleep(100);
			//int progress_per = (this.list_pane.file_list.Items.Count + 1) * 100 / sel_cnt;
			int progress_per = (this.file_list.Count + 1) * 100 / sel_cnt;
			//Console.WriteLine("tot cnt : " + this.sel_files.Count().ToString() + ", item cnt : " + this.list_pane.file_list.Items.Count.ToString() + ", per : " + progress_per.ToString());
			((BackgroundWorker)sender).ReportProgress(progress_per);
			this.file_list.Add(file_path);
			//this.list_pane.SetImg(file_path);
			//this.list_pane.AddFile(file_idx, file_path);
		}

		private void RunAnalyze(object sender, EventArgs e)
		{
			ProgressWin progress_win = new ProgressWin(100, "Fossil Analysis in progress");
			BackgroundWorker main_worker = null;

			foreach (ListBoxItem file_item in this.list_pane.file_list.Items)
			{
				int file_idx = (int)file_item.Tag;
				string file_path = (string)file_item.ToolTip;

				if (file_path != null)
				{
					string[] worker_args = { file_idx.ToString(), file_path };
					main_worker = new BackgroundWorker();
					//main_worker.DoWork += (_sender, _e) => this._tRunDetector(_sender, _e, file_idx, file_path);
					main_worker.DoWork += new DoWorkEventHandler(this._tRunDetector);
					main_worker.ProgressChanged += new ProgressChangedEventHandler(progress_win.SetVal);
					main_worker.RunWorkerCompleted += (_sender, _e) => progress_win.CloseWin(_sender, _e, this.list_pane.file_list.Items.Count, "an");
					main_worker.WorkerReportsProgress = true;
					main_worker.RunWorkerAsync(worker_args);
				}
			}
		}

		private void _tRunDetector(object sender, DoWorkEventArgs e)
		{
			string[] worker_args = (string[])e.Argument;
			int file_idx = Convert.ToInt32(worker_args[0]);
			string file_path = worker_args[1];
			//IntPtr fossil_detector = SetupFossilDetector(null, eDETECTOR_TYPE.LAPLACIAN_DETECTOR);
			IntPtr fossil_detector = SetupFossilDetector("test.xml");
			int max_cnt = 0;
			int detect_cnt = 0;
			OpenCvSharp.CPlusPlus.Rect cookie_rect;
			int progress_per = (this.cookie_list.Count + 1) * 100 / this.list_pane.file_list.Items.Count;

			System.Threading.Thread.Sleep(100);
			((BackgroundWorker)sender).ReportProgress(progress_per);

			Console.WriteLine("tot cnt : " + this.list_pane.file_list.Items.Count.ToString() + ", item cnt : " + this.cookie_list.Count.ToString() + ", per : " + progress_per.ToString());

			this.img_list[file_idx] = new Mat(file_path, LoadMode.Color);
			this.disp_list[file_idx] = this.img_list[file_idx].Clone();

			ProcessFossilDetector(fossil_detector, this.img_list[file_idx].Width, this.img_list[file_idx].Height,
					(int)this.img_list[file_idx].Step1(), this.img_list[file_idx].Data, ref max_cnt);

			List<FOSSILDETECTInfo> fossil_list = new List<FOSSILDETECTInfo>();
			FOSSILDETECTInfo[] fossil_data = new FOSSILDETECTInfo[max_cnt];

			GetFossilObject(fossil_detector, fossil_data, ref detect_cnt);

			for (int j = 0; j < detect_cnt; j++)
			{
				fossil_data[j].image_index = file_idx;
				fossil_data[j].class_no = -1;

				cookie_rect = CommonUtil.SubRect(fossil_data[j].rc, this.img_list[file_idx].Width, this.img_list[file_idx].Height);

				Mat img_cookie = this.img_list[file_idx].SubMat(cookie_rect);

				//this.list_pane.AddCookie(img_cookie.ToMemoryStream(), cookie_rect.X, cookie_rect.Y, cookie_rect.Width, cookie_rect.Height);

				this.disp_list[file_idx].Rectangle(cookie_rect, Scalar.Red);

				fossil_list.Add(fossil_data[j]);
			}

			this.tot_cnt += detect_cnt;
			this.cookie_list.Add(fossil_list);

			this.list_pane.ApplyFile(file_idx, file_path, this.disp_list[file_idx].ToMemoryStream());

			EndupFossilDetector(fossil_detector);
		}

		public void CookieList(int file_idx)
		{
			OpenCvSharp.CPlusPlus.Rect cookie_rect;

			this.list_pane.InitList();

			foreach (List<FOSSILDETECTInfo> sub_list in this.cookie_list)
			{
				foreach (FOSSILDETECTInfo fossil_val in sub_list)
				{
					if (fossil_val.image_index == file_idx)
					{
						cookie_rect = CommonUtil.SubRect(fossil_val.rc, this.img_list[file_idx].Width, this.img_list[file_idx].Height);
						Mat img_cookie = this.img_list[file_idx].SubMat(cookie_rect);
						this.list_pane.AddCookie(img_cookie.ToMemoryStream(), cookie_rect.X, cookie_rect.Y, cookie_rect.Width, cookie_rect.Height);
					}
				}
			}
		}

		public void RunClassifier()
		{
			// classifier
			if (this.cookie_list.Count > 0)
			{
				FOSSILDETECTInfo[] fossil_buffer = new FOSSILDETECTInfo[this.tot_cnt];

				int k = 0;
				foreach (List<FOSSILDETECTInfo> sub_list in this.cookie_list)
				{
					foreach (FOSSILDETECTInfo fossil_val in sub_list)
					{
						fossil_buffer[k++] = fossil_val;
					}
				}

				IntPtr fossil_classifier = CreateClassifier();
				ProcessClassification2(fossil_classifier, this.tot_cnt, fossil_buffer, fossil_buffer);

				//Update List 
				k = 0;
				for (int i = 0; i < this.cookie_list.Count; i++)
				{
					List<FOSSILDETECTInfo> fossil_res = this.cookie_list[i];

					for (int j = 0; j < this.cookie_list[i].Count; j++)
					{
						fossil_res[j] = fossil_buffer[k++];
					}
				}

				DestroyClassifier(fossil_classifier);
			}
		}

		private void LoadCancel(object sender, EventArgs e)
		{
			System.Windows.Window open_win =
					(System.Windows.Window)((StackPanel)((StackPanel)((System.Windows.Controls.Button)sender).Parent).Parent).Parent;
			if (open_win == null) return;
			open_win.Close();
		}

		private void CateWin(object sender, EventArgs e)
		{
			new CateWin();
		}
	}
}

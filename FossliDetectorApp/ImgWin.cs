﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.IO;


namespace FossilDetectorApp
{
	public class ImgWin : Window
	{
		Image img_src = null;

		public ImgWin(MemoryStream img_stream)
		{
			this.MinWidth = this.MaxWidth = 1000;
			this.MinHeight = this.MaxHeight = 600;
			this.WindowStyle = WindowStyle.ToolWindow;
			this.Title = "Image";

			ScrollViewer scroll_viewer = new ScrollViewer
			{
				VerticalScrollBarVisibility = ScrollBarVisibility.Auto,
				HorizontalAlignment = System.Windows.HorizontalAlignment.Left,
				VerticalAlignment = VerticalAlignment.Top,
				Width = MinWidth,
				Height = MinHeight
			};

			this.img_src = new Image
			{
				HorizontalAlignment = System.Windows.HorizontalAlignment.Left,
				VerticalAlignment = System.Windows.VerticalAlignment.Top,
				Source = CommonUtil.MemImg(img_stream)
			};
			this.img_src.Width = this.img_src.Source.Width;
			this.img_src.Height = this.img_src.Source.Height;

			scroll_viewer.Content = this.img_src;
			this.Content = scroll_viewer;
			this.ShowDialog();
		}
	}
}

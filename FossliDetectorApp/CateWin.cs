﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;


namespace FossilDetectorApp
{
	class CateWin : Window
	{
		MainWindow main_win = null;

		private GroupPane[] group_pane = new GroupPane[512]; // Group Panel
		private CookieCut[] cookie_cut = new CookieCut[8 * 1024]; // Cookie Cut

		StackPanel win_pane = null;
		Border sort_border = null;
		StackPanel new_group_pane = null;
		ScrollViewer pane_scroll = null;
		StackPanel wrap_pane = null;
		StackPanel cookie_pane = null;

		public CateWin()
		{
			this.main_win = (MainWindow)Application.Current.MainWindow;

			this.Width = this.MinWidth = 1000;
			this.Height = this.MinHeight = 600;
			this.WindowStyle = WindowStyle.ToolWindow;
			this.Background = MainWindow.bg_color;
			this.Title = "Category";

			this.win_pane = new StackPanel
			{
				Orientation = System.Windows.Controls.Orientation.Vertical,
				Width = this.MinWidth
			};

			this.sort_border = new Border
			{
				//Orientation = System.Windows.Controls.Orientation.Horizontal,
				//BorderBrush = new SolidColorBrush(Colors.DarkGray),
				BorderBrush = MainWindow.bg_color,
				CornerRadius = new CornerRadius(10),
				BorderThickness = new Thickness(5),
				Margin = new Thickness(-15, 0, 0, 0),
				Background = MainWindow.fg_color,
				Height = 40
			};
			DockPanel sort_pane = new DockPanel
			{
				//Orientation = System.Windows.Controls.Orientation.Horizontal,
				Height = 30
			};
			Label sort_by = new Label
			{
				VerticalAlignment = System.Windows.VerticalAlignment.Center,
				Content = "Sort Image by : "
			};
			RadioButton sort_shape = new RadioButton
			{
				VerticalAlignment = System.Windows.VerticalAlignment.Center,
				Content = "Shape Only",
				Tag = 0,
				IsChecked = true,
			};
			RadioButton sort_size = new RadioButton
			{
				VerticalAlignment = System.Windows.VerticalAlignment.Center,
				Margin = new Thickness(10, 0, 0, 0),
				Content = "Shape by size",
				Tag = 1
			};
			Button save_btn = new Button
			{
				HorizontalAlignment = System.Windows.HorizontalAlignment.Right,
				Margin = new Thickness(0, 0, 10, 0),
				Height = 20,
				Padding = new Thickness(10, 0, 10, 0),
				Content = "Save"
			};

			sort_shape.Click += new RoutedEventHandler(this.SortItem);
			sort_size.Click += new RoutedEventHandler(this.SortItem);
			save_btn.Click += new RoutedEventHandler(this.SaveRes);

			sort_pane.Children.Add(sort_by);
			sort_pane.Children.Add(sort_shape);
			sort_pane.Children.Add(sort_size);
			sort_pane.Children.Add(save_btn);
			this.sort_border.Child = sort_pane;
			this.win_pane.Children.Add(this.sort_border);

			StackPanel top_pane = new StackPanel
			{
				Orientation = System.Windows.Controls.Orientation.Horizontal,
				Height = 40,
				Background = Brushes.PowderBlue,
				Margin = new Thickness(0, 5, 0, 0)
			};
			this.new_group_pane = new StackPanel
			{
				Orientation = System.Windows.Controls.Orientation.Horizontal,
				VerticalAlignment = System.Windows.VerticalAlignment.Center,
			};
			Button new_group_btn = new Button
			{
				HorizontalAlignment = System.Windows.HorizontalAlignment.Right,
				Margin = new Thickness(5, 0, 0, 0),
				Height = 20,
				Padding = new Thickness(10, 0, 10, 0),
				Content = "New Label"
			};
			StackPanel undefined_group_pane = new StackPanel
			{
				HorizontalAlignment = System.Windows.HorizontalAlignment.Right,
				VerticalAlignment = System.Windows.VerticalAlignment.Center,
				Width = 200,
				Height = 40
			};
			Border undefined_group_border = new Border
			{
				BorderBrush = MainWindow.fg_color,
				BorderThickness = new Thickness(5, 0, 0, 0),
				Margin = new Thickness(5, 0, 5, 0),
				Height = 40
			};
			Label undefined_group_label = new Label
			{
				HorizontalAlignment = System.Windows.HorizontalAlignment.Center,
				VerticalAlignment = System.Windows.VerticalAlignment.Center,
				Content = "Unidentified potential fossils"
			};

			new_group_btn.Click += new RoutedEventHandler(this.NewLabel);

			this.new_group_pane.Children.Add(new_group_btn);
			undefined_group_border.Child = undefined_group_label;
			undefined_group_pane.Children.Add(undefined_group_border);
			top_pane.Children.Add(this.new_group_pane);
			top_pane.Children.Add(undefined_group_pane);
			this.win_pane.Children.Add(top_pane);

			this.pane_scroll = new ScrollViewer
			{
				HorizontalAlignment = HorizontalAlignment.Left,
				VerticalScrollBarVisibility = ScrollBarVisibility.Auto
			};
			StackPanel main_pane = new StackPanel
			{
				Orientation = System.Windows.Controls.Orientation.Horizontal
			};
			this.wrap_pane = new StackPanel
			{
				Orientation = System.Windows.Controls.Orientation.Vertical,
				Width = this.MinWidth - 200 - 30,
				VerticalAlignment = VerticalAlignment.Top,
				HorizontalAlignment = System.Windows.HorizontalAlignment.Left
			};
			Border box_border = new Border
			{
				BorderBrush = MainWindow.fg_color,
				BorderThickness = new Thickness(5, 0, 0, 0),
				Margin = new Thickness(5, 0, 5, 0)
			};
			/*StackPanel box_pane = new StackPanel
			{
				Orientation = System.Windows.Controls.Orientation.Vertical,
				Width = 200,
				VerticalAlignment = VerticalAlignment.Top,
				HorizontalAlignment = System.Windows.HorizontalAlignment.Right
			};*/
			this.cookie_pane = new StackPanel
			{
				AllowDrop = true,
				Width = 200,
				MinHeight = 300,
				VerticalAlignment = VerticalAlignment.Top,
				HorizontalAlignment = System.Windows.HorizontalAlignment.Right
			};

			this.cookie_pane.Drop += new System.Windows.DragEventHandler(DropPane);

			//box_pane.Children.Add(this.cookie_pane);
			box_border.Child = this.cookie_pane;
			main_pane.Children.Add(this.wrap_pane);
			main_pane.Children.Add(box_border);
			pane_scroll.Content = main_pane;
			this.win_pane.Children.Add(pane_scroll);

			// run classifier
			this.main_win.RunClassifier();

			// insert cookies
			this.SortItem(sort_shape, null);

			this.SizeChanged += new SizeChangedEventHandler(this.SizeWin);

			this.Content = this.win_pane;
			this.ShowDialog();
		}

		private void SizeWin(object sender, EventArgs e)
		{
			System.Windows.Window cate_win = (System.Windows.Window)sender;
			this.win_pane.Width = cate_win.ActualWidth;
			this.sort_border.Width = cate_win.ActualWidth - 15;
			this.pane_scroll.Height = cate_win.ActualHeight - 87 - 40;
			this.wrap_pane.Width = this.new_group_pane.Width = cate_win.ActualWidth - 205 - 40;

			/*foreach (object child_obj in this.wrap_pane.Children)
			{
				if (child_obj is Label)
				{
					Label child_label = 
				}
			}*/
		}

		private void DropPane(object sender, System.Windows.DragEventArgs e)
		{
			StackPanel cur_pane = (StackPanel)sender;
			if (cur_pane == null) return;

			CookieCut cur_cookie = (e.Data.GetData("cookie_cut") as CookieCut);
			if (cur_cookie == null) return;

			StackPanel item_pane = cur_cookie.GetPane();
			Border cur_border = cur_cookie.GetBorder();
			if (cur_border == null) return;
			if (cur_pane == item_pane) return;

			cur_cookie.ToPane(cur_pane);
		}

		private void SaveRes(object sender, EventArgs e)
		{
			System.Windows.Forms.SaveFileDialog save_diag = new System.Windows.Forms.SaveFileDialog();
			//save_diag.InitialDirectory = Convert.ToString(Environment.SpecialFolder.MyDocuments);
			save_diag.InitialDirectory = Convert.ToString(Environment.SpecialFolder.Desktop);
			//save_diag.Filter = "html (*.html)|*.html|xml (*.xml)|*.xml";
			save_diag.Filter = "html (*.html)|*.html";
			save_diag.FilterIndex = 1;

			if (save_diag.ShowDialog() != System.Windows.Forms.DialogResult.Cancel)
			{
				//Console.WriteLine(save_diag.FileName);
				SaveRes(save_diag.FileName);
			}
		}

		private void SaveRes(string save_name)
		{
			string output_html = "<html><style>";
			output_html += "body { background-color: #ddd; }";
			output_html += "ul li dd { margin-top: 10px; font-size: 18px; font-weight: bold; }";
			output_html += "ul li dt { margin-top: 5px; }";
			output_html += "ul li dt table thead th { background-color: #555; color: #eee; }";
			output_html += "ul li dt table tbody td.props { width: 100px; text-align: center; }";
			output_html += "</style><body><ul>";
			string save_path = System.IO.Path.GetDirectoryName(save_name);
			//Console.WriteLine(save_path);
			int group_idx = 0;

			foreach (StackPanel group_pane in this.wrap_pane.Children)
			{
				int img_idx = 0;
				string cut_path = System.IO.Path.GetFileName(save_name) + ".cuts";
				TextBox group_name = (TextBox)group_pane.Children[0];
				//Console.WriteLine((string)group_name.Text);

				Border group_border = (Border)group_pane.Children[1];
				ScrollViewer group_scroll = (ScrollViewer)group_border.Child;
				StackPanel item_pane = (StackPanel)group_scroll.Content;

				if (System.IO.Directory.Exists(save_path + "\\" + cut_path) == false)
					System.IO.Directory.CreateDirectory(save_path + "\\" + cut_path);

				if (item_pane.Children.Count > 0)
				{
					output_html += "<li><dd>" + group_name.Text.ToString() + "</dd><dt><table border='1'><thead><tr><th>Image</th><th>Width</th><th>Height</th><th>Top</th><th>Left</th></tr></thead><tbody>";

					foreach (Border item_border in item_pane.Children)
					{
						Image item_img = (Image)item_border.Child;
						string cut_name = group_idx.ToString() + "_" + img_idx.ToString() + ".jpg";
						CommonUtil.SaveImg(item_img, save_path + "\\" + cut_path + "\\" + cut_name);
						output_html += "<tr><td style='width: 300px;'><img src='" + cut_path + "/" + cut_name + "' /></td>";
						output_html += "<td class='props'>" + item_img.Width.ToString() + "</td>";
						output_html += "<td class='props'>" + item_img.Height.ToString() + "</td>";
						output_html += "<td class='props'>" + this.main_win.cookie_data[(int)item_img.Tag].rc.top.ToString() + "</td>";
						output_html += "<td class='props'>" + this.main_win.cookie_data[(int)item_img.Tag].rc.left.ToString() + "</td></tr>";
						img_idx ++;
					}

					output_html += "</tbody></table></dt></li>";
				}

				group_idx++;
			}

			output_html += "</ul></html>";

			System.IO.File.WriteAllText(save_name, output_html);

			MessageBox.Show(save_name);
		}

		private void SortItem(object sender, EventArgs e)
		{
			RadioButton cur_btn = (RadioButton)sender;
			if (cur_btn == null) return;
			int cur_sort = (int)cur_btn.Tag;

			for (int i = 0; i < this.group_pane.Count(); i++)
				this.group_pane[i] = null;

			this.main_win.cookie_data.Initialize();
			this.cookie_pane.Children.Clear();
			this.wrap_pane.Children.Clear();

			List<MainWindow.FOSSILDETECTInfo> fossil_buffer = new List<MainWindow.FOSSILDETECTInfo>();
			int cookie_idx = 0;

			foreach (List<MainWindow.FOSSILDETECTInfo> sub_list in this.main_win.cookie_list)
			{
				foreach (MainWindow.FOSSILDETECTInfo fossil_val in sub_list)
					fossil_buffer.Add(fossil_val);
			}

			fossil_buffer = fossil_buffer.OrderBy(o => o.class_no).ToList();

			if (cur_sort == 1)
			{
				//fossil_buffer = fossil_buffer.OrderBy(o => ((o.rc.right - o.rc.left) * (o.rc.bottom - o.rc.top))).ToList();

				int last_class = -100;
				List<MainWindow.FOSSILDETECTInfo> class_buffer = new List<MainWindow.FOSSILDETECTInfo>();

				foreach (MainWindow.FOSSILDETECTInfo buffer_data in fossil_buffer)
				{
					if (buffer_data.class_no != last_class)
					{
						class_buffer = class_buffer.OrderBy(o => ((o.rc.right - o.rc.left) * (o.rc.bottom - o.rc.top))).ToList();

						foreach (MainWindow.FOSSILDETECTInfo cookie_data in class_buffer)
						{
							this.AddCookie(cookie_idx, cookie_data);
							cookie_idx ++;
						}

						last_class = buffer_data.class_no;
						class_buffer.Clear();
					}

					class_buffer.Add(buffer_data);
				}
			}
			else
			{
				foreach (MainWindow.FOSSILDETECTInfo cookie_data in fossil_buffer)
				{
					this.AddCookie(cookie_idx, cookie_data);
					cookie_idx ++;
				}
			}
		}

		private void AddCookie(int cookie_idx, MainWindow.FOSSILDETECTInfo cookie_data)
		{
			this.main_win.cookie_data[cookie_idx] = cookie_data;
			//MainWindow.FOSSILDETECTInfo cookie_data = this.main_win.cookie_list[cookie_idx];

			if (cookie_data.class_no <= 0)
			{
				new CookieCut(this.main_win.img_list[cookie_data.image_index], cookie_idx, cookie_data, this.cookie_pane, this.wrap_pane, cookie_pane);
			}
			else
			{
				if (this.group_pane[cookie_data.class_no] == null)
					this.group_pane[cookie_data.class_no] = new GroupPane(this.cookie_pane, this.wrap_pane,
							"Group #" + cookie_data.class_no.ToString(), cookie_data.class_no);

				new CookieCut(this.main_win.img_list[cookie_data.image_index], cookie_idx, cookie_data, this.cookie_pane, this.wrap_pane,
						this.group_pane[cookie_data.class_no].item_pane);
			}
		}

		private void NewLabel(object sender, EventArgs e)
		{
			new GroupPane(this.cookie_pane, this.wrap_pane, "New Group");
		}
	}
}

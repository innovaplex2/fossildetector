﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;


namespace FossilDetectorApp
{
	class GroupPane
	{
		Brush gp_default_bg_color = MainWindow.fg_color; // default group panel background color
		Brush gp_enter_bg_color = Brushes.LightGray; // panel background color when mouse enter
		StackPanel box_pane = null;
		ScrollViewer item_scroll = null;
		public StackPanel item_pane = null;
		StackPanel cookie_pane = null;
		StackPanel group_pane = null;
		TextBox item_label = null;
		String pane_name = "";
		int class_no = -100;

		public GroupPane(StackPanel cookie_pane, StackPanel group_pane, String pane_name, int class_no = -100)
		{
			this.cookie_pane = cookie_pane;
			this.group_pane = group_pane;
			this.pane_name = pane_name;
			this.class_no = class_no;

			this.box_pane = new StackPanel
			{
				Orientation = Orientation.Vertical,
				Margin = new Thickness(0, 5, 0, 0),
				VerticalAlignment = VerticalAlignment.Top,
				HorizontalAlignment = HorizontalAlignment.Left
			};
			this.item_label = new TextBox
			{
				VerticalAlignment = VerticalAlignment.Top,
				HorizontalAlignment = HorizontalAlignment.Left,
				Text = this.pane_name,
				BorderThickness = new Thickness(0),
				FontFamily = new FontFamily("Courier New"),
				FontSize = 14.5,
				FontWeight = FontWeights.Bold,
				Foreground = MainWindow.fg_color,
				Background = MainWindow.pt_color
			};
			Border item_border = new Border
			{
				BorderBrush = MainWindow.bg_color,
				CornerRadius = new CornerRadius(10),
				BorderThickness = new Thickness(5),
				Background = MainWindow.fg_color
			};
			this.item_scroll = new ScrollViewer
			{
				Width = this.group_pane.Width - 24,
				Margin = new Thickness(0, 0, 0, 5),
				HorizontalScrollBarVisibility = ScrollBarVisibility.Auto,
				VerticalScrollBarVisibility = ScrollBarVisibility.Disabled
			};
			this.item_pane = new StackPanel
			{
				Orientation = Orientation.Horizontal,
				AllowDrop = true,
				MinWidth = this.item_scroll.Width,
				MinHeight = 50,
				Background = this.gp_default_bg_color,
				VerticalAlignment = VerticalAlignment.Top,
				HorizontalAlignment = HorizontalAlignment.Left,
				CanHorizontallyScroll = true
			};

			/*if (this.class_no > -1)
				this.item_pane.Name = "GROUP_PANE_" + this.class_no.ToString();*/

			this.item_pane.MouseMove += new MouseEventHandler(this.MouseMovePane);
			this.item_pane.MouseEnter += new MouseEventHandler(this.MouseEnterPane);
			this.item_pane.DragOver += new DragEventHandler(this.DragOverPane);
			this.item_pane.DragEnter += new DragEventHandler(this.DragEnterPane);
			this.item_pane.DragLeave += new DragEventHandler(this.DragLeavePane);
			this.item_pane.Drop += new DragEventHandler(this.DropPane);

			this.item_scroll.Content = this.item_pane;
			item_border.Child = this.item_scroll;

			this.box_pane.Children.Add(this.item_label);
			this.box_pane.Children.Add(item_border);
			this.group_pane.Children.Add(this.box_pane);

			//this.cookie_pane.Height = ((this.group_pane.Parent as StackPanel).Parent as ScrollViewer).ViewportHeight - 30;

			//Console.WriteLine("height : " + ((this.group_pane.Parent as StackPanel).Parent as ScrollViewer).Height.ToString());

			/*this.cookie_pane.Height = this.group_pane.Height;

			Console.WriteLine("cookie_pane height : " + this.cookie_pane.Height.ToString());
			Console.WriteLine("group_pane height : " + this.group_pane.Height.ToString());
			Console.WriteLine("group_pane actualheight : " + this.group_pane.Parent.ViewportHeight.ToString());*/
		}

		private void MouseMovePane(object sender, MouseEventArgs e)
		{
			//StackPanel cur_pane = (StackPanel)sender;
			//cur_pane = (StackPanel)sender;

			//if (cur_pane == null) return;
			//cur_img.Margin = new Thickness(30);

			//Console.WriteLine("x: " + e.GetPosition(cur_pane).X + ", y : " + e.GetPosition(cur_pane).Y);
		}

		private void MouseEnterPane(object sender, MouseEventArgs e)
		{
			//Console.WriteLine("mouse enter");
			//cur_pane = (StackPanel)sender;
		}

		private void DragOverPane(object sender, DragEventArgs e)
		{
			//Console.WriteLine("drag over");
			StackPanel cur_pane = (StackPanel)sender;
			if (cur_pane == null) return;
			cur_pane.Background = this.gp_enter_bg_color;
		}

		private void DragEnterPane(object sender, DragEventArgs e)
		{
			//Console.WriteLine("drag enter");
		}

		private void DragLeavePane(object sender, DragEventArgs e)
		{
			//Console.WriteLine("drag leave");
			Console.WriteLine((e.Data.GetType()));
			StackPanel cur_pane = (StackPanel)sender;
			if (cur_pane == null) return;
			cur_pane.Background = this.gp_default_bg_color;
		}

		private void DropPane(object sender, DragEventArgs e)
		{
			StackPanel cur_pane = (StackPanel)sender;
			if (cur_pane == null) return;

			//Console.WriteLine("Drop");
			CookieCut cur_cookie = (e.Data.GetData("cookie_cut") as CookieCut);
			if (cur_cookie == null) return;

			StackPanel item_pane = cur_cookie.GetPane();
			Border cur_border = cur_cookie.GetBorder();
			if (cur_border == null) return;
			if (cur_pane == item_pane) return;

			cur_cookie.ToPane(cur_pane);
			cur_pane.Background = this.gp_default_bg_color;
		}
	}
}

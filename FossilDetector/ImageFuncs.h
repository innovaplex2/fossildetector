#pragma once

int custom_laplacian_filter(unsigned char *src, unsigned char *dst_limit, unsigned char *dst, int width, int height, int patch);
int custom_valcano_filter16(unsigned char *src, unsigned char *dst, int width, int height, int patch);
int custom_valcano_filter32(unsigned char *src, unsigned char *dst, int width, int height, int patch);
void downsize_maxium_nn(unsigned char *pSrc, unsigned char *pDst, int width, int height, int src_pitch, int dst_pitch, 
											float x_factor, float y_factor, int *dst_w, int *dst_h );
#include "FossilDetectorIPP.h"
#include "IPPFunc.h"
#include "ImageFuncs.h"
#include "opencv/highgui.h"
#include <fstream>

#include "../json/include/json.h"
#pragma comment(lib, "../json/" JSON_LIB_PATH)


using namespace std;
using namespace cv;

//IPP link static library
#ifdef _WIN64 
#pragma comment(lib, "ippcoreem64tl.lib")
#pragma comment(lib, "ippimergedem64t.lib")
#pragma comment(lib, "ippiemergedem64t.lib")
#pragma comment(lib, "ippcvmergedem64t.lib")
#pragma comment(lib, "ippcvemergedem64t.lib")
#pragma comment(lib, "ippsmergedem64t.lib")
#pragma comment(lib, "ippsemergedem64t.lib")
#pragma comment(lib, "ippccmergedem64t.lib")
#pragma comment(lib, "ippccemergedem64t.lib")
#else
#pragma comment(lib, "ippcorel.lib")
#pragma comment(lib, "ippimerged.lib")
#pragma comment(lib, "ippiemerged.lib")
#pragma comment(lib, "ippcvmerged.lib")
#pragma comment(lib, "ippcvemerged.lib")
#pragma comment(lib, "ippsmerged.lib")
#pragma comment(lib, "ippsemerged.lib")
#pragma comment(lib, "ippccmerged.lib")
#pragma comment(lib, "ippccemerged.lib")
#endif


#ifdef _DEBUG
#pragma comment(lib, "opencv_core2410d.lib")
#pragma comment(lib, "opencv_highgui2410d.lib")
#pragma comment(lib, "opencv_imgproc2410d.lib")
#else	
#pragma comment(lib, "opencv_core2410.lib")
#pragma comment(lib, "opencv_highgui2410.lib")
#pragma comment(lib, "opencv_imgproc2410.lib")
#endif

static int		g_color_level[OBJECT_SIZE_LEVEL_CNT] = { 255, 180, 150, 120, 100 };
	
//#define DEBUG_SHOW
//#define DEBUG_SHOW_FOSSIL

#define ABS(x) (((x)<0)?(0-(x)):(x))
int inline inCircle( int x, int y, int xo, int yo, int R ){  
  int dx = ABS(x-xo);
  if (    dx >  R ) return FALSE;
  int dy = ABS(y-yo);
  if (    dy >  R ) return FALSE;
  if ( dx+dy <= R ) return TRUE;
  return ( dx*dx + dy*dy <= R*R );
}


const float LOW_CONTRAST_LEVEL = 8;
const float MIDDLE_CONTRAST_LEVEL = 12;


// 
//Look up testbale
static FOSSILEDetectorParam g_default_param[3] ={
	{
		{68, 50, 38, 28, 22},		//object size
		{70, 60, 60, 50, 40},	//theshold_level
		{1.0,1.0,1.0,1.0,1.2},	//level_weight
		1.2f,	//min_distance
		10,		//foreground_thread_low
		8,		//foreground_thread_high
		76,		//max_width
		8,		//threadhold_fossil_occapy_ratio
		4,		//threadhold_fossil_distance_ratio
		23		
	},
	{
		{68, 50, 38, 28, 22},		//object size
		{140, 140, 120, 68, 52 },	//theshold_level
		{1.0,1.0,1.0,1.2,1.2},	//level_weight
		1.2,	//min_distance
		14,		//foreground_thread_low
		12,		//foreground_thread_high
		76,		//max_width
		6,		//threadhold_fossil_occapy_ratio
		4,		//threadhold_fossil_distance_ratio
		23		
	},
	{
		{68, 50, 38, 28, 22},		//object size
		{200, 135, 101, 76, 57},	//theshold_level
		{0.6,0.72,0.86,1.0,1.2},	//level_weight
		1.2f,	//min_distance
		18,		//foreground_thread_low
		16,		//foreground_thread_high
		76,		//max_width
		8,		//threadhold_fossil_occapy_ratio
		4,		//threadhold_fossil_distance_ratio
		23		
	}
};



CFossilDetectorIPP::CFossilDetectorIPP()
{
	//set default parameters
	m_DetectionParam		= g_default_param[1];
	m_bAutoAjudstMenetParam	= true;
}


CFossilDetectorIPP::~CFossilDetectorIPP()
{
}

/*
config sample
char* g_szConf= (	\"object_size_level\" : [68, 50, 38, 28, 22]\
					\"theshold_level\" : [200, 135, 101, 76, 57]\
					\"level_weight\" : [0.6,0.72,0.86,1.0,1.2]\
					\"min_distance\" : 1.2\
					\"foreground_thread_low\" : 14\
					\"foreground_thread_high\" : 12\
					\"max_width\" : 76\
					\"threadhold_fossil_occapy_ratio\" : 8\
					\"threadhold_fossil_distance_ratio\" : 4\
					);
*/

BOOL	CFossilDetectorIPP::Open(const char *szConfig)
{
	//Set configuration
	m_bAutoAjudstMenetParam = true;
	if(szConfig == NULL)return TRUE;
	
	std::ifstream inFile(szConfig);
	if (!inFile.is_open()){
		return TRUE;
	}
	
	Json::Value DetectorConfig; 
	Json::Reader reader; 
	bool parsedSuccess = reader.parse(inFile, DetectorConfig,  false);		
	if(parsedSuccess == false) {
		return TRUE;
	}
	
	int ArraySize = DetectorConfig["object_size_level"].size();
	if(OBJECT_SIZE_LEVEL_CNT != ArraySize) return FALSE;

	for(int i = 0 ; i < OBJECT_SIZE_LEVEL_CNT ; i ++){
		m_DetectionParam.object_size_level[i]	= DetectorConfig["object_size_level"][i].asInt();
		m_DetectionParam.theshold_level[i]		= DetectorConfig["theshold_level"][i].asInt();
		m_DetectionParam.level_weight[i]		= DetectorConfig["level_weight"][i].asFloat();
	}
		
	m_DetectionParam.foreground_thread_low		= DetectorConfig["foreground_thread_low"].asFloat();
	m_DetectionParam.foreground_thread_high		= DetectorConfig["foreground_thread_high"].asFloat();
	m_DetectionParam.max_width					= DetectorConfig["max_width"].asInt();
	m_DetectionParam.min_distance				= DetectorConfig["min_distance"].asFloat();
	m_DetectionParam.threadhold_fossil_occapy_ratio		= DetectorConfig["threadhold_fossil_occapy_ratio"].asInt();
	m_DetectionParam.threadhold_fossil_distance_ratio	= DetectorConfig["threadhold_fossil_distance_ratio"].asInt();
	m_DetectionParam.increase_size_factor		= DetectorConfig["increase_size_factor"].asFloat();

	m_bAutoAjudstMenetParam = false;

	return TRUE;
}


BOOL	CFossilDetectorIPP::ProcessFossilDetector( int width, int height, int step, BYTE *pData, int* pMaxDetectCount )
{
#ifdef DEBUG_SHOW		
	cv::Mat	input(height, width, CV_8UC3, pData, step);
#endif 
	cv::Mat	temp, edge, source;
	*pMaxDetectCount = 0; 
	
	cv::Size size(height, width);

	m_input = cv::Mat(height, width, CV_8UC3, pData);
	
	m_gray.create( height, width, CV_8UC1 );
	m_gray_bin.create( height, width, CV_8UC1 );

	m_edge.create( (height-2)/2 , (width-2 )/2 , CV_8UC1);	//la
///	temp.create( (height-2)/2, (width-2)/2, CV_8UC1 );
//	temp2.create( (height-2)/4, (width-2)/4, CV_8UC1 );
//	edge.create( (height-2)/2 , (width-2 )/2 , CV_8UC1 );
	
	m_cFossilList.clear();
	m_cWeightList.clear();
	
	//1. color conversion
	IppiSize srcSize = {width , height };
	ippiRGBToGray_8u_C3C1R(pData, step, m_gray.data, width, srcSize);
	m_gray.copyTo(m_gray_bin);

	//get mean value to seperate background and foreground
	//ippiMean_8u_C1R(m_gray.data, m_gray.step.buf[0], srcSize, &m_mean);
	ippiMean_8u_C3R(m_input.data, m_input.step.buf[0], srcSize, m_RGBMean);

	cv::Scalar avr, var;
	cv::meanStdDev(m_gray, avr, var);

	m_mean	= (float)avr[0];
	m_stddev= (float)var[0];
	
	//Get Varation for adjust threshold
	if(m_bAutoAjudstMenetParam){
		if(m_stddev < LOW_CONTRAST_LEVEL){
			m_DetectionParam = g_default_param[0];
		}else if(m_stddev < MIDDLE_CONTRAST_LEVEL){
			m_DetectionParam = g_default_param[1];
		}else{	//HIGH Constrast
			m_DetectionParam = g_default_param[2];
		}
	}
		
	//2. laplacian filer & resize
	//custom_laplacian_filter(m_gray.data, edge.data, m_edge.data, width , height,  m_gray.step.buf[0]);
			
	//3. apply box filter to detect high entropy.
	//srcSize.width = (width-2)/2;
	//srcSize.height= (height-2)/2;
		
	std::vector<cv::Rect> cCandiate[OBJECT_SIZE_LEVEL_CNT];
	std::vector<float> cWeight[OBJECT_SIZE_LEVEL_CNT];	
	//std::vector<cv::Rect> cCandiate;
		
	IppiSize maskSize, markSize;

	maskSize.width = 16;
	maskSize.height= 16;
	markSize.width = int(maskSize.width * m_DetectionParam.min_distance);
	markSize.height= int(maskSize.height * m_DetectionParam.min_distance); 

	float resize_factor = 3.0/4.0;
	float resize_ratio = 1;

	  
	cv::Rect rc;
	IppiSize inSize, outSize;
	
	source = m_gray.clone();
	//equalizeHist(m_gray, source);
	for( int level = OBJECT_SIZE_LEVEL_CNT-1 ; level >= 0 ; level--){

		//1. laplacian result generate half of image.
		inSize.width = (source.cols-1)/2;
		inSize.height= (source.rows-1)/2;
		
		edge.create( inSize.height, inSize.width , CV_8UC1 );
		m_edge.create( inSize.height, inSize.width, CV_8UC1 );
		
		custom_laplacian_filter(source.data, edge.data, m_edge.data, source.cols, source.rows, source.step.buf[0]);

		//2 apply valcano filter
		outSize.width	= (inSize.width-16)/2 ;
		outSize.width	+= (inSize.width)%2 ; 
		outSize.height	= (inSize.height-16)/2 +2;
		
		temp.create( outSize.height, outSize.width, CV_8UC1 );
		ippiSet_8u_C1R(0, temp.data, temp.step.buf[0], outSize);
		
		custom_valcano_filter16(edge.data, temp.data, inSize.width, inSize.height, edge.step.buf[0]);

		while(1){
			int x,y;
			Ipp8u nMax;
			//float fWeight;
			ippiMaxIndx_8u_C1R(temp.data, temp.step.buf[0], outSize, &nMax, &x, &y);
			
			if(nMax < m_DetectionParam.theshold_level[level]) break;
		
			rc.x		= int(x/resize_ratio * 4);
			rc.y		= int(y/resize_ratio * 4);
			rc.width	= 2*m_DetectionParam.object_size_level[level]; 
			rc.height	= 2*m_DetectionParam.object_size_level[level]; 

#ifdef DEBUG_SHOW		
			cv::Scalar color(0, 255, 0);
			color[1] = g_color_level[level];
			rectangle(input, rc, color, 2); 
#endif 
			cCandiate[level].push_back(rc);
			cWeight[level].push_back(nMax);
			//cCandiate.push_back(rc);
						
			x = (x > (markSize.width>>1))?(x - (markSize.width>>1)):0;
			y = (y > (markSize.height>>1))?(y - (markSize.height>>1)):0;
			
			if( (markSize.width + x) > temp.cols){
				x = temp.cols - markSize.width;
			}
			if( (markSize.height + y) > temp.rows){
				y = temp.rows - markSize.height;		
			}

			//need to modify circluar set not rectangle.
			Ipp8u* pDst = temp.data + x + temp.step.buf[0] * y;						
			ippiSet_8u_C1R(0, pDst, temp.step.buf[0], markSize);
		}

		temp.copyTo(edge);
		srcSize.width = inSize.width;
		srcSize.height= inSize.height;

/*
		imshow("Source", source);	
		imshow("Edge", edge);	
		imshow("Edge2", m_edge);	
		imshow("OutputFilter", temp);	
		waitKey(0);
*/		

		//resize 
		resize_ratio *= resize_factor;
		IppiSize	srcSize	= {width,height};
		IppiRect	srcRoi	= {0,0,width,height};
		IppiSize	dstSize = {int(width*resize_ratio), int(height*resize_ratio)};

		source.create( dstSize.height, dstSize.width, CV_8UC1 );
		ippiResize_8u_C1R(m_gray.data, srcSize, m_gray.step.buf[0], srcRoi, source.data, source.step.buf[0], dstSize, resize_ratio, resize_ratio, IPPI_INTER_CUBIC);
	}
	
	//backward merge 
	BOOL merge;
	int center1_x, center1_y;
	int center2_x, center2_y;
	float distance, offset;

	for( int level = OBJECT_SIZE_LEVEL_CNT-1 ; level >= 0 ; level--){
		
		for(unsigned int i = 0 ; i < cCandiate[level].size() ; i++){
			merge = FALSE; 

			center1_x	= cCandiate[level][i].x + cCandiate[level][i].width/2;
			center1_y	= cCandiate[level][i].y + cCandiate[level][i].height/2;
			//width와 height가 같으로 offset은 불필요한 작업임 바로 구하면 됨.
			//offset = sqrt(float((cCandiate[level][i].width*cCandiate[level][i].width) /4+ (cCandiate[level][i].height*cCandiate[level][i].height)/4));
			offset		= float(cCandiate[level][i].width/2 * 0.8);
				
			//find current to merged fossil
			for (unsigned int j = 0; j < m_cFossilList.size(); j++){
				center2_x	= m_cFossilList[j].x + m_cFossilList[j].width/2;
				center2_y	= m_cFossilList[j].y + m_cFossilList[j].height/2;

				distance = sqrt ( float ((center1_x - center2_x) * (center1_x - center2_x) + (center1_y - center2_y)*(center1_y - center2_y)));
				//if( offset > distance && distance < (m_cFossilList[j].width/2)){
				if( offset > distance){
					merge = TRUE; 
					//float weight = cWeight[level][i]*(g_level_weight + 1.0/(level+g_level_weight_offset));
					float weight = cWeight[level][i]*m_DetectionParam.level_weight[level];
					if(weight > m_cWeightList[j] ){
						m_cFossilList[j] = cCandiate[level][i];	
						m_cWeightList[j] = weight;
					}
					break;
				}
			}

			//not merged rect then add 
			if( FALSE == merge){
				m_cFossilList.push_back(cCandiate[level][i]);
				m_cWeightList.push_back(cWeight[level][i]);
			}
		}
	}
		
	//Remove embeded fossil object then merge.
#if 1
	for(unsigned int i = 0 ; i < m_cFossilList.size() ; i++){
		center1_x	= m_cFossilList[i].x + m_cFossilList[i].width/2;
		center1_y	= m_cFossilList[i].y + m_cFossilList[i].height/2;
		offset		= m_cFossilList[i].width/1.4f;//sqrt(float(m_cFossilList[i].width*m_cFossilList[i].width/4 + m_cFossilList[i].height*m_cFossilList[i].height/4));
		for (unsigned int j = 1; j < m_cFossilList.size(); j++){
			if(i == j) continue;
			center2_x	= m_cFossilList[j].x + m_cFossilList[j].width/2;
			center2_y	= m_cFossilList[j].y + m_cFossilList[j].height/2;

			distance = sqrt ( float ((center1_x - center2_x) * (center1_x - center2_x) + (center1_y - center2_y)*(center1_y - center2_y)));
			if( offset > distance ){
				m_cFossilList[j].x = m_cFossilList[j].y =  m_cFossilList[j].width = m_cFossilList[j].height = 0;
			}
		}
	}
#endif
	

#ifdef DEBUG_SHOW		
	cv::Scalar color(255, 0, 0);
	for( int i = 0 ; i < m_cFossilList.size() ; i++)
	{
		rectangle(input, m_cFossilList[i], color, 2); 
	}
	
	imshow("EDGE", edge);	
	imshow("OUTPUT", input);	
	imshow("Laplace Edeg", m_edge);	
	//imshow("Temp2", temp2);		
#endif
	
	
	for (unsigned int i = 0; i < m_cFossilList.size(); i++){
		if(m_cFossilList[i].width == 0 || m_cFossilList[i].height == 0 ) continue;
		(*pMaxDetectCount)++;
	}

	return TRUE;
}


BOOL	CFossilDetectorIPP::GetFossilObject( FOSSILDETECTInfo *pOjbectInfo, int* pDetectCount)
{
	unsigned int i, k;
	
	//Find best fossil position using moment.
	FOSSILDETECTInfo info;
	cv::Mat fossil_gray_mask;
	int increase_x, increase_y;

	*pDetectCount = 0;
	for( i = 0, k = 0 ; i < m_cFossilList.size() ; i++){
		if(m_cFossilList[i].width == 0 || m_cFossilList[i].height == 0 ) continue;
		
		//check boundary
		increase_x = int(m_cFossilList[i].width*m_DetectionParam.increase_size_factor)/100;
		increase_y = int(m_cFossilList[i].height*m_DetectionParam.increase_size_factor)/100;
		
		m_cFossilList[i].x	= (m_cFossilList[i].x > increase_x)?(m_cFossilList[i].x - increase_x):0;
		m_cFossilList[i].y	= (m_cFossilList[i].y > increase_y)?(m_cFossilList[i].y - increase_y):0;

		m_cFossilList[i].width	= m_cFossilList[i].width + increase_x;
		m_cFossilList[i].height	= m_cFossilList[i].height+ increase_y;
		
		m_cFossilList[i].width	= ((m_cFossilList[i].width + m_cFossilList[i].x) > m_gray.cols)? (m_gray.cols - m_cFossilList[i].x ) : m_cFossilList[i].width;
		m_cFossilList[i].height = ((m_cFossilList[i].height + m_cFossilList[i].y) > m_gray.rows)? (m_gray.rows - m_cFossilList[i].y ) : m_cFossilList[i].height;

		memset(&info, 0, sizeof(info));
		fossil_gray_mask = m_gray_bin(m_cFossilList[i]);

#ifdef DEBUG_SHOW_FOSSIL		
		cv::Mat fossil		= m_input(m_cFossilList[i]);
		//cv::Mat fossil_gray = m_gray(m_cFossilList[i]);

		cv::imshow("FOSSILE", fossil);	
		cv::imshow("FOSSILE_GRAY", fossil_gray_mask);	
#endif
		//cv::threshold(fossil_gray, temp, m_mean + g_foreground_thread_high, 255, cv::THRESH_BINARY);
		IppiSize SrcRoiSize = {fossil_gray_mask.cols, fossil_gray_mask.rows};
		ippiThreshold_LTValGTVal_8u_C1R(fossil_gray_mask.data, fossil_gray_mask.step.buf[0], 
			fossil_gray_mask.data, fossil_gray_mask.step.buf[0], SrcRoiSize, (Ipp8u)(m_mean - m_DetectionParam.foreground_thread_low), 255, (Ipp8u)(m_mean + m_DetectionParam.foreground_thread_high), 255);
		//ippiThreshold_LTVal_8u_C1R(temp.data, temp.step.buf[0], temp.data, temp.step.buf[0], SrcRoiSize, 255, 0);
		//cv::Mat element(3, 3, CV_8U, cv::Scalar(1));
		//morphologyEx(temp, temp, CV_MOP_CLOSE, element);
		
#ifdef DEBUG_SHOW_FOSSIL		
		cv::imshow("FOSSILE_BINARY", fossil_gray_mask);	
#endif

		int center_x, center_y;
		int move_x, move_y;
		int weight_w, weight_h, weight;

		if (AdjustFossilPostion(fossil_gray_mask.data, fossil_gray_mask.cols, fossil_gray_mask.rows, fossil_gray_mask.step.buf[0],
					m_DetectionParam.max_width, &center_x, &center_y, &weight_w, &weight_h) < 0){
#ifdef DEBUG_SHOW_FOSSIL	
			cv::waitKey(0);
#endif
			continue;
		}

		move_x = (m_cFossilList[i].width)/2 - center_x;
		move_y = (m_cFossilList[i].height)/2 - center_y;

		//move position by moment.
		m_cFossilList[i].x -= move_x;
		m_cFossilList[i].y -= move_y;

		weight = (weight_w> weight_h)?weight_w : weight_h;
		increase_x = (m_cFossilList[i].width*weight)/100;
		increase_y = (m_cFossilList[i].height*weight)/100;
		
		//Check boundary and make retangle to squere 
		pOjbectInfo[k].rc.left	= (m_cFossilList[i].x > increase_x)?(m_cFossilList[i].x - increase_x):0;
		pOjbectInfo[k].rc.top	= (m_cFossilList[i].y > increase_y)?(m_cFossilList[i].y - increase_y):0;

		pOjbectInfo[k].rc.right	= m_cFossilList[i].x + m_cFossilList[i].width + increase_x;
		pOjbectInfo[k].rc.bottom= m_cFossilList[i].y + m_cFossilList[i].height+ increase_y;
		
		pOjbectInfo[k].rc.right	= (pOjbectInfo[k].rc.right > m_gray.cols)? m_gray.cols : pOjbectInfo[k].rc.right;
		pOjbectInfo[k].rc.bottom= (pOjbectInfo[k].rc.bottom > m_gray.rows)? m_gray.rows : pOjbectInfo[k].rc.bottom;


				
#ifdef DEBUG_SHOW_FOSSIL	
		cv::Rect region(pOjbectInfo[k].rc.left, pOjbectInfo[k].rc.top, pOjbectInfo[k].rc.right - pOjbectInfo[k].rc.left, pOjbectInfo[k].rc.bottom - pOjbectInfo[k].rc.top);
		fossil		= m_input(region);
		cv::imshow("FOSSILE_CENTER", fossil);	

		cv::waitKey(0);
#endif

/*
		cv::Rect region(pOjbectInfo[k].rc.left, pOjbectInfo[k].rc.top, pOjbectInfo[k].rc.right - pOjbectInfo[k].rc.left, pOjbectInfo[k].rc.bottom - pOjbectInfo[k].rc.top);
		cv::Scalar avr, var;
		cv::meanStdDev(m_gray(region), avr, var);

		pOjbectInfo[k].mean		= (float)avr[0];
		pOjbectInfo[k].stddev	= (float)var[0];
	
		int theshold = STDDEV_THRESHOLD *(float(g_object_size_level[0]*g_object_size_level[0])/ float(region.width * region.height)  );
		if(pOjbectInfo[k].stddev < theshold ){
   			continue;
		}
*/
		//2. sort fossil type
		if(pOjbectInfo[k].stddev < STDDEV_THRESHOLD){
 			pOjbectInfo[k].type = FOSSIL_TYPE_LINE;
		}else{
			pOjbectInfo[k].type = FOSSIL_TYPE_BLOB;
		}
				
		//Get Fossil Info 
		GetFossilInfo(pOjbectInfo[k].rc, &info);
		pOjbectInfo[k] = info;
		pOjbectInfo[k].flag = FOSSIL_UPDATE_PROC;
		pOjbectInfo[k].fossil_id = k;
		
		k++;
	}

	*pDetectCount = k;
	return TRUE;
}


int	CFossilDetectorIPP::AdjustFossilPostion(BYTE* pSrc, int width, int height, int pitch, int limit_max_width, int* x, int *y, int *weight_w, int *weight_h)
{
	int count, sum_x, sum_y, center_x, center_y;
	count = sum_x = sum_y = 0;
	int max_width = (width > limit_max_width) ? limit_max_width : width;
	
	for(int i = 0 ; i < height ; i++){
		for(int j = 0 ; j < width ; j++){
			if(pSrc[j + i*pitch] == 255){
				count++;
				sum_x += j;
				sum_y += i;
			}
		}
	}
	center_x	= sum_x/count;
	center_y	= sum_y/count;

	sum_x = sum_y = 0;
	float distance= 0;
	int diffx, diify;
	for(int i = 0 ; i < height ; i++){
		for(int j = 0 ; j < width ; j++){
			if(pSrc[j + i*pitch] == 255){
				diffx = ABS(j-center_x);
				diify = ABS(i-center_y);
				sum_x += diffx;
				sum_y += diify;
				distance += (float)sqrt(diffx*diffx + diify*diify);
			}
		}
	}
	
	distance = distance/count;
	//if( (count < (height*width)/4) && (varinace > sqrt(width*height)/2) ) return -1;
	//반지름의 절반일 경우 (width/sin(45))/2 
	if( (count < (max_width*max_width)/m_DetectionParam.threadhold_fossil_occapy_ratio) && (distance > max_width/m_DetectionParam.threadhold_fossil_distance_ratio) ) return -1;

	*x	= center_x;
	*y	= center_y ;	//weight to height.... 
	*weight_w	= ((sum_x/count) * 90)/width ;
	*weight_h	= ((sum_y/count) * 90)/height;

	*weight_w	*= max_width/width;
	*weight_h	*= max_width/width;
	
	return 0;
}



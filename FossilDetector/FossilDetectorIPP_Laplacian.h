#pragma once

#include <Windows.h>
#include "../include/FossilDetector.h"
#include "opencv/cv.h"


#define	FILTER_PYRAMID_CNT	6

class CFossilDetectorIPP_Laplacian : public IFossilDetector
{
public:
	CFossilDetectorIPP_Laplacian();
	~CFossilDetectorIPP_Laplacian();

	BOOL	Open(const char *szConfigXML);
	BOOL	ProcessFossilDetector( int width, int height, int step, BYTE *pData, int* pMaxDetectCount );
	BOOL	GetFossilObject( FOSSILDETECTInfo *pOjbectInfo, int* pDetectCount );

	void	DestorySelf(){delete this;}

private:
	double				m_mean;
	double				m_RGBMean[3];


	cv::Mat				m_input;
	cv::Mat				m_gray;
	cv::Mat				m_edge;
	

	std::vector<cv::Rect>	m_cFossilList;
	std::vector<int>		m_cWeightList;
		
	BOOL	GetFossilInfo(RECT& rc, FOSSILDETECTInfo *Info);
};



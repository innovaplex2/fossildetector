#include "FossilDetectorIPP_Laplacian.h"
#include "IPPFunc.h"
#include "highgui.h"

using namespace std;
using namespace cv;

//IPP link static library
#pragma comment(lib, "ippcorel.lib")
#pragma comment(lib, "ippimerged.lib")
#pragma comment(lib, "ippiemerged.lib")
#pragma comment(lib, "ippcvmerged.lib")
#pragma comment(lib, "ippcvemerged.lib")
#pragma comment(lib, "ippsmerged.lib")
#pragma comment(lib, "ippsemerged.lib")
#pragma comment(lib, "ippccmerged.lib")
#pragma comment(lib, "ippccemerged.lib")

#ifdef _DEBUG
#pragma comment(lib, "opencv_core2410d.lib")
#pragma comment(lib, "opencv_highgui2410d.lib")
#pragma comment(lib, "opencv_imgproc2410d.lib")
#else
#pragma comment(lib, "opencv_core2410.lib")
#pragma comment(lib, "opencv_highgui2410.lib")
#pragma comment(lib, "opencv_imgproc2410.lib")
#endif

#if 0
#define	OBJECT_SIZE_LEVEL_CNT	5
static int	object_size_level[OBJECT_SIZE_LEVEL_CNT]= {60, 50, 40, 30, 25};
static int color_level[OBJECT_SIZE_LEVEL_CNT]		= {255,210,180,150,120};
//static int theshold_level[OBJECT_SIZE_LEVEL_CNT]	= {50 ,52, 52, 54, 60};
static int theshold_level[OBJECT_SIZE_LEVEL_CNT]	= {30 ,25, 25, 20, 20};
#else

#define	OBJECT_SIZE_LEVEL_CNT	6
static int	object_size_level[OBJECT_SIZE_LEVEL_CNT]= {60, 48, 38, 30, 24, 20};
static int color_level[OBJECT_SIZE_LEVEL_CNT]		= {255,210,180,150,120,100};
//static int theshold_level[OBJECT_SIZE_LEVEL_CNT]	= {50 ,52, 52, 54, 60};
static int theshold_level[OBJECT_SIZE_LEVEL_CNT]	= {28, 30, 34, 38, 42, 46};


//추후 파라미터화 시킴
#define	STDDEV_THRESHOLD	20


#endif
//#define DEBUG_SHOW
//#define DEBUG_SHOW_FOSSIL
//#define DEBUG_SHOW_FOSSIL2
//#define DEBUG_SHOW_FOSSIL3

#define ABS(x) (((x)<0)?(0-(x)):(x))
int inline inCircle( int x, int y, int xo, int yo, int R ){  
  int dx = ABS(x-xo);
  if (    dx >  R ) return FALSE;
  int dy = ABS(y-yo);
  if (    dy >  R ) return FALSE;
  if ( dx+dy <= R ) return TRUE;
  return ( dx*dx + dy*dy <= R*R );
}

void	FindCenter(BYTE* pSrc, int width, int height, int pitch, int* x, int *y, int *weight_w, int *weight_h) 
{
	int count, sum_x, sum_y, center_x, center_y;
	count = sum_x = sum_y = 0;

	for(int i = 0 ; i < height ; i++){
		for(int j = 0 ; j < width ; j++){
			if(pSrc[j + i*pitch]){
				count++;
				sum_x += j;
				sum_y += i;
			}
		}
	}

	center_x	= sum_x/count;
	center_y	= sum_y/count;

	sum_x = sum_y = 0;
	for(int i = 0 ; i < height ; i++){
		for(int j = 0 ; j < width ; j++){
			if(pSrc[j + i*pitch]){
				sum_x += ABS(j-center_x);
				sum_y += ABS(i-center_y);
			}
		}
	}
	
	*x	= center_x;
	*y	= center_y + 2;	//weight to height.... 
	*weight_w	= ((sum_x/count) * 100)/width + (ABS(center_x - width/2)*20)/width;
	*weight_h	= ((sum_y/count) * 100)/height+ (ABS(center_y - height/2)*20)/height;
}

CFossilDetectorIPP_Laplacian::CFossilDetectorIPP_Laplacian()
{
	
}


CFossilDetectorIPP_Laplacian::~CFossilDetectorIPP_Laplacian()
{
}


BOOL	CFossilDetectorIPP_Laplacian::Open(const char *szConfigXML)
{
	return TRUE;
}


BOOL	CFossilDetectorIPP_Laplacian::ProcessFossilDetector( int width, int height, int step, BYTE *pData, int* pMaxDetectCount )
{
#ifdef DEBUG_SHOW		
	cv::Mat	input(height, width, CV_8UC3, pData, step);
#endif 
	cv::Mat	temp, edge, temp2;
	*pMaxDetectCount = 0; 
	
	cv::Size size(height, width);

	m_input = cv::Mat(height, width, CV_8UC3, pData);
	
	m_gray.create( height, width, CV_8UC1 );
	m_edge.create( height/2, width/2, CV_8UC1);	//la
	temp.create( height/2, width/2, CV_8UC1 );
	edge.create( height/2, width/2, CV_8UC1 );
	
	m_cFossilList.clear();
	m_cWeightList.clear();
	
	
	//1. color conversion
	IppiSize roiSize = {width , height };
	ippiRGBToGray_8u_C3C1R(pData, step, m_gray.data, width, roiSize);

	//2. resize 0.5
	IppiRect srcRoi={0,0,width,height};
	IppiSize dstSize = {width/2,height/2};
	ippiResize_8u_C1R(m_gray.data, roiSize, m_gray.step.buf[0], srcRoi, temp.data, temp.step.buf[0], dstSize, 0.5, 0.5, IPPI_INTER_CUBIC);
	
	//get mean value to seperate background and foreground
	ippiMean_8u_C1R(m_gray.data, m_gray.step.buf[0], dstSize, &m_mean);
	ippiMean_8u_C3R(m_input.data, m_input.step.buf[0], dstSize, m_RGBMean);

	
	//3. edge 
	temp2.create( height/2 + object_size_level[0], width/2 + object_size_level[0], CV_8UC1 );

	IppiSize maskSize, markSize;

	temp2.create( height/2 + 3, width/2 + 3, CV_8UC1 );
	laplacian_filter(temp.data, m_edge.data, edge.data, temp.cols, temp.rows, temp2.data, ippMskSize3x3);

	//4. apply box filter to detect high entropy.
	roiSize.width = width/2;
	roiSize.height= height/2;
		
	cv::Rect rc;
	std::vector<cv::Rect> cCandiate[OBJECT_SIZE_LEVEL_CNT];
	std::vector<float> cWeight[OBJECT_SIZE_LEVEL_CNT];	
	//std::vector<cv::Rect> cCandiate;
	
	float eps = 2.2;
	for( int level = 0 ; level < OBJECT_SIZE_LEVEL_CNT ; level++)
	{
		//maskSize.width = maskSize.height = object_size_level[level];
		maskSize.width = maskSize.height = object_size_level[level];
		temp2.create( height/2 + maskSize.width, width/2 + maskSize.height, CV_8UC1 );
		
		box_filter(edge.data, temp.data, dstSize.width, dstSize.height, maskSize.width, maskSize.height,  temp2.data);

		while(1){
			int x,y;
			Ipp8u nMax;
			float fWeight;
		
			ippiMaxIndx_8u_C1R(temp.data, temp.step.buf[0], roiSize, &nMax, &x, &y);

			//need normaize this nMax value to compare maxium hint point
			fWeight = (float)nMax/(maskSize.width *maskSize.height);

			if(nMax < theshold_level[level]) break;
		
			rc.x		= (2*x > maskSize.width)? (2*x - maskSize.width):0;
			rc.y		= (2*y > maskSize.height)? (2*y - maskSize.height):0;
			rc.width	= 2*maskSize.width; 
			rc.height	= 2*maskSize.height; 

#ifdef DEBUG_SHOW		
			cv::Scalar color(0, 255, 0);
			color[1] = color_level[level];
			rectangle(input, rc, color, 2); 
#endif 
			cCandiate[level].push_back(rc);
			cWeight[level].push_back(nMax);
			//cCandiate.push_back(rc);

			markSize.width = int(maskSize.width * eps);
			markSize.height= int(maskSize.height * eps); 
			x = (x > (markSize.width>>1))?(x - (markSize.width>>1)):0;
			y = (y > (markSize.height>>1))?(y - (markSize.height>>1)):0;
			
			if( (markSize.width + x) > temp.cols){
				x = temp.cols - markSize.width;
			}
			if( (markSize.height + y) > temp.rows){
				y = temp.rows - markSize.height;		
			}

			Ipp8u* pDst = temp.data + x + temp.step.buf[0] * y;						
			ippiSet_8u_C1R(0, pDst, temp.step.buf[0], markSize);
		}
	}
	
	//backward merge 
	BOOL merge;
	int center1_x, center1_y;
	int center2_x, center2_y;
	float distance, offset;

	for( int level = OBJECT_SIZE_LEVEL_CNT-1 ; level >= 0 ; level--){
		
		for(int i = 0 ; i < cCandiate[level].size() ; i++){
			merge = FALSE; 

			center1_x	= cCandiate[level][i].x + cCandiate[level][i].width/2;
			center1_y	= cCandiate[level][i].y + cCandiate[level][i].height/2;
			offset = sqrt(float((cCandiate[level][i].width*cCandiate[level][i].width) /4+ (cCandiate[level][i].height*cCandiate[level][i].height)/4));
				
			//find current to merged fossil
			for(int j = 0 ; j < m_cFossilList.size() ; j++){
				center2_x	= m_cFossilList[j].x + m_cFossilList[j].width/2;
				center2_y	= m_cFossilList[j].y + m_cFossilList[j].height/2;

				distance = sqrt ( float ((center1_x - center2_x) * (center1_x - center2_x) + (center1_y - center2_y)*(center1_y - center2_y)));
				if( offset > distance ){
					merge = TRUE; 
					if(cWeight[level][i]*1.3 > m_cWeightList[j] ){
						m_cFossilList[j] = cCandiate[level][i];	
						m_cWeightList[j] = cWeight[level][i];
					}
					break;
				}
			}

			//not merged rect then add 
			if( FALSE == merge){
				m_cFossilList.push_back(cCandiate[level][i]);
				m_cWeightList.push_back(cWeight[level][i]);
			}
		}
	}
	
	
	//Remove embeded fossil object then merge.
#if 1
	for(int i = 0 ; i < m_cFossilList.size() ; i++){
		center1_x	= m_cFossilList[i].x + m_cFossilList[i].width/2;
		center1_y	= m_cFossilList[i].y + m_cFossilList[i].height/2;
		offset = sqrt(float(m_cFossilList[i].width*m_cFossilList[i].width/4 + m_cFossilList[i].height*m_cFossilList[i].height/4));
		for(int j = 1 ; j < m_cFossilList.size() ; j++){
			if(i == j) continue;
			center2_x	= m_cFossilList[j].x + m_cFossilList[j].width/2;
			center2_y	= m_cFossilList[j].y + m_cFossilList[j].height/2;

			distance = sqrt ( float ((center1_x - center2_x) * (center1_x - center2_x) + (center1_y - center2_y)*(center1_y - center2_y)));
			if( offset > distance ){
				m_cFossilList[j].x = m_cFossilList[j].y =  m_cFossilList[j].width = m_cFossilList[j].height = 0;
			}
		}
	}
#endif
	

#ifdef DEBUG_SHOW		
	cv::Scalar color(255, 0, 0);
	for( int i = 0 ; i < m_cFossilList.size() ; i++)
	{
		rectangle(input, m_cFossilList[i], color, 2); 
	}
	
	imshow("EDGE", edge);	
	imshow("OUTPUT", input);	
	imshow("Temp", temp);	
	//imshow("Temp2", temp2);		
#endif
	

	*pMaxDetectCount	= 0;
	for(int i = 0 ; i < m_cFossilList.size() ; i++){
		if(m_cFossilList[i].x  == 0 ) continue;
		(*pMaxDetectCount)++;
	}

	return TRUE;
}


BOOL	CFossilDetectorIPP_Laplacian::GetFossilObject( FOSSILDETECTInfo *pOjbectInfo, int* pDetectCount)
{
	int i, k;
	
	//Find best fossil position using moment.
	FOSSILDETECTInfo info;
	cv::Mat temp, temp2, fossil_gray;
	int increase_x, increase_y;

	*pDetectCount = 0;
	for( i = 0, k = 0 ; i < m_cFossilList.size() ; i++){
		if(m_cFossilList[i].x  == 0 ) continue;
		
		//check boundary
		increase_x = (m_cFossilList[i].width*20)/100;
		increase_y = (m_cFossilList[i].height*20)/100;
		
		m_cFossilList[i].x	= (m_cFossilList[i].x > increase_x)?(m_cFossilList[i].x - increase_x):0;
		m_cFossilList[i].y	= (m_cFossilList[i].y > increase_y)?(m_cFossilList[i].y - increase_y):0;

		m_cFossilList[i].width	= m_cFossilList[i].width + increase_x;
		m_cFossilList[i].height	= m_cFossilList[i].height+ increase_y;
		
		m_cFossilList[i].width	= ((m_cFossilList[i].width + m_cFossilList[i].x) > m_gray.cols)? (m_gray.cols - m_cFossilList[i].x ) : m_cFossilList[i].width;
		m_cFossilList[i].height = ((m_cFossilList[i].height + m_cFossilList[i].y) > m_gray.rows)? (m_gray.rows - m_cFossilList[i].y ) : m_cFossilList[i].height;

		memset(&info, 0, sizeof(info));
		fossil_gray = m_gray(m_cFossilList[i]);

#ifdef DEBUG_SHOW_FOSSIL		
		cv::Mat fossil		= m_input(m_cFossilList[i]);
		//cv::Mat fossil_gray = m_gray(m_cFossilList[i]);

		cv::imshow("FOSSILE", fossil);	
		cv::imshow("FOSSILE_GRAY", fossil_gray);	
#endif
		cv::threshold(fossil_gray, temp, m_mean+10, 255, cv::THRESH_BINARY);
#ifdef DEBUG_SHOW_FOSSIL		
		cv::imshow("FOSSILE_BINARY", temp);	
#endif

		int center_x, center_y;
		int move_x, move_y;
		int weight_w, weight_h;

		FindCenter(temp.data, temp.cols, temp.rows, temp.step.buf[0], &center_x, &center_y, &weight_w, &weight_h);
		move_x = (m_cFossilList[i].width)/2 - center_x;
		move_y = (m_cFossilList[i].height)/2 - center_y;

		//move position by moment.
		m_cFossilList[i].x -= move_x;
		m_cFossilList[i].y -= move_y;

		//Check boundary
		increase_x = (m_cFossilList[i].width*weight_w)/100;
		increase_y = (m_cFossilList[i].height*weight_h)/100;
		
		pOjbectInfo[k].rc.left	= (m_cFossilList[i].x > increase_x)?(m_cFossilList[i].x - increase_x):0;
		pOjbectInfo[k].rc.top	= (m_cFossilList[i].y > increase_y)?(m_cFossilList[i].y - increase_y):0;

		pOjbectInfo[k].rc.right	= m_cFossilList[i].x + m_cFossilList[i].width + increase_x;
		pOjbectInfo[k].rc.bottom= m_cFossilList[i].y + m_cFossilList[i].height+ increase_y;
		
		pOjbectInfo[k].rc.right	= (pOjbectInfo[k].rc.right > m_gray.cols)? m_gray.cols : pOjbectInfo[k].rc.right;
		pOjbectInfo[k].rc.bottom= (pOjbectInfo[k].rc.bottom > m_gray.rows)? m_gray.rows : pOjbectInfo[k].rc.bottom;
		
#ifdef DEBUG_SHOW_FOSSIL	
		cv::Rect region(pOjbectInfo[k].rc.left, pOjbectInfo[k].rc.top, pOjbectInfo[k].rc.right - pOjbectInfo[k].rc.left, pOjbectInfo[k].rc.bottom - pOjbectInfo[k].rc.top);
		fossil		= m_input(region);
		cv::imshow("FOSSILE_CENTER", fossil);	

		cv::waitKey(0);
#endif
		//Get Fossil Info 
		GetFossilInfo(pOjbectInfo[k].rc, &info);
		pOjbectInfo[k] = info;

		k++;
	}

	*pDetectCount = k;
	return TRUE;
}



#include "FossilDetectorIPP_Canny.h"
#include "IPPFunc.h"
#include "highgui.h"
#include <math.h>

//IPP link static library
#pragma comment(lib, "ippcorel.lib")
#pragma comment(lib, "ippimerged.lib")
#pragma comment(lib, "ippiemerged.lib")
#pragma comment(lib, "ippcvmerged.lib")
#pragma comment(lib, "ippcvemerged.lib")
#pragma comment(lib, "ippsmerged.lib")
#pragma comment(lib, "ippsemerged.lib")
#pragma comment(lib, "ippccmerged.lib")
#pragma comment(lib, "ippccemerged.lib")

#ifdef _DEBUG
#pragma comment(lib, "opencv_core2410d.lib")
#pragma comment(lib, "opencv_highgui2410d.lib")
#pragma comment(lib, "opencv_imgproc2410d.lib")
#else
#pragma comment(lib, "opencv_core2410.lib")
#pragma comment(lib, "opencv_highgui2410.lib")
#pragma comment(lib, "opencv_imgproc2410.lib")
#endif

#if 0
#define	OBJECT_SIZE_LEVEL_CNT	5
static int	object_size_level[OBJECT_SIZE_LEVEL_CNT]= {60, 50, 40, 30, 22};
static int color_level[OBJECT_SIZE_LEVEL_CNT]		= {255,210,180,150,120};
//static int theshold_level[OBJECT_SIZE_LEVEL_CNT]	= {50 ,52, 52, 54, 60};
static int theshold_level[OBJECT_SIZE_LEVEL_CNT]	= {60 ,61, 62, 63, 65};

#else

#define	OBJECT_SIZE_LEVEL_CNT	5
static int	object_size_level[OBJECT_SIZE_LEVEL_CNT]= {48, 38, 30, 24, 20};
static int color_level[OBJECT_SIZE_LEVEL_CNT]		= {255,210,180,150,120};
//static int theshold_level[OBJECT_SIZE_LEVEL_CNT]	= {50 ,52, 52, 54, 60};
static int theshold_level[OBJECT_SIZE_LEVEL_CNT]	= {61, 62, 63, 64, 66};

#endif
//#define DEBUG_SHOW


CFossilDetectorIPP_Canny::CFossilDetectorIPP_Canny()
{
}


CFossilDetectorIPP_Canny::~CFossilDetectorIPP_Canny()
{
}


BOOL	CFossilDetectorIPP_Canny::Open(const char *szConfigXML)
{
	return TRUE;
}


BOOL	CFossilDetectorIPP_Canny::ProcessFossilDetector( int width, int height, int step, BYTE *pData, int* pDetectCount )
{
#ifdef DEBUG_SHOW		
	cv::Mat	input(height, width, CV_8UC3, pData, step);
#endif 
	cv::Mat	gray, temp, edge, temp2;
	* pDetectCount = 0; 
	
	gray.create( height, width, CV_8UC1 );
	temp.create( height/2, width/2, CV_8UC1 );
	edge.create( height/2, width/2, CV_8UC1 );
	
	m_cFossilList.clear();
	m_cWeightList.clear();

	//1. color conversion
	IppiSize roiSize = {width , height };
	ippiRGBToGray_8u_C3C1R(pData, step, gray.data, width, roiSize);

	//2. resize 0.5
	IppiRect srcRoi={0,0,width,height};
	IppiSize dstSize = {width/2,height/2};
	ippiResize_8u_C1R(gray.data, roiSize, gray.step.buf[0], srcRoi, temp.data, temp.step.buf[0], dstSize, 0.5, 0.5, IPPI_INTER_CUBIC);

	//3. canny edge 
	filter_canny(temp.data, edge.data, temp.cols, temp.rows, 50, 100);
		
	//4. apply box filter to detect high entropy.
	roiSize.width = width/2;
	roiSize.height= height/2;

	IppiSize maskSize, markSize;
	cv::Rect rc;
	std::vector<cv::Rect> cCandiate[OBJECT_SIZE_LEVEL_CNT];
	std::vector<int> cWeight[OBJECT_SIZE_LEVEL_CNT];	
	//std::vector<cv::Rect> cCandiate;
	

	float eps = 1.8;
	for( int level = 0 ; level < OBJECT_SIZE_LEVEL_CNT ; level++)
	{
		maskSize.width = maskSize.height = object_size_level[level];
		
		temp2.create( height/2 + maskSize.width, width/2 + maskSize.height, CV_8UC1 );
		box_filter(edge.data, temp.data, dstSize.width, dstSize.height, maskSize.width, maskSize.height, temp2.data);

		while(1){
			int x,y;
			Ipp8u nMax;
		
			ippiMaxIndx_8u_C1R(temp.data, temp.step.buf[0], roiSize, &nMax, &x, &y);
			if(nMax < theshold_level[level]) break;
		
			rc.x		= (2*x > maskSize.width)? (2*x - maskSize.width):0;
			rc.y		= (2*y > maskSize.height)? (2*y - maskSize.height):0;
			rc.width	= 2*maskSize.width; 
			rc.height	= 2*maskSize.height; 

#ifdef DEBUG_SHOW		
			cv::Scalar color(0, 255, 0);
			color[1] = color_level[level];
			rectangle(input, rc, color, 2); 
#endif 
			cCandiate[level].push_back(rc);
			cWeight[level].push_back(nMax);
			//cCandiate.push_back(rc);

			markSize.width = int(maskSize.width * eps);
			markSize.height= int(maskSize.height * eps); 
			x = (x > (markSize.width>>1))?(x - (markSize.width>>1)):0;
			y = (y > (markSize.height>>1))?(y - (markSize.height>>1)):0;
			
			if( (markSize.width + x) > temp.cols){
				x = temp.cols - markSize.width;
			}
			if( (markSize.height + y) > temp.rows){
				y = temp.rows - markSize.height;		
			}

			Ipp8u* pDst = temp.data + x + temp.step.buf[0] * y;						
			ippiSet_8u_C1R(0, pDst, temp.step.buf[0], markSize);
		}

	}
	
	//backward merge 
	BOOL merge;
	int center1_x, center1_y;
	int center2_x, center2_y;
	float distance, offset;

	for( int level = OBJECT_SIZE_LEVEL_CNT-1 ; level >= 0 ; level--){
		
		for(int i = 0 ; i < cCandiate[level].size() ; i++){
			merge = FALSE; 

			center1_x	= cCandiate[level][i].x + cCandiate[level][i].width/2;
			center1_y	= cCandiate[level][i].y + cCandiate[level][i].height/2;
			offset = sqrt(float((cCandiate[level][i].width*cCandiate[level][i].width) /4+ (cCandiate[level][i].height*cCandiate[level][i].height)/4));
				
			//find current to merged fossil
			for(int j = 0 ; j < m_cFossilList.size() ; j++){
				center2_x	= m_cFossilList[j].x + m_cFossilList[j].width/2;
				center2_y	= m_cFossilList[j].y + m_cFossilList[j].height/2;

				distance = sqrt ( float ((center1_x - center2_x) * (center1_x - center2_x) + (center1_y - center2_y)*(center1_y - center2_y)));
				if( offset > distance ){
					merge = TRUE; 
					if(cWeight[level][i]*1.12 > m_cWeightList[j] ){
						m_cFossilList[j] = cCandiate[level][i];	
						m_cWeightList[j] = cWeight[level][i];
					}
					break;
				}
			}

			//not merged rect then add 
			if( FALSE == merge){
				m_cFossilList.push_back(cCandiate[level][i]);
				m_cWeightList.push_back(cWeight[level][i]);
			}
		}
	}
	
	
	//Remove embeded fossil object then merge.
#if 1
	for(int i = 0 ; i < m_cFossilList.size() ; i++){
		center1_x	= m_cFossilList[i].x + m_cFossilList[i].width/2;
		center1_y	= m_cFossilList[i].y + m_cFossilList[i].height/2;
		offset = sqrt(float(m_cFossilList[i].width*m_cFossilList[i].width/4 + m_cFossilList[i].height*m_cFossilList[i].height/4));
		for(int j = 1 ; j < m_cFossilList.size() ; j++){
			if(i == j) continue;
			center2_x	= m_cFossilList[j].x + m_cFossilList[j].width/2;
			center2_y	= m_cFossilList[j].y + m_cFossilList[j].height/2;

			distance = sqrt ( float ((center1_x - center2_x) * (center1_x - center2_x) + (center1_y - center2_y)*(center1_y - center2_y)));
			if( offset > distance ){
				m_cFossilList[j].x = m_cFossilList[j].y =  m_cFossilList[j].width = m_cFossilList[j].height = 0;
			}
		}
	}
#endif
	

#ifdef DEBUG_SHOW		
	cv::Scalar color(255, 0, 0);
	for( int i = 0 ; i < m_cFossilList.size() ; i++)
	{
		rectangle(input, m_cFossilList[i], color, 2); 
	}
	
	imshow("EDGE", edge);	
	imshow("OUTPUT", input);	
#endif
	
	*pDetectCount	= 0;
	for(int i = 0 ; i < m_cFossilList.size() ; i++){
		if(m_cFossilList[i].x  == 0 ) continue;
		(*pDetectCount)++;
	}
		
	return TRUE;
}


BOOL	CFossilDetectorIPP_Canny::GetFossilObject( FOSSILDETECTInfo *pOjbectInfo)
{
	int i, k;
	
	for( i = 0, k = 0 ; i < m_cFossilList.size() ; i++){
		if(m_cFossilList[i].x  == 0 ) continue;

		int increase_x = (m_cFossilList[i].width*20)/100;
		int increase_y = (m_cFossilList[i].height*20)/100;
		
		pOjbectInfo[k].rc.left	= (m_cFossilList[i].x > increase_x)?(m_cFossilList[i].x - increase_x):0;
		pOjbectInfo[k].rc.top	= (m_cFossilList[i].y > increase_y)?(m_cFossilList[i].y - increase_y):0;

		pOjbectInfo[k].rc.right	= m_cFossilList[i].x + m_cFossilList[i].width + increase_x;
		pOjbectInfo[k].rc.bottom= m_cFossilList[i].y + m_cFossilList[i].height+ increase_y;

		pOjbectInfo[k].weight	= m_cWeightList[i];
		k++;
	}
	
	return TRUE;
}

#ifndef _IPP_UTILS_FUNC_H_
#define _IPP_UTILS_FUNC_H_

#include "ipp.h"

int filter_canny(Ipp8u *src, Ipp8u *dst, int width, int height, float low, float high);
int laplacian_filter(Ipp8u *src, Ipp8u *dst, Ipp8u *dst_threshold, int width, int height, Ipp8u *src_border, IppiMaskSize maskSize);
int box_filter(Ipp8u *src, Ipp8u *dst, int width, int height, int mask_width, int mask_height, Ipp8u *src_border);

#endif
#include "immintrin.h"
#include "emmintrin.h"
#include "ipp.h"

static char pKernal32[32 * 32] = {
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 2, 2, 2, 2, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 0, 1, 1, 2, 2, 2, 2, 2, 3, 3, 3, 3, 2, 2, 2, 2, 2, 1, 1, 0, 0, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 1, 1, 2, 2, 2, 2, 3, 3, 2, 2, 2, 2, 3, 3, 2, 2, 2, 2, 1, 1, 0, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 1, 1, 2, 2, 2, 3, 3, 2, 2, 2, 1, 1, 2, 2, 2, 3, 3, 2, 2, 2, 1, 1, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 1, 1, 2, 2, 2, 3, 2, 2, 2, 2, 1, 1, 1, 1, 2, 2, 2, 2, 3, 2, 2, 2, 1, 1, 0, 0, 0, 0,
	0, 0, 0, 1, 1, 2, 2, 2, 3, 2, 2, 2, 2, 1, 1, 1, 1, 1, 1, 2, 2, 2, 2, 3, 2, 2, 2, 1, 1, 0, 0, 0,
	0, 0, 1, 1, 2, 2, 2, 3, 2, 2, 2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 2, 2, 3, 2, 2, 2, 1, 1, 0, 0,
	0, 0, 1, 2, 2, 2, 3, 2, 2, 2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 2, 2, 3, 2, 2, 2, 1, 0, 0,
	0, 1, 1, 2, 2, 3, 2, 2, 2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 2, 2, 3, 2, 2, 1, 1, 0,
	0, 1, 2, 2, 2, 3, 2, 2, 2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 2, 3, 2, 2, 2, 1, 0,
	0, 1, 2, 2, 3, 2, 2, 2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 2, 3, 2, 2, 1, 0,
	1, 1, 2, 2, 3, 2, 2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 2, 3, 2, 2, 1, 1,
	1, 2, 2, 3, 2, 2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 2, 3, 2, 2, 1,
	1, 2, 2, 3, 2, 2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 2, 3, 2, 2, 1,
	1, 2, 2, 3, 2, 2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 2, 3, 2, 2, 1,
	1, 2, 2, 3, 2, 2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 2, 3, 2, 2, 1,
	1, 1, 2, 2, 3, 2, 2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 2, 3, 2, 2, 1, 1,
	0, 1, 2, 2, 3, 2, 2, 2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 2, 2, 3, 2, 2, 1, 0,
	0, 1, 2, 2, 2, 3, 2, 2, 2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 2, 2, 3, 2, 2, 2, 1, 0,
	0, 1, 1, 2, 2, 3, 2, 2, 2, 2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 2, 2, 2, 3, 2, 2, 1, 1, 0,
	0, 0, 1, 2, 2, 2, 3, 2, 2, 2, 2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 2, 2, 2, 3, 2, 2, 2, 1, 0, 0,
	0, 0, 1, 1, 2, 2, 2, 3, 2, 2, 2, 2, 1, 1, 1, 1, 1, 1, 1, 1, 2, 2, 2, 2, 3, 2, 2, 2, 1, 1, 0, 0,
	0, 0, 0, 1, 1, 2, 2, 2, 3, 2, 2, 2, 2, 1, 1, 1, 1, 1, 1, 2, 2, 2, 2, 3, 2, 2, 2, 1, 1, 0, 0, 0,
	0, 0, 0, 0, 1, 1, 2, 2, 2, 3, 2, 2, 2, 2, 1, 1, 1, 1, 2, 2, 2, 2, 3, 2, 2, 2, 1, 1, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 1, 1, 2, 2, 2, 3, 3, 2, 2, 2, 1, 1, 2, 2, 2, 3, 3, 2, 2, 2, 1, 1, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 1, 1, 2, 2, 2, 2, 3, 3, 2, 2, 2, 2, 3, 3, 2, 2, 2, 2, 1, 1, 0, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 0, 1, 1, 2, 2, 2, 2, 2, 3, 3, 3, 3, 2, 2, 2, 2, 2, 1, 1, 0, 0, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 2, 2, 2, 2, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
};

/*
static char pKernal16[16 * 16] = {
	0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 1, 1, 2, 2, 2, 2, 1, 1, 0, 0, 0, 0,
	0, 0, 0, 1, 2, 2, 3, 2, 2, 3, 2, 2, 1, 0, 0, 0,
	0, 0, 1, 2, 2, 2, 2, 1, 1, 2, 2, 2, 2, 1, 0, 0,
	0, 1, 2, 2, 2, 2, 1, 1, 1, 1, 2, 2, 2, 2, 1, 0,
	0, 1, 2, 2, 2, 1, 1, 1, 1, 1, 1, 2, 2, 2, 1, 0,
	0, 2, 3, 2, 1, 1, 1, 1, 1, 1, 1, 1, 2, 3, 2, 0,
	1, 2, 2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 2, 1,
	1, 2, 2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 2, 1,
	0, 2, 3, 2, 1, 1, 1, 1, 1, 1, 1, 1, 2, 3, 2, 0,
	0, 1, 2, 2, 2, 1, 1, 1, 1, 1, 1, 2, 2, 2, 1, 0,
	0, 1, 2, 2, 2, 2, 1, 1, 1, 1, 2, 2, 2, 2, 1, 0,
	0, 0, 1, 2, 2, 2, 2, 1, 1, 2, 2, 2, 2, 1, 0, 0,
	0, 0, 0, 1, 2, 2, 3, 2, 2, 3, 2, 2, 1, 0, 0, 0,
	0, 0, 0, 0, 1, 1, 2, 2, 2, 2, 1, 1, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0,
};

static char pKernal16[16 * 16] = {
	0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 1, 1, 2, 2, 2, 2, 1, 1, 0, 0, 0, 0,
	0, 0, 0, 1, 2, 2, 2, 2, 2, 2, 2, 2, 1, 0, 0, 0,
	0, 0, 1, 2, 2, 2, 2, 1, 1, 2, 2, 2, 2, 1, 0, 0,
	0, 1, 2, 2, 2, 2, 1, 1, 1, 1, 2, 2, 2, 2, 1, 0,
	0, 1, 2, 2, 2, 1, 1, 1, 1, 1, 1, 2, 2, 2, 1, 0,
	1, 2, 2, 2, 1, 1, 1, 2, 2, 1, 1, 1, 2, 2, 2, 1,
	1, 2, 2, 1, 1, 1, 2, 2, 2, 2, 1, 1, 1, 2, 2, 1,
	1, 2, 2, 1, 1, 1, 2, 2, 2, 2, 1, 1, 1, 2, 2, 1,
	1, 2, 2, 2, 1, 1, 1, 2, 2, 1, 1, 1, 2, 2, 2, 1,
	0, 1, 2, 2, 2, 1, 1, 1, 1, 1, 1, 2, 2, 2, 1, 0,
	0, 1, 2, 2, 2, 2, 1, 1, 1, 1, 2, 2, 2, 2, 1, 0,
	0, 0, 1, 2, 2, 2, 2, 1, 1, 2, 2, 2, 2, 1, 0, 0,
	0, 0, 0, 1, 2, 2, 2, 2, 2, 2, 2, 2, 1, 0, 0, 0,
	0, 0, 0, 0, 1, 1, 2, 2, 2, 2, 1, 1, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0,
};

static char pKernal16[16 * 16] = {
	0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0,
	0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0,
	0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0,
	0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0,
	0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0,
	1, 1, 1, 1, 1, 1, 1, 2, 2, 1, 1, 1, 1, 1, 1, 1,
	1, 1, 1, 1, 1, 1, 2, 2, 2, 2, 1, 1, 1, 1, 1, 1,
	1, 1, 1, 1, 1, 1, 2, 2, 2, 2, 1, 1, 1, 1, 1, 1,
	1, 1, 1, 1, 1, 1, 1, 2, 2, 1, 1, 1, 1, 1, 1, 1,
	0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0,
	0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0,
	0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0,
	0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0,
	0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0,
};

static char pKernal16[16 * 16] = {
	0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 1, 1, 2, 2, 2, 2, 1, 1, 0, 0, 0, 0,
	0, 0, 0, 1, 2, 2, 4, 4, 4, 4, 2, 2, 1, 0, 0, 0,
	0, 0, 1, 2, 2, 4, 2, 2, 2, 2, 4, 2, 2, 1, 0, 0,
	0, 1, 2, 2, 4, 2, 1, 1, 1, 1, 2, 4, 2, 2, 1, 0,
	0, 1, 2, 4, 2, 1, 1, 1, 1, 1, 1, 2, 4, 2, 1, 0,
	0, 2, 4, 2, 1, 1, 1, 1, 1, 1, 1, 1, 2, 4, 2, 0,
	1, 2, 4, 2, 1, 1, 1, 1, 1, 1, 1, 1, 2, 4, 2, 1,
	1, 2, 4, 2, 1, 1, 1, 1, 1, 1, 1, 1, 2, 4, 2, 1,
	0, 2, 4, 2, 1, 1, 1, 1, 1, 1, 1, 1, 2, 4, 2, 0,
	0, 1, 2, 4, 2, 1, 1, 1, 1, 1, 1, 2, 4, 2, 1, 0,
	0, 1, 2, 2, 4, 2, 1, 1, 1, 1, 2, 4, 2, 2, 1, 0,
	0, 0, 1, 2, 2, 4, 2, 2, 2, 2, 4, 2, 2, 1, 0, 0,
	0, 0, 0, 1, 2, 2, 4, 4, 4, 4, 2, 2, 1, 0, 0, 0,
	0, 0, 0, 0, 1, 1, 2, 2, 2, 2, 1, 1, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0,
};

static char pKernal16[16 * 16] = {
	0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 1, 1, 2, 2, 2, 2, 1, 1, 0, 0, 0, 0,
	0, 0, 0, 1, 1, 2, 1, 1, 1, 1, 2, 1, 1, 0, 0, 0,
	0, 0, 1, 1, 2, 1, 1, 1, 1, 1, 1, 2, 1, 1, 0, 0,
	0, 1, 1, 2, 1, 1, 1, 1, 1, 1, 1, 1, 2, 1, 1, 0,
	0, 1, 2, 1, 1, 1, 1, 2, 2, 1, 1, 1, 1, 2, 1, 0,
	1, 2, 1, 1, 1, 1, 2, 2, 2, 2, 1, 1, 1, 1, 2, 1,
	1, 2, 1, 1, 1, 2, 2, 2, 2, 2, 2, 1, 1, 1, 2, 1,
	1, 2, 1, 1, 1, 2, 2, 2, 2, 2, 2, 1, 1, 1, 2, 1,
	1, 2, 1, 1, 1, 1, 2, 2, 2, 2, 1, 1, 1, 1, 2, 1,
	0, 1, 2, 1, 1, 1, 1, 2, 2, 1, 1, 1, 1, 2, 1, 0,
	0, 1, 1, 2, 1, 1, 1, 1, 1, 1, 1, 1, 2, 1, 1, 0,
	0, 0, 1, 1, 2, 1, 1, 1, 1, 1, 1, 2, 1, 1, 0, 0,
	0, 0, 0, 1, 1, 2, 1, 1, 1, 1, 2, 1, 1, 0, 0, 0,
	0, 0, 0, 0, 1, 1, 2, 2, 2, 2, 1, 1, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0,
};



*/

static char pKernal16[16 * 16] = {
	0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 1, 1, 2, 2, 2, 2, 1, 1, 0, 0, 0, 0,
	0, 0, 0, 1, 1, 2, 1, 1, 1, 1, 2, 1, 1, 0, 0, 0,
	0, 0, 1, 1, 2, 1, 1, 1, 1, 1, 1, 2, 1, 1, 0, 0,
	0, 1, 1, 2, 1, 1, 1, 1, 1, 1, 1, 1, 2, 1, 1, 0,
	0, 1, 2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 1, 0,
	1, 2, 1, 1, 1, 1, 1, 2, 2, 1, 1, 1, 1, 1, 2, 1,
	1, 2, 1, 1, 1, 1, 2, 2, 2, 2, 1, 1, 1, 1, 2, 1,
	1, 2, 1, 1, 1, 1, 2, 2, 2, 2, 1, 1, 1, 1, 2, 1,
	1, 2, 1, 1, 1, 1, 1, 2, 2, 1, 1, 1, 1, 1, 2, 1,
	0, 1, 2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 1, 0,
	0, 1, 1, 2, 1, 1, 1, 1, 1, 1, 1, 1, 2, 1, 1, 0,
	0, 0, 1, 1, 2, 1, 1, 1, 1, 1, 1, 2, 1, 1, 0, 0,
	0, 0, 0, 1, 1, 2, 1, 1, 1, 1, 2, 1, 1, 0, 0, 0,
	0, 0, 0, 0, 1, 1, 2, 2, 2, 2, 1, 1, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0,
};


int custom_valcano_filter32(unsigned char *src, unsigned char *dst, int width, int height, int patch)
{
	int i, j, n, m, r;
	__m128i		Input1, Input2, Zero, Kernel1, Kernel2, Result, Temp;

	_mm_prefetch(pKernal32, _MM_HINT_T0);
	_mm_prefetch((const char*)src, _MM_HINT_NTA);
	_mm_prefetch((const char*)dst, _MM_HINT_NTA);

	int kernal_sum = 0;
	for(i = 0 ; i < 32 ; i++)
		for(j = 0 ; j < 32 ; j++) kernal_sum+= pKernal32[i*32 + j];

//	kernal_sum = kernal_sum>>1;

	Zero = _mm_setzero_si128();
	r = 0;
	for (i = 0; i < height - 32; i+=2){
		for (j = 0; j < width - 32; j+=2){
			Result = _mm_setzero_si128();
			for (n = 0; n < 32; n++){
				for (m = 0; m < 32; m += 16){
					Temp = _mm_loadu_si128((__m128i*)(src + (i + n) * patch + m + j)); 
					Input1 = _mm_unpacklo_epi8(Temp, Zero);
					Input2 = _mm_unpackhi_epi8(Temp, Zero);

					Temp = _mm_loadu_si128((__m128i*)(pKernal32 + n * 32 + m));
					Kernel1 = _mm_unpacklo_epi8(Temp, Zero);
					Kernel2 = _mm_unpackhi_epi8(Temp, Zero);

					Input1 = _mm_madd_epi16(Input1, Kernel1);
					Input2 = _mm_madd_epi16(Input2, Kernel2);
					Input1 = _mm_packs_epi32(Input1, Input2);
					Result = _mm_adds_epi16(Input1, Result);
				}
			}
			dst[r] = (Result.m128i_u16[0] + Result.m128i_u16[1]	\
					+ Result.m128i_u16[2] + Result.m128i_u16[3] \
					+ Result.m128i_u16[4] + Result.m128i_u16[5] \
					+ Result.m128i_u16[6] + Result.m128i_u16[7])/kernal_sum; \
			r++;
		}
	}
	return r;
}


int custom_valcano_filter16(unsigned char *src, unsigned char *dst, int width, int height, int patch)
{
	int i, j, r, n;
	__m128i		Input1, Input2, Zero, Kernel1, Kernel2, Result, Temp;

	_mm_prefetch(pKernal16, _MM_HINT_T0);
	_mm_prefetch((const char*)src, _MM_HINT_NTA);
	_mm_prefetch((const char*)dst, _MM_HINT_NTA);

	int kernal_sum = 0;
	for(i = 0 ; i < 16 ; i++)
		for(j = 0 ; j < 16 ; j++) kernal_sum+= pKernal16[i*16 + j];

	kernal_sum = kernal_sum>>1;

	Zero = _mm_setzero_si128();
	r = 0;
	for (i = 0; i < height - 16; i+=2){
		for (j = 0; j < width - 16; j+=2){
			Result = _mm_setzero_si128();
			for (n = 0; n < 16; n++){
				Temp = _mm_loadu_si128((__m128i*)(src + (i + n) * patch  + j)); 
				Input1 = _mm_unpacklo_epi8(Temp, Zero);
				Input2 = _mm_unpackhi_epi8(Temp, Zero);

				Temp = _mm_loadu_si128((__m128i*)(pKernal16 + n * 16));
				Kernel1 = _mm_unpacklo_epi8(Temp, Zero);
				Kernel2 = _mm_unpackhi_epi8(Temp, Zero);

				Input1 = _mm_madd_epi16(Input1, Kernel1);
				Input2 = _mm_madd_epi16(Input2, Kernel2);
				Input1 = _mm_packs_epi32(Input1, Input2);
				Result = _mm_adds_epi16(Input1, Result);
			}
			
			dst[r] = (Result.m128i_u16[0] + Result.m128i_u16[1]	\
					+ Result.m128i_u16[2] + Result.m128i_u16[3] \
					+ Result.m128i_u16[4] + Result.m128i_u16[5] \
					+ Result.m128i_u16[6] + Result.m128i_u16[7])/kernal_sum; \
			r++;
		}
	}

	return 0;
}


#define ABS(value) (value>0)?value:-value;

int custom_laplacian_filter(unsigned char *src, unsigned char *dst_limit, unsigned char *dst, int width, int height, int patch)
{
	int i, j, r, n, remain, value;
	__m128i		Input1, Input2, Zero, Kernel[3], Result1, Result2, Temp;
	Zero	= _mm_setzero_si128();
	r =0;
	Kernel[0]	= _mm_set_epi16(0, -1, -1, -1, 0, -1, -1, -1);
	Kernel[1]	= _mm_set_epi16(0, -1,  8, -1, 0, -1,  8, -1);
	Kernel[2]	= _mm_set_epi16(0, -1, -1, -1, 0, -1, -1, -1);

	for(i = 0 ; i < height-2 ; i+=2){
		for(j = 0 ; j < width-2 ; j+=16){
			Result1 = _mm_setzero_si128();
			Result2 = _mm_setzero_si128();

			for(n = 0 ; n < 3 ; n++){

				Temp	= _mm_loadu_si128((__m128i*)(src + (i + n)*patch + j));
				Input1	= _mm_unpacklo_epi8(Temp,Zero);
				Input2	= _mm_unpackhi_epi8(Temp,Zero);

				Input1	= _mm_madd_epi16(Input1, Kernel[n]);
				Input2	= _mm_madd_epi16(Input2, Kernel[n]);
				Input1	= _mm_packs_epi32(Input1, Input2);
				Result1	= _mm_adds_epi16(Input1, Result1);
				
				Temp	= _mm_loadu_si128((__m128i*)(src + (i + n)*patch + j + 2));
				Input1	= _mm_unpacklo_epi8(Temp,Zero);
				Input2	= _mm_unpackhi_epi8(Temp,Zero);

				Input1	= _mm_madd_epi16(Input1, Kernel[n]);
				Input2	= _mm_madd_epi16(Input2, Kernel[n]);
				Input1	= _mm_packs_epi32(Input1, Input2);
				Result2	= _mm_adds_epi16(Input1, Result2);
			}

			if(j < width-16){	
				value = (Result1.m128i_i16[0] + Result1.m128i_i16[1]);
				dst[r++] = ABS(value);
				value = (Result2.m128i_i16[0] + Result2.m128i_i16[1]);
				dst[r++] = ABS(value);
				value = (Result1.m128i_i16[2] + Result1.m128i_i16[3]);
				dst[r++] = ABS(value);
				value = (Result2.m128i_i16[2] + Result2.m128i_i16[3]);
				dst[r++] = ABS(value);
				value = (Result1.m128i_i16[4] + Result1.m128i_i16[5]);
				dst[r++] = ABS(value);
				value = (Result2.m128i_i16[4] + Result2.m128i_i16[5]);
				dst[r++] = ABS(value);
				value = (Result1.m128i_i16[6] + Result1.m128i_i16[7]);
				dst[r++] = ABS(value);
				value = (Result2.m128i_i16[6] + Result2.m128i_i16[7]);
				dst[r++] = ABS(value);
			}else{
				remain = ((width-16) % 16);
				if(remain == 0 || remain == 15){
					value = (Result1.m128i_i16[0] + Result1.m128i_i16[1]);
					dst[r++] = ABS(value);
					value = (Result2.m128i_i16[0] + Result2.m128i_i16[1]);
					dst[r++] = ABS(value);
					value = (Result1.m128i_i16[2] + Result1.m128i_i16[3]);
					dst[r++] = ABS(value);
					value = (Result2.m128i_i16[2] + Result2.m128i_i16[3]);
					dst[r++] = ABS(value);
					value = (Result1.m128i_i16[4] + Result1.m128i_i16[5]);
					dst[r++] = ABS(value);
					value = (Result2.m128i_i16[4] + Result2.m128i_i16[5]);
					dst[r++] = ABS(value);
					value = (Result1.m128i_i16[6] + Result1.m128i_i16[7]);
					dst[r++] = ABS(value);
				}else if(remain >= 13){
					value = (Result1.m128i_i16[0] + Result1.m128i_i16[1]);
					dst[r++] = ABS(value);
					value = (Result2.m128i_i16[0] + Result2.m128i_i16[1]);
					dst[r++] = ABS(value);
					value = (Result1.m128i_i16[2] + Result1.m128i_i16[3]);
					dst[r++] = ABS(value);
					value = (Result2.m128i_i16[2] + Result2.m128i_i16[3]);
					dst[r++] = ABS(value);
					value = (Result1.m128i_i16[4] + Result1.m128i_i16[5]);
					dst[r++] = ABS(value);
					value = (Result2.m128i_i16[4] + Result2.m128i_i16[5]);
					dst[r++] = ABS(value);
				}else if(remain >= 11){
					value = (Result1.m128i_i16[0] + Result1.m128i_i16[1]);
					dst[r++] = ABS(value);
					value = (Result2.m128i_i16[0] + Result2.m128i_i16[1]);
					dst[r++] = ABS(value);
					value = (Result1.m128i_i16[2] + Result1.m128i_i16[3]);
					dst[r++] = ABS(value);
					value = (Result2.m128i_i16[2] + Result2.m128i_i16[3]);
					dst[r++] = ABS(value);
					value = (Result1.m128i_i16[4] + Result1.m128i_i16[5]);
					dst[r++] = ABS(value);
				}else if(remain >= 9){
					value = (Result1.m128i_i16[0] + Result1.m128i_i16[1]);
					dst[r++] = ABS(value);
					value = (Result2.m128i_i16[0] + Result2.m128i_i16[1]);
					dst[r++] = ABS(value);
					value = (Result1.m128i_i16[2] + Result1.m128i_i16[3]);
					dst[r++] = ABS(value);
					value = (Result2.m128i_i16[2] + Result2.m128i_i16[3]);
					dst[r++] = ABS(value);
				}else if(remain >= 7){
					value = (Result1.m128i_i16[0] + Result1.m128i_u16[1]);
					dst[r++] = ABS(value);
					value = (Result2.m128i_u16[0] + Result2.m128i_u16[1]);
					dst[r++] = ABS(value);
					value = (Result1.m128i_u16[2] + Result1.m128i_u16[3]);
					dst[r++] = ABS(value);
				}else if(remain >= 5){
					value = (Result1.m128i_u16[0] + Result1.m128i_u16[1]);
					dst[r++] = ABS(value);
					value = (Result2.m128i_u16[0] + Result2.m128i_u16[1]);
					dst[r++] = ABS(value);
				}else if(remain >= 3){
					value = (Result1.m128i_u16[0] + Result1.m128i_u16[1]);
					dst[r++] = ABS(value);
				}//else if(remain >= 1){} does not need
			}
		}
	}


	int threshold = 100;
	width	= (width-2)/2;
	height	= (height-2)/2;
	IppiSize	SrcRoiSize	= {width, height};
	ippiThreshold_LTValGTVal_8u_C1R(dst, width, dst_limit, width, SrcRoiSize, 20, 0, 40, 80);
	ippiLShiftC_8u_C1R(dst_limit, width, 1, dst_limit, width, SrcRoiSize);	//2x
	return r;
}


void	downsize_maxium_nn(unsigned char *pSrc, unsigned char *pDst, int width, int height, int src_pitch, int dst_pitch, 
						   float x_factor, float y_factor, int *dst_w, int *dst_h )
{
	int i, j, n, m;
	unsigned char sample1, sample2;
	m = 0;

	if( int(y_factor*height) == int(y_factor*(height-1)))  height -= 1;
	if( int(x_factor*width) == int(x_factor*(width-1)))  width -= 1;
	
	for( i = 0 ; i < height ; i++, m++ ){
		n = 0;
		if( int(y_factor*i) == int(y_factor*(i+1))){
			for( j = 0 ; j < width ; j++, n++ ){
				if( int(x_factor*j) == int(x_factor*(j+1))){
					sample1 = (pSrc[ src_pitch*i + j ] > pSrc[ src_pitch*i + (j +1) ])? pSrc[ src_pitch*i + j ] : pSrc[ src_pitch*i + (j +1) ];
					sample2 = (pSrc[ src_pitch*(i+1) + j ] > pSrc[ src_pitch*(i+1) + (j +1) ])? pSrc[ src_pitch*(i+1) + j ] : pSrc[ src_pitch*(i+1) + (j +1) ];
					pDst[ dst_pitch*m + n ] = sample1 > sample2? sample1 : sample2;
					j++;
				}else{
					pDst[ dst_pitch*m + n ] = (pSrc[ src_pitch*i + j ] > pSrc[src_pitch*(i+1) + j])? pSrc[ src_pitch*i + j ] : pSrc[src_pitch*(i+1) + j];
				}
			}
			i++;
		}else{
			for( j = 0 ; j < width ; j++, n++ ){
				if( int(x_factor*j) == int(x_factor*(j+1))){
					pDst[ dst_pitch*m + n ] = (pSrc[ src_pitch*i + j ] > pSrc[ src_pitch*i + (j +1) ])? pSrc[ src_pitch*i + j ] : pSrc[ src_pitch*i + (j +1) ];
					j++;
				}else{
					pDst[ dst_pitch*m + n ] = pSrc[ src_pitch*i + j ];
				}
			}
		}
	}

	*dst_w = n;
	*dst_h = m;
}

#pragma once

#include <Windows.h>
#include "../include/FossilDetector.h"
#include "opencv/cv.h"

class CFossilDetectorIPP_Canny : public IFossilDetector
{
public:
	CFossilDetectorIPP_Canny();
	~CFossilDetectorIPP_Canny();

	BOOL	Open(const char *szConfigXML);
	BOOL	ProcessFossilDetector( int width, int height, int step, BYTE *pData, int* pDetectCount );
	BOOL	GetFossilObject( FOSSILDETECTInfo *pOjbectInfo);

	void	DestorySelf(){delete this;}

private:
	std::vector<cv::Rect>	m_cFossilList;
	std::vector<int>		m_cWeightList;


};



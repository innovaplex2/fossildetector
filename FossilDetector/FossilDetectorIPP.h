#pragma once

#include <Windows.h>
#include "../include/FossilDetector.h"
#include "opencv/cv.h"

const	int	FILTER_PYRAMID_CNT		= 6;
const	int	STDDEV_THRESHOLD		= 10;
const	int	OBJECT_SIZE_LEVEL_CNT	= 5;

typedef struct {
	int		object_size_level[OBJECT_SIZE_LEVEL_CNT];
	int		theshold_level[OBJECT_SIZE_LEVEL_CNT];

	float	level_weight[OBJECT_SIZE_LEVEL_CNT]; //for new 

	float	min_distance;
	float	foreground_thread_low;
	float	foreground_thread_high;
	int		max_width;

	int		threadhold_fossil_occapy_ratio;	//전체 영역에서 적어도 1/6의 사이즈를 차지해야 된다
	int		threadhold_fossil_distance_ratio;	//평균 분포가 width의 1/2이내에 존재해야 도니다.

	float	increase_size_factor;

}FOSSILEDetectorParam;


class CFossilDetectorIPP : public IFossilDetector
{
public:
	CFossilDetectorIPP();
	~CFossilDetectorIPP();

	BOOL	Open(const char *szConfigXML);
	BOOL	ProcessFossilDetector( int width, int height, int step, BYTE *pData, int* pMaxDetectCount );
	BOOL	GetFossilObject( FOSSILDETECTInfo *pOjbectInfo, int* pDetectCount );
	BOOL	UpdateFossilObject( int nDetectCount, FOSSILDETECTInfo *pOjbectInfo );

	BOOL	ProcessUpdateFossilObject( int width, int height, int step, BYTE *pData, int nDetectCount, FOSSILDETECTInfo *pOjbectInfo);
	
	void	DestorySelf(){delete this;}

private:
	double				m_mean;
	float				m_stddev;
	double				m_RGBMean[3];

	cv::Mat				m_input;
	
	cv::Mat				m_gray;
	cv::Mat				m_gray_bin;

	cv::Mat				m_edge;
	
	std::vector<cv::Rect>	m_cFossilList;
	std::vector<float>		m_cWeightList;
	
	bool					m_bAutoAjudstMenetParam;
	FOSSILEDetectorParam	m_DetectionParam;

	BOOL	GetFossilInfo(RECT& rc, FOSSILDETECTInfo *Info);
	int		AdjustFossilPostion(BYTE* pSrc, int width, int height, int pitch, int limit_max_width, int* x, int *y, int *weight_w, int *weight_h);
};



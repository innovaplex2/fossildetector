// Test.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <stdlib.h>
#include <stdio.h>

#include "opencv/cv.h"
#include "opencv/highgui.h"
#include <vector>
#include <string>

#ifdef _DEBUG
#pragma comment(lib, "opencv_core2410d.lib")
#pragma comment(lib, "opencv_highgui2410d.lib")
#pragma comment(lib, "opencv_imgproc2410d.lib")

#else

#pragma comment(lib, "opencv_core2410.lib")
#pragma comment(lib, "opencv_highgui2410.lib")
#pragma comment(lib, "opencv_imgproc2410.lib")

#endif

typedef struct{
	int catalogy;
	float center_x, center_y, width, height;
}LABELInfo;


int _tmain(int argc, _TCHAR* argv[])
{

	cv::Mat img_input;
	char strInputName[256];
	int i;
	FILE* label;
	char buf[256];
	std::vector<LABELInfo> LabelList;

	std::vector<std::string> LabelFileList;
	FILE *train_list = fopen("U:\\yolo\\test\\training_list.txt", "r+t");
	while (fgets(buf, 256, train_list)){
		LabelFileList.push_back(buf);
	}
	fclose(train_list);

	char *str_replace;
	int length;
	
	for (i = 0; i < LabelFileList.size(); i++)
	{	
		strcpy(strInputName, LabelFileList[i].c_str());
		length = strlen(strInputName);
		strInputName[length - 1] = 0;
				
		img_input = cv::imread(strInputName);
		
		str_replace = strstr(strInputName, "images");
		memcpy(str_replace, "labels", 6);

		str_replace = strstr(strInputName, ".jpg");
		if (!str_replace){
			str_replace = strstr(strInputName, ".JPG");
		}
		memcpy(str_replace, ".txt", 4);
			

		

		label = fopen(strInputName, "r+t");
		if (label == NULL) continue;
		LabelList.clear();

		while(fgets(buf, 256, label)){
			LABELInfo info;
			sscanf(buf, "%d %f %f %f %f \n", &info.catalogy, &info.center_x, &info.center_y, &info.width, &info.height);
			LabelList.push_back(info);
		}
		
				
		int image_width, image_height;

		image_width = img_input.cols;
		image_height = img_input.rows;
		cv::Rect rect;
		cv::Scalar color0(0, 0, 255);
		cv::Scalar color1(255, 0, 0);

		for (int j = 0; j < LabelList.size(); j++){
			rect.x = LabelList[j].center_x * image_width;
			rect.y = LabelList[j].center_y * image_height;
			rect.width = LabelList[j].width * image_width;
			rect.height = LabelList[j].height * image_height;

			rect.x -= rect.width / 2;
			rect.y -= rect.height / 2;

			if (LabelList[j].catalogy == 0)
				rectangle(img_input, rect, color0, 2);
			else
				rectangle(img_input, rect, color1, 2);
		}


		imshow(LabelFileList[i].c_str(), img_input);
		cv::waitKey(0);
		
	}


	return 0;
}


#include "FossilDetectorIPP.h"
#include "IPPFunc.h"
#include "opencv/highgui.h"
#include "opencv/cv.h"

#ifdef _DEBUG
#pragma comment(lib, "opencv_core2410d.lib")
#pragma comment(lib, "opencv_highgui2410d.lib")
#pragma comment(lib, "opencv_imgproc2410d.lib")
#pragma comment(lib, "opencv_objdetect2410d.lib")
#else
#pragma comment(lib, "opencv_core2410.lib")
#pragma comment(lib, "opencv_highgui2410.lib")
#pragma comment(lib, "opencv_imgproc2410.lib")
#pragma comment(lib, "opencv_objdetect2410.lib")
#endif

using namespace std;
using namespace cv;

//#define DEBUG_SHOW_FOSSIL2
//#define DEBUG_SHOW_FOSSIL3

//추후 파라미터화 시킴
static float	g_foreground_thread_high = 12;


vector <Point> GetLocalMaxima(const cv::Mat Src,int MatchingSize, int Threshold, int GaussKernel  )
{  
  vector <Point> vMaxLoc(0); 

  if ((MatchingSize % 2 == 0) || (GaussKernel % 2 == 0)) // MatchingSize and GaussKernel have to be "odd" and > 0
  {
    return vMaxLoc;
  }

  vMaxLoc.reserve(100); // Reserve place for fast access 
  Mat ProcessImg = Src.clone();
  int W = Src.cols;
  int H = Src.rows;
  int SearchWidth  = W - MatchingSize;
  int SearchHeight = H - MatchingSize;
  int MatchingSquareCenter = MatchingSize/2;

  if(GaussKernel > 1) // If You need a smoothing
  {
    GaussianBlur(ProcessImg,ProcessImg,Size(GaussKernel,GaussKernel),0,0,4);
  }
  uchar* pProcess = (uchar *) ProcessImg.data; // The pointer to image Data 

  int Shift = MatchingSquareCenter * ( W + 1);
  int k = 0;

  for(int y=0; y < SearchHeight; ++y)
  { 
    int m = k + Shift;
    for(int x=0;x < SearchWidth ; ++x)
    {
      if (pProcess[m++] >= Threshold)
      {
        Point LocMax;
        Mat mROI(ProcessImg, Rect(x,y,MatchingSize,MatchingSize));
        minMaxLoc(mROI,NULL,NULL,NULL,&LocMax);
        if (LocMax.x == MatchingSquareCenter && LocMax.y == MatchingSquareCenter)
        { 
          vMaxLoc.push_back(Point( x+LocMax.x,y + LocMax.y )); 
          // imshow("W1",mROI);cvWaitKey(0); //For gebug              
        }
      }
    }
    k += W;
  }
  return vMaxLoc; 
}


int	AnaysisFossil(BYTE* pSrc, int width, int height, int pitch, float* f_width, float* f_height, float* f_area ,float *f_angle)
{
	int count, sum_x, sum_y;
	count = sum_x = sum_y = 0;
	
	int startx, endx, starty, endy;
	startx	= width;
	starty	= height;
	endy	= endx = 0;


	for (int i = 0; i < height; i++){
		for (int j = 0; j < width; j++){
			if (pSrc[j + i*pitch]){
				
				if (startx > j) startx = j;
				if (starty > i) starty = i;
				if (endx < j) endx = j;
				if (endy < i) endy = i;

				count++;
				sum_x += j;
				sum_y += i;
				endx = j;
			}
		}
	}

	*f_width	= float(endx - startx);
	*f_height	= float(endy - starty);
	*f_area		= float(count);
	*f_angle	= 0;
		
	return 0;
}


BOOL	CFossilDetectorIPP::UpdateFossilObject( int nDetectCount, FOSSILDETECTInfo *pOjbectInfo )
{
	int i;
	for( i = 0 ; i < nDetectCount ; i++){
		if( pOjbectInfo[i].flag == FOSSIL_UPDATE_UPDATE ){
			GetFossilInfo(pOjbectInfo[i].rc, &pOjbectInfo[i]);
			pOjbectInfo[i].flag = FOSSIL_UPDATE_NONE;
		}
	}
	return TRUE;
}


BOOL	CFossilDetectorIPP::GetFossilInfo(RECT& rc,  FOSSILDETECTInfo *pInfo)
{
	//1. find mean and std divegence data 
	cv::Mat	temp, mask, fossil_gray, img_binary, fossil_temp;
	cv::Rect FossilRect(rc.left, rc.top, rc.right-rc.left, rc.bottom-rc.top);
	cv::Rect FossilRect2(rc.left/2, rc.top/2, (rc.right-rc.left)/2-1, (rc.bottom-rc.top)/2-1);

	m_gray(FossilRect).copyTo(fossil_gray);
	m_gray_bin(FossilRect).copyTo(fossil_temp);
		
	/*
	int border_percent = 0;	
	if( FOSSIL_TYPE_BLOB == pInfo->type ){
		border_percent = 20;
		//Ipp8u Threshold;
		//ippiComputeThreshold_Otsu_8u_C1R(pSrc, m_gray.step.buf[0], roiSize, &Threshold);
		//ippiThreshold_LTVal_8u_C1R(pSrc, m_gray.step.buf[0], temp.data, temp.step.buf[0], roiSize, Threshold, 0);
		//temp2 = m_input(FossilRect);
		cv::threshold( fossil_gray, temp, m_mean+20, 255, cv::THRESH_BINARY);
		cv::Mat element(2, 2, CV_8U, cv::Scalar(1));
		morphologyEx( temp, img_binary, CV_MOP_OPEN, element);

	}else{
		border_percent = 15;
		cv::GaussianBlur( fossil_edge, temp, cv::Size(3,3), 2);
		cv::threshold(temp, img_binary, 128, 255, THRESH_BINARY|THRESH_OTSU);
	}
	*/
	
	const float border_percent = (float)0.6;
	cv::threshold(fossil_temp, temp, m_mean + g_foreground_thread_high, 255, cv::THRESH_BINARY);

	cv::Mat element(2, 2, CV_8U, cv::Scalar(1));
	morphologyEx(temp, img_binary, CV_MOP_OPEN, element);

		
	//Analysis Fossil
	int center_x, center_y;
	center_x = img_binary.cols / 2;
	center_y = img_binary.rows / 2;
	
	img_binary.copyTo(mask);
	vector<vector<Point> > contours;
	findContours(img_binary, contours, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_SIMPLE);
			
	Point pt;
	float boundary_width, boundary_height;
	boundary_width = (img_binary.cols / 2) * border_percent;
	boundary_height = (img_binary.rows / 2) * border_percent;

	
	for (unsigned int i = 0; i < contours.size(); i++){
		Scalar m = mean(contours[i]);
		pt.x = (int)m.val[0];
		pt.y = (int)m.val[1];
				
		//Check in ellipse
		if ((((pt.x - center_x)*(pt.x - center_x)) / (boundary_width*boundary_width) + ((pt.y - center_y)* (pt.y - center_y)) / (boundary_height*boundary_height)) >= 1){
			drawContours(mask, contours, i, Scalar(0, 0, 0), CV_FILLED);
			drawContours(mask, contours, i, Scalar(0, 0, 0), 3);

			drawContours(fossil_gray, contours, i, Scalar(m_mean, m_mean, m_mean), CV_FILLED);
			drawContours(fossil_gray, contours, i, Scalar(m_mean, m_mean, m_mean), 3);
		}
	}

	//Clear boundary.
	cv::Rect boundaryRC(1, 1, FossilRect.width - 1, FossilRect.height - 1);
	rectangle(mask, boundaryRC, Scalar(0, 0, 0), 5);

	pInfo->rc.left	= (FossilRect.x < 0)?0:FossilRect.x;
	pInfo->rc.top	= (FossilRect.y < 0)?0:FossilRect.y;

	pInfo->rc.right	= FossilRect.x + FossilRect.width;
	pInfo->rc.bottom= FossilRect.y + FossilRect.height;
		
	pInfo->rc.right	= (pInfo->rc.right > m_gray.cols)? m_gray.cols : pInfo->rc.right;
	pInfo->rc.bottom= (pInfo->rc.bottom > m_gray.rows)? m_gray.rows : pInfo->rc.bottom;

	
	cv::Scalar avr, var;
	cv::meanStdDev(fossil_gray, avr, var, mask);

	pInfo->mean		= (float)avr[0];
	pInfo->stddev	= (float)var[0];
	
	//2. sort fossil type
	int theshold = int(STDDEV_THRESHOLD * (float(FossilRect.width * FossilRect.height) / float(m_DetectionParam.object_size_level[0] * m_DetectionParam.object_size_level[0])));
	if (pInfo->stddev < theshold ){
		pInfo->type = FOSSIL_TYPE_LINE;

		int ddepth = CV_16S;
		cv::Laplacian( fossil_gray, fossil_temp, ddepth, 3, 1, 0, cv::BORDER_DEFAULT );
		cv::convertScaleAbs( fossil_temp, mask);
		cv::GaussianBlur( mask, fossil_temp, cv::Size(3,3), 2);
		cv::threshold(fossil_temp, mask, 128, 255, THRESH_BINARY|THRESH_OTSU);

	}else{
		pInfo->type = FOSSIL_TYPE_BLOB;
	}
	

	//Get RGB Color for foreground
	if( FOSSIL_TYPE_BLOB == pInfo->type ){
		temp = m_input(FossilRect);
		cv::Scalar avr = cv::mean(temp, mask);
		
		pInfo->rgb_mean[0] = (float)avr[0];
		pInfo->rgb_mean[1] = (float)avr[1];
		pInfo->rgb_mean[2] = (float)avr[2];
	}

	//AnaysisFossil
	AnaysisFossil( mask.data, mask.cols, mask.rows, mask.step.buf[0], 
		&pInfo->width, &pInfo->height, &pInfo->area, &pInfo->angle);
	
	//Get Hu Moment
	cv::Moments moment;
	moment = cv::moments(mask, true);
	cv::HuMoments(moment, pInfo->hu_moments);
	
	pInfo->angle = float(0.5 * atan((2 * moment.mu11) / (moment.mu20 - moment.mu02)));
	pInfo->angle = float(pInfo->angle * 180 / 3.141592);

#if 0
	//Find local feature point
	vector<KeyPoint> keypoints;
	Mat descriptors;
	Rect region(pInfo->rc.left, pInfo->rc.top, pInfo->rc.right - pInfo->rc.left, pInfo->rc.bottom - pInfo->rc.top);
	fossil_gray = m_input(region);
	SurfFeatureDetector detector;
	detector.detect(fossil_gray, keypoints);
	pInfo->num_of_feature = keypoints.size();
	Ptr<DescriptorExtractor> DescriptorExtractor = DescriptorExtractor::create("SURF");
	DescriptorExtractor->compute(fossil_gray, keypoints, descriptors);

	for(int i = 0; i < pInfo->num_of_feature; i++){
		pInfo->keypoints[i*7 + 0] = keypoints[i].pt.x;
		pInfo->keypoints[i*7 + 1] = keypoints[i].pt.y;
		pInfo->keypoints[i*7 + 2] = keypoints[i].size;
		pInfo->keypoints[i*7 + 3] = keypoints[i].angle;
		pInfo->keypoints[i*7 + 4] = keypoints[i].octave;
		pInfo->keypoints[i*7 + 5] = keypoints[i].response;
		pInfo->keypoints[i*7 + 6] = keypoints[i].class_id;
		pInfo->size_of_descriptor = descriptors.cols;

		for(int j = 0; j < descriptors.cols; j++){
			pInfo->descriptors[i*64 + j] = descriptors.at<float>(i,j);
		}
	}
	
#else
	Point center = Point((int)(moment.m10/moment.m00), (int)(moment.m01 / moment.m00));
	cv::Mat rot_mat = getRotationMatrix2D(center, pInfo->angle, 1);

	/// Rotate the warped image
	warpAffine(fossil_gray, fossil_temp, rot_mat, fossil_gray.size(), CV_INTER_LINEAR, BORDER_CONSTANT, m_mean);
	
	// Normalize size
	Size nomalizeSize(64, 64);
	resize(fossil_temp, fossil_gray, nomalizeSize);
/*
	char strFossil[256];
	static int fossil_cnt = 0;
	sprintf(strFossil, "D:\\work2\\InnovaPlex\\fossil\\fossil_%02d_mask.jpg", fossil_cnt++);
	imwrite(strFossil, fossil_gray); 
*/	
	//Apply HOG
	HOGDescriptor HoGD(nomalizeSize, cv::Size(16, 16), cv::Size(8, 8), cv::Size(8, 8), 9);

	vector<float> descriptorsValues;
	//short descriptor[2048] = {0,};
	HoGD.compute(fossil_gray, descriptorsValues);
	
	pInfo->size_of_descriptor = descriptorsValues.size();
	for (unsigned int i = 0; i < descriptorsValues.size() ; i++){
		pInfo->descriptors[i] = (short)(descriptorsValues[i]*1000);
	}

#endif

#ifdef DEBUG_SHOW_FOSSIL2	
	
	imshow("FOSSILE_ORG", m_input(FossilRect));
	imshow("FOSSILE_BINARY_2", temp2);
		
	cv::imshow("FOSSILE_HOG_INPUT", fossil_gray);
	
	cv::waitKey(0);
#endif
		
#ifdef DEBUG_SHOW_FOSSIL3
	vector <Point> local_feature = GetLocalMaxima(fossil_edge, 5, 40, 1);

	//cv::Mat fossil_feature = m_input(FossilRect).clone();
	//cv::Mat fossil2 = m_input(FossilRect);
	cv::Scalar color(0, 0, 255);
	

	//int radius = (FossilRect2.width * 9) / 20;
	for(int i = 0 ; i < local_feature.size() ; i++ ){
		if (temp2.at<uchar>(local_feature[i]*2)){
			circle(fossil, local_feature[i] * 2, 3, color);
		}
				
		//if(temp2.at<uchar>(local_feature[i] * ((FOSSIL_TYPE_BLOB == pInfo->type)?2:1)))
		
//		if(inCircle(local_feature[i].x, local_feature[i].y, center_x, center_y, radius)){
//			circle(fossil2, local_feature[i]*2, 2, color2);
//		}
	}
	
	//cv::imshow("FOSSILE_LOCAL", temp2);	
	cv::imshow("FOSSILE_LOCAL_FEATURE1", fossil);	
	//cv::imshow("FOSSILE_LOCAL_FEATURE2", fossil2);	
	cv::waitKey(0);
#endif

	return TRUE;
}


BOOL	CFossilDetectorIPP::ProcessUpdateFossilObject( int width, int height, int step, BYTE *pData, int nDetectCount, FOSSILDETECTInfo *pOjbectInfo)
{
	m_input = cv::Mat(height, width, CV_8UC3, pData);
	m_gray.create( height, width, CV_8UC1 );
	
	//1. color conversion
	IppiSize srcSize = {width , height };
	ippiRGBToGray_8u_C3C1R(pData, step, m_gray.data, width, srcSize);
	m_gray.copyTo(m_gray_bin);

	//get mean value to seperate background and foreground
	ippiMean_8u_C1R(m_gray.data, m_gray.step.buf[0], srcSize, &m_mean);
//	ippiMean_8u_C3R(m_input.data, m_input.step.buf[0], srcSize, m_RGBMean);

	int i;
	for( i = 0 ; i < nDetectCount ; i++){
		if( pOjbectInfo[i].flag == FOSSIL_UPDATE_UPDATE ){
			GetFossilInfo(pOjbectInfo[i].rc, &pOjbectInfo[i]);
			pOjbectInfo[i].flag = FOSSIL_UPDATE_NONE;
		}
	}
	
	return TRUE;
}



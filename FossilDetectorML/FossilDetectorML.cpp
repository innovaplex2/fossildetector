#include "FossilDetectorIPP.h"
#include <atlbase.h>


HANDLE	__stdcall	SetupFossilDetector(const char* szJSONConfig)
{
	IFossilDetector *pDetector;
	pDetector = new CFossilDetectorIPP();
	
	if(!pDetector->Open(szJSONConfig)){
		MessageBox(NULL, "fail", "fail", MB_OK);
		AtlTrace("Fail Open FossilDetector \n");
		return NULL;
	}

	return pDetector;
}


void	__stdcall	EndupFossilDetector(HANDLE handle)
{
	if(handle){
		IFossilDetector *pDetector = (IFossilDetector *) handle;
		pDetector->DestorySelf();
	}
}


BOOL	__stdcall	ProcessFossilDetector(HANDLE handle, int width, int height, int step, BYTE *pData, int* pMaxDetectCount )
{
	if(handle == NULL){
		AtlTrace("ProcessFossilDetector not handle set \n");
		return FALSE;
	}

	IFossilDetector *pDetector = (IFossilDetector *) handle;
	return pDetector->ProcessFossilDetector( width, height, step, pData, pMaxDetectCount );
}


BOOL	__stdcall	GetFossilObject(HANDLE handle, FOSSILDETECTInfo *pOjbectInfo, int* pDetectCount )
{
	if(handle == NULL){
		AtlTrace("GetFossilObject not handle set \n");
		return FALSE;
	}

	IFossilDetector *pDetector = (IFossilDetector *) handle;
	return pDetector->GetFossilObject( pOjbectInfo, pDetectCount );
}


BOOL	__stdcall	UpdateFossilObject(HANDLE handle, int nDetectCount, FOSSILDETECTInfo *pOjbectInfo)
{
	if(handle == NULL){
		AtlTrace("GetFossilObject not handle set \n");
		return FALSE;
	}

	IFossilDetector *pDetector = (IFossilDetector *) handle;
	return pDetector->UpdateFossilObject( nDetectCount, pOjbectInfo );
}


BOOL	__stdcall	ProcessUpdateFossilObject(HANDLE handle, int width, int height, int step, BYTE *pData, int nDetectCount, FOSSILDETECTInfo *pOjbectInfo)
{
	if(handle == NULL){
		AtlTrace("GetFossilObject not handle set \n");
		return FALSE;
	}

	IFossilDetector *pDetector = (IFossilDetector *) handle;
	return pDetector->ProcessUpdateFossilObject(width, height, step, pData, nDetectCount, pOjbectInfo);
}


BOOL	__stdcall	UpdateFossilObject2(HANDLE handle, int nDetectCount, FOSSILDETECTInfo* pInFossilInfos, FOSSILDETECTInfo* pOutFossilInfos)
{
	if(handle == NULL){
		AtlTrace("GetFossilObject not handle set \n");
		return FALSE;
	}

	IFossilDetector *pDetector = (IFossilDetector *) handle;
	memcpy(pOutFossilInfos, pInFossilInfos, sizeof(FOSSILDETECTInfo)*nDetectCount);
	return pDetector->UpdateFossilObject( nDetectCount, pOutFossilInfos);
}


BOOL	__stdcall	ProcessUpdateFossilObject2(HANDLE handle, int width, int height, int step, BYTE *pData, 
											   int nDetectCount, FOSSILDETECTInfo* pInFossilInfos, FOSSILDETECTInfo* pOutFossilInfos)
{
	if(handle == NULL){
		AtlTrace("GetFossilObject not handle set \n");
		return FALSE;
	}

	IFossilDetector *pDetector = (IFossilDetector *) handle;
	memcpy(pOutFossilInfos, pInFossilInfos, sizeof(FOSSILDETECTInfo)*nDetectCount);
	return pDetector->ProcessUpdateFossilObject(width, height, step, pData, nDetectCount, pOutFossilInfos);
}


IFossilDetector*	__stdcall	CreateFossilDetector()
{
	return (IFossilDetector*)new CFossilDetectorIPP();
}

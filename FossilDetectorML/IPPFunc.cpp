#include "IPPFunc.h"
#include <atlbase.h>
#include "opencv/cv.h"
#include "opencv/highgui.h"

int filter_canny(Ipp8u *src, Ipp8u *dst, int width, int height, float low, float high)
{
   IppStatus sts;
   int size, size1, srcStep=width*sizeof(Ipp8u), dxStep=width*sizeof(Ipp16s), dyStep=width*sizeof(Ipp16s), dstStep=width*sizeof(Ipp8u);
   IppiSize roi;
   Ipp8u  *buffer;
   Ipp16s *dx, *dy;

   roi.width=width;
   roi.height=height;
      
   dx=ippsMalloc_16s(width*height);
   dy=ippsMalloc_16s(width*height);

   sts = ippiFilterSobelNegVertGetBufferSize_8u16s_C1R(roi, ippMskSize3x3, &size);
   if (sts!=ippStsNoErr){
      AtlTrace( "Error en ipp\n");
      return -1;
   }
   sts = ippiFilterSobelHorizGetBufferSize_8u16s_C1R(roi, ippMskSize3x3, &size1);
   if (sts!=ippStsNoErr){
      AtlTrace( "Error en ipp\n");
      return -1;
   }

   if (size<size1) size=size1;
   ippiCannyGetSize(roi, &size1);
   if (size<size1) size=size1;

   buffer = ippsMalloc_8u(size);

   sts = ippiFilterSobelNegVertBorder_8u16s_C1R (src, srcStep, dx, dxStep, roi, ippMskSize3x3, ippBorderRepl, 0, buffer);
   if (sts!=ippStsNoErr){
      AtlTrace( "Error en ipp\n");
      return -1;
   }
   sts = ippiFilterSobelHorizBorder_8u16s_C1R(src, srcStep, dy, dyStep, roi,ippMskSize3x3, ippBorderRepl, 0, buffer);
   if (sts!=ippStsNoErr){
      AtlTrace( "Error en ipp\n");
      return -1;
   }
   sts = ippiCanny_16s8u_C1R(dx, dxStep, dy, dyStep, dst, dstStep, roi, low, high, buffer);
   if (sts!=ippStsNoErr){
      AtlTrace( "Error en ipp\n");
      return -1;
   }
      
   ippsFree(buffer);
   ippsFree(dx);
   ippsFree(dy);

   return 0;
}


int laplacian_filter(Ipp8u *src, Ipp8u *dst, Ipp8u *dst_threshold, int width, int height, Ipp8u *src_border, IppiMaskSize maskSize)
{
	int mask_width, mask_height;
	mask_width =  mask_height = 3;

	IppiSize	SrcRoiSize	= {width, height};
	IppiSize	DstRoiSize	= {width+mask_width, height+mask_height};
		   
	ippiCopyReplicateBorder_8u_C1R(src, SrcRoiSize.width, SrcRoiSize, src_border, DstRoiSize.width, DstRoiSize, 1, 1);
	//ippiCopyConstBorder_8u_C1R(src, SrcRoiSize.width, SrcRoiSize, src_border, DstRoiSize.width, DstRoiSize, 3, 3, 255);

	Ipp8u* pSrc = src_border + 1 + 1 * DstRoiSize.width;
	//need boarder condition
	ippiFilterLaplace_8u_C1R(pSrc, DstRoiSize.width, dst, width, SrcRoiSize, maskSize);

	//need to parametor 
	int threshold = 100;
	ippiThreshold_LTValGTVal_8u_C1R(dst, width, dst_threshold, width, SrcRoiSize, 20, 0, 40, 100);
	
	ippiLShiftC_8u_C1R(dst_threshold, width, 1, dst_threshold, width, SrcRoiSize);	//2x
	return 0;
}


int box_filter(Ipp8u *src, Ipp8u *dst, int width, int height, int mask_width, int mask_height, Ipp8u *src_border)
{
	//need border 
	IppiSize	SrcRoiSize	= {width, height};
	IppiSize	DstRoiSize	= {width+mask_width, height+mask_height};
	IppiSize	maskSize= {mask_width, mask_height};
	IppiPoint	anchor	= {mask_width/2, mask_height/2};

	
	//ippiCopyReplicateBorder_8u_C1R(src, SrcRoiSize.width, SrcRoiSize, src_border, DstRoiSize.width, DstRoiSize, mask_width/2, mask_height/2);
	ippiCopyConstBorder_8u_C1R(src, SrcRoiSize.width, SrcRoiSize, src_border, DstRoiSize.width, DstRoiSize, mask_width/2, mask_height/2, 32);

	Ipp8u* pSrc = src_border + anchor.x + anchor.y * DstRoiSize.width;
	//Ipp8u* pSrc = src + anchor.x + anchor.y * DstRoiSize.width;
	ippiFilterBox_8u_C1R(pSrc, DstRoiSize.width, dst, width, SrcRoiSize, maskSize, anchor);
		
	return 0;
}

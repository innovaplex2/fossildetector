﻿using System;
using System.Windows;
using System.Windows.Controls;
//using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Runtime.InteropServices;
//using System.Data;
using System.IO;
using OpenCvSharp;
using OpenCvSharp.CPlusPlus;


namespace WpfApplication1
{
	class ListPane
	{
		StackPanel main_pane = null;
		ListBox file_list = null;
		StackPanel res_pane = null;
		Button img_btn = null;
		ScrollViewer cookie_scroll = null;
		StackPanel cookie_list = null;
		//DataGrid fossil_list = null;
		Image img_src = null;
		FontFamily font_fam = new FontFamily("Courier New");
		double font_size = 10;

		public ListPane(StackPanel main_pane)
		{
			this.main_pane = main_pane;

			this.file_list = new ListBox
			{
				Width = 300,
				Margin = new Thickness(10, 0, 0, 0)
			};
			this.res_pane = new StackPanel
			{
				Orientation = Orientation.Vertical,
				Margin = new Thickness(10, 0, 0, 0),
			};
			this.img_btn = new Button
			{
				Background = Brushes.Gray,
				Margin = new Thickness(0),
				Padding = new Thickness(0)
			};
			this.img_src = new Image
			{
				Stretch = Stretch.Fill,
				Cursor = System.Windows.Input.Cursors.Hand
			};
			this.cookie_scroll = new ScrollViewer
			{
				VerticalScrollBarVisibility = ScrollBarVisibility.Auto,
				Height = 150,
				Margin = new Thickness(0, 10, 0, 0)
			};
			this.cookie_list = new StackPanel
			{
				Orientation = Orientation.Vertical
			};

			/*this.fossil_list = new DataGrid
			{
				Height = 150,
				Margin = new Thickness(0, 10, 0, 0)
			};

			DataGridColumn a1 = new DataGridColumn
			{
			};

			this.fossil_list.Columns.Add(a1);*/

			this.file_list.SelectionChanged += new SelectionChangedEventHandler(this.SelFile);
			this.img_btn.Click += new RoutedEventHandler(this.ImgWin);

			this.img_btn.Content = this.img_src;
			this.cookie_scroll.Content = this.cookie_list;
			this.res_pane.Children.Add(this.img_btn);
			this.res_pane.Children.Add(this.cookie_scroll);
			this.main_pane.Children.Add(this.file_list);
			this.main_pane.Children.Add(this.res_pane);
		}

		public void ResizePane()
		{
			//StackPanel main_pane = (StackPanel)this.file_list.Parent;
			this.res_pane.Width = this.main_pane.Width - this.file_list.Width - 45;
			this.file_list.Height = this.res_pane.Height = this.main_pane.Height - 20;
			this.img_btn.Height = this.res_pane.Height - this.cookie_scroll.Height - 10;
			this.img_src.Width = this.res_pane.Width;
			this.img_src.Height = this.img_btn.Height;
		}

		public void InitList()
		{
			this.file_list.Items.Clear();
			this.cookie_list.Children.Clear();

			StackPanel column_pane = new StackPanel
			{
				Orientation = Orientation.Horizontal
			};
			column_pane.Children.Add(
				new Label
				{
					Width = 150,
					Height = 25,
					HorizontalContentAlignment = HorizontalAlignment.Center,
					VerticalContentAlignment = VerticalAlignment.Center,
					Background = Brushes.LightGray,
					Content = "Cut",
					FontFamily = this.font_fam,
					FontSize = this.font_size
				}
			);
			column_pane.Children.Add(
				new Label
				{
					Width = 70,
					Height = 25,
					HorizontalContentAlignment = HorizontalAlignment.Center,
					VerticalContentAlignment = VerticalAlignment.Center,
					Background = Brushes.Gray,
					Content = "Left-Pos",
					FontFamily = this.font_fam,
					FontSize = this.font_size
				}
			);
			column_pane.Children.Add(
				new Label
				{
					Width = 70,
					Height = 25,
					HorizontalContentAlignment = HorizontalAlignment.Center,
					VerticalContentAlignment = VerticalAlignment.Center,
					Background = Brushes.LightGray,
					Content = "Top-Pos",
					FontFamily = this.font_fam,
					FontSize = this.font_size
				}
			);
			column_pane.Children.Add(
				new Label
				{
					Width = 70,
					Height = 25,
					HorizontalContentAlignment = HorizontalAlignment.Center,
					VerticalContentAlignment = VerticalAlignment.Center,
					Background = Brushes.Gray,
					Content = "Width",
					FontFamily = this.font_fam,
					FontSize = this.font_size
				}
			);
			column_pane.Children.Add(
				new Label
				{
					Width = 70,
					Height = 25,
					HorizontalContentAlignment = HorizontalAlignment.Center,
					VerticalContentAlignment = VerticalAlignment.Center,
					Background = Brushes.LightGray,
					Content = "Height",
					FontFamily = this.font_fam,
					FontSize = this.font_size
				}
			);

			this.cookie_list.Children.Add(column_pane);
		}

		public void SelFile(object sender, SelectionChangedEventArgs e)
		{
			ListBox file_list = (ListBox)sender;
			ListBoxItem cur_item = (ListBoxItem)file_list.SelectedItem;
			if (cur_item == null) return;
			MainWindow main_win = (MainWindow)Application.Current.Windows[0];
			this.img_src.Source = CommonUtil.MemImg(main_win.disp_list[(int)cur_item.Tag].ToMemoryStream());
		}

		public void ImgWin(object sender, RoutedEventArgs e)
		{
			ListBoxItem cur_item = (ListBoxItem)this.file_list.SelectedItem;
			if (cur_item == null) return;
			MainWindow main_win = (MainWindow)Application.Current.Windows[0];
			//OpenCvSharp.Cv.ShowImage("", (CvArr)main_win.disp_list[(int)cur_item.Tag]);
			MessageBox.Show("1234");
		}

		public void AddFile(string file_path, MemoryStream img_stream, int img_idx)
		{
			ListBoxItem file_item = new ListBoxItem
			{
				Content = Path.GetFileName(file_path),
				Tag = img_idx
			};
			this.file_list.Items.Add(file_item);
			//this.file_list.Items.Add(Path.GetFileName(file_path));
			this.SetImg(img_stream);
		}

		public void SetImg(String file_path)
		{
			this.img_src.Source = (ImageSource)new ImageSourceConverter().ConvertFromString(file_path);
		}

		public void SetImg(MemoryStream img_stream)
		{
			this.img_src.Source = CommonUtil.MemImg(img_stream);
		}

		public void AddCookie(MemoryStream img_cookie, int img_left, int img_top, int img_width, int img_height)
		{
			StackPanel item_pane = new StackPanel
			{
				Orientation = Orientation.Horizontal
			};
			StackPanel img_pane = new StackPanel
			{
				Width = 150,
				Height = img_height + 20,
				HorizontalAlignment = HorizontalAlignment.Left
			};
			Image cookie_img = new Image
			{
				Width = img_width,
				Height = img_height,
				Source = CommonUtil.MemImg(img_cookie),
				HorizontalAlignment = HorizontalAlignment.Left,
				Margin = new Thickness(10, 10, 0, 0)
			};
			Label left_label = new Label
			{
				Background = Brushes.Aqua,
				Width = 70,
				Height = img_height + 20,
				HorizontalContentAlignment = HorizontalAlignment.Center,
				VerticalContentAlignment = VerticalAlignment.Center,
				Content = img_left.ToString(),
				FontFamily = this.font_fam,
				FontSize = this.font_size
			};
			Label top_label = new Label
			{
				Background = Brushes.Orange,
				Width = 70,
				Height = img_height + 20,
				HorizontalContentAlignment = HorizontalAlignment.Center,
				VerticalContentAlignment = VerticalAlignment.Center,
				Content = img_top.ToString(),
				FontFamily = this.font_fam,
				FontSize = this.font_size
			};
			Label width_label = new Label
			{
				Background = Brushes.Aqua,
				Width = 70,
				Height = img_height + 20,
				HorizontalContentAlignment = HorizontalAlignment.Center,
				VerticalContentAlignment = VerticalAlignment.Center,
				Content = img_width.ToString(),
				FontFamily = this.font_fam,
				FontSize = this.font_size
			};
			Label height_label = new Label
			{
				Background = Brushes.Orange,
				Width = 70,
				Height = img_height + 20,
				HorizontalContentAlignment = HorizontalAlignment.Center,
				VerticalContentAlignment = VerticalAlignment.Center,
				Content = img_height.ToString(),
				FontFamily = this.font_fam,
				FontSize = this.font_size
			};

			img_pane.Children.Add(cookie_img);
			item_pane.Children.Add(img_pane);
			item_pane.Children.Add(left_label);
			item_pane.Children.Add(top_label);
			item_pane.Children.Add(width_label);
			item_pane.Children.Add(height_label);

			this.cookie_list.Children.Add(item_pane);
			this.cookie_list.Children.Add(
				new StackPanel
				{
					Height = 1,
					Background = Brushes.Gray
				}
			);
		}

		/*
		ScrollViewer fossil_pane = null;
		StackPanel img_pane = null;
		TabControl res_tab = null;
		DataTable fossil_list = null;

		public ListPane(StackPanel main_pane)
		{
			this.main_pane = main_pane;

			this.file_list = new ListBox
			{
				Width = 300,
				Margin = new Thickness(10, 0, 0, 0)
			};
			this.res_tab = new TabControl
			{
				Margin = new Thickness(10, 0, 10, 0)
			};
			TabItem img_box = new TabItem
			{
				Header = "Images",
				Padding = new Thickness(10, 5, 10, 5)
			};
			TabItem fossil_tab = new TabItem
			{
				Header = "Potential Fossil",
				Padding = new Thickness(10, 5, 10, 5)
			};
			this.img_pane = new StackPanel
			{
				Background = Brushes.Gold
			};
			this.fossil_pane = new ScrollViewer
			{
				VerticalScrollBarVisibility = ScrollBarVisibility.Auto
			};
			this.fossil_list = new DataTable
			{
			};
			this.fossil_list.Columns.Add(
				new DataColumn
				{
					DataType = System.Type.GetType("System.Byte[]"),
					AllowDBNull = true,
					Caption = "Img"
				}
			);
			this.fossil_list.Columns.Add(
				new DataColumn
				{
					ColumnName = "FIMG",
					DataType = System.Type.GetType("System.Byte[]"),
					AllowDBNull = true,
					Caption = "Width"
				}
			);
			this.fossil_list.Columns.Add(
				new DataColumn
				{
					ColumnName = "FWIDTH",
					DataType = typeof(int),
					AllowDBNull = true,
					Caption = "Height"
				}
			);

			this.fossil_pane.Content = this.fossil_list;
			img_box.Content = this.img_pane;
			fossil_tab.Content = this.fossil_pane;
			this.res_tab.Items.Add(img_box);
			this.res_tab.Items.Add(fossil_tab);

			this.main_pane.Children.Add(this.file_list);
			this.main_pane.Children.Add(this.res_tab);

			this.ResizePane();
		}

		public void ResizePane()
		{
			main_pane = (StackPanel)this.file_list.Parent;
			this.res_tab.Width = main_pane.Width - this.file_list.Width - 45;
			this.file_list.Height = this.res_tab.Height = main_pane.Height - 20;
		}

		public void ImgTab(String file_path)
		{
			this.img_pane.Children.Clear();

			Image img_src = new Image
			{
				Stretch = Stretch.Fill,
				Visibility = Visibility.Visible,
				Source = (ImageSource)new ImageSourceConverter().ConvertFromString(file_path)
			};

			this.img_pane.Children.Add(img_src);
		}

		public void FossilTab(String file_path)
		{
			this.fossil_list.Rows.Clear();
		}
		*/
	}
}

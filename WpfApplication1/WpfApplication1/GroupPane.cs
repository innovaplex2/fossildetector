﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;


namespace WpfApplication1
{
	class GroupPane
	{
		SolidColorBrush gp_default_bg_color = Brushes.LightBlue; // default group panel background color
		SolidColorBrush gp_enter_bg_color = Brushes.Purple; // panel background color when mouse enter
		StackPanel box_pane = null;
		public StackPanel item_pane = null;
		StackPanel cookie_pane = null;
		StackPanel group_pane = null;
		TextBox item_label = null;
		String pane_name = "";
		int class_no = -100;

		public GroupPane(StackPanel cookie_pane, StackPanel group_pane, String pane_name, int class_no = -100)
		{
			this.cookie_pane = cookie_pane;
			this.group_pane = group_pane;
			this.pane_name = pane_name;
			this.class_no = class_no;

			this.box_pane = new StackPanel
			{
				Orientation = Orientation.Vertical,
				Margin = new Thickness(0, 5, 0, 0),
				VerticalAlignment = VerticalAlignment.Top,
				HorizontalAlignment = HorizontalAlignment.Left
			};
			this.item_label = new TextBox
			{
				VerticalAlignment = VerticalAlignment.Top,
				HorizontalAlignment = HorizontalAlignment.Left,
				Text = this.pane_name,
				BorderThickness = new Thickness(0),
				FontFamily = new FontFamily("Courier New"),
				FontSize = 14.5,
				FontWeight = FontWeights.Bold
			};
			this.item_pane = new StackPanel
			{
				Orientation = Orientation.Horizontal,
				AllowDrop = true,
				Width = 820,
				MinHeight = 50,
				Margin = new Thickness(0, 0, 0, 5),
				Background = this.gp_default_bg_color,
				VerticalAlignment = VerticalAlignment.Top,
				HorizontalAlignment = HorizontalAlignment.Left,
				CanHorizontallyScroll = true
			};

			if (this.class_no > -1)
				this.item_pane.Name = "GROUP_PANE_" + this.class_no.ToString();

			this.item_pane.MouseMove += new MouseEventHandler(this.MouseMovePane);
			this.item_pane.MouseEnter += new MouseEventHandler(this.MouseEnterPane);
			this.item_pane.DragOver += new DragEventHandler(this.DragOverPane);
			this.item_pane.DragEnter += new DragEventHandler(this.DragEnterPane);
			this.item_pane.DragLeave += new DragEventHandler(this.DragLeavePane);
			this.item_pane.Drop += new DragEventHandler(this.DropPane);

			this.box_pane.Children.Add(this.item_label);
			this.box_pane.Children.Add(this.item_pane);
			this.group_pane.Children.Add(this.box_pane);

			//this.cookie_pane.Height = ((this.group_pane.Parent as StackPanel).Parent as ScrollViewer).ViewportHeight - 30;

			//Console.WriteLine("height : " + ((this.group_pane.Parent as StackPanel).Parent as ScrollViewer).Height.ToString());

			/*this.cookie_pane.Height = this.group_pane.Height;

			Console.WriteLine("cookie_pane height : " + this.cookie_pane.Height.ToString());
			Console.WriteLine("group_pane height : " + this.group_pane.Height.ToString());
			Console.WriteLine("group_pane actualheight : " + this.group_pane.Parent.ViewportHeight.ToString());*/
		}

		private void MouseMovePane(object sender, MouseEventArgs e)
		{
			//StackPanel cur_pane = (StackPanel)sender;
			//cur_pane = (StackPanel)sender;

			//if (cur_pane == null) return;
			//cur_img.Margin = new Thickness(30);

			//Console.WriteLine("x: " + e.GetPosition(cur_pane).X + ", y : " + e.GetPosition(cur_pane).Y);
		}

		private void MouseEnterPane(object sender, MouseEventArgs e)
		{
			//Console.WriteLine("mouse enter");
			//cur_pane = (StackPanel)sender;
		}

		private void DragOverPane(object sender, DragEventArgs e)
		{
			//Console.WriteLine("drag over");
			StackPanel cur_pane = (StackPanel)sender;
			if (cur_pane == null) return;
			cur_pane.Background = this.gp_enter_bg_color;
		}

		private void DragEnterPane(object sender, DragEventArgs e)
		{
			//Console.WriteLine("drag enter");
		}

		private void DragLeavePane(object sender, DragEventArgs e)
		{
			//Console.WriteLine("drag leave");
			Console.WriteLine((e.Data.GetType()));
			StackPanel cur_pane = (StackPanel)sender;
			if (cur_pane == null) return;
			cur_pane.Background = this.gp_default_bg_color;
		}

		private void DropPane(object sender, DragEventArgs e)
		{
			StackPanel cur_pane = (StackPanel)sender;
			if (cur_pane == null) return;

			//Console.WriteLine("Drop");
			CookieCut cur_cookie = (e.Data.GetData("cookie_cut") as CookieCut);
			if (cur_cookie == null) return;

			StackPanel item_pane = cur_cookie.GetPane();
			Border cur_border = cur_cookie.GetBorder();
			if (cur_border == null) return;
			if (cur_pane == item_pane) return;

			cur_cookie.ToPane(cur_pane);
			cur_pane.Background = this.gp_default_bg_color;
		}
	}
}

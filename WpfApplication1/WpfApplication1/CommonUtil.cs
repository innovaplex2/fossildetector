﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Runtime.InteropServices;
using System.IO;
using OpenCvSharp;
using OpenCvSharp.CPlusPlus;

namespace WpfApplication1
{
	static class CommonUtil
	{
		static System.Windows.Window progress_win = null;
		static int progress_val = 0;
		static System.Windows.Forms.ProgressBar progress_bar = null;

		public static ImageSource MemImg(MemoryStream img_stream)
		{
			try
			{
				BitmapImage bitmap_img = new BitmapImage();
				bitmap_img.BeginInit();
				bitmap_img.StreamSource = img_stream;
				bitmap_img.EndInit();
				return bitmap_img;
			}
			catch (Exception ex)
			{
				string ex_msg = ex.Message;
				return null;
			}
		}

		public static OpenCvSharp.CPlusPlus.Rect SubRect(MainWindow.RECT org_rect, int img_width, int img_height)
		{
			OpenCvSharp.CPlusPlus.Rect buff_rect;
			OpenCvSharp.CPlusPlus.Rect fixed_rect;

			buff_rect.X = fixed_rect.X = org_rect.left;
			buff_rect.Y = fixed_rect.Y = org_rect.top;
			buff_rect.Width = fixed_rect.Width = org_rect.right - org_rect.left;
			buff_rect.Height = fixed_rect.Height = org_rect.bottom - org_rect.top;

			if (buff_rect.X + buff_rect.Width > img_width)
				fixed_rect.Width = img_width - buff_rect.X;

			if (buff_rect.Y + buff_rect.Height > img_height)
				fixed_rect.Height = img_height - buff_rect.Y;

			return fixed_rect;
		}

		public static void ProgressWin(int progress_max)
		{
			CommonUtil.progress_win = new System.Windows.Window
			{
				Width = 500,
				Height = 50,
				Background = Brushes.Lime,
				WindowStyle = System.Windows.WindowStyle.None
			};
			CommonUtil.progress_bar = new System.Windows.Forms.ProgressBar
			{
				Width = 500,
				Height = 50,
				Minimum = 0,
				Maximum = progress_max
			};
			CommonUtil.progress_win.Content = CommonUtil.progress_bar;
			CommonUtil.progress_win.Show();
		}

		public static void ProgressVal(int progress_val)
		{
			if (CommonUtil.progress_win == null) return;
			CommonUtil.progress_win.Focus();
			CommonUtil.progress_win.Activate();
		}

		public static void ProgressClose()
		{
			CommonUtil.progress_win.Close();
			CommonUtil.progress_bar = null;
			CommonUtil.progress_win = null;
		}
	}
}

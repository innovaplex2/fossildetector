﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using System.IO;
using OpenCvSharp;
using OpenCvSharp.CPlusPlus;


namespace WpfApplication1
{
	/// <summary>
	/// Interaction logic for MainWindow.xaml
	/// </summary>
	unsafe public partial class MainWindow : System.Windows.Window
	{
		public enum eDETECTOR_TYPE { CANNY_DETECTOR, LAPLACIAN_DETECTOR };
		public enum eFOSSILE_TYPE { FOSSIL_TYPE_BLOB, FOSSIL_TYPE_LINE };

		public struct RECT
		{
			public int left;
			public int top;
			public int right;
			public int bottom;
		};

		public struct FOSSILDETECTInfo
		{
			public int image_index;
			public int class_no;

			public RECT rc;
			public eFOSSILE_TYPE type;

			public float mean;			//normalize by global mean
			public float stddev;

			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 3)]
			public float[] rgb_mean;

			//Moment for sort
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 10)]
			public double[] moments;

			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 7)]
			public double[] hu_moments;
		};

		//[DllImport("FossilDetector.dll", CallingConvention = CallingConvention.Cdecl)]
		[DllImport("FossilDetector.dll")]
		public static extern IntPtr SetupFossilDetector(String szConfigXML, eDETECTOR_TYPE type);
		[DllImport("FossilDetector.dll")]
		public static extern bool ProcessFossilDetector(IntPtr handle, int width, int height, int step, IntPtr pData, ref int pDetectCount);
		[DllImport("FossilDetector.dll")]
		public static extern bool GetFossilObject(IntPtr handle, [Out] FOSSILDETECTInfo[] info, ref int pDetectCount);
		//public static extern bool GetFossilObject(IntPtr handle, [Out] FOSSILDETECTInfo[] info, ref int pDetectCount);
		[DllImport("FossilDetector.dll")]
		public static extern void EndupFossilDetector(IntPtr handle);

		[DllImport("FossilClassifier.dll")]
		public static extern IntPtr CreateClassifier();
		[DllImport("FossilClassifier.dll")]
		public static extern bool ProcessClassification2(IntPtr handle, int nTotalFossil, [In] FOSSILDETECTInfo[] in_info, [Out] FOSSILDETECTInfo[] out_info);
		[DllImport("FossilClassifier.dll")]
		public static extern void DestroyClassifier(IntPtr handle);

		private static int MAX_FILE = 512;
		private static int MAX_GROUP = 256;
		private static int MAX_COOKIE = 4 * 1024;

		GroupPane[] group_pane = new GroupPane[MAX_GROUP]; // Group Panel
		CookieCut[] cookie_cut = new CookieCut[MAX_COOKIE]; // Cookie Cut

		List<List<FOSSILDETECTInfo>> cookie_list = new List<List<FOSSILDETECTInfo>>(); // Cookie Cut
		//List<FOSSILDETECTInfo> cookie_list = new List<FOSSILDETECTInfo>();

		//public string[] file_path = new string[MAX_GROUP];
		public Mat[] img_list = new Mat[MAX_FILE];
		public Mat[] disp_list = new Mat[MAX_FILE];

		ListPane list_pane = null;

		public MainWindow()
		{
			InitializeComponent();

			// font
			//this.FontFamily = new FontFamily("Liberation Sans");
			//this.FontSize = 10.5;

			// init buttons
			btn_an.IsEnabled = btn_st.IsEnabled = btn_sa.IsEnabled = btn_ca.IsEnabled = false;

			// main
			this.list_pane = new ListPane(main_pane);
			//this.list_pane.SetImg("img\\test.jpg");
		}

		private void ResizeWin(object sender, EventArgs e)
		{
			System.Windows.Window main_win = (System.Windows.Window)sender;
			main_pane.Width = main_win.ActualWidth;
			main_pane.Height = main_win.ActualHeight - 120;

			try { this.list_pane.ResizePane(); } catch {}

			/*if(main_pane.Children.GetType() == typeof(ListPane)) {
			}*/
		}

		private void CloseWin(object sender, EventArgs e) {
			this.group_pane = new GroupPane[MAX_GROUP];
			this.cookie_cut = new CookieCut[MAX_COOKIE];
			this.img_list = new Mat[MAX_FILE];
		}

		private void LoadWin(object sender, RoutedEventArgs e)
		{
			System.Windows.Window open_win = new System.Windows.Window
			{
				MinWidth = 300,
				MinHeight = 300,
				MaxWidth = 300,
				MaxHeight = 300,
				WindowStyle = WindowStyle.ToolWindow,
				Title = "Load Images"
			};
			StackPanel win_pane = new StackPanel
			{
				Orientation = System.Windows.Controls.Orientation.Vertical,
				Width = 300
			};
			StackPanel radio_pane = new StackPanel
			{
				Orientation = System.Windows.Controls.Orientation.Vertical,
				Width = 300,
				Height = 90
			};
			StackPanel input_pane = new StackPanel
			{
				Orientation = System.Windows.Controls.Orientation.Vertical,
				Width = 300,
				Height = 125
			};
			StackPanel button_pane = new StackPanel
			{
				Orientation = System.Windows.Controls.Orientation.Horizontal,
				HorizontalAlignment = System.Windows.HorizontalAlignment.Right,
				Width = 175,
				Height = 47
			};
			System.Windows.Controls.RadioButton load_remote = new System.Windows.Controls.RadioButton
			{
				Margin = new Thickness(80, 10, 0, 0),
				Content = "Load from Remote."
			};
			/*RadioButton load_local = new RadioButton
			{
				IsChecked = true,
				Margin = new Thickness(80, 10, 0, 0),
				Content = "Load from Local."
			};*/
			System.Windows.Controls.RadioButton load_dir = new System.Windows.Controls.RadioButton
			{
				IsChecked = true,
				Margin = new Thickness(80, 10, 0, 0),
				Content = "Load from Dir."
			};
			System.Windows.Controls.RadioButton load_file = new System.Windows.Controls.RadioButton
			{
				IsChecked = false,
				Margin = new Thickness(80, 10, 0, 0),
				Content = "Load from File(s)."
			};
			System.Windows.Controls.Label path_label = new System.Windows.Controls.Label
			{
				HorizontalContentAlignment = System.Windows.HorizontalAlignment.Center,
				Height = 30
			};
			StackPanel wellid_pane = new StackPanel
			{
				Orientation = System.Windows.Controls.Orientation.Horizontal,
				Width = 300,
				Height = 30
			};
			System.Windows.Controls.Label wellid_label = new System.Windows.Controls.Label
			{
				Width = 55,
				Height = 25,
				Margin = new Thickness(70, 0, 0, 0),
				HorizontalContentAlignment = System.Windows.HorizontalAlignment.Right,
				Content = "Well ID :"
			};
			System.Windows.Controls.TextBox wellid_textbox = new System.Windows.Controls.TextBox
			{
				Width = 100,
				Height = 20,
				Margin = new Thickness(5, 0, 0, 0)
			};
			StackPanel depth_pane = new StackPanel
			{
				Orientation = System.Windows.Controls.Orientation.Horizontal,
				Width = 300,
				Height = 30
			};
			System.Windows.Controls.Label depth_label = new System.Windows.Controls.Label
			{
				Width = 55,
				Height = 25,
				Margin = new Thickness(70, 0, 0, 0),
				HorizontalContentAlignment = System.Windows.HorizontalAlignment.Right,
				Content = "Depth :"
			};
			System.Windows.Controls.TextBox depth_textbox = new System.Windows.Controls.TextBox
			{
				Width = 100,
				Height = 20,
				Margin = new Thickness(5, 0, 0, 0)
			};
			System.Windows.Controls.Label date_label = new System.Windows.Controls.Label
			{
				//VerticalAlignment = VerticalAlignment.Top,
				HorizontalContentAlignment = System.Windows.HorizontalAlignment.Center,
				Height = 30,
				Content = "Current Time : " + DateTime.Now.ToString("M/dd/yyyy HH:mm tt")
			};
			System.Windows.Controls.Button load_cancel = new System.Windows.Controls.Button
			{
				VerticalAlignment = VerticalAlignment.Center,
				Width = 70,
				Height = 28,
				Margin = new Thickness(10, 0, 0, 0),
				Content = "Cancel"
			};
			System.Windows.Controls.Button load_ok = new System.Windows.Controls.Button
			{
				VerticalAlignment = VerticalAlignment.Center,
				Width = 70,
				Height = 28,
				Content = "OK"
			};

			//open_win.Closing += new System.ComponentModel.CancelEventHandler(this.MainClose);
			load_ok.Click += new RoutedEventHandler(this.LoadRun);
			load_cancel.Click += new RoutedEventHandler(this.LoadCancel);
			//DockPanel.SetDock(load_cancel, Dock.Right);
			//DockPanel.SetDock(load_ok, Dock.Right);

			radio_pane.Children.Add(load_remote);
			radio_pane.Children.Add(load_dir);
			radio_pane.Children.Add(load_file);

			wellid_pane.Children.Add(wellid_label);
			wellid_pane.Children.Add(wellid_textbox);
			depth_pane.Children.Add(depth_label);
			depth_pane.Children.Add(depth_textbox);
			input_pane.Children.Add(path_label);
			input_pane.Children.Add(wellid_pane);
			input_pane.Children.Add(depth_pane);
			input_pane.Children.Add(date_label);

			button_pane.Children.Add(load_ok);
			button_pane.Children.Add(load_cancel);
			win_pane.Children.Add(radio_pane);
			win_pane.Children.Add(input_pane);
			win_pane.Children.Add(button_pane);
			open_win.Content = win_pane;
			open_win.ShowDialog();
		}

		unsafe private void LoadRun(object sender, EventArgs e)
		{
			System.Windows.Window open_win = (System.Windows.Window)((StackPanel)((StackPanel)((System.Windows.Controls.Button)sender).Parent).Parent).Parent;
			if (open_win == null) return;

			System.Windows.Forms.FolderBrowserDialog folder_diag = new System.Windows.Forms.FolderBrowserDialog();

			if(folder_diag.ShowDialog() != System.Windows.Forms.DialogResult.Cancel)
			{
				int img_idx = 0;
				int tot_cnt = 0;
				int max_cnt = 0;
				int detect_cnt = 0;
				OpenCvSharp.CPlusPlus.Rect cookie_rect;

				IntPtr fossil_detector = SetupFossilDetector(null, eDETECTOR_TYPE.LAPLACIAN_DETECTOR);

				foreach (string file_name in Directory.GetFiles(folder_diag.SelectedPath, "*.jpg", SearchOption.TopDirectoryOnly))
				{
					if (img_idx == 0)
					{
						open_win.Close();
						this.cookie_list.Clear();
						this.list_pane.InitList();
						btn_ca.IsEnabled = true;
						CommonUtil.ProgressWin(300);
					}

					this.img_list[img_idx] = new Mat(file_name, LoadMode.Color);
					this.disp_list[img_idx] = this.img_list[img_idx].Clone();

					ProcessFossilDetector(fossil_detector, this.img_list[img_idx].Width, this.img_list[img_idx].Height, (int)this.img_list[img_idx].Step1(), this.img_list[img_idx].Data, ref max_cnt);

					List<FOSSILDETECTInfo> fossil_list = new List<FOSSILDETECTInfo>();
					FOSSILDETECTInfo[] fossil_data = new FOSSILDETECTInfo[max_cnt];

					GetFossilObject(fossil_detector, fossil_data, ref detect_cnt);

					//Console.WriteLine(detect_cnt);

					//this.file_path[img_idx] = file_name;

					for (int j = 0; j < detect_cnt; j ++)
					{
						fossil_data[j].image_index = img_idx;
						fossil_data[j].class_no = -1;

						cookie_rect = CommonUtil.SubRect(fossil_data[j].rc, this.img_list[img_idx].Width, this.img_list[img_idx].Height);


						//Console.WriteLine("rc_left : " + fossil_data[j].rc.left.ToString() + ", rc_right : " + fossil_data[j].rc.right.ToString() + ", cookie_width : " + cookie_rect.Width.ToString());
						//Console.WriteLine("rc_top : " + fossil_data[j].rc.top.ToString() + ", rc_bottom : " + fossil_data[j].rc.bottom.ToString() + ", cookie_height : " + cookie_rect.Height.ToString());

						Mat img_cookie = this.img_list[img_idx].SubMat(cookie_rect);

						this.list_pane.AddCookie(img_cookie.ToMemoryStream(), cookie_rect.X, cookie_rect.Y, cookie_rect.Width, cookie_rect.Height);

						this.disp_list[img_idx].Rectangle(cookie_rect, Scalar.Red);


						fossil_list.Add(fossil_data[j]);
					}

					tot_cnt += detect_cnt;
					this.cookie_list.Add(fossil_list);

					this.list_pane.AddFile(file_name, this.disp_list[img_idx].ToMemoryStream(), img_idx);

					img_idx++;
				}

				EndupFossilDetector(fossil_detector);

				// classifier
				if (img_idx > 0)
				{
					FOSSILDETECTInfo[] fossil_buffer = new FOSSILDETECTInfo[tot_cnt];

					int k = 0;
					foreach (List<FOSSILDETECTInfo> sub_list in this.cookie_list)
					{
						foreach (FOSSILDETECTInfo fossil_val in sub_list)
						{
							fossil_buffer[k ++] = fossil_val;
						}
					}

					IntPtr fossil_classifier = CreateClassifier();
					ProcessClassification2(fossil_classifier, tot_cnt, fossil_buffer, fossil_buffer);

					//Update List 
					k = 0;
					for (int i = 0; i < this.cookie_list.Count; i++)
					{
						List<FOSSILDETECTInfo> fossil_res = this.cookie_list[i];

						for (int j = 0; j < this.cookie_list[i].Count; j++)
						{
							fossil_res[j] = fossil_buffer[k ++];
						}
					}

					DestroyClassifier(fossil_classifier);
				}

				CommonUtil.ProgressClose();
			}
		}

		private void LoadCancel(object sender, EventArgs e)
		{
			System.Windows.Window open_win = (System.Windows.Window)((StackPanel)((StackPanel)((System.Windows.Controls.Button)sender).Parent).Parent).Parent;
			if (open_win == null) return;
			open_win.Close();
		}

		private void CateWin(object sender, RoutedEventArgs e)
		{
			this.group_pane = new GroupPane[MAX_GROUP];
			this.cookie_cut = new CookieCut[MAX_COOKIE];

			System.Windows.Window open_win = new System.Windows.Window
			{
				MinWidth = 1000,
				MinHeight = 600,
				MaxWidth = 1000,
				MaxHeight = 600,
				WindowStyle = WindowStyle.ToolWindow,
				Title = "Category"
			};
			ScrollViewer pane_scroll = new ScrollViewer
			{
				VerticalScrollBarVisibility = ScrollBarVisibility.Auto
			};
			StackPanel win_pane = new StackPanel
			{
				Orientation = System.Windows.Controls.Orientation.Horizontal
			};
			StackPanel group_pane = new StackPanel
			{
				Orientation = System.Windows.Controls.Orientation.Vertical,
				Width = 770,
				VerticalAlignment = VerticalAlignment.Top,
				HorizontalAlignment = System.Windows.HorizontalAlignment.Left
			};
			StackPanel box_pane = new StackPanel
			{
				Orientation = System.Windows.Controls.Orientation.Vertical,
				Width = 200,
				VerticalAlignment = VerticalAlignment.Top,
				HorizontalAlignment = System.Windows.HorizontalAlignment.Right,
				Background = Brushes.HotPink
			};
			System.Windows.Controls.Label cookie_label = new System.Windows.Controls.Label
			{
				Content = "Unidentified potential fossils",
				FontFamily = new FontFamily("Courier New"),
				FontSize = 10,
				FontWeight = FontWeights.Bold
			};
			StackPanel cookie_pane = new StackPanel
			{
				AllowDrop = true,
				Width = 200,
				MinHeight = 300,
				Background = Brushes.Gold,
				VerticalAlignment = VerticalAlignment.Top,
				HorizontalAlignment = System.Windows.HorizontalAlignment.Right
			};

			cookie_pane.Drop += new System.Windows.DragEventHandler(DropPane);

			box_pane.Children.Add(cookie_label);
			box_pane.Children.Add(cookie_pane);
			win_pane.Children.Add(group_pane);
			win_pane.Children.Add(box_pane);
			pane_scroll.Content = win_pane;

			// insert cookies
			List<FOSSILDETECTInfo> fossil_buffer = new List<FOSSILDETECTInfo>();

			int k = 0;
			foreach (List<FOSSILDETECTInfo> sub_list in this.cookie_list)
			{
				foreach (FOSSILDETECTInfo fossil_val in sub_list)
				{
					fossil_buffer.Add(fossil_val);
				}
			}

			List<FOSSILDETECTInfo> sorted_list = fossil_buffer.OrderBy(o => o.class_no).ToList();

			/*for (int i = 0; i < this.cookie_list.Count; i++)
			{
				new CookieCut(this.cookie_list[i], this.cookie_stream[i], cookie_pane, group_pane, cookie_pane);
			}*/

			foreach (FOSSILDETECTInfo cookie_data in sorted_list)
			{
				if (cookie_data.class_no == -1)
				{
					new CookieCut(this.img_list[cookie_data.image_index], cookie_data, cookie_pane, group_pane, cookie_pane);
					continue;
				}

				/*if (group_pane.FindName("GROUP_PANE_" + cookie_data.class_no.ToString()) is StackPanel)
				{
				}
				else
					new GroupPane(cookie_pane, group_pane, "Group #" + cookie_data.class_no.ToString(), cookie_data.class_no);

				StackPanel start_pane = group_pane.FindName("GROUP_PANE_" + cookie_data.class_no.ToString()) as StackPanel;

				Console.WriteLine(start_pane.Name);

				new CookieCut(this.file_path, cookie_data, cookie_pane, group_pane, start_pane);*/

				if (this.group_pane[cookie_data.class_no] == null)
					this.group_pane[cookie_data.class_no] = new GroupPane(cookie_pane, group_pane, "Group #" + cookie_data.class_no.ToString(), cookie_data.class_no);

				new CookieCut(this.img_list[cookie_data.image_index], cookie_data, cookie_pane, group_pane, this.group_pane[cookie_data.class_no].item_pane);
			}

			/*for (int i = 0; i < 3; i ++)
				this.group_pane[i] = new GroupPane(cookie_pane, group_pane, "Group-" + i.ToString());

			for (int i = 0; i < 10; i ++)
				this.cookie_cut[i] = new CookieCut(cookie_pane, group_pane, cookie_pane, i);*/

			open_win.Content = pane_scroll;
			open_win.ShowDialog();
		}

		private void DropPane(object sender, System.Windows.DragEventArgs e)
		{
			StackPanel cur_pane = (StackPanel)sender;
			if (cur_pane == null) return;

			//Console.WriteLine("Drop");
			CookieCut cur_cookie = (e.Data.GetData("cookie_cut") as CookieCut);
			if (cur_cookie == null) return;

			StackPanel item_pane = cur_cookie.GetPane();
			Border cur_border = cur_cookie.GetBorder();
			if (cur_border == null) return;
			if (cur_pane == item_pane) return;

			cur_cookie.ToPane(cur_pane);
		}

		private void CloseCate(object sender, EventArgs e)
		{
		}
	}
}
